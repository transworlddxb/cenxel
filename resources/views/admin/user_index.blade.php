@include('admin.includes.head')


      <div class="slim-mainpanel">
        <div class="container">
          <div class="slim-pageheader">
            <ol class="breadcrumb slim-breadcrumb">
              {{-- <li class="breadcrumb-item"><a href="#">Home</a></li> --}}
              {{-- <li class="breadcrumb-item active" aria-current="page">Dashboard</li> --}}
            </ol>
            <h6 class="slim-pagetitle">our Products</h6>
          </div><!-- slim-pageheader -->

        <div class="section-wrapper mg-t-20">
            {{-- <label class="section-title">Top Image Card</label> --}}
            {{-- <p class="mg-b-20 mg-sm-b-40">Places an image to the top, bottom or overlay of the card.</p> --}}

            <div class="row">
              @foreach($prod_show as $prod)
              <div class="col-lg-3">
                <div class="card bd-0">
                <a href="{{ url('/admin/single_product/'.$prod->prod_pk_id) }}">
                  <img style="width: 100%; height: 155px; overflow: hidden;" class="img-fluid product-view-image" src="{{ asset('uploads/product/'.$prod->prod_image1)}}" alt="Image">
                    <div class="card-body bd bd-t-0 d-flex justify-content-between">
                        <h6 class="card-text-title text-uppercase">{{ $prod->prod_title }}</h6>
                        <h6 class="card-text-title text-uppercase" style="color: #1629B9;">{{ $prod->prod_quantity }}{{ $prod->carry_units }}&nbsp;= &nbsp;₹&nbsp;{{ $prod->prod_price }}</h6>
                    </div>
                    
                </a>
                </div><!-- card -->
              </div><!-- col-3 -->
              @endforeach
            </div><!-- row -->
          </div><!-- section-wrapper -->


        </div><!-- container -->

        @include('admin.includes.footer')