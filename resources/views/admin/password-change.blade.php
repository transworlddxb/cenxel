@include('admin.includes.head')
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.21.0/moment.min.js" type="text/javascript"></script>

<?php
$message=Session::get('message');
$msg=explode("|",$message);

?>


@if ($message = Session::get('message'))
<div  style='z-index:9999; position:absolute;width:30%;right:0px; padding:10px 15px 0px 15px;' id='msg'>
   <div class="alert alert-{{$msg[0]}}" role="alert">
     <button type="button" class="close" data-dismiss="alert" aria-label="Close">
       <span aria-hidden="true">&times;</span>
   </button>
   <strong>{{ ucfirst($msg[0]) }}!</strong>{{ $msg[1] }}
</div><!-- alert -->
</div>
@endif 


    <div class="slim-mainpanel">
        <div class="container">
            <div class="slim-pageheader">
                <ol class="breadcrumb slim-breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ url('/admin/dashboard') }}">Home</a></li>
                    <li class="breadcrumb-item"><a href="#">Change password</a></li>
                    {{-- <li class="breadcrumb-item active" aria-current="page">Update-users</li> --}}
                </ol>
                <h6 class="slim-pagetitle mb-2">change password</h6>
            </div><!-- slim-pageheader -->           

            <div class="section-wrapper">
                {{-- <label class="section-title">Update users</label> --}}
                {{-- <p class="mg-b-20 mg-sm-b-40">Please complete the form below.</p> --}}
                <div class="col-lg-10">
                  <div class="section-wrapper">
                    <label class="section-title">Fill valid data to change your password</label>
                    <p class="mg-b-20 mg-sm-b-40"></p>
                    <form class="form" action="{{url('admin/evaluate')}}" method="post" enctype="multipart/form-data">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div class="form-layout form-layout-4">
                          <div class="row">
                            <label class="col-sm-4 form-control-label">Email: <span class="tx-danger">*</span></label>
                            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                              <input type="text" class="form-control" placeholder="Enter email" value="{{$email}}" name="email" readonly>
                          </div>
                      </div><!-- row -->
                      <div class="row mg-t-20">
                        <label class="col-sm-4 form-control-label">Current password: <span class="tx-danger">*</span></label>
                        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                          <input type="password" class="form-control" placeholder="Enter current-password" name="current-password">
                          @if ($errors->has('current-password'))
                          <span class="help-block">
                              <strong style="color:red">{{ $errors->first('current-password') }}</strong>
                          </span>
                          @endif
                      </div>
                  </div>
                  <div class="row mg-t-20">
                    <label class="col-sm-4 form-control-label">New password: <span class="tx-danger">*</span></label>
                    <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                      <input type="password" class="form-control" placeholder="Enter new-password" name="new-password">
                      @if ($errors->has('new-password'))
                      <span class="help-block">
                          <strong style="color:red">{{ $errors->first('new-password') }}</strong>
                      </span>
                      @endif
                  </div>
              </div>
              <div class="row mg-t-20">
                <label class="col-sm-4 form-control-label">Confirm password: <span class="tx-danger">*</span></label>
                <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                  <input type="password" class="form-control" placeholder="Enter confirm-password" name="new-password_confirmation">
              </div>
          </div>
          <div class="form-layout-footer mg-t-30">
            <button class="btn btn-primary bd-0" type="Submit">Submit</button>
            {{-- <a class="btn btn-primary bd-0" style="color: white;" href="{{ url('/admin/dashboard') }}">Cancel</a> --}}
        </div><!-- form-layout-footer -->
    </div><!-- form-layout -->
</form>
</div><!-- section-wrapper -->
</div><!-- col-6 -->
</div><!-- section-wrapper -->
</div>

@include('admin.includes.footer')

<script type="text/javascript">
    $(function () {
      $('#example').datetimepicker();      
  });
    setTimeout(function(){ $("#msg").fadeOut(2000); }, 2000);
</script>