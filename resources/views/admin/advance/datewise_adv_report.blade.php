<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
    </head>
    <style>
        td {
            border-bottom: 1px solid #ddd;
            margin: 5px;
        }
    </style>
    <body>
        <div>
            <div style="float: left;">
                <p>Al Qasr Al Wardi Medical Centre<br/>
                Office No: 906, NBD Bank Building PO Box: 63515, <br>
                    Sharjah – United Arab Emirates<br>
                MOB No: (+971) 6 566 83 66<br>
                  Email: info@alqasercosme.com<br>
                TRN: 100443299100003</p>
            </div>
            <div style="float: right">
                <img src="{{ url('image/al_logo.png')}}" style="width: 270px;">
            </div>
        </div>
        <div>
            <div style="text-align: center; padding-top: 130px;">
                <h1>REPORT OF {{$report_date}}</h1>
            </div>
        </div>
        <div>
            <table cellspacing="0">
                <thead style="background-color: #eeeeee; border: none;">
                    <tr>
                        <th width="200px" height="35px">SI.NO</th>
                        <th width="200px">NAME</th>
                        <th width="190px">DATE</th>
                        <th width="150px">AMOUNT</th>
                    </tr>
                </thead>
                <tbody>
                     @foreach($advance as $key => $report)
                    <tr>
                      <td>{{ $key+1 }}</td>
                      <td >{{ $report->first_name }}</td>
                      <td >{{ $report->advance_date }}</td>
                      <td >{{ $report->advance_amount}}</td>
                  </tr>
                  @endforeach 
                </tbody>
            </table>
        </div>
    </body>
</html>           
