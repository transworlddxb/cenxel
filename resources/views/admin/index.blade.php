@include('admin.includes.head')


      <div class="slim-mainpanel">
        <div class="container">
          <div class="slim-pageheader">
            <ol class="breadcrumb slim-breadcrumb">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active" aria-current="page">Dashboard</li>
            </ol>
            <h6 class="slim-pagetitle">Welcome back, {{Auth::user()->roll_name}}</h6>
          </div><!-- slim-pageheader -->        

          <div class="row row-xs mg-t-10">
            <div class="col-sm-6 col-lg-3 mg-t-10 mg-sm-t-0">
              <div class="card card-status">
                <div class="media">
                  <i class="icon ion-person tx-purple"></i>
                  <div class="media-body">
                    <h1 style="color: purple;">{{$count}}</h1>
                    <p>Total Users</p>
                  </div><!-- media-body -->
                </div><!-- media -->
              </div><!-- card -->
            </div>
            {{-- <button type="button" class="edit-item btn btn-primary btn-block print_but" style="color: white; background-color: green; border: green;" data-toggle="modal" data-item-id=""> <a href="{{ url('/admin/testbill_pdf') }}"> Print the bill</a></button> --}}

            <div class="col-sm-6 col-lg-3 mg-t-10 mg-sm-t-0">
              <div class="card card-status">
                <div class="media"> 
                  <i class="icon ion-person-stalker tx-purple"></i>
                  <div class="media-body">
                    <h1 style="color: purple;">{{$count2}}</h1>
                    <p>Registered Customer</p>
                  </div><!-- media-body -->
                </div><!-- media -->
              </div><!-- card -->
            </div>

            <div class="col-sm-6 col-lg-3 mg-t-10 mg-sm-t-0">
              <div class="card card-status">
                <div class="media">
                  <i class="icon ion-ios-bookmarks-outline tx-purple"></i>
                  <div class="media-body">
                    <h1 style="color: purple;">{{$apoointment}}</h1>
                    <p>Today's Appointments</p>
                  </div><!-- media-body -->
                </div><!-- media -->
              </div><!-- card -->
            </div>

            <div class="col-sm-6 col-lg-3 mg-t-10 mg-sm-t-0">
              <div class="card card-status">
                <div class="media">
                  <i class="icon ion-cube tx-purple"></i>
                  <div class="media-body">
                    <h1 style="color: purple;">{{$product}}</h1>
                    <p>Total Products</p>
                  </div><!-- media-body -->
                </div><!-- media -->
              </div><!-- card -->
            </div>
          </div>
           

        </div><!-- container -->

        @include('admin.includes.footer')
      