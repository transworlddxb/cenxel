@include('admin.includes.head')
<link href="{{asset('assets/backend/css/darkbox.css')}}" rel="stylesheet">
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.21.0/moment.min.js" type="text/javascript"></script>
<style type="text/css">
.photogallery li{position: relative; overflow: hidden; min-height: 200px; background: #eee; border: 2px solid #fff;}
.photogallery li:nth-child(odd){background: #bbb;}
.photogallery li img{position: absolute; top: -9999px; right: -9999px; bottom: -9999px; left: -9999px; margin: auto; max-width: none; /*max-height: 100%;*/ width: 100%; height: auto;}
</style>

<div class="slim-mainpanel">
  <div class="container">
    <div class="slim-pageheader">
      <ol class="breadcrumb slim-breadcrumb">
        <li class="breadcrumb-item"><a href="#">Home</a></li>
        <li class="breadcrumb-item">Daily accounts</li>
        {{-- <li class="breadcrumb-item active" aria-current="page">Form Wizards</li> --}}
      </ol>
      <h6 class="slim-pagetitle mb-2">Reports</h6>
    </div><!-- slim-pageheader -->    


    <div class="section-wrapper">

      <div class="row">
        <div class="col-sm-12 col-lg-3">
          <a href="{{ url('/admin/today_account_report') }}" class="btn btn-primary" style="margin-bottom: 10px;background-color: green; border-color: green;"><i class="icon ion-arrow-down-a"></i> Print Todays Report</a>
        </div>
        <div class="col-sm-12 col-lg-3"> 
          <form action="{{ url('/admin/datewise_account_pdf')}}" method="post">
            {{csrf_field()}}
            <div class="row  justify-content-md-end justify-content-center xs-p2">
              <input class="form-control form-input" id="example"  type="text" name="report_date" placeholder="Select date" required>
              <button type="submit" class="btn btn-primary icon ion-document-text" style="margin-right:15px;background-color: green; border-color: green;"> &nbsp;Print</button>
            </div>
          </form>
        </div>
        <div class="col-sm-12 col-lg-6">
          <form action="{{ url('/admin/datebtw_account_pdf')}}" method="post">
            {{csrf_field()}}
            <div class="row  justify-content-md-end justify-content-center xs-p2">
              <input class="form-control form-input col-lg-3" id="example1"  type="text" name="from_date" placeholder="Select FROM date" required style="">
              <input class="form-control form-input col-lg-3" id="example2"  type="text" name="to_date" placeholder="Select TO date" required style="">
              <button type="submit" class="btn btn-primary icon ion-document-text" style="margin-right:15px;background-color: green; border-color: green;"> &nbsp;Print</button>
            </div>
          </form>
        </div>
        </div>
        <hr>
        <br>
        
  
        <div class="table-responsive">
            <table class="table table-bordered table-colored table-primary">
              <thead>
              <tr>
                <th >SI.NO</th>
                <th >NAME</th>
                <th >DOCTOR</th>
                {{-- <th >TREATMENT</th> --}}
                <th >DATE</th>
                <th >TIME</th>
                <th >ACTUAL AMOUNT</th>
                <th >VAT</th>
                <th >PRODUCT PRICE</th>
                <th >PREVIOUS DUE</th>
                <th >AMOUNT PAYED</th>
                <th >BALANCE TO PAY</th>
                {{-- <th class="wd-10p">ACTION</th> --}}
              </tr>
            </thead>
            <tbody>
               @foreach($appointment as $key => $report)
                    <tr>
                      <td>{{ $key+1 }}</td>
                      <td >{{ $report->first_name }}</td>
                      <td >{{ $report->roll_name }}</td>
                      {{-- <td >{{ $report->treatment_one }}<br>{{ $report->treatment_two }}<br>{{ $report->treatment_three }}<br>{{ $report->treatment_four }}<br>{{ $report->treatment_five }}</td> --}}
                      <td >{{ $report->appointment_date }}</td>
                      <td >{{ $report->appointment_time }}</td>
                      <td >{{ $report->total_amount_to_pay }}</td>
                      <td >{{ $report->vat_amount }}</td>
                      <td >{{ $report->purchased_amount }}</td>
                      <td >{{ $report->prev_due_amount}}</td>
                      <td >{{ $report->payedbill_amount}}</td>
                      <td >{{ $report->duebill_amount}}</td>
                      {{-- <td style="width: 200px;"><center>

                        <a href="{{URL::to('/admin/delete-gst/'.$report->pk_treatment_id)}}" type="button"  class="del btn btn-light waves-effect"><i class="fa fa-trash"></i></a> --}}
                        {{-- <button type="button" class="btn btn-success edit-item" id="{{ $g->pk_int_role_id }}" data-toggle="modal" data-item-id="">Edit</button> --}}

                        {{-- <a type="button" class="edit-item btn btn-primary" style="color: white;" id="{{ $report->pk_treatment_id }}" data-toggle="modal" data-item-id=""><i class="fa fa-pencil"></i></a>
                      </center>
                    </td> --}}
                  </tr>
                  @endforeach 
            </tbody>
            </table>
            <table class="table table-bordered">
                <thead class="thead-colored bg-teal">
                  <tr>
                    <th style="text-align-last: right;">TOTAL TREATMENT AMOUNT</th>
                    <th class="wd-20p" style="text-align-last: left;">{{$total_amount_to_pay}} AED</th>
                  </tr>
                </thead>
              </table>
              <table class="table table-bordered">
                <thead class="thead-colored bg-danger">
                  <tr>
                    <th style="text-align-last: right;">TOTAL VAT AMOUNT</th>
                    <th class="wd-20p" style="text-align-last: left;">{{$vat_amount}} AED</th>
                  </tr>
                </thead>
              </table>
              <table class="table table-bordered">
                <thead class="thead-colored bg-teal">
                  <tr>
                    <th style="text-align-last: right;">TOTAL PRODUCT PURCHASED AMOUNT</th>
                    <th class="wd-20p" style="text-align-last: left;">{{$prod_purch_amount}} AED</th>
                  </tr>
                </thead>
              </table>
              <table class="table table-bordered">
                <thead class="thead-colored bg-danger">
                  <tr>
                    <th style="text-align-last: right;">TOTAL PAYED AMOUNT</th>
                    <th class="wd-20p" style="text-align-last: left;">{{$payed_bill_amount}} AED</th>
                  </tr>
                </thead>
              </table>
              <table class="table table-bordered">
                <thead class="thead-colored bg-teal">
                  <tr>
                    <th style="text-align-last: right;">TOTAL BALANCE TO BE PAYED</th>
                    <th class="wd-20p" style="text-align-last: left;">{{$balance}} AED</th>
                  </tr>
                </thead>
              </table>
          </div>
      </div><!-- section-wrapper -->
    </div>

    @include('admin.includes.footer')
    </script>
    <script src="{{asset('assets/backend/lib/datatables/js/jquery.dataTables.js')}}"></script>
    <script src="{{asset('assets/backend/lib/select2/js/select2.min.js')}}"></script>
    <script src="{{asset('assets/backend/js/darkbox.min.js')}}"></script>

    <script type="text/javascript">

      $('#productlistListTable').DataTable({
        scrollX:true,
        language: {
          searchPlaceholder: 'Search...',
          sSearch: '',
          lengthMenu: '_MENU_ items/page',
        },
        "ordering":false,
        "destroy": true,
        "processing": true,
      });
    </script>
    <script type="text/javascript">
        $(function () {
            $('#example').datetimepicker({
                format: 'YYYY-MM-DD'
            });      
        });
    </script>
    <script type="text/javascript">
        $(function () {
            $('#example1').datetimepicker({
                format: 'YYYY-MM-DD'
            });      
        });
    </script>
    <script type="text/javascript">
        $(function () {
            $('#example2').datetimepicker({
                format: 'YYYY-MM-DD'
            });      
        });
    </script>
    <script type="text/javascript">
        $(function () {
            $('#example3').datetimepicker({
                format: 'YYYY-MM-DD'
            });      
        });
    </script>
    <script type="text/javascript">
        $(function () {
            $('#example4').datetimepicker({
                format: 'YYYY-MM-DD'
            });      
        });
    </script>
    <script type="text/javascript">
        $(function () {
            $('#example5').datetimepicker({
                format: 'YYYY-MM-DD'
            });      
        });
    </script>