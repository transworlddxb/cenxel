<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
    </head>
    <style>
        td {
            border-bottom: 1px solid #ddd;
            margin: 5px;
        }
    </style>
    <body>
        <div>
            <div style="float: left;">
                <p>Al Qasr Al Wardi Medical Centre<br/>
                Office No: 906, NBD Bank Building PO Box: 63515, <br>
                    Sharjah – United Arab Emirates<br>
                MOB No: (+971) 6 566 83 66<br>
                  Email: info@alqasercosme.com<br>
                TRN: 100443299100003</p>
            </div>
            <div style="float: right">
                <img src="{{ url('image/al_logo.png')}}" style="width: 270px;">
            </div>
        </div>
        <div>
            <div style="text-align: center; padding-top: 130px;">
                <h1>REPORT OF DATE {{$from_date}} TO {{$to_date}}</h1>
            </div>
        </div>
        <div>
            <table cellspacing="0">
                <thead style="background-color: #eeeeee; border: none;">
                    <tr>
                        <th >SI.NO</th>
                        <th>NAME</th>
                        <th>DOCTOR</th>
                        <th>DATE</th>
                        <th>TIME</th>
                        <th>AMOUNT</th>
                        <th>VAT</th>
                        <th>PRODUCT PRICE</th>
                        <th>DUE</th>
                        <th>PAYED</th>
                        <th>BALANCE</th>
                    </tr>
                </thead>
                <tbody>
                      @foreach($appointment as $key => $report)
                    <tr>
                      <td>{{ $key+1 }}</td>
                      <td >{{ $report->first_name }}</td>
                      <td >{{ $report->roll_name }}</td>
                      <td >{{ $report->appointment_date }}</td>
                      <td >{{ $report->appointment_time }}</td>
                      <td >{{ $report->total_amount_to_pay }}</td>
                      <td >{{ $report->vat_amount }}</td>
                      <td >{{ $report->purchased_amount }}</td>
                      <td >{{ $report->prev_due_amount}}</td>
                      <td >{{ $report->payedbill_amount}}</td>
                      <td >{{ $report->duebill_amount}}</td>
                  </tr>
                  @endforeach
                </tbody>
            </table>
            <hr>
            <table class="table table-bordered">
                <thead>
                  <tr>
                    <th>TOTAL TREATMENT AMOUNT ------ </th>
                    <th>{{$total_amount_to_pay}} AED</th>
                  </tr>
                </thead>
              </table>
              <hr>
              <table class="table table-bordered">
                <thead>
                  <tr>
                    <th>TOTAL VAT AMOUNT ------ </th>
                    <th>{{$vat_amount}} AED</th>
                  </tr>
                </thead>
              </table>
              <hr>
              <table class="table table-bordered">
                <thead>
                  <tr>
                    <th>TOTAL PRODUCT PURCHASED AMOUNT ------ </th>
                    <th>{{$prod_purch_amount}} AED</th>
                  </tr>
                </thead>
              </table>
              <hr>
              <hr>
              <table class="table table-bordered">
                <thead>
                  <tr>
                    <th>TOTAL PAYED AMOUNT ------ </th>
                    <th>{{$payed_bill_amount}} AED</th>
                  </tr>
                </thead>
              </table>
              <hr>
               <table class="table table-bordered">
                <thead >
                  <tr>
                    <th>TOTAL BALANCE TO BE PAYED ------ </th>
                    <th>{{$balance}} AED</th>
                  </tr>
                </thead>
              </table>
              <hr>
        </div>
    </body>
</html>           
