@include('admin.includes.head')
<link href="{{asset('assets/backend/css/darkbox.css')}}" rel="stylesheet">
<style type="text/css">
.photogallery li{position: relative; overflow: hidden; min-height: 200px; background: #eee; border: 2px solid #fff;}
.photogallery li:nth-child(odd){background: #bbb;}
.photogallery li img{position: absolute; top: -9999px; right: -9999px; bottom: -9999px; left: -9999px; margin: auto; max-width: none; /*max-height: 100%;*/ width: 100%; height: auto;}
</style>

<div class="slim-mainpanel">
  <div class="container">
    <div class="slim-pageheader">
      <ol class="breadcrumb slim-breadcrumb">
        <li class="breadcrumb-item"><a href="#">Home</a></li>
        <li class="breadcrumb-item">Statement of cusultation</li>
        {{-- <li class="breadcrumb-item active" aria-current="page">Form Wizards</li> --}}
      </ol>
      <h6 class="slim-pagetitle mb-2">Statement details</h6>
    </div><!-- slim-pageheader -->    
        
        <div class="section-wrapper">

      <div class="row">
        <div class="col-sm-12 col-lg-6">
          <label class="section-title"> Details</label>
          <p class="mg-b-20 mg-sm-b-40">Consulting details of file number : <b style="color:red;">{{$file->file_no}}</b></p></div>
          <div class="col-sm-12 col-lg-6">
            <div class="d-flex  justify-content-md-end justify-content-center">
              <a href="{{ url('/admin/today_appointment') }}" class="btn btn-primary " style="margin-bottom: 10px;"><i class="icon ion-arrow-left-a"></i> Back</a>
            </div>  
          </div>
        </div>

          <div class="card card-invoice">
            <div class="card-body">
              <div class="invoice-header">
                <h1 class="invoice-title">Statement</h1>
                <div class="billed-from">
                  <h6>Al Qasr Al Wardi Medical Centre</h6>
                  <p>Office No: 906, Emirates Sail Tower, Buhaira Corniche<br>
                    Sharjah – United Arab Emirates<br>
                  MOB No: (+971) 6 566 83 66<br>
                  Email: info@alqasercosme.com<br>
                TRN: 100443299100003</p>
                </div><!-- billed-from -->
              </div><!-- invoice-header -->

              <div class="row mg-t-20">
                <div class="col-md">
                  <label class="section-label-sm tx-gray-500">Billed To</label>
                  <div class="billed-to">
                    <h6>Name : {{$file->first_name}} {{$file->middle_name}} {{$file->last_name}}</h6>
                  <p>Emirates ID: {{$file->emirates_id}}<br>
                  Mobile: {{$file->mob_number}}<br>
                  Email: </p>
                  </div>
                </div><!-- col -->
                <div class="col-md">
                  <label class="section-label-sm tx-gray-500">Invoice Information</label>
                  <p class="invoice-info-row">
                    <span>Bill No</span>
                    <span>AQAW{{$billcount}}</span>
                  </p>
                  <p class="invoice-info-row">
                    <span>File Number</span>
                    <span>{{$file->file_no}}</span>
                  </p>
                  <p class="invoice-info-row">
                    <span>Issue Date:</span>
                    <span>{{$appointment_trt_data->appointment_date}}</span>
                  </p>
                </div><!-- col -->
              </div><!-- row -->

              <div class="table-responsive mg-t-40">
                <table class="table table-invoice">
                  <thead>
                    <tr>
                      <th class="wd-30p">Treatment</th>
                      <th></th>
                      <th></th>
                      <th class="tx-right">Amount</th>
                    </tr>
                  </thead>
                  <tbody>
                        @if($amount_one!=0)
                            <tr>
                              <td>{{$treatment_one->treatment_name}}</td>
                              <td></td>
                              <td></td>
                              <td class="tx-right">{{$amount_one}}</td>
                            </tr>
                        @endif
                        @if($amount_one==0)
                           
                        @endif
                    @if($amount_two!=0)
                            <tr>
                              <td>{{$treatment_two->treatment_name}}</td>
                              <td></td>
                              <td></td>
                              <td class="tx-right">{{$amount_two}}</td>
                            </tr>
                        @endif
                        @if($amount_two==0)
                           
                        @endif
                    @if($amount_three!=0)
                            <tr>
                              <td>{{$treatment_three->treatment_name}}</td>
                              <td></td>
                              <td></td>
                              <td class="tx-right">{{$amount_three}}</td>
                            </tr>
                        @endif
                        @if($amount_three==0)
                           
                        @endif
                    @if($amount_four!=0)
                            <tr>
                              <td>{{$treatment_four->treatment_name}}</td>
                              <td></td>
                              <td></td>
                              <td class="tx-right">{{$amount_four}}</td>
                            </tr>
                        @endif
                        @if($amount_four==0)
                            
                        @endif
                    @if($amount_five!=0)
                            <tr>
                              <td>{{$treatment_five->treatment_name}}</td>
                              <td></td>
                              <td></td>
                              <td class="tx-right">{{$amount_five}}</td>
                            </tr>
                        @endif
                        @if($amount_five==0)
                           
                        @endif
                    <tr>


                        <thead>
                            <tr>
                              <th class="wd-30p">Purchased Product</th>
                              <th></th>
                              <th></th>
                              <th class="tx-right">Amount</th>
                            </tr>
                          </thead>
                          <tbody>
                              @if($prod_one!=Null)
                                    <tr>
                                      <td>{{$product_one->prod_name}}</td>
                                      <td></td>
                                      <td></td>
                                      <td class="tx-right">{{$product_one->prod_price}}</td>
                                    </tr>
                                @endif
                                @if($prod_one==Null)

                                @endif

                                @if($prod_two!=Null)
                                    <tr>
                                      <td>{{$product_two->prod_name}}</td>
                                      <td></td>
                                      <td></td>
                                      <td class="tx-right">{{$product_two->prod_price}}</td>
                                    </tr>
                                @endif
                                @if($prod_two==Null)

                                @endif

                                @if($prod_three!=Null)
                                    <tr>
                                      <td>{{$product_three->prod_name}}</td>
                                      <td></td>
                                      <td></td>
                                      <td class="tx-right">{{$product_three->prod_price}}</td>
                                    </tr>
                                @endif
                                @if($prod_three==Null)

                                @endif

                                @if($prod_four!=Null)
                                    <tr>
                                      <td>{{$product_four->prod_name}}</td>
                                      <td></td>
                                      <td></td>
                                      <td class="tx-right">{{$product_four->prod_price}}</td>
                                    </tr>
                                @endif
                                @if($prod_four==Null)

                                @endif

                                @if($prod_five!=Null)
                                    <tr>
                                      <td>{{$product_five->prod_name}}</td>
                                      <td></td>
                                      <td></td>
                                      <td class="tx-right">{{$product_five->prod_price}}</td>
                                    </tr>
                                @endif
                                @if($prod_five==Null)

                                @endif
                          </tbody>



                      <td colspan="2" rowspan="9" class="valign-middle">
                        <div class="invoice-notes">
                          <label class="section-label-sm tx-gray-500" style="color: #1b84e7;">Description</label>
                          <p style="color: green;">{{$cunsultdata->consult_details}}</p>
                        </div><!-- invoice-notes -->
                      </td>

                      @if($file->insurance_status==0)
                      <td class="tx-right">Insurance</td>
                      <td colspan="2" class="tx-right" style="color: red;">Not Applicable</td>
                      @endif
                       @if($file->insurance_status==1)
                      <td class="tx-right">Insurance</td>
                      <td colspan="2" class="tx-right" style="color: green;">{{$file->percent_insurance}} %</td>
                      @endif
                    </tr>
                    <tr>
                      <td class="tx-right">total treatment amount</td>
                      <td colspan="2"  class="tx-right">{{$amount}}</td>
                    </tr>
                    <tr>
                      <td class="tx-right">total purchase</td>
                      <td colspan="2"  class="tx-right">{{$total_purch_price}}</td>
                    </tr>
{{--                     <tr>
                      <td class="tx-right"> discount</td>
                      <td colspan="2"  class="tx-right">{{$discount}}</td>
                    </tr> --}}
                    <tr>
                      <td class="tx-right">last Due amount</td>
                      <td colspan="2"  class="tx-right">{{$due}}</td>
                    </tr>
                    <tr>
                      <td class="tx-right">Net amount</td>
                      <td colspan="2"  class="tx-right">AED. {{$total_amount}}</td>
                    </tr>
                    <tr>
                      <td class="tx-right">Vat amount</td>
                      <td colspan="2"  class="tx-right">AED. {{$vat_amount}}</td>
                    </tr> 
                    <tr>
                      <td class="tx-right tx-uppercase tx-bold tx-inverse">Total amount</td>
                      <td colspan="2" class="tx-right"><h4 class="tx-primary tx-bold tx-lato" style="color: green;">{{$grand_total}}</h4></td>
                    </tr>                   

                  </tbody>
                </table>
              </div><!-- table-responsive -->

              <hr class="mg-b-60">

              {{-- <a href="{{ url('/admin/download_bill_pdf/'.$file->pk_int_appoint_id)}}" class="btn btn-primary btn-block">Print the statement</a> --}}
                @if($cunsultdata->complete_status==3)
               <button type="button" class="edit-item btn btn-primary btn-block print_but" style="color: white; background-color: green; border: green;" id="{{ $file->pk_int_appoint_id }}" data-toggle="modal" data-item-id="">Print the bill</button>
                @endif
                @if($cunsultdata->complete_status==4)
               <button type="button" class="edit-item btn btn-primary btn-block disabled print_but" style="color: white; background-color: green; border: green;" id="{{ $file->pk_int_appoint_id }}" data-toggle="modal" data-item-id=""> Print the bill</button>
                @endif
              {{-- <a href="{{ url('/admin/download_bill_pdf/'.$file->pk_int_appoint_id)}}" class="btn btn-primary btn-block">Print the bill</a> --}}

            </div><!-- card-body -->
          </div><!-- card -->
      </div>

    @include('admin.includes.footer')
    </script>
    <script src="{{asset('assets/backend/lib/datatables/js/jquery.dataTables.js')}}"></script>
    <script src="{{asset('assets/backend/lib/select2/js/select2.min.js')}}"></script>
    <script src="{{asset('assets/backend/js/darkbox.min.js')}}"></script>
    
     {{-- @foreach ($file as $g)  --}}
        <div id="edit-modal" class="modal fade modal-min" >
          <div class="modal-dialog modal-dialog-vertical-center" role="document">
           <div class="modal-content bd-0 tx-14">
            <div class="modal-header pd-x-20">
              <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold">ENTER PAYING AMOUNT</h6>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <form role="form" action="{{ url('/admin/update_bill_paymnt/'.$file->pk_int_appoint_id) }}" method="post">
              <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
              <div class="box-body col-md-12">
                <label></label>
                <div class="form-group"> 
                  <input type="text" class="form-control" placeholder="Enter paying amount" id="payed_amount" name="payed_amount" required>
                </div>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary" id="print">Print</button>
              </div>
            </form>
          </div>
        </div><!-- modal-dialog -->
        </div><!-- modal -->
        {{-- @endforeach --}}

    <script type="text/javascript">

     $(document).ready(function() {

       $(".edit-item").click(function()
       {
        
         var id=$(this).attr('id');
         $(this).attr('data-target','#edit-modal');
             // alert(id);
             jQuery.ajax({
              type: "get",
              url: 'bill_pdf/'+id,
                //data: {smsid:id},
                success: function(res)
                {
                   alert(res);
                   var d1= res.split(',');
                   $("#payed_amount").val(d1[0]);
                 }
               });
             $('#add_post').foundation('close');
             
           });

     });
    </script>

    <script type="text/javascript">
       $("#print").click(function()
       {
        $('#edit-modal').modal('hide')
        alert('Bill is Downloading,Go to BILLING section for the statement....');
        $('.print_but').attr("disabled", true);
        });
    </script>

