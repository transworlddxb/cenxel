<!DOCTYPE html>
<html lang="en">
		<head>
				<meta charset="utf-8">
		</head>
		<style>
				td {
						border-bottom: 1px solid #ddd;
						margin: 5px;
				}
		</style>
		<body>
				<div>
						<div style="float: left;">
								<p>Al Qasr Al Wardi Medical Centre<br/>
								Office No: 906, Emirates Sail Tower, Buhaira Corniche<br>
										Sharjah – United Arab Emirates<br>
								MOB No: (+971) 6 566 83 66<br>
									Email: info@alqasercosme.com<br>
								TRN: 100443299100003</p>
						</div>
						<div style="float: right">
								<img src="{{ url('image/al_logo.png')}}" style="width: 270px;">
						</div>
				</div>
				<div>
						<div style="text-align: center; padding-top: 130px;">
								<h1>RECEIPT</h1>
						</div>
				</div>
				<div>
						<div style="float: right">
								<p>Receipt for: {{$file->first_name}} {{$file->middle_name}} {{$file->last_name}}<br/>
								Emirates ID: {{$file->emirates_id}}<br>
								Mobile: {{$file->mob_number}}<br>
								</p>
						</div>
						<div style="text-align: left">
								<p>Bill No : AQAW{{$billcount}}<br/>
								File Number : {{$file->file_no}}<br/>
								Issue Date: {{$appointment_trt_data->appointment_date}}<br>
								</p>
						</div>
				</div>
				<div>
						<table cellspacing="0">
								<thead style="background-color: #eeeeee; border: none;">
										<tr>
												<th width="120px" height="35px" style="margin: 5px">Treatment</th>
												<th width="220px" align="center">.</th>
												<th width="260px" align="center">.</th>
												<th width="118px">Amount</th>
										</tr>
								</thead>
								<tbody>
										@if($amount_one!=0)
														<tr>
															<td>{{$treatment_one->treatment_name}}</td>
															<td></td>
															<td></td>
															<td class="tx-right">{{$amount_one}}</td>
														</tr>
												@endif
												@if($amount_one==0)
													 
												@endif
										@if($amount_two!=0)
														<tr>
															<td>{{$treatment_two->treatment_name}}</td>
															<td></td>
															<td></td>
															<td class="tx-right">{{$amount_two}}</td>
														</tr>
												@endif
												@if($amount_two==0)
													 
												@endif
										@if($amount_three!=0)
														<tr>
															<td>{{$treatment_three->treatment_name}}</td>
															<td></td>
															<td></td>
															<td class="tx-right">{{$amount_three}}</td>
														</tr>
												@endif
												@if($amount_three==0)
													 
												@endif
										@if($amount_four!=0)
														<tr>
															<td>{{$treatment_four->treatment_name}}</td>
															<td></td>
															<td></td>
															<td class="tx-right">{{$amount_four}}</td>
														</tr>
												@endif
												@if($amount_four==0)
														
												@endif
										@if($amount_five!=0)
														<tr>
															<td>{{$treatment_five->treatment_name}}</td>
															<td></td>
															<td></td>
															<td class="tx-right">{{$amount_five}}</td>
														</tr>
												@endif
												@if($amount_five==0)
													 
												@endif
												
												@if($prod_one)
												<tr>
															<th class="wd-30p">Purchased Product</th>
															<th></th>
															<th></th>
															<th class="tx-right">Amount</th>
														</tr>
												@endif
														 @if($prod_one!=Null)
																		<tr>
																			<td>{{$product_one->prod_name}}</td>
																			<td></td>
																			<td></td>
																			<td class="tx-right">{{$product_one->prod_price}}</td>
																		</tr>
																@endif
																@if($prod_one==Null)

																@endif

																@if($prod_two!=Null)
																		<tr>
																			<td>{{$product_two->prod_name}}</td>
																			<td></td>
																			<td></td>
																			<td class="tx-right">{{$product_two->prod_price}}</td>
																		</tr>
																@endif
																@if($prod_two==Null)

																@endif

																@if($prod_three!=Null)
																		<tr>
																			<td>{{$product_three->prod_name}}</td>
																			<td></td>
																			<td></td>
																			<td class="tx-right">{{$product_three->prod_price}}</td>
																		</tr>
																@endif
																@if($prod_three==Null)

																@endif

																@if($prod_four!=Null)
																		<tr>
																			<td>{{$product_four->prod_name}}</td>
																			<td></td>
																			<td></td>
																			<td class="tx-right">{{$product_four->prod_price}}</td>
																		</tr>
																@endif
																@if($prod_four==Null)

																@endif

																@if($prod_five!=Null)
																		<tr>
																			<td>{{$product_five->prod_name}}</td>
																			<td></td>
																			<td></td>
																			<td class="tx-right">{{$product_five->prod_price}}</td>
																		</tr>
																@endif
																@if($prod_five==Null)

																@endif

										<tr>

											<td colspan="2" rowspan="9" class="valign-middle">
												<div class="invoice-notes">
													<label class="section-label-sm tx-gray-500"></label>
													{{-- <p>{{$cunsultdata->consult_details}}</p> --}}
													<p></p>
												</div><!-- invoice-notes -->
											</td>
											@if($file->insurance_status==0)
											<td class="tx-right">Insurance</td>
											<td colspan="2" class="tx-right" style="color: red;">Not Applicable</td>
											@endif
											 @if($file->insurance_status==1)
											<td class="tx-right">Insurance</td>
											<td colspan="2" class="tx-right" style="color: green;">{{$file->percent_insurance}} %</td>
											@endif
										
										</tr>
										 <tr>
											<td class="tx-right">total Treatment amount</td>
											<td colspan="2"  class="tx-right">{{$amount}}</td>
										</tr>
										<tr>
											<td class="tx-right">total Product amount</td>
											<td colspan="2"  class="tx-right">{{$total_purch_price}}</td>
										</tr>
										<tr>
											<td class="tx-right">Previous Due amount</td>
											<td colspan="2"  class="tx-right">{{$due}}</td>
										</tr>     
										<tr>
											<td class="tx-right">VAT amount</td>
											<td colspan="2"  class="tx-right">{{$vat_amount}}</td>
										</tr>
										<tr>
											<td class="tx-right tx-uppercase tx-bold tx-inverse">Total Amount to Pay</td>
											<td colspan="2" class="tx-right"><h4 class="tx-primary tx-bold tx-lato">{{$grand_total}}</h4></td>
										</tr>
										
										<tr>
											<td class="tx-right tx-uppercase tx-bold tx-inverse">Paid Amount</td>
											<td colspan="2" class="tx-right"><h4 class="tx-primary tx-bold tx-lato">{{$payed}}</h4></td>
										</tr>
								</tbody>
						</table>
				</div>
				{{-- <footer>
						<div style="position:fixed;bottom:50;">
								Bill was created on a computer and is valid without the signature and seal.
						</div>
				</footer> --}}

		</body>
</html>           
