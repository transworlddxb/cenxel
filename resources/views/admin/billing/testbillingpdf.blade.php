<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
    </head>
    <style>
        td {
            border-bottom: 1px solid #ddd;
            margin: 5px;
        }
    </style>
    <body>
        <div>
            <div style="float: left;">
                <p>Al Qasr Al Wardi Medical Centre<br/>
                Office No: 906, Emirates Sail Tower, Buhaira Corniche<br>
                Sharjah – United Arab Emirates<br>
                MOB No: (+971) 6 566 83 66<br>
                  Email: info@alqasercosme.com<br>
                TRN: 100443299100003</p>
            </div>
            <div style="float: right;">
                <img src="{{ url('image/al_logo.png')}}" style="width: 270px;">
            </div>
        </div>
        <div>
          <center>
            <div style="text-align: center; padding-top: 130px;">
                <h1>INVOICE</h1>
            </div>
          </center>
        </div>
        <div>
            <div style="float: right">
                <p>Name: Mohammed Abdullah<br/>
                Emirates ID: -- <br>
                Mobile: 0507030006<br>
                </p>
            </div>
            <div style="text-align: left">
                <p>Bill No : 1100 <br/>
                File Number : DC429 <br/>
                Issue Date: 24/07/2019<br>
                </p>
            </div>
        </div>
        <div>
            <table cellspacing="0">
                <thead style="background-color: #eeeeee; border: none;">
                    <tr>
                        <th width="120px" height="35px" style="margin: 5px">Treatment</th>
                        <th width="220px" align="center">.</th>
                        <th width="260px" align="center">.</th>
                        <th width="118px">Amount</th>
                    </tr>
                </thead>
                <tbody>
                            <tr>
                              <td>Beard Line</td>
                              <td></td>
                              <td></td>
                              <td class="tx-right">160</td>
                            </tr>
                        
                    <tr>
                      {{-- <thead>
                            <tr>
                              <th class="wd-30p">Purchased Product</th>
                              <th></th>
                              <th></th>
                              <th class="tx-right">Amount</th>
                            </tr>
                          </thead>
                          <tbody>
                                    <tr>
                                      <td>{{$product_one->prod_name}}</td>
                                      <td></td>
                                      <td></td>
                                      <td class="tx-right">{{$product_one->prod_price}}</td>
                                    </tr>


                                
                          </tbody> --}}
                      <td colspan="2" rowspan="6" class="valign-middle">
                        <div class="invoice-notes">
                          <label class="section-label-sm tx-gray-500">Description :</label>
                          <p>  </p>
                        </div><!-- invoice-notes -->
                      </td>
                      <td class="tx-right">Insurance</td>
                      <td colspan="2" class="tx-right" style="color: red;">Not Applicable</td>
                       {{-- @if($file->insurance_status==1)
                      <td class="tx-right">Insurance</td>
                      <td colspan="2" class="tx-right" style="color: green;">{{$file->percent_insurance}} %</td>
                      @endif --}}
                    
                    </tr>
                     <tr>
                      <td class="tx-right">total treatment amount</td>
                      <td colspan="2"  class="tx-right">160</td>
                    </tr>
                    <tr>
                      <td class="tx-right">total purchace</td>
                      <td colspan="2"  class="tx-right">0</td>
                    </tr>
                    <tr>
                      <td class="tx-right"> discount</td>
                      <td colspan="2"  class="tx-right">0</td>
                    </tr>
                    <tr>
                      <td class="tx-right"> Due amount</td>
                      <td colspan="2"  class="tx-right">0</td>
                    </tr>     
                    <tr>
                      <td class="tx-right tx-uppercase tx-bold tx-inverse">Total Due</td>
                      <td colspan="2" class="tx-right"><h4 class="tx-primary tx-bold tx-lato">0</h4></td>
                    </tr>
                    {{-- <tr>
                      <td class="tx-right">VAT amount</td>
                      <td colspan="2"  class="tx-right">22.50</td>
                    </tr>  --}}
                    <tr>
                      <td class="tx-right tx-uppercase tx-bold tx-inverse">TOTAL</td>
                      <td colspan="2" class="tx-right"><h4 class="tx-primary tx-bold tx-lato">160</h4></td>
                    </tr>
                </tbody>
            </table>
        </div>
        {{-- <footer>
            <div style="position:fixed;bottom:50;">
                Invoice was created on a computer and is valid without the signature and seal.
            </div>
        </footer> --}}

    </body>
</html>           
