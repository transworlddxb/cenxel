@include('admin.includes.head')
<link href="{{asset('assets/backend/css/darkbox.css')}}" rel="stylesheet">
<style type="text/css">
.photogallery li{position: relative; overflow: hidden; min-height: 200px; background: #eee; border: 2px solid #fff;}
.photogallery li:nth-child(odd){background: #bbb;}
.photogallery li img{position: absolute; top: -9999px; right: -9999px; bottom: -9999px; left: -9999px; margin: auto; max-width: none; /*max-height: 100%;*/ width: 100%; height: auto;}
</style>

<div class="slim-mainpanel">
  <div class="container">
    <div class="slim-pageheader">
      <ol class="breadcrumb slim-breadcrumb">
        <li class="breadcrumb-item"><a href="#">Home</a></li>
        <li class="breadcrumb-item">Today's appointments</li>
        {{-- <li class="breadcrumb-item active" aria-current="page">Form Wizards</li> --}}
      </ol>
      <h6 class="slim-pagetitle mb-2">Today's appointments</h6>
    </div><!-- slim-pageheader -->    


    <div class="section-wrapper">

      <div class="row">
        <div class="col-sm-12 col-lg-6">
          <label class="section-title"> Details</label>
          <p class="mg-b-20 mg-sm-b-40">List of Today's appointments</p></div>
          {{-- <div class="col-sm-12 col-lg-6">
            <div class="d-flex  justify-content-md-end justify-content-center">
              <a href="{{ url('/admin/reg_user') }}" class="btn btn-primary " style="margin-bottom: 10px;"><i class="fa fa-plus"></i> Click to Add new Appointment</a>
            </div>  
          </div> --}}
        </div>


        <div class="table-wrapper">
          <table id="productlistListTable" class="table display" width='100%'>
            <thead>
              <tr>
                <th >SI.NO</th>
                <th >Apt.NO</th>
                <th >FILE No.</th>
                <th >PATIENT NAME</th>
                <th >DOCTOR</th>
                <th >DATE</th>
                <th >TOTAL AMOUNT</th>
                <th >PAID AMOUNT</th>
                <th >PURCHASED AMOUNT</th>
                <th >BALANCE TO PAY</th>
                <th >STATUS</th>
                <th >ACTION</th>
              </tr>
            </thead>
            <tbody>
                @foreach($appointment as $key => $appoint)
                    <tr>
                      <td >{{ $key+1 }}</td>
                      <td >{{ $appoint->pk_int_appoint_id }}</td>
                      <td >{{ $appoint->file_no }}</td>
                      <td >{{ $appoint->appoint_patient }}</td>
                      <td >{{ $appoint->roll_name}}</td>
                      <td >{{ $appoint->appointment_date }}</td>
                      <td >{{ $appoint->total_amount_to_pay }}</td>
                      <td >{{ $appoint->payedbill_amount }}</td>
                      <td >{{ $appoint->purchased_amount }}</td>
                      <td >{{ $appoint->duebill_amount }}</td>
                      @if($appoint->complete_status==4)
                      <td style="color: green;">BILLED</td>
                        <td>
                            <a href="{{ url('/admin/open_billed_status/'.$appoint->pk_int_appoint_id) }}" type="button" class="btn btn-primary" title="Open" value="Edit">Open</a>
                        </td>
                      @endif
                  </tr>
                  @endforeach
            </tbody>
          </table>
        </div>
      </div><!-- section-wrapper -->
    </div>

    @include('admin.includes.footer')
    </script>
    <script src="{{asset('assets/backend/lib/datatables/js/jquery.dataTables.js')}}"></script>
    <script src="{{asset('assets/backend/lib/select2/js/select2.min.js')}}"></script>
    <script src="{{asset('assets/backend/js/darkbox.min.js')}}"></script>

    <script type="text/javascript">
      $(document).on("click", ".del", function (event) {
    var result = confirm("Are you sure want to delete?");
    if (result) {
                  //Logic to delete the item
                  return true;
                }
                else
                {
                 return false;
               }
             });
  


      $('#productlistListTable').DataTable({
        scrollX:true,
        language: {
          searchPlaceholder: 'Search...',
          sSearch: '',
          lengthMenu: '_MENU_ items/page',
        },
        "ordering":false,
        "destroy": true,
        "processing": true,
      });
    </script>
