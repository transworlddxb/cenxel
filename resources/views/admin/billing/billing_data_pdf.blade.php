<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
    </head>
    <style>
        td {
            border-bottom: 1px solid #ddd;
            margin: 5px;
        }
    </style>
    <body>
        <div>
            <div style="float: left;">
                <p>Al Qasr Al Wardi Medical Centre<br/>
                Office No: 906, Emirates Sail Tower, Buhaira Corniche<br>
                    Sharjah – United Arab Emirates<br>
                MOB No: (+971) 6 566 83 66<br>
                  Email: info@alqasercosme.com<br>
                TRN: 100443299100003</p>
            </div>
            <div style="float: right">
                <img src="{{ url('image/al_logo.png')}}" style="width: 270px;">
            </div>
        </div>
        <div>
            <div style="text-align: center; padding-top: 130px;">
                <h1>TAX INVOICE</h1>
            </div>
        </div>
        <div>
            <div style="float: right">
                <p>Invoice No : {{$appointment_trt_data->inv_nmbr}}<br/>
                File Number : {{$file->file_no}}<br/>
                Issue Date: {{$date_appo}}<br>
                </p>
            </div>
            <div style="text-align: left">
                <p>Name: {{$file->first_name}} {{$file->middle_name}} {{$file->last_name}}<br/>
                Emirates ID: {{$file->emirates_id}}<br>
                Mobile: {{$file->mob_number}}<br>
                </p>
            </div>
        </div>
        <div>
            <table cellspacing="0" style="width:100%">
                <thead style="background-color: #eeeeee; border: none;">
                    <tr>
                        <th width="15px" height="35px" style="margin: 5px">Sl No&nbsp;&nbsp;</th>
                        <th width="40px" height="35px" style="margin: 5px">Service Description&nbsp;&nbsp;</th>
                        <th width="40px" align="center">Price&nbsp;&nbsp;</th>
                        <th width="40px" align="center">Discount&nbsp;&nbsp;</th>
                        <th width="40px" align="center">Net Amount&nbsp;&nbsp;</th>
                        <th width="40px" align="center">Vat amount&nbsp;&nbsp;</th>
                        <th width="40px">Amount&nbsp;&nbsp;</th>
                    </tr>
                </thead>
                <tbody>
                    @if($amount_one!=0)
                            <tr>
                              <td align="left" height="35px">1</td>
                              <td align="left" height="35px">{{$treatment_one->treatment_name}}</td>
                              <td align="center" height="35px">{{$amount_one}}</td>
                              <td align="center" height="35px">0.00</td>
                              <td align="center" height="35px">{{$amount_one}}</td>
                              <td align="center" height="35px">{{$amount_one_tax}}</td>
                              <td align="center" height="35px">{{$grand_one}}</td>
                            </tr>
                        @endif
                        @if($amount_one==0)
                           
                        @endif
                    @if($amount_two!=0)
                            <tr>
                              <td align="left" height="35px">2</td>
                              <td align="left" height="35px">{{$treatment_two->treatment_name}}</td>
                              <td align="center" height="35px">{{$amount_two}}</td>
                              <td align="center" height="35px">0.00</td>
                              <td align="center" height="35px">{{$amount_two}}</td>
                              <td align="center" height="35px">{{$amount_two_tax}}</td>
                              <td align="center" height="35px">{{$grand_two}}</td>
                            </tr>
                        @endif
                        @if($amount_two==0)
                           
                        @endif
                    @if($amount_three!=0)
                            <tr>
                              <td align="left" height="35px">3</td>
                              <td align="left" height="35px">{{$treatment_three->treatment_name}}</td>
                              <td align="center" height="35px">{{$amount_three}}</td>
                              <td align="center" height="35px">0.00</td>
                              <td align="center" height="35px">{{$amount_three}}</td>
                              <td align="center" height="35px">{{$amount_three_tax}}</td>
                              <td align="center" height="35px">{{$grand_three}}</td>
                            </tr>
                        @endif
                        @if($amount_three==0)
                           
                        @endif
                    @if($amount_four!=0)
                            <tr>
                              <td align="left" height="35px">4</td>
                              <td align="left" height="35px">{{$treatment_four->treatment_name}}</td>
                              <td align="center" height="35px">{{$amount_four}}</td>
                              <td align="center" height="35px">0.00</td>
                              <td align="center" height="35px">{{$amount_four}}</td>
                              <td align="center" height="35px">{{$amount_four_tax}}</td>
                              <td align="center" height="35px">{{$grand_four}}</td>
                            </tr>
                        @endif
                        @if($amount_four==0)
                            
                        @endif
                    @if($amount_five!=0)
                            <tr>
                              <td align="left" height="35px">5</td>
                              <td align="left" height="35px">{{$treatment_five->treatment_name}}</td>
                              <td align="center" height="35px">{{$amount_five}}</td>
                              <td align="center" height="35px">0.00</td>
                              <td align="center" height="35px">{{$amount_five}}</td>
                              <td align="center" height="35px">{{$amount_five_tax}}</td>
                              <td align="center" height="35px">{{$grand_five}}</td>
                            </tr>
                        @endif
                        @if($amount_five==0)
                           
                        @endif
                        @if($prod_one!=Null)
                        <thead style="background-color: #eeeeee; border: none;margin-top: -20px;">
                          <tr>
                            <th width="15px" height="35px" style="margin: 5px">Sl No</th>
                            <th width="220px" height="35px" style="margin: 5px">Purchased Products</th>
                            <th align="center"></th>
                            <th align="center"></th>
                            <th align="center"></th>
                            <th align="center"></th>
                            <th>Amount</th>
                            </tr>
                        </thead>

                             @if($prod_one!=Null)
                                    <tr>
                                      <td width="120px">{{$product_one->prod_name}}</td>
                                      <td></td>
                                      <td></td>
                                      <td></td>
                                      <td></td>
                                      <td></td>
                                      <td class="tx-right">{{$product_one->prod_price}}</td>
                                    </tr>
                                @endif
                                @if($prod_one==Null)
                                    <tr>
                                      <td width="80px">No Purchased Products</td>
                                      <td></td>
                                      <td></td>
                                      <td></td>
                                      <td></td>
                                      <td></td>
                                      <td></td>
                                    </tr>
                                @endif

                                @if($prod_two!=Null)
                                    <tr>
                                      <td>{{$product_two->prod_name}}</td>
                                      <td></td>
                                      <td></td>
                                      <td class="tx-right">{{$product_two->prod_price}}</td>
                                    </tr>
                                @endif
                                @if($prod_two==Null)

                                @endif

                                @if($prod_three!=Null)
                                    <tr>
                                      <td>{{$product_three->prod_name}}</td>
                                      <td></td>
                                      <td></td>
                                      <td class="tx-right">{{$product_three->prod_price}}</td>
                                    </tr>
                                @endif
                                @if($prod_three==Null)

                                @endif

                                @if($prod_four!=Null)
                                    <tr>
                                      <td>{{$product_four->prod_name}}</td>
                                      <td></td>
                                      <td></td>
                                      <td class="tx-right">{{$product_four->prod_price}}</td>
                                    </tr>
                                @endif
                                @if($prod_four==Null)

                                @endif

                                @if($prod_five!=Null)
                                    <tr>
                                      <td>{{$product_five->prod_name}}</td>
                                      <td></td>
                                      <td></td>
                                      <td class="tx-right">{{$product_five->prod_price}}</td>
                                    </tr>
                                @endif
                                @if($prod_five==Null)

                                @endif
                                @endif
                                 <tr>
                            <th width="15px" height="35px" style="margin: 5px">--</th>
                            <th width="220px" height="35px" style="margin: 5px">--TOTAL--</th>
                            <th align="center">{{$amount}}</th>
                            <th align="center">0.00</th>
                            <th align="center">{{$amount}}</th>
                            <th align="center">{{$vat_amount}}</th>
                            <th align="center">{{$grand_total}}</th>
                            </tr>
                    <tr>
                      <td colspan="4" rowspan="9" class="align-right">
                        <div class="invoice-notes">
                          <label class="section-label-sm tx-gray-900"></label>
                          <p></p>
                          {{-- <p>{{$cunsultdata->consult_details}}</p> --}}
                        </div><!-- invoice-notes -->
                      </td>
                         

{{--                       @if($file->insurance_status==0)
                      <td class="tx-right" align="right">Insurance</td>
                      <td colspan="2" class="tx-right" style="color: red;" align="right">Not Applicable</td>
                      @endif
                       @if($file->insurance_status==1)
                      <td class="tx-right" align="right">Insurance</td>
                      <td colspan="2" class="tx-right" style="color: green;" align="right">{{$file->percent_insurance}} %</td>
                      @endif
                    
                    </tr>
                     <tr>
                      <td class="tx-right" align="right">Total Treatment amount</td>
                      <td colspan="2"  class="tx-right" align="right">{{$amount}}</td>
                    </tr>
 
                    <tr>
                      <td class="tx-right" align="right">Total purchase amount</td>
                      <td colspan="2"  class="tx-right" align="right">{{$total_purch_price}}</td>
                    </tr>  
                    <tr>
                      <td class="tx-right" align="right"> VAT amount</td>
                      <td colspan="2"  class="tx-right" align="right">{{$vat_amount}}</td>
                    </tr>
                    <tr> --}}
                      <td class="tx-right tx-uppercase tx-bold tx-inverse" align="right">Total amount</td>
                      <td colspan="2" class="tx-right" align="center"><h4 class="tx-primary tx-bold tx-lato" style="color: green;">{{$grand_total}}</h4></td>
                    </tr>
                    <tr>
                      <td class="tx-right" align="right"> Previous Due To Pay</td>
                      <td colspan="2"  class="tx-right" align="center">{{$appointment_trt_data->prev_due_amount}}</td>
                    </tr>
                    <tr>
                      <td class="tx-right" align="right"> Total amount to Pay</td>
                      <td colspan="2"  class="tx-right" align="center">{{$grand_total_with_prev}}</td>
                    </tr>
                    <tr>
                      <td class="tx-right tx-uppercase tx-bold tx-inverse" align="right">Amount Paid</td>
                      <td colspan="2" class="tx-right" align="center"><h4 class="tx-primary tx-bold tx-lato">AED. {{$appointment_trt_data->payedbill_amount}}</h4></td>
                    </tr>
                    <tr>
                      <td class="tx-right" align="right">Balance Amount to be paid</td>
                      <td colspan="2"  class="tx-center" style="color: red;" align="center">{{$app->duebill_amount}}</td>
                    </tr>
                </tbody>
            </table>
        </div>
        {{-- <footer>
            <div style="position:fixed;bottom:50;">
                Bill was created on a computer and is valid without the signature and seal.
            </div>
        </footer> --}}

    </body>
</html>           
