@include('admin.includes.head')
<link href="{{asset('assets/backend/css/darkbox.css')}}" rel="stylesheet">
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.21.0/moment.min.js" type="text/javascript"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css">
<style type="text/css">
.photogallery li{position: relative; overflow: hidden; min-height: 200px; background: #eee; border: 2px solid #fff;}
.photogallery li:nth-child(odd){background: #bbb;}
.photogallery li img{position: absolute; top: -9999px; right: -9999px; bottom: -9999px; left: -9999px; margin: auto; max-width: none; /*max-height: 100%;*/ width: 100%; height: auto;}
</style>

<div class="slim-mainpanel">
  <div class="container">
    <div class="slim-pageheader">
      <ol class="breadcrumb slim-breadcrumb">
        <li class="breadcrumb-item"><a href="#">Home</a></li>
        <li class="breadcrumb-item">Today's appointments</li>
        {{-- <li class="breadcrumb-item active" aria-current="page">Form Wizards</li> --}}
      </ol>
      <h6 class="slim-pagetitle mb-2">Today's appointments</h6>
    </div><!-- slim-pageheader -->    


    <div class="section-wrapper">

      <div class="row">
        <div class="col-sm-12 col-lg-3">
          <label class="section-title"> Details</label>
          <p class="mg-b-20 mg-sm-b-40">List of Today's appointments</p></div>
          <div class="col-sm-12 col-lg-3">

          <form action="{{ url('/admin/search_date_appo')}}" method="post">
            {{csrf_field()}}
            <div class="row  justify-content-md-end justify-content-center xs-p2">
              <label style="margin-top: 10px;">Search by Date:&nbsp;</label>
              {{-- <input class="form-control form-input col-lg-5" id="example5"  type="text" name="search_time" placeholder="Time" required style=""> --}}
              <input class="form-control form-input col-lg-5" id="example"  type="text" name="search_date" placeholder="Select date" required>
              <button type="submit" class="btn btn-primary fa fa-search" style="margin-right:15px;"> &nbsp;</button>
            </div>
          </form>
        </div>
          <div class="col-sm-12 col-lg-6">
            <div class="d-flex  justify-content-md-end justify-content-center">
              <a href="{{ url('/admin/reg_user') }}" class="btn btn-primary " style="margin-bottom: 10px;"><i class="fa fa-plus"></i> Click to Add new Appointment</a>
            </div>  
          </div>
        </div>


        <div class="table-wrapper">
          <table id="productlistListTable" class="table display" width='100%'>
            <thead>
              <tr>
                <th >SI.NO</th>
                <th >FILE No.</th>
                <th >APPOINT. No.</th>
                <th >PATIENT NAME</th>
                <th >DOCTOR</th>
                <th >DATE</th>
                <th >TIME</th>
                <th >PAYMENT STATUS</th>
                <th >OTHER DETAILS</th>
                <th >STATUS</th>
                <th>ACTION</th>
              </tr>
            </thead>
            <tbody>
                @foreach($appointment as $key => $appoint)
                    <tr>
                      <td >{{ $key+1 }}</td>
                      <td >{{ $appoint->file_no }}</td>
                      <td >{{ $appoint->pk_int_appoint_id }}</td>
                      <td >{{ $appoint->appoint_patient }}</td>
                      <td >{{ $appoint->roll_name}}</td>
                      <td >{{ $appoint->appointment_date }}</td>
                      <td >{{ $appoint->appointment_time }}</td>
                      {{-- @if($appoint->trt_total_amount!=0)
                      <td >{{ $appoint->trt_total_amount }}</td>
                      @endif --}}
                      {{-- @if($appoint->trt_total_amount==0) --}}
                      @if($appoint->payment_status==0)
                      <td>
                            <a type="button" class="payment-edit btn btn-danger" style="color: white;" id="{{ $appoint->pk_int_appoint_id }}" data-toggle="modal" data-item-id="">PAYMENT</a>
                        </td>
                      @endif
                      @if($appoint->payment_status==1)
                      <td style="color: green;">PAYMENT DONE</td>
                      @endif
                     
                     @if($appoint->other_details_status==0)
                      <td>
                        <a href="{{URL::to('/admin/add_other_deatils/'.$appoint->pk_int_appoint_id)}}" type="button"  class="btn btn-primary waves-effect"><i class="fa fa-plus"></i> ADD</a>
                      </td>
                      @endif
                       @if($appoint->other_details_status==1)
                      <td>
                        <a href="{{URL::to('/admin/add_other_deatils_edit/'.$appoint->pk_int_appoint_id)}}" type="button"  class="btn btn-success waves-effect"><i class="fa fa-pencil"></i> EDIT</a>
                      </td>
                      @endif
                      
                      @if($appoint->complete_status==0)
                      <td style="color: red;">CUNSULTATION<br>NOT COMPLETED</td>
                      <td></td>
                      <td></td>
                      @endif
                      

                      @if($appoint->complete_status==1 && $appoint->prod_purch_status==1)
                        <td style="color: green;">CUNSULTATION<br>COMPLETED</td>
                        
                        <td>
                            <a type="button" class="details-edit btn btn-primary" style="color: white;" id="{{ $appoint->pk_int_appoint_id }}" data-toggle="modal" data-item-id=""><i class="fa fa-pencil"></i></a>
                            <a href="{{URL::to('/admin/delete-appo/'.$appoint->pk_int_appoint_id)}}" type="button"  class="del btn btn-danger waves-effect"><i class="fa fa-trash"></i></a><br>
                            {{-- @if($appoint->other_details_status==1) --}}
                            <a href="{{URL::to('/admin/view_full_report/'.$appoint->pk_int_appoint_id)}}" type="button"  class="btn btn-primary waves-effect" style="margin-top: 10px;"><i class="fa fa-eye"></i> VIEW </a>
                            {{-- @endif --}}
                        </td>
                      @endif
                      @if($appoint->complete_status==1 && $appoint->prod_purch_status==0)
                        <td style="color: green;">Details Added<br>

                        
                        <td>
                            <a type="button" class="details-edit btn btn-success" style="color: white;" id="{{ $appoint->pk_int_appoint_id }}" data-toggle="modal" data-item-id=""><i class="fa fa-pencil"></i></a>
                            <a href="{{URL::to('/admin/delete-appo/'.$appoint->pk_int_appoint_id)}}" type="button"  class="del btn btn-danger waves-effect"><i class="fa fa-trash"></i></a><br>
                            {{-- @if($appoint->other_details_status==1) --}}
                            <a href="{{URL::to('/admin/view_full_report/'.$appoint->pk_int_appoint_id)}}" type="button"  class="btn btn-primary waves-effect" style="margin-top: 10px;"><i class="fa fa-eye"></i> VIEW</a>
                            {{-- @endif --}}

                        </td>
                      @endif

                      @if($appoint->complete_status==2)
                        <td style="color: blue;">NOT BILLED</td>
                        <td>
                            <a href="{{ url('/admin/billing/'.$appoint->pk_int_appoint_id) }}" type="button" class="btn btn-primary" title="Billing" value="Edit">Open for Statement</a>
                        </td>
                        {{-- <td style="color: blue;">Go to Billing to get statement</td> --}}
                      @endif

                      @if($appoint->complete_status==4)
                        <td style="color: green;">BILLED</td>
                        <td style="color: blue;">Go to Billing to get statement</td>
                      @endif

                  </tr>
                  @endforeach


            </tbody>
          </table>
        </div>
      </div><!-- section-wrapper -->
    </div>

    @include('admin.includes.footer')
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
    <script src="{{asset('assets/backend/lib/datatables/js/jquery.dataTables.js')}}"></script>
    <script src="{{asset('assets/backend/lib/select2/js/select2.min.js')}}"></script>
    <script src="{{asset('assets/backend/js/darkbox.min.js')}}"></script>

    @foreach ($appointment as $g) 
<div id="edit-modal" class="modal fade modal-min" >
  <div class="modal-dialog modal-dialog-vertical-center" role="document">
   <div class="modal-content bd-0 tx-14">
    <div class="modal-header pd-x-20">
      <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold">PAYMENT REMARKS</h6>
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <form role="form" action="{{ url('/admin/add_payment_trt') }}" method="post">
      <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
      <div class="box-body col-md-12">
        {{-- <input type="hidden" name="prev_due_amount" id="prev_due_amount"> --}}
        <label>Total Amount</label>
        <div class="form-group"> 
          <input type="text" class="form-control" placeholder="Enter total amount" id="total_amount" name="total_amount">
          <input type="text" class="form-control" placeholder="Enter Payed amount" id="payed_bill_amount" name="payed_bill_amount" style="margin-top: 10px;">

          <input type="text" class="form-control" id="appo_id" name="appo_id" hidden>
        </div>
      </div>
{{--       <div class="box-body col-md-12">
        <label>Previous due amount</label>
        <div class="form-group"> 
          <input type="number" class="form-control" placeholder="Enter discount amount" id="prev_due_amount" name="prev_due_amount" readonly>
        </div>
      </div>
      <div class="box-body col-md-12">
        <label>Discount Amount</label>
        <div class="form-group"> 
          <input type="number" class="form-control" placeholder="Enter discount amount" id="Discount_amount" name="Discount_amount">
        </div>
      </div> --}}
      <div class="modal-footer">
        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-success">UPDATE PAYMENT</button>
      </div>
    </form>
  </div>
</div><!-- modal-dialog -->
</div><!-- modal -->
@endforeach

 @foreach ($appointment as $g) 
<div id="edit-modals" class="modal fade modal-min" >
  <div class="modal-dialog modal-dialog-vertical-center" role="document">
   <div class="modal-content bd-0 tx-14">
    <div class="modal-header pd-x-20">
      <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold">PURCHASE PRODUCT</h6>
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <form role="form" action="{{ url('/admin/update_appoit_details') }}" method="post">
      <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
     <div class="form-group" style="margin-top: 10px;">
                <div class="row">
                         <label class='col-md-4 control-label' style="margin-top:5px;text-align: right;">Patient Name:<span class="tx-danger">*</span>: </label>
                         <div class="col-md-4">
                            <input class="form-control" type="text" name="appoint_patient" placeholder="Enter number" id="appoint_patient" readonly>
                            <input class="form-control" type="text" name="appoint_id" id="appoint_id" hidden>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="row">
                    <label class='col-md-4 control-label' style="margin-top:5px;text-align: right;">Discription of Cunsultation<span class="tx-danger">*</span>: </label>
                        <div class="col-md-4">
                            <textarea class="form-control" type="text" id="consult_description" name="consult_description" placeholder="enter consultation details" style="width: 300px;height: 200px;"></textarea>
                        </div>
                </div>
            </div>
            <div class="form-group" id="add_one">
                <div class="row">
                    <label class='col-md-4 control-label' style="margin-top:5px;text-align: right;">Prescribe Medicine</label>
                    <div class="col-md-2">
                        <a type="button" class="edit-item btn btn-primary" style="color: white;" data-toggle="modal" data-item-id=""><i class="fa fa-plus"></i></a>
                    </div>
                </div>
            </div>
             <div class="form-group" id="product_one">
                <div class="row">
                         <label class='col-md-4 control-label' style="margin-top:5px;text-align: right;"> Medicine 1<span class="tx-danger">*</span>: </label>
                         <div class="col-md-2">
                             <select class="form-control select2" data-placeholder="Choose Doctor" name="product_one" id="product_one_one" style="width: 130px;">
                                    <option label="Choose Category"></option>
                                        @foreach($product as $key=>$cat) 
                                        <option value="{{$cat->product_id}}">{{$cat->prod_name}}</option>
                                        @endforeach
                                </select>
                    </div>
                    <div class="col-md-2">
                             <input class="form-control" id="product_one_count"  type="text" name="product_count_one" placeholder="Select Quantity" style="margin-left: 50px;">
                             @if ($errors->has('product_count_one'))
                                <span class="help-block">
                                    <strong class="error">{{ $errors->first('product_count_one') }}</strong>
                                </span>
                            @endif

                    </div>
                </div>
            </div>
            <div class="form-group" id="add_two">
                <div class="row">
                    <label class='col-md-4 control-label' style="margin-top:5px;text-align: right;">ADD More</label>
                    <div class="col-md-2">
                        <a type="button" class="edit-item_two btn btn-primary" style="color: white;" data-toggle="modal" data-item-id=""><i class="fa fa-plus"></i></a>
                    </div>
                </div>
            </div>
            
            <div class="form-group" id="product_two">
                <div class="row">
                         <label class='col-md-4 control-label' style="margin-top:5px;text-align: right;"> Medicine 2<span class="tx-danger">*</span>: </label>
                         <div class="col-md-2">
                             <select class="form-control select2" data-placeholder="Choose Doctor" name="product_two" id="product_two_two" style="width: 130px;">
                                    <option label="Choose Category"></option>
                                       @foreach($product as $key=>$cat) 
                                        <option value="{{$cat->product_id}}">{{$cat->prod_name}}</option>
                                        @endforeach
                                </select>
                    </div>
                    <div class="col-md-2">
                             <input class="form-control" id="product_two_count"  type="text" name="product_count_two" placeholder="Select Quantity" id="product_two_count" style="margin-left: 50px;">
                             @if ($errors->has('product_count_two'))
                                <span class="help-block">
                                    <strong class="error">{{ $errors->first('product_count_two') }}</strong>
                                </span>
                            @endif

                    </div>
                </div>
            </div>
            <div class="form-group" id="add_three">
                <div class="row">
                    <label class='col-md-4 control-label' style="margin-top:5px;text-align: right;">ADD More</label>
                    <div class="col-md-2">
                        <a type="button" class="edit-item_three btn btn-primary" style="color: white;" data-toggle="modal" data-item-id=""><i class="fa fa-plus"></i></a>
                    </div>
                </div>
            </div>
            <div class="form-group" id="product_three">
                <div class="row">
                         <label class='col-md-4 control-label' style="margin-top:5px;text-align: right;"> Medicine 3<span class="tx-danger">*</span>: </label>
                         <div class="col-md-2">
                             <select class="form-control select2" data-placeholder="Choose Doctor" name="product_three" id="product_three_three"style="width: 130px;">
                                    <option label="Choose Category"></option>
                                       @foreach($product as $key=>$cat) 
                                        <option value="{{$cat->product_id}}">{{$cat->prod_name}}</option>
                                        @endforeach
                                </select>
                    </div>
                    <div class="col-md-2">
                             <input class="form-control" id="product_three_count"  type="text" name="product_count_three" placeholder="Select Quantity" style="margin-left: 50px;">
                             @if ($errors->has('product_count_three'))
                                <span class="help-block">
                                    <strong class="error">{{ $errors->first('product_count_three') }}</strong>
                                </span>
                            @endif

                    </div>
                </div>
            </div>
            <div class="form-group" id="add_four">
                <div class="row">
                    <label class='col-md-4 control-label' style="margin-top:5px;text-align: right;">ADD More</label>
                    <div class="col-md-2">
                        <a type="button" class="edit-item_four btn btn-primary" style="color: white;" data-toggle="modal" data-item-id=""><i class="fa fa-plus"></i></a>
                    </div>
                </div>
            </div>
            <div class="form-group" id="product_four">
                <div class="row">
                         <label class='col-md-4 control-label' style="margin-top:5px;text-align: right;"> Medicine 4<span class="tx-danger">*</span>: </label>
                         <div class="col-md-2">
                             <select class="form-control select2" data-placeholder="Choose Doctor" name="product_four" id="product_four_four" style="width: 130px;">
                                    <option label="Choose Category"></option>
                                       @foreach($product as $key=>$cat) 
                                        <option value="{{$cat->product_id}}">{{$cat->prod_name}}</option>
                                        @endforeach
                                </select>
                    </div>
                    <div class="col-md-2">
                             <input class="form-control" id="product_four_count"  type="text" name="product_count_four" placeholder="Select Quantity" style="margin-left: 50px;">
                             @if ($errors->has('product_count_four'))
                                <span class="help-block">
                                    <strong class="error">{{ $errors->first('product_count_four') }}</strong>
                                </span>
                            @endif

                    </div>
                </div>
            </div>
            <div class="form-group" id="add_five">
                <div class="row">
                    <label class='col-md-4 control-label' style="margin-top:5px;text-align: right;">ADD More</label>
                    <div class="col-md-2">
                        <a type="button" class="edit-item_five btn btn-primary" style="color: white;" data-toggle="modal" data-item-id=""><i class="fa fa-plus"></i></a>
                    </div>
                </div>
            </div>
            <div class="form-group" id="product_five">
                <div class="row">
                         <label class='col-md-4 control-label' style="margin-top:5px;text-align: right;"> Medicine 5<span class="tx-danger">*</span>: </label>
                         <div class="col-md-2">
                             <select class="form-control select2" data-placeholder="Choose Doctor" name="product_five" id="product_five_five" style="width: 130px;">
                                    <option label="Choose Category"></option>
                                      @foreach($product as $key=>$cat) 
                                        <option value="{{$cat->product_id}}">{{$cat->prod_name}}</option>
                                        @endforeach
                                </select>

                    </div>
                    <div class="col-md-2">
                             <input class="form-control" id="product_five_count"  type="text" name="product_count_five" placeholder="Select Quantity" style="margin-left: 50px;">
                             @if ($errors->has('product_count_five'))
                                <span class="help-block">
                                    <strong class="error">{{ $errors->first('product_count_five') }}</strong>
                                </span>
                            @endif

                    </div>
                </div>
            </div>
            <div class="form-group" id="add_six">
                <div class="row">
                    <label class='col-md-4 control-label' style="margin-top:5px;text-align: right;">ADD More</label>
                    <div class="col-md-2">
                        <a type="button" class="edit-item_six btn btn-primary" style="color: white;" data-toggle="modal" data-item-id=""><i class="fa fa-plus"></i></a>
                    </div>
                </div>
            </div>
            <div class="form-group" id="product_six">
                <div class="row">
                         <label class='col-md-4 control-label' style="margin-top:5px;text-align: right;"> Medicine 6<span class="tx-danger">*</span>: </label>
                         <div class="col-md-2">
                             <select class="form-control select2" data-placeholder="Choose Doctor" name="product_six" id="product_six_six" style="width: 130px;">
                                    <option label="Choose Category"></option>
                                      @foreach($product as $key=>$cat) 
                                        <option value="{{$cat->product_id}}">{{$cat->prod_name}}</option>
                                        @endforeach
                                </select>

                    </div>
                    <div class="col-md-2">
                             <input class="form-control" id="product_six_count"  type="text" name="product_count_six" placeholder="Select Quantity" style="margin-left: 50px;">
                             @if ($errors->has('product_count_six'))
                                <span class="help-block">
                                    <strong class="error">{{ $errors->first('product_count_six') }}</strong>
                                </span>
                            @endif

                    </div>
                </div>
            </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save changes</button>
      </div>
    </form>
  </div>
</div><!-- modal-dialog -->
</div><!-- modal -->
@endforeach

@foreach ($appointment as $g) 
<div id="edit-trt-models" class="modal fade modal-min" >
  <div class="modal-dialog modal-dialog-vertical-center" role="document">
   <div class="modal-content bd-0 tx-14">
    <div class="modal-header pd-x-20">
      <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold">PURCHASE PRODUCT</h6>
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <form role="form" action="{{ url('/admin/add_trt_amount') }}" method="post">
      <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
      <input type="hidden" name="appointment_id" id="appointment_id">
      <div class="box-body col-md-12">
        <div class="form-group"> 
          <input type="text" class="form-control" placeholder="Enter amount" name="trt_amount" required>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save changes</button>
      </div>
    </form>
  </div>
</div><!-- modal-dialog -->
</div><!-- modal -->
@endforeach


    <script type="text/javascript">

     $(document).ready(function() {

       $(".payment-edit").click(function()
       {
         var id=$(this).attr('id');
         $(this).attr('data-target','#edit-modal');
             // alert(id);
             jQuery.ajax({
              type: "get",
              url: 'paymnt_remrks/'+id,
                //data: {smsid:id},
                success: function(res)
                {
                   // alert(res);
                   var d1= res.split(',');
                   $("#appo_id").val(d1[0]);
                 }
               });
           });
     });
    </script>

    <script type="text/javascript">

     $(document).ready(function() {

       $(".details-edit").click(function()
       {
         var id=$(this).attr('id');
         $(this).attr('data-target','#edit-modals');
             // alert(id);
             jQuery.ajax({
              type: "get",
              url: 'edit-details/'+id,
                //data: {smsid:id},
                success: function(res)
                {
                   // alert(res);
                   var d1= res.split(',');
                   $("#appoint_id").val(d1[14]);
                   $("#product_six_count").val(d1[13]);
                   $("#product_six_six").val(d1[12]);
                   $("#product_five_count").val(d1[11]);
                   $("#product_five_five").val(d1[10]);
                   $("#product_four_count").val(d1[9]);
                   $("#product_four_four").val(d1[8]);
                   $("#product_three_count").val(d1[7]);
                   $("#product_three_three").val(d1[6]);
                   $("#product_two_count").val(d1[5]);
                   $("#product_two_two").val(d1[4]);
                   $("#product_one_count").val(d1[3]);
                   $("#product_one_one").val(d1[2]);
                   $("#consult_description").val(d1[1]);
                   $("#appoint_patient").val(d1[0]);
                 }
               });
           });
     });
    </script>

     <script type="text/javascript">

     $(document).ready(function() {

       $(".edit-trt-model").click(function()
       {
         var id=$(this).attr('id');
         $(this).attr('data-target','#edit-trt-models');
             // alert(id);
             jQuery.ajax({
              type: "get",
              url: 'prod_purch_details/'+id,
                //data: {smsid:id},
                success: function(res)
                {
                   alert(res);
                   var d1= res.split(',');
                   $("#appointment_id").val(d1[0]);
                 }
               });
           });
     });
    </script>
    <script type="text/javascript">
    $(document).ready(function() {
                $('#product_one').hide();
                $('#product_two').hide();
                $('#product_three').hide();
                $('#product_four').hide();
                $('#product_five').hide();
                $('#product_six').hide();
                $('#add_two').hide();
                $('#add_three').hide();
                $('#add_four').hide();
                $('#add_five').hide();
                $('#add_six').hide();
   $(".edit-item").click(function()
   {
              $('#product_one').show();
              $('#add_one').hide();
              $('#add_two').show();
        })
});

   $(".edit-item_two").click(function()
   {
              $('#product_two').show();
              $('#add_two').hide();
              $('#add_three').show();
});
   $(".edit-item_three").click(function()
   {
              $('#product_three').show();
              $('#add_three').hide();
              $('#add_four').show();
});
   $(".edit-item_four").click(function()
   {
              $('#product_four').show();
              $('#add_four').hide();
              $('#add_five').show();
});
   $(".edit-item_five").click(function()
   {
              $('#product_five').show();
              $('#add_five').hide();
              $('#add_six').show();
});

$(".edit-item_six").click(function()
   {
              $('#product_six').show();
              $('#add_six').hide();
});
</script>



    <script type="text/javascript">
        $(function () {
      $('#example').datetimepicker({
        format: 'YYYY-MM-DD'
      });      
  });   
    </script>
<script type="text/javascript">
      $(document).on("click", ".del", function (event) {
    var result = confirm("Are you sure want to delete?");
    if (result) {
                  //Logic to delete the item
                  return true;
                }
                else
                {
                 return false;
               }
             });
  


      $('#productlistListTable').DataTable({
        scrollX:true,
        language: {
          searchPlaceholder: 'Search...',
          sSearch: '',
          lengthMenu: '_MENU_ items/page',
        },
        "ordering":false,
        "destroy": true,
        "processing": true,
      });
    </script>