@include('admin.includes.head')
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.21.0/moment.min.js" type="text/javascript"></script>

<style type="text/css">
     input[type=number]::-webkit-inner-spin-button, 
   input[type=number]::-webkit-outer-spin-button { 
  -webkit-appearance: none; 
  margin: 0; 
}
</style>

<?php
$message=Session::get('message');
$msg=explode("|",$message);

?>

@if ($message = Session::get('message'))
<div  style='z-index:9999; position:absolute;width:30%;right:0px; padding:10px 15px 0px 15px;' id='msg'>
   <div class="alert alert-{{$msg[0]}}" role="alert">
     <button type="button" class="close" data-dismiss="alert" aria-label="Close">
       <span aria-hidden="true">&times;</span>
   </button>
   <strong>{{ ucfirst($msg[0]) }}!</strong>{{ $msg[1] }}
</div><!-- alert -->
</div>
@endif 
    <div class="slim-mainpanel">
        <div class="container">
            <div class="slim-pageheader">
                <ol class="breadcrumb slim-breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ url('/admin/dashboard') }}">Home</a></li>
                    <li class="breadcrumb-item">New Consultation</li>
                    {{-- <li class="breadcrumb-item active" aria-current="page">register</li> --}}
                </ol>
                <h6 class="slim-pagetitle mb-2">Consultation</h6>
            </div><!-- slim-pageheader -->

            <div class="section-wrapper">
            <div class="row">
              <div class="col-sm-12 col-lg-6">
                <label class="section-title">New Consultation for {{$register->first_name}}</label>
                <p class="mg-b-20 mg-sm-b-40">The customers file number is {{$register->file_no}}</p></div>
                <div class="col-sm-12 col-lg-6">
                  <div class="d-flex  justify-content-md-end justify-content-center">
                    <a href="{{ url('/admin/reg_user') }}" class="btn btn-primary " style="margin-bottom: 10px;"><i class="icon ion-arrow-left-a"></i> BACk</a>
                  </div>  
                </div>
              </div>
            <div class="form-layout">
            <form id="basic-form" method="post" action="{{url('/admin/store_appointment/'.$register->pk_int_reg_id)}}" enctype="multipart/form-data">
                            {{csrf_field()}}
                
            <div class="form-group">
                    <div class="row">
                         <label class='col-md-4 control-label' style="margin-top:5px;text-align: right;"> Date<span class="tx-danger">*</span>: </label>
                         <div class="col-md-4">
                             <input class="form-control" id="example"  type="text" name="appointment_date" placeholder="Select date" required>
                             @if ($errors->has('appointment_date'))
                                <span class="help-block">
                                    <strong class="error">{{ $errors->first('appointment_date') }}</strong>
                                </span>
                            @endif

                    </div>
                </div>
            </div>
            <div class="form-group">
                    <div class="row">
                         <label class='col-md-4 control-label' style="margin-top:5px;text-align: right;"> Time <span class="tx-danger">*</span>: </label>
                         <div class="col-md-4">
                             <input class="form-control" id="example1"  type="text" name="appointment_time" placeholder="Select Time" required>
                             @if ($errors->has('appointment_time'))
                                <span class="help-block">
                                    <strong class="error">{{ $errors->first('appointment_time') }}</strong>
                                </span>
                            @endif

                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="row">
                         <label class='col-md-4 control-label' style="margin-top:5px;text-align: right;">Patient Name:<span class="tx-danger">*</span>: </label>
                         <div class="col-md-4">
                            <input class="form-control" type="text" name="appoint_patient" placeholder="Enter number" value="{{$register->first_name}} {{$register->middle_name}} {{$register->last_name}}" readonly>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="row">
                    <label class='col-md-4 control-label' style="margin-top:5px;text-align: right;">Discription of Cunsultation<span class="tx-danger">*</span>: </label>
                        <div class="col-md-4">
                            <textarea class="form-control" type="text" name="consult_description" placeholder="enter consultation details" style="width: 500px;height: 200px;"></textarea>
                        </div>
                </div>
            </div>
            <div class="form-group" id="add_one">
                <div class="row">
                    <label class='col-md-4 control-label' style="margin-top:5px;text-align: right;">Prescribe Medicine</label>
                    <div class="col-md-2">
                        <a type="button" class="edit-item btn btn-primary" style="color: white;" data-toggle="modal" data-item-id=""><i class="fa fa-plus"></i></a>
                    </div>
                </div>
            </div>
             <div class="form-group" id="product_one">
                <div class="row">
                         <label class='col-md-4 control-label' style="margin-top:5px;text-align: right;"> Medicine 1<span class="tx-danger">*</span>: </label>
                         <div class="col-md-2">
                             <select class="form-control select2" data-placeholder="Choose Doctor" name="product_one" >
                                    <option label="Choose Category"></option>
                                        @foreach($product as $key=>$cat) 
                                        <option value="{{$cat->product_id}}">{{$cat->prod_name}}</option>
                                        @endforeach
                                </select>
                    </div>
                    <div class="col-md-2">
                             <input class="form-control" id="example1"  type="text" name="product_count_one" placeholder="Select Quantity">
                             @if ($errors->has('product_count_one'))
                                <span class="help-block">
                                    <strong class="error">{{ $errors->first('product_count_one') }}</strong>
                                </span>
                            @endif

                    </div>
                </div>
            </div>
            <div class="form-group" id="add_two">
                <div class="row">
                    <label class='col-md-4 control-label' style="margin-top:5px;text-align: right;">ADD More</label>
                    <div class="col-md-2">
                        <a type="button" class="edit-item_two btn btn-primary" style="color: white;" data-toggle="modal" data-item-id=""><i class="fa fa-plus"></i></a>
                    </div>
                </div>
            </div>
            
            <div class="form-group" id="product_two">
                <div class="row">
                         <label class='col-md-4 control-label' style="margin-top:5px;text-align: right;"> Medicine 2<span class="tx-danger">*</span>: </label>
                         <div class="col-md-2">
                             <select class="form-control select2" data-placeholder="Choose Doctor" name="product_two">
                                    <option label="Choose Category"></option>
                                       @foreach($product as $key=>$cat) 
                                        <option value="{{$cat->product_id}}">{{$cat->prod_name}}</option>
                                        @endforeach
                                </select>
                    </div>
                    <div class="col-md-2">
                             <input class="form-control" id="example1"  type="text" name="product_count_two" placeholder="Select Quantity">
                             @if ($errors->has('product_count_two'))
                                <span class="help-block">
                                    <strong class="error">{{ $errors->first('product_count_two') }}</strong>
                                </span>
                            @endif

                    </div>
                </div>
            </div>
            <div class="form-group" id="add_three">
                <div class="row">
                    <label class='col-md-4 control-label' style="margin-top:5px;text-align: right;">ADD More</label>
                    <div class="col-md-2">
                        <a type="button" class="edit-item_three btn btn-primary" style="color: white;" data-toggle="modal" data-item-id=""><i class="fa fa-plus"></i></a>
                    </div>
                </div>
            </div>
            <div class="form-group" id="product_three">
                <div class="row">
                         <label class='col-md-4 control-label' style="margin-top:5px;text-align: right;"> Medicine 3<span class="tx-danger">*</span>: </label>
                         <div class="col-md-2">
                             <select class="form-control select2" data-placeholder="Choose Doctor" name="product_three" >
                                    <option label="Choose Category"></option>
                                       @foreach($product as $key=>$cat) 
                                        <option value="{{$cat->product_id}}">{{$cat->prod_name}}</option>
                                        @endforeach
                                </select>
                    </div>
                    <div class="col-md-2">
                             <input class="form-control" id="example1"  type="text" name="product_count_three" placeholder="Select Quantity">
                             @if ($errors->has('product_count_three'))
                                <span class="help-block">
                                    <strong class="error">{{ $errors->first('product_count_three') }}</strong>
                                </span>
                            @endif

                    </div>
                </div>
            </div>
            <div class="form-group" id="add_four">
                <div class="row">
                    <label class='col-md-4 control-label' style="margin-top:5px;text-align: right;">ADD More</label>
                    <div class="col-md-2">
                        <a type="button" class="edit-item_four btn btn-primary" style="color: white;" data-toggle="modal" data-item-id=""><i class="fa fa-plus"></i></a>
                    </div>
                </div>
            </div>
            <div class="form-group" id="product_four">
                <div class="row">
                         <label class='col-md-4 control-label' style="margin-top:5px;text-align: right;"> Medicine 4<span class="tx-danger">*</span>: </label>
                         <div class="col-md-2">
                             <select class="form-control select2" data-placeholder="Choose Doctor" name="product_four" >
                                    <option label="Choose Category"></option>
                                       @foreach($product as $key=>$cat) 
                                        <option value="{{$cat->product_id}}">{{$cat->prod_name}}</option>
                                        @endforeach
                                </select>
                    </div>
                    <div class="col-md-2">
                             <input class="form-control" id="example1"  type="text" name="product_count_four" placeholder="Select Quantity">
                             @if ($errors->has('product_count_four'))
                                <span class="help-block">
                                    <strong class="error">{{ $errors->first('product_count_four') }}</strong>
                                </span>
                            @endif

                    </div>
                </div>
            </div>
            <div class="form-group" id="add_five">
                <div class="row">
                    <label class='col-md-4 control-label' style="margin-top:5px;text-align: right;">ADD More</label>
                    <div class="col-md-2">
                        <a type="button" class="edit-item_five btn btn-primary" style="color: white;" data-toggle="modal" data-item-id=""><i class="fa fa-plus"></i></a>
                    </div>
                </div>
            </div>
            <div class="form-group" id="product_five">
                <div class="row">
                         <label class='col-md-4 control-label' style="margin-top:5px;text-align: right;"> Medicine 5<span class="tx-danger">*</span>: </label>
                         <div class="col-md-2">
                             <select class="form-control select2" data-placeholder="Choose Doctor" name="product_five">
                                    <option label="Choose Category"></option>
                                      @foreach($product as $key=>$cat) 
                                        <option value="{{$cat->product_id}}">{{$cat->prod_name}}</option>
                                        @endforeach
                                </select>

                    </div>
                    <div class="col-md-2">
                             <input class="form-control" id="example1"  type="text" name="product_count_five" placeholder="Select Quantity">
                             @if ($errors->has('product_count_five'))
                                <span class="help-block">
                                    <strong class="error">{{ $errors->first('product_count_five') }}</strong>
                                </span>
                            @endif

                    </div>
                </div>
            </div>
            <div class="form-group" id="add_six">
                <div class="row">
                    <label class='col-md-4 control-label' style="margin-top:5px;text-align: right;">ADD More</label>
                    <div class="col-md-2">
                        <a type="button" class="edit-item_six btn btn-primary" style="color: white;" data-toggle="modal" data-item-id=""><i class="fa fa-plus"></i></a>
                    </div>
                </div>
            </div>
            <div class="form-group" id="product_six">
                <div class="row">
                         <label class='col-md-4 control-label' style="margin-top:5px;text-align: right;"> Medicine 6<span class="tx-danger">*</span>: </label>
                         <div class="col-md-2">
                             <select class="form-control select2" data-placeholder="Choose Doctor" name="product_six">
                                    <option label="Choose Category"></option>
                                      @foreach($product as $key=>$cat) 
                                        <option value="{{$cat->product_id}}">{{$cat->prod_name}}</option>
                                        @endforeach
                                </select>

                    </div>
                    <div class="col-md-2">
                             <input class="form-control" id="example1"  type="text" name="product_count_six" placeholder="Select Quantity">
                             @if ($errors->has('product_count_six'))
                                <span class="help-block">
                                    <strong class="error">{{ $errors->first('product_count_six') }}</strong>
                                </span>
                            @endif

                    </div>
                </div>
            </div>

                <div class="form-group">
                <div class="row">
                         <label class='col-md-4 control-label' style="margin-top:5px;text-align: right;"></label>
                         <div class="col-md-4">
                           <button class="btn btn-primary bd-0">Submit</button>
                        <a href="{{ url('/admin/reg_user') }}" class="btn btn-secondary bd-0">Back</a>

                    </div>
                </div>
            </div>

            </form>    
        </div><!-- section-wrapper -->
    </div>
    </div>


@include('admin.includes.footer')
<script type="text/javascript">
    $(function () {
      $('#example').datetimepicker({
        format: 'YYYY-MM-DD'
      });      
  });
     $(function () {
      $('#example1').datetimepicker({
        format: 'h:mm a'
      });      
  });
    setTimeout(function(){ $("#msg").fadeOut(2000); }, 2000);
</script>

<script type="text/javascript">
    $(document).ready(function() {
                $('#product_one').hide();
                $('#product_two').hide();
                $('#product_three').hide();
                $('#product_four').hide();
                $('#product_five').hide();
                $('#product_six').hide();
                $('#add_two').hide();
                $('#add_three').hide();
                $('#add_four').hide();
                $('#add_five').hide();
                $('#add_six').hide();
   $(".edit-item").click(function()
   {
              $('#product_one').show();
              $('#add_one').hide();
              $('#add_two').show();
        })
});

   $(".edit-item_two").click(function()
   {
              $('#product_two').show();
              $('#add_two').hide();
              $('#add_three').show();
});
   $(".edit-item_three").click(function()
   {
              $('#product_three').show();
              $('#add_three').hide();
              $('#add_four').show();
});
   $(".edit-item_four").click(function()
   {
              $('#product_four').show();
              $('#add_four').hide();
              $('#add_five').show();
});
   $(".edit-item_five").click(function()
   {
              $('#product_five').show();
              $('#add_five').hide();
              $('#add_six').show();
});

$(".edit-item_six").click(function()
   {
              $('#product_six').show();
              $('#add_six').hide();
});
</script>
