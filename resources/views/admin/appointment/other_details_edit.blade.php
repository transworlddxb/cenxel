@include('admin.includes.head')
<link href="{{asset('assets/backend/css/darkbox.css')}}" rel="stylesheet">
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.21.0/moment.min.js" type="text/javascript"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css">
<style type="text/css">
.photogallery li{position: relative; overflow: hidden; min-height: 200px; background: #eee; border: 2px solid #fff;}
.photogallery li:nth-child(odd){background: #bbb;}
.photogallery li img{position: absolute; top: -9999px; right: -9999px; bottom: -9999px; left: -9999px; margin: auto; max-width: none; /*max-height: 100%;*/ width: 100%; height: auto;}
</style>

<div class="slim-mainpanel">
  <div class="container">
    <div class="slim-pageheader">
      <ol class="breadcrumb slim-breadcrumb">
        <li class="breadcrumb-item"><a href="#">Home</a></li>
        <li class="breadcrumb-item">Edit other Details</li>
        {{-- <li class="breadcrumb-item active" aria-current="page">Form Wizards</li> --}}
      </ol>
      <h6 class="slim-pagetitle mb-2">Edit Other Details</h6>
    </div><!-- slim-pageheader -->    


    <div class="section-wrapper mg-t-20">
        <div class="row">
              <div class="col-sm-12 col-lg-6">
                <label class="section-title">Edit other details of patient : <span style="color: #1570C7">{{$appointment->appoint_patient}}</span></label>
                <p class="mg-b-20 mg-sm-b-40"><span style="color: green"><b>File Number: </b></span><span style="color: blue">{{ $appointment->file_no }} .</span></p></div>
                <div class="col-sm-12 col-lg-6">
                  <div class="d-flex  justify-content-md-end justify-content-center">
                    <a href="{{ url('/admin/today_appointment') }}" class="btn btn-primary " style="margin-bottom: 10px;"><i class="icon ion-arrow-left-a"></i> BACk</a>
                  </div>  
                </div>
              </div>
        <form id="basic-form" method="post" action="{{url('/admin/update_other_deatails/'.$appointment->pk_int_appoint_id)}}" enctype="multipart/form-data">
                            {{csrf_field()}}

          <div class="form-layout form-layout-2">
            <div class="row no-gutters">
              <div class="col-md-4">
                <div class="form-group">
                  <label class="form-control-label">Name of Patient: <span class="tx-danger">*</span></label>
                  <input class="form-control" type="text" name="patient_name" value="{{$appointment->appoint_patient}}" readonly>
                </div>
              </div><!-- col-4 -->
              <div class="col-md-4 mg-t--1 mg-md-t-0">
                <div class="form-group mg-md-l--1">
                  <label class="form-control-label mg-b-0-force">Height / weight check: <span class="tx-danger">*</span></label>
                  <select id="select2-a" class="form-control"  value="{{$appointment->height_weight_check}}" name="height_weight_check">
                    <option></option>
                    <option value="1" @if($appointment->height_weight_check=='1'){{  'selected' }} @endif>Yes</option>
                    <option value="0" @if($appointment->height_weight_check=='0'){{  'selected' }} @endif>No</option>
                  </select>
                  <hr>
                  <textarea class="form-control" type="text" name="height_weight_check_descrip" placeholder="Enter description">{{$appointment->height_weight_check_descrip}}</textarea>
                </div>
              </div><!-- col-4 -->
              <div class="col-md-4 mg-t--1 mg-md-t-0">
                <div class="form-group mg-md-l--1">
                  <label class="form-control-label">Blood pressure check: <span class="tx-danger">*</span></label>
                  <select id="select2-a" class="form-control" name="blood_pressure_check">
                    <option></option>
                    <option value="1" @if($appointment->blood_pressure_check=='1'){{  'selected' }} @endif >Yes</option>
                    <option value="0" @if($appointment->blood_pressure_check=='0'){{  'selected' }} @endif >No</option>
                  </select>
                  <hr>
                  <textarea class="form-control" type="text" name="blood_pressure_check_descrip" placeholder="Enter description">{{$appointment->blood_pressure_check_descrip}}</textarea>
                </div>
              </div><!-- col-4 -->
              <div class="col-md-4">
                <div class="form-group bd-t-0-force">
                  <label class="form-control-label">Cholesterol level check: <span class="tx-danger">*</span></label>
                  <select id="select2-a" class="form-control"  value="{{$appointment->cholestrol_check}}" name="cholestrol_check">
                    <option></option>
                    <option value="1" @if($appointment->cholestrol_check=='1'){{  'selected' }} @endif>Yes</option>
                    <option value="0" @if($appointment->cholestrol_check=='0'){{  'selected' }} @endif>No</option>
                  </select>
                  <hr>
                  <textarea class="form-control" type="text"  value="{{$appointment->cholestrol_check_descrip}}" name="cholestrol_check_descrip" placeholder="Enter description"></textarea>
                </div>
              </div><!-- col-4 -->
               <div class="col-md-4">
                <div class="form-group bd-t-0-force">
                  <label class="form-control-label">Blood sugar test: <span class="tx-danger">*</span></label>
                  <select id="select2-a" class="form-control"  value="{{$appointment->blood_sugar_test}}" name="blood_sugar_test">
                    <option></option>
                    <option value="1" @if($appointment->blood_sugar_test=='1'){{  'selected' }} @endif>Yes</option>
                    <option value="0" @if($appointment->blood_sugar_test=='0'){{  'selected' }} @endif>No</option>
                  </select>
                  <hr>
                  <textarea class="form-control" type="text" name="blood_sugar_test_descrip" placeholder="Enter description">{{$appointment->blood_sugar_test_descrip}}</textarea>
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group mg-md-l--1 bd-t-0-force">
                  <label class="form-control-label mg-b-0-force">Throat check: <span class="tx-danger">*</span></label>
                  <select id="select2-a" class="form-control"  value="{{$appointment->throat_check}}" name="throat_check">
                    <option></option>
                    <option value="1" @if($appointment->throat_check=='1'){{  'selected' }} @endif>Yes</option>
                    <option value="0" @if($appointment->throat_check=='0'){{  'selected' }} @endif>No</option>
                  </select>
                  <hr>
                  <textarea class="form-control" type="text" name="throat_check_descrip" placeholder="Enter description">{{$appointment->throat_check_descrip}}</textarea>
                </div>
              </div><!-- col-4 -->
              <div class="col-md-4">
                <div class="form-group bd-t-0-force">
                  <label class="form-control-label">Ear check: <span class="tx-danger">*</span></label>
                  <select id="select2-a" class="form-control"  value="{{$appointment->ear_check}}" name="ear_check">
                    <option></option>
                    <option value="1" @if($appointment->ear_check=='1'){{  'selected' }} @endif>Yes</option>
                    <option value="0" @if($appointment->ear_check=='0'){{  'selected' }} @endif>No</option>
                  </select>
                  <hr>
                  <textarea class="form-control" type="text" name="ear_check_descrip" placeholder="Enter description">{{$appointment->ear_check_descrip}}</textarea>
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group bd-t-0-force">
                  <label class="form-control-label">Eye check: <span class="tx-danger">*</span></label>
                  <select id="select2-a" class="form-control"  value="{{$appointment->eye_check}}" name="eye_check">
                    <option></option>
                    <option value="1" @if($appointment->eye_check=='1'){{  'selected' }} @endif>Yes</option>
                    <option value="0" @if($appointment->eye_check=='0'){{  'selected' }} @endif>No</option>
                  </select>
                  <hr>
                  <textarea class="form-control" type="text" name="eye_check_descrip" placeholder="Enter description">{{$appointment->eye_check_descrip}}</textarea>
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group bd-t-0-force">
                  <label class="form-control-label">Electrocardiogram (for those at a higher risk of heart disease): <span class="tx-danger">*</span></label>
                  <select id="select2-a" class="form-control"  value="{{$appointment->electrocardiogram_check}}" name="electrocardiogram_check">
                    <option></option>
                    <option value="1" @if($appointment->electrocardiogram_check=='1'){{  'selected' }} @endif>Yes</option>
                    <option value="0" @if($appointment->electrocardiogram_check=='0'){{  'selected' }} @endif>No</option>
                  </select>
                  <hr>
                  <textarea class="form-control" type="text" name="electrocardiogram_check_descrip" placeholder="Enter description">{{$appointment->electrocardiogram_check_descrip}}</textarea>
                </div>
              </div>
            </div><!-- row -->
            <div class="form-layout-footer bd pd-20 bd-t-0">
              <button class="btn btn-primary bd-0">SAVE</button>
              <a type="button" href="{{ url('/admin/today_appointment') }}" class="btn btn-secondary">BACk</a>
              {{-- <button class="btn btn-secondary bd-0" >Cancel</button> --}}
            </div><!-- form-group -->
          </div><!-- form-layout -->
        </form>
        </div><!-- section-wrapper -->
    </div>

    @include('admin.includes.footer')
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
    <script src="{{asset('assets/backend/lib/datatables/js/jquery.dataTables.js')}}"></script>
    <script src="{{asset('assets/backend/lib/select2/js/select2.min.js')}}"></script>
    <script src="{{asset('assets/backend/js/darkbox.min.js')}}"></script>

    