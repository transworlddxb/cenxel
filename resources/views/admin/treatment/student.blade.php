@include('admin.includes.head')
<link href="{{asset('assets/backend/css/darkbox.css')}}" rel="stylesheet">
<style type="text/css">
.photogallery li{position: relative; overflow: hidden; min-height: 200px; background: #eee; border: 2px solid #fff;}
.photogallery li:nth-child(odd){background: #bbb;}
.photogallery li img{position: absolute; top: -9999px; right: -9999px; bottom: -9999px; left: -9999px; margin: auto; max-width: none; /*max-height: 100%;*/ width: 100%; height: auto;}
</style>

<div class="slim-mainpanel">
  <div class="container">
    <div class="slim-pageheader">
      <ol class="breadcrumb slim-breadcrumb">
        <li class="breadcrumb-item"><a href="#">Home</a></li>
        <li class="breadcrumb-item">Products</li>
        {{-- <li class="breadcrumb-item active" aria-current="page">Form Wizards</li> --}}
      </ol>
      <h6 class="slim-pagetitle mb-2">Products</h6>
    </div><!-- slim-pageheader -->    


    <div class="section-wrapper">

      <div class="row">
        <div class="col-sm-12 col-lg-6">
          <label class="section-title"> Details</label>
          <p class="mg-b-20 mg-sm-b-40">List of products you added</p></div>
          <div class="col-sm-12 col-lg-6">
            <div class="d-flex  justify-content-md-end justify-content-center">
              <a href="{{ url('/admin/treatment') }}" class="btn btn-primary" style="margin-bottom: 10px;"><i class="fa fa-plus"></i> Add new</a>
            </div>
          </div>
        </div>


        <div class="table-wrapper">
          <table id="productlistListTable" class="table display" width='100%'>
            <thead>
              <tr>
                <th class="wd-15p">SI.NO</th>
                <th class="wd-15p">TITLE</th>
                <th class="wd-20p">PRICE</th>
                <th class="wd-20p">CATEGORY</th>
                {{-- <th class="wd-10p"></th> --}}
              </tr>
            </thead>
            <tbody>
               @foreach($stud_list as $key => $treatment)
                    <tr>
                      <td>{{ $key+1 }}</td>
                      <td >{{ $treatment->treatment_name }}</td>
                      <td >{{ $treatment->treatment_price }}</td>
                      <td >{{ $treatment->class_name}}</td>
                      {{-- <td style="width: 200px;"><center> --}}

                        {{-- <a href="{{URL::to('/admin/delete-gst/'.$treatment->pk_treatment_id)}}" type="button"  class="del btn btn-light waves-effect"><i class="fa fa-trash"></i></a> --}}
                        {{-- <button type="button" class="btn btn-success edit-item" id="{{ $g->pk_int_role_id }}" data-toggle="modal" data-item-id="">Edit</button> --}}

                        {{-- <a type="button" class="edit-item btn btn-primary" style="color: white;" id="{{ $treatment->pk_treatment_id }}" data-toggle="modal" data-item-id=""><i class="fa fa-pencil"></i></a> --}}
                      {{-- </center> --}}
                    {{-- </td> --}}
                  </tr>
                  @endforeach 
                normal_list
            </tbody>
          </table>
        </div>
      </div><!-- section-wrapper -->
    </div>

    @include('admin.includes.footer')
    </script>
    <script src="{{asset('assets/backend/lib/datatables/js/jquery.dataTables.js')}}"></script>
    <script src="{{asset('assets/backend/lib/select2/js/select2.min.js')}}"></script>
    <script src="{{asset('assets/backend/js/darkbox.min.js')}}"></script>

    <script type="text/javascript">
      $(document).on("click", ".del", function (event) {
    var result = confirm("Are you sure want to delete?");
    if (result) {
                  //Logic to delete the item
                  return true;
                }
                else
                {
                 return false;
               }
             });
  


      $('#productlistListTable').DataTable({
        scrollX:true,
        language: {
          searchPlaceholder: 'Search...',
          sSearch: '',
          lengthMenu: '_MENU_ items/page',
        },
        "ordering":false,
        "destroy": true,
        "processing": true,
      });
    </script>
