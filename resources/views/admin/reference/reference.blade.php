@include('admin.includes.head')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css">

<style type="text/css">
input[type=number]::-webkit-inner-spin-button, 
input[type=number]::-webkit-outer-spin-button { 
  -webkit-appearance: none; 
  margin: 0; 
}
</style>

<?php
$message=Session::get('message');
$msg=explode("|",$message);
?>

@if ($message = Session::get('message'))
<div  style='z-index:9999; position:absolute;width:30%;right:0px; padding:10px 15px 0px 15px;' id='msg'>
 <div class="alert alert-{{$msg[0]}}" role="alert">
   <button type="button" class="close" data-dismiss="alert" aria-label="Close">
     <span aria-hidden="true">&times;</span>
   </button>
   <strong>{{ ucfirst($msg[0]) }}!</strong>{{ $msg[1] }}
 </div><!-- alert -->
</div>
@endif


<div class="slim-mainpanel">
  <div class="container">
    <div class="slim-pageheader">
      <ol class="breadcrumb slim-breadcrumb">
        <li class="breadcrumb-item"><a href="{{ url('/admin/dashboard') }}">Home</a></li>
        <li class="breadcrumb-item">Add Reference Person Data </li>
        {{-- <li class="breadcrumb-item active" aria-current="page">Form Wizards</li> --}}
      </ol>
      <h6 class="slim-pagetitle">Add Reference Person Data</h6>
    </div><!-- slim-pageheader -->



    <div class="section-wrapper mg-t-20">
      <section>
        <div class="row">
          <div class="col-md-4">
            <label class="section-title">Add new</label>
            <p class="mg-b-20 mg-sm-b-40">add Reference Person Data</p>
            <div class="card-box">
              <form class="form-horizontal" method="post" action="{{url('/admin/add_ref_person')}}" enctype="multipart/form-data">
                {{csrf_field()}}
                
                <div>
                  <input id="autocomplete-ajax" class="form-control" placeholder="Enter name of Reference Person" required name="reference_person_name" >
                  @if ($errors->has('reference_person_name'))
                  <span class="help-block">
                    <p class="error">{{ $errors->first('reference_person_name') }}</p>
                    <strong style="color:red">{{ $errors->first('reference_person_name') }}</strong>
                  </span>
                  @endif
                </div>

                <div style="margin-top: 10px">
                  <input id="autocomplete-ajax" class="form-control" placeholder="Enter Description" required name="ref_description" >
                  @if ($errors->has('ref_description'))
                  <span class="help-block">
                    <p class="error">{{ $errors->first('ref_description') }}</p>
                    <strong style="color:red">{{ $errors->first('ref_description') }}</strong>
                  </span>
                  @endif
                </div>

                <button style="margin-top: 20px; background-color: #1B84E7; color: white; margin-left: 100px; margin-bottom: 10px;" class="btn btn-custom waves-light waves-effect" type="submit">Submit</button>
              </form>
            </div>
          </div>
          <div class="col-md-8">
            <div class="section-wrapper">
              <label class="section-title">List of Reference Person Data</label>
              <hr style="margin:5px 0px 12px 0px;background-color:#dee2e6;">
              <div class="table-wrapper">
                <table id="roleListTable" class="table display" width='100%'>
                  <thead>
                    <tr>
                      <tr>
                        <th>SI.NO</th>
                        <th>Person Name</th>
                        <th>Description</th>
                        <th><center>ACTION</center></th>
                      </tr>
                    </tr>
                  </thead>
                  <tbody>
                       @foreach($reference as $key => $ref)
                        <tr>
                          <td>{{ $key+1 }}</td>
                          <td>{{ $ref->reference_person_name }}</td>
                          <td>{{ $ref->ref_description }}</td>
                          
                          <td style="width: 200px;"><center>

                            <a href="{{URL::to('/admin/delete_reference/'.$ref->refernce_person_id)}}" type="button"  class="del btn btn-light waves-effect"><i class="fa fa-trash"></i></a>
                            {{-- <button type="button" class="btn btn-success edit-item" id="{{ $g->pk_int_role_id }}" data-toggle="modal" data-item-id="">Edit</button> --}}

                            <a type="button" class="edit-item btn btn-primary" style="color: white;" id="{{ $ref->refernce_person_id }}" data-toggle="modal" data-item-id=""><i class="fa fa-pencil"></i></a>
                          </center>
                        </td>
                      </tr>
                      @endforeach
                </tbody>
              </table>
            </div><!-- table-wrapper -->
          </div><!-- section-wrapper -->    
        </div>
      </div>
    </section>
  </div>


</div><!-- container -->

@include('admin.includes.footer')

<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
<script src="{{asset('assets/backend/lib/datatables/js/jquery.dataTables.js')}}"></script>
<script src="{{asset('assets/backend/lib/select2/js/select2.min.js')}}"></script>

{{-- model --}}
{{-- @foreach ($doctor as $g)  --}}
<div id="edit-modal" class="modal fade modal-min" >
  <div class="modal-dialog modal-dialog-vertical-center" role="document">
   <div class="modal-content bd-0 tx-14">
    <div class="modal-header pd-x-20">
      <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold">Update</h6>
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <form role="form" action="{{ url('/admin/update_reference') }}" method="post">
      <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
      <div class="box-body col-md-12">
        <input type="hidden" name="refernce_person_id" id="refernce_person_id">
        <label>Person Name</label>
        <div class="form-group"> 
          <input type="text" class="form-control" id="reference_person_name" name="reference_person_name">
        </div>
        
        <label>Description</label>
        <div class="form-group"> 
          <input type="text" class="form-control" id="ref_description" name="ref_description">
        </div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save changes</button>
      </div>
    </form>
  </div>
</div><!-- modal-dialog -->
</div><!-- modal -->
{{-- @endforeach --}}

<script type="text/javascript">

 $(document).ready(function() {

   $(".edit-item").click(function()
   {
     var id=$(this).attr('id');
     $(this).attr('data-target','#edit-modal');
         // alert(id);
         jQuery.ajax({
          type: "get",
          url: 'edit_reference/'+id,
            //data: {smsid:id},
            success: function(res)
            {
               // alert(res);
               var d1= res.split(',');
               $("#ref_description").val(d1[2]);
               $("#reference_person_name").val(d1[1]);
               $("#refernce_person_id").val(d1[0]);
             }
           });
       });
 });
</script>

<script type="text/javascript">

  setTimeout(function(){ $("#msg").fadeOut(2000); }, 2000);


  $(document).on("click", ".del", function (event) {
    var result = confirm("Are you sure want to delete?");
    if (result) {
                  //Logic to delete the item
                  return true;
                }
                else
                {
                 return false;
               }
             });
  
            $('#roleListTable').DataTable({
              scrollX:true,
              language: {
                searchPlaceholder: 'Search...',
                sSearch: '',
                lengthMenu: '_MENU_ items/page',
              },
              "ordering":false,
              "destroy": true,
              "processing": true,
            });
//   $(document).ready(function(){
//     $("#successMessage").delay(1700).slideUp(300);
// });
setTimeout(function(){ $("#msg").fadeOut(2000); }, 2000);
</script>
