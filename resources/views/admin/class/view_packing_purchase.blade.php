@include('admin.includes.head')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css">

<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.21.0/moment.min.js" type="text/javascript"></script>
<?php
$message=Session::get('message');
$msg=explode("|",$message);
?>

@if ($message = Session::get('message'))
<div  style='z-index:9999; position:absolute;width:30%;right:0px; padding:10px 15px 0px 15px;' id='msg'>
	<div class="alert alert-{{$msg[0]}}" role="alert">
		<button type="button" class="close" data-dismiss="alert" aria-label="Close">
			<span aria-hidden="true">&times;</span>
		</button>
		<strong>{{ ucfirst($msg[0]) }}!</strong>{{ $msg[1] }}
	</div><!-- alert -->
</div>
@endif


<div class="slim-mainpanel">
	<div class="container">
		<div class="slim-pageheader">
			<ol class="breadcrumb slim-breadcrumb">
				<li class="breadcrumb-item"><a href="{{ url('/admin/dashboard') }}">Home</a></li>
				<li class="breadcrumb-item"> Purchase</li>
				{{-- <li class="breadcrumb-item active" aria-current="page">List Packing items</li> --}}
			</ol>
			<h6 class="slim-pagetitle">Packing items</h6>
		</div><!-- slim-pageheader -->
		
		
		<div class="section-wrapper">
			<div class="row">
				<div class="col-sm-12 col-lg-6">
					<label class="section-title">Details</label>
					<p class="mg-b-20 mg-sm-b-40">List of packing items you added</p></div>
					<div class="col-sm-12 col-lg-6">
						<div class="d-flex  justify-content-md-end justify-content-center">		       	     	

							<a href="{{ url('/admin/packing_purchase') }}" class="btn btn-primary" style="margin-bottom: 10px;">Add new</a>
						</div>
					</div>
				</div>

				<div class="table-wrapper">
					<table id="roleListTable" class="table display" width='100%'>
						<thead>
							<tr>
								<th>SI.NO</th>
								<th>CATEGORY</th>
								<th>DATE</th>
								<th>BILL NO:</th>
								<th>ITEMS</th>
								<th>QUANTITY</th>
								<th>UNIT</th>
								<th>BILL AMOUNT</th>
								<th>GST %</th>
								<th>TAX AMOUNT</th>
								<th>NARRATION</th>
								<th><center>ACTION</center></th>
							</tr>
						</thead>
						<tbody>

							@foreach($carry_items as $key => $carry_item)
							<tr>
								<td>{{ $key+1 }}</td>
								<td>{{ $carry_item->carry_items }}</td>	                	
								<td>{{ $carry_item->botl_date }}</td>
								<td>{{ $carry_item->botl_bill }}</td>
								<td>{{ $carry_item->botl_item_name }}</td>
								<td>{{ $carry_item->botl_quant }}</td>
								<td>{{ $carry_item->carry_units }}</td>
								<td>{{ $carry_item->botl_bill_amount }}</td>
								<td>{{ $carry_item->botl_tax}}</td>
								<td>{{ $carry_item->botl_tax_amt }}</td>
								<td>{{ $carry_item->botl_narration}}</td>
								<td>
									<div class="btn-group mb-2">
										<a href="{{URL::to('/admin/delete_packing_purchase/'.$carry_item->pk_int_botl_id)}}"  type="button"  class="deleteImg btn btn-light waves-effect"><i class="fa fa-trash"></i></a>

										<a href="{{ url('/admin/edit_packing_purchase/'.$carry_item->pk_int_botl_id) }}" type="button" class="btn btn-primary" title="Edit" value="Edit"><i class="fa fa-pencil"></i></a>
									</div>
								</td>
								@endforeach
							</tr>

						</tbody>
					</table>
				</div><!-- table-wrapper -->
			</div><!-- section-wrapper -->
		</div>


		@include('admin.includes.footer')


		<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
		<script src="{{asset('assets/backend/lib/datatables/js/jquery.dataTables.js')}}"></script>
		<script src="{{asset('assets/backend/lib/select2/js/select2.min.js')}}"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/alertify.js/0.5.0/alertify.min.js"></script>
		<script type="text/javascript">

			$(document).on("click", ".deleteImg", function (event) {
				var result = confirm("Are you sure want to delete?");
				if (result) {
			            //Logic to delete the item
			            return true;
			        }
			        else
			        {
			        	return false;
			        }
			    });



			
			$('#roleListTable').DataTable({
				scrollX:true,
				language: {
					searchPlaceholder: 'Search...',
					sSearch: '',
					lengthMenu: '_MENU_ items/page',
				},
				"ordering":false,
				"destroy": true,
				"processing": true,
			});

		</script>


