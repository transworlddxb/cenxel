@include('admin.includes.head')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css">

<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.21.0/moment.min.js" type="text/javascript"></script>
<?php
$message=Session::get('message');
$msg=explode("|",$message);
?>

@if ($message = Session::get('message'))
<div  style='z-index:9999; position:absolute;width:30%;right:0px; padding:10px 15px 0px 15px;' id='msg'>
    <div class="alert alert-{{$msg[0]}}" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        <p>{{ ucfirst($msg[0]) }}!</p>{{ $msg[1] }}
    </div><!-- alert -->
</div>
@endif


<div class="slim-mainpanel">
    <div class="container">
        <div class="slim-pageheader">
            <ol class="breadcrumb slim-breadcrumb">
                <li class="breadcrumb-item"><a href="{{ url('/admin/dashboard') }}">Home</a></li>
                <li class="breadcrumb-item"><a href="{{url('/admin/view_packing_purchase')}}"> Purchase</a></li>
                <li class="breadcrumb-item active" aria-current="page">Edit</li>
            </ol>
            <h6 class="slim-pagetitle">Packing items</h6>
        </div><!-- slim-pageheader -->

        <div class="section-wrapper">
            <label class="section-title">Packing items</label>
            <p class="mg-b-20 mg-sm-b-40">Please complete the form below,Fields marked with a <span class="tx-danger">*</span> are mandatory.</p>

            <div class="form-layout">
                <form class="form-horizontal" method="post" action="{{url('/admin/update_packing_purchase/'.$carry_item->pk_int_botl_id) }})}}" enctype="multipart/form-data">
                    @csrf
                    <div class="row mg-b-25">
                        <div class="col-lg-4">
                          <div class="form-group">
                            <label class="form-control-label">Date: <span class="tx-danger">*</span></label>
                            <input type="text" class="form-control" id="example" name="botl_date" placeholder="Date" value="{{ $carry_item->botl_date }}">
                            @if ($errors->has('botl_date'))
                            <span class="help-block">
                                <p class="error">{{ $errors->first('botl_date') }}</p>
                            </span>
                            @endif
                        </div>
                    </div><!-- col-4 -->
                    <div class="col-lg-4">
                      <div class="form-group">
                        <label class="form-control-label">Bill no: <span class="tx-danger">*</span></label>
                        <input type="text" class="form-control" name="botl_bill" placeholder="Bill number" value="{{ $carry_item->botl_bill }}">
                        @if ($errors->has('botl_bill'))
                        <span class="help-block">
                            <p class="error">{{ $errors->first('botl_bill') }}</p>
                        </span>
                        @endif
                    </div>
                </div><!-- col-4 -->
                <div class="col-lg-4">
                  <div class="form-group">
                    <label class="form-control-label">Bill amount: <span class="tx-danger">*</span></label>
                    <input type="text" class="form-control" name="botl_bill_amount" placeholder="Enter total amount" value="{{ $carry_item->botl_bill_amount }}">
                    @if ($errors->has('botl_bill_amount'))
                    <span class="help-block">
                        <p class="error">{{ $errors->first('botl_bill_amount') }}</p>
                    </span>
                    @endif
                </div>
            </div><!-- col-4 -->
            <div class="col-lg-6">
              <div class="form-group mg-b-10-force">
                <label class="form-control-label">Item name: <span class="tx-danger">*</span></label>
                <input type="text" class="form-control" name="botl_item_name" placeholder="Enter name of item" value="{{ $carry_item->botl_item_name }}">
                @if ($errors->has('botl_item_name'))
                <span class="help-block">
                    <p class="error">{{ $errors->first('botl_item_name') }}</p>
                </span>
                @endif
            </div>
        </div><!-- col-8 -->
        <div class="col-lg-6">
          <div class="form-group mg-b-10-force">
            <label class="form-control-label">Item category: <span class="tx-danger">*</span></label>
            <select class="form-control select2"  data-placeholder="Choose category" name="botl_item_fk" required>                                  
                <option label="Choose category"></option>                                  

                @foreach($category_item as $key=>$category)

                <option value="{{$category->carry_items_id}}" @if($carry_item->botl_item_fk==$category->carry_items_id){{  'selected' }} @endif >{{$category->carry_items}}</option>
                @if ($errors->has('botl_unit_fk'))
                <span class="help-block">
                    <p class="error">{{ $errors->first('botl_unit_fk') }}</p>
                </span>
                @endif

                @endforeach

            </select>

        </div>
    </div><!-- col-4 -->
    <div class="col-lg-6">
      <div class="form-group mg-b-10-force">
        <label class="form-control-label">Quantity: <span class="tx-danger">*</span></label>
        <input type="text" class="form-control" name="botl_quant" placeholder="Enter quantity/count" value="{{ $carry_item->botl_quant }}" >
        @if ($errors->has('botl_quant'))
        <span class="help-block">
            <p class="error">{{ $errors->first('botl_quant') }}</p>
        </span>
        @endif
    </div>
</div><!-- col-8 -->
<div class="col-lg-6">
  <div class="form-group mg-b-10-force">
    <label class="form-control-label">Unit: <span class="tx-danger">*</span></label>
    <select class="form-control select2" data-placeholder="Choose Unit" name="botl_unit_fk" required>                                   
        <option label="Choose unit"></option>

        @foreach($units as $key=>$unit)

        <option value="{{$unit->carry_unit_id}}" @if($carry_item->botl_unit_fk==$unit->carry_unit_id){{  'selected' }} @endif>{{$unit->carry_units}}</option>
        @if ($errors->has('botl_item_fk'))
        <span class="help-block">
            <p class="error">{{ $errors->first('botl_item_fk') }}</p>
        </span>
        @endif      

        @endforeach


    </select>
</div>
</div><!-- col-4 -->

<div class="col-lg-6">
  <div class="form-group mg-b-10-force">
    <label class="form-control-label">Tax (%): <span class="tx-danger">*</span></label>
    {{-- <input type="text" class="form-control" name="botl_tax" placeholder="Enter quantity/count"> --}}
    <select class="form-control select2"  data-placeholder="Choose tax (%)" name="botl_tax" required>                                  
        <option label="Choose tax %"></option> 

        @foreach($gst as $key=>$g) 

        <option value="{{$g->pk_int_gst_id}}" @if($carry_item->botl_tax==$g->pk_int_gst_id){{  'selected' }} @endif>{{$g->gst_value}}</option>
        
        @if ($errors->has('botl_tax'))
        <span class="help-block">
            <p class="error">{{ $errors->first('botl_tax') }}</p>
        </span>
        @endif

        @endforeach

    </select>

</div>
</div><!-- col-4 -->
<div class="col-lg-6">
  <div class="form-group mg-b-10-force">
    <label class="form-control-label">Tax amount: <span class="tx-danger">*</span></label>
    <input type="text" class="form-control" name="botl_tax_amt" placeholder="Enter tax amount" value="{{ $carry_item->botl_tax_amt }}">
    @if ($errors->has('botl_tax_amt'))
    <span class="help-block">
        <p class="error">{{ $errors->first('botl_tax_amt') }}</p>
    </span>
    @endif
</div>
</div><!-- col-8 -->
<div class="col-lg-12">
  <div class="form-group mg-b-10-force">
    <label class="form-control-label">Narration: <span class="tx-danger">*</span></label>
    <input type="text" class="form-control" name="botl_narration" placeholder="Enter narration" value="{{ $carry_item->botl_narration}}">
    @if ($errors->has('botl_narration'))
    <span class="help-block">
        <p class="error">{{ $errors->first('botl_narration') }}</p>
    </span>
    @endif
</div>
</div><!-- col-8 -->
</div><!-- row -->

<div class="form-layout-footer">
    <button type="submit" class="btn btn-primary bd-0">Submit</button>
    <a href="{{url('/admin/view_packing_purchase')}}" class="btn btn-secondary bd-0">Back</a>
</div><!-- form-layout-footer -->
</div><!-- form-layout -->
</form>
</div><!-- section-wrapper -->
</div>


@include('admin.includes.footer')

<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
<script src="{{asset('assets/backend/lib/datatables/js/jquery.dataTables.js')}}"></script>
<script src="{{asset('assets/backend/lib/select2/js/select2.min.js')}}"></script>

<script type="text/javascript">
    $(function () {
        $('#example').datetimepicker({
            format: 'YYYY-MM-DD'
        });      
    });
    setTimeout(function(){ $("#msg").fadeOut(2000); }, 2000);
</script>
<script type="text/javascript">

    $(document).ready(function() {

        $('#roleListTable').on('click','.edit-item',function ()
        {

            var id=$(this).attr('id');
            $(this).attr('data-target','#edit-modal');
         // alert(id);
         jQuery.ajax({
            type: "get",
            url: 'read-role-data/'+id,
            //data: {smsid:id},
            success: function(res)
            {
               // alert(res);
               var d1= res.split(',');
               $("#roll_name").val(d1[1]);
               $("#user_id").val(d1[0]);
           }
       });
     });
    });
</script>

<script type="text/javascript">

    setTimeout(function(){ $("#msg").fadeOut(2000); }, 2000);


    $(document).on("click", ".del", function (event) {
        var result = confirm("Are you sure want to delete?");
        if (result) {
                  //Logic to delete the item
                  return true;
              }
              else
              {
                return false;
            }
        });
    
    $('#roleListTable').DataTable({
        scrollX:true,
        language: {
            searchPlaceholder: 'Search...',
            sSearch: '',
            lengthMenu: '_MENU_ items/page',
        },
        "ordering":false,
        "destroy": true,
        "processing": true,
    });
//   $(document).ready(function(){
//     $("#successMessage").delay(1700).slideUp(300);
// });
setTimeout(function(){ $("#msg").fadeOut(2000); }, 2000);
</script>

