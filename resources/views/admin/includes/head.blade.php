<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Twitter -->
    <meta name="twitter:site" content="@themepixels">
    <meta name="twitter:creator" content="@themepixels">
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:title" content="Slim">
    <meta name="twitter:description" content="Premium Quality and Responsive UI for Dashboard.">
    <meta name="twitter:image" content="http://themepixels.me/slim/img/slim-social.png">

    <!-- Facebook -->
    <meta property="og:url" content="http://themepixels.me/slim">
    <meta property="og:title" content="Slim">
    <meta property="og:description" content="Premium Quality and Responsive UI for Dashboard.">

    <meta property="og:image" content="http://themepixels.me/slim/img/slim-social.png">
    <meta property="og:image:secure_url" content="http://themepixels.me/slim/img/slim-social.png">
    <meta property="og:image:type" content="image/png">
    <meta property="og:image:width" content="1200">
    <meta property="og:image:height" content="600">

    <!-- Meta -->
    <meta name="description" content="Premium Quality and Responsive UI for Dashboard.">
    <meta name="author" content="ThemePixels">

    <title>Clinic Soft</title>

  <!-- vendor css -->
  <link href="{{asset('assets/backend/lib/font-awesome/css/font-awesome.css')}}" rel="stylesheet">
  <link href="{{asset('assets/backend/lib/Ionicons/css/ionicons.css')}}" rel="stylesheet">
  <link href="{{asset('assets/backend/lib/perfect-scrollbar/css/perfect-scrollbar.min.css')}}" rel="stylesheet">
  <link href="{{asset('assets/backend/lib/rickshaw/css/rickshaw.min.css')}}" rel="stylesheet">
  <link href="{{asset('assets/backend/lib/chartist/css/chartist.css')}}" rel="stylesheet">
  <link href="{{asset('assets/backend/lib/jquery.steps/css/jquery.steps.css')}}" rel="stylesheet">
  <link href="{{asset('assets/backend/lib/datatables/css/jquery.dataTables.css')}}" rel="stylesheet">
  <link href="{{asset('assets/backend/lib/jquery-toggles/css/toggles-full.css')}}" rel="stylesheet">
  <!-- Slim CSS -->
  <link rel="stylesheet" href="{{asset('assets/backend/css/slim.css')}}">
  <link rel="shortcut icon" href="{{ url('image/dcsymbol.jpg') }}">
  <link href="{{asset('assets/backend/lib/datepicker/css/bootstrap-datetimepicker.css')}}" rel="stylesheet">
  <link href="{{asset('assets/backend/lib/datepicker/css/bootstrap-datetimepicker.min.css')}}" rel="stylesheet">
  <link rel="stylesheet" href="{{asset('assets/backend/css/sweetalert.css')}}">
  </head>
  <body>
    <div class="slim-header">
      <div class="container">
        <div class="slim-header-left">
          <h2 class="slim-logo" style="color: #1B84E7;">@can('isSUPERADMIN')<a href="{{ url('/admin/dashboard') }}">@endcan <img src="{{ url('image/doctor_2.png')}}"></a></h2>


        </div><!-- slim-header-left -->
        <div class="slim-header-right">

           <div class="dropdown dropdown-c">
              <a href="#" class="logged-user" data-toggle="dropdown">
                <img src="http://via.placeholder.com/500x500" alt="">
                <span>{{Auth::user()->roll_name}}</span>
                <i class="fa fa-angle-down"></i>
            </a>
            <div class="dropdown-menu dropdown-menu-right">
                <nav class="nav">
                        {{-- <a href="page-profile.html" class="nav-link"><i class="icon ion-person"></i> View Profile</a>
                        <a href="page-edit-profile.html" class="nav-link"><i class="icon ion-compose"></i> Edit Profile</a>
                        <a href="page-activity.html" class="nav-link"><i class="icon ion-ios-bolt"></i> Activity Log</a> --}}
                        <a href="{{ url('/admin/password-change') }}" class="nav-link"><i class="icon ion-ios-gear"></i> Change Password</a>
                        {{-- <a type="button" class="nav-link"><i class="icon ion-forward"></i> Sign Out</a>
                        --}}
                        {!!Form::open(['route' => 'admin.auth.logout', 'method' => 'get'])!!}
                        <button style="width: 167px; background-color: white; color: #4662D4" type="submit" class="btn btn-default btn-flat">Sign out</button>
                        {!!Form::close()!!}
                    </nav>
                </div><!-- dropdown-menu -->
    </div><!-- dropdown -->
        </div><!-- header-right -->
      </div><!-- container -->
    </div><!-- slim-header -->

    <div class="slim-navbar">
      <div class="container">
        <ul class="nav">


          @can('isSUPERADMIN')
          <li class="nav-item {{ request()->is('admin/roles') ? 'active' : '' }}">
                    <a class="nav-link" href="{{ url('/admin/roles') }}">
                      <i class="icon ion-document"></i> 
                      <span>Add Roles</span>
                    </a>
            </li>
            <li class="nav-item {{ request()->is('admin/users') ? 'active' : '' }}">
                    <a class="nav-link" href="{{ url('/admin/users') }}">
                      <i class="icon ion-document"></i> 
                      <span>Add Users</span>
                    </a>
            </li>
          @endcan


          @can('isRECEPTIONIST')
          <li class="nav-item {{ request()->is('admin/reg_usr') ? 'active' : '' }}">
            <a class="nav-link" data-toggle="dropdown">
              <i class="icon ion-clipboard"></i>
              <span>Registration</span>
            </a>
            <div class="sub-item">
              <ul>
                <li><a href="{{ url('/admin/reg_usr') }}">Register</a></li>
              </ul>
            </div><!-- dropdown-menu -->
          </li>
          @endcan


          @can('isRECEPTIONIST')
          <li class="nav-item with-sub {{ request()->is('admin/today_appointment') ? 'active' : '' }} {{ request()->is('admin/reg_user') ? 'active' : '' }}">
            <a class="nav-link" data-toggle="dropdown">
              <i class="icon ion-compose"></i>
              <span>Appointment</span>
            </a>
            <div class="sub-item">
              <ul>
                <li><a href="{{ url('/admin/reg_user') }}">Customer List</a></li>
                <li><a href="{{ url('/admin/today_appointment') }}">Add / Appointment List</a></li>
              </ul>
            </div><!-- dropdown-menu -->
          </li>
          @endcan
          
            @can('isDOCTOR')
              <li class="nav-item with-sub {{ request()->is('admin/reg_usr') ? 'active' : '' }} {{ request()->is('admin/reg_user') ? 'active' : '' }}">
            <a class="nav-link" data-toggle="dropdown">
              <i class="icon ion-clipboard"></i>
              <span>New Patient</span>
            </a>
            <div class="sub-item">
              <ul>
                <li><a href="{{ url('/admin/reg_usr') }}">ADD Patients</a></li>
                <li><a href="{{ url('/admin/reg_user') }}">Patients List</a></li>
              </ul>
            </div><!-- dropdown-menu -->
          </li>
            <li class="nav-item with-sub {{ request()->is('admin/today_appointment') ? 'active' : '' }}">
            <a class="nav-link" data-toggle="dropdown">
              <i class="icon ion-compose"></i>
              <span>Cunsultation</span>
            </a>
            <div class="sub-item">
              <ul>
                {{-- <li><a href="{{ url('/admin/reg_user') }}">Customer List</a></li> --}}
                <li><a href="{{ url('/admin/today_appointment') }}">Consultation List</a></li>
              </ul>
            </div><!-- dropdown-menu -->
          </li>
            {{-- <li class="nav-item {{ request()->is('admin/billing_list') ? 'active' : '' }}">
                    <a class="nav-link" href="{{ url('/admin/billing_list') }}">
                      <i class="icon ion-document"></i> 
                      <span>Billing</span>
                    </a>
                </li> --}}
            <li class="nav-item with-sub {{ request()->is('admin/add_product') ? 'active' : '' }}">
                    <a class="nav-link" data-toggle="dropdown">
                      <i class="icon ion-filing"></i>
                      <span>Stock</span>
                    </a>
                    <div class="sub-item">
                      <ul>
                        <li><a href="{{ url('/admin/add_product') }}">Add Product</a></li>
                      </ul>
                    </div><!-- dropdown-menu -->
                  </li>

                {{-- <li class="nav-item with-sub {{ request()->is('admin/daily') ? 'active' : '' }} {{ request()->is('admin/weekly') ? 'active' : '' }} {{ request()->is('admin/monthly') ? 'active' : '' }} {{ request()->is('admin/date_search') ? 'active' : '' }} {{ request()->is('admin/doctor_report') ? 'active' : '' }} {{ request()->is('admin/advance_report') ? 'active' : '' }}">
                    <a class="nav-link" data-toggle="dropdown">
                      <i class="icon ion-clipboard"></i>
                      <span>Reports</span>
                    </a>
                    <div class="sub-item">
                      <ul>
                        <li><a href="{{ url('/admin/daily') }}">Daily</a></li>
                        <li><a href="{{ url('/admin/weekly') }}">Weekly</a></li>
                        <li><a href="{{ url('/admin/monthly') }}">Monthly</a></li>
                        <li><a href="{{ url('/admin/doctor_report') }}">Doctor Report</a></li>
                        <li><a href="{{ url('/admin/advance_report') }}">Advance Report</a></li>
                      </ul>
                    </div><!-- dropdown-menu -->
                  </li> --}}
              @endcan

                  @can('isACCOUNTS')
                  <li class="nav-item with-sub {{ request()->is('admin/daily') ? 'active' : '' }} {{ request()->is('admin/weekly') ? 'active' : '' }} {{ request()->is('admin/monthly') ? 'active' : '' }} {{ request()->is('admin/doctor_report') ? 'active' : '' }} {{ request()->is('admin/advance_report') ? 'active' : '' }}">
                    <a class="nav-link" data-toggle="dropdown">
                      <i class="icon ion-clipboard"></i>
                      <span>Treatment Reports</span>
                    </a>
                    <div class="sub-item" style="width: 550px;">
                      <ul>
                        <li><a href="{{ url('/admin/daily') }}">Daily</a></li>
                        <li><a href="{{ url('/admin/weekly') }}">Weekly</a></li>
                        <li><a href="{{ url('/admin/monthly') }}">Monthly</a></li>
                        <li><a href="{{ url('/admin/doctor_report') }}">Doctor Report</a></li>
                        <li><a href="{{ url('/admin/advance_report') }}">Advance Report</a></li>
                      </ul>
                    </div><!-- dropdown-menu -->
                  </li>
                  @endcan

                  @can('isACCOUNTS')
                  <li class="nav-item with-sub {{ request()->is('admin/daily_accounts') ? 'active' : '' }}">
                    <a class="nav-link" data-toggle="dropdown">
                      <i class="icon ion-clipboard"></i>
                      <span>Accounts Reports</span>
                    </a>
                    <div class="sub-item" style="width: 550px;">
                      <ul>
                        <li><a href="{{ url('/admin/daily_accounts') }}">Daily / Monthly</a></li>
                        <li><a href="{{ url('/admin/datewise_account') }}">From-To Date Report</a></li>
                      </ul>
                    </div><!-- dropdown-menu -->
                  </li>
                  @endcan



        </ul>
      </div><!-- container -->
    </div><!-- slim-navbar -->


    