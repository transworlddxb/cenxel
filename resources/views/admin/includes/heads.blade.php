<!DOCTYPE html>
<html lang="en">
<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <!-- Twitter -->
  <meta name="twitter:site" content="@themepixels">
  <meta name="twitter:creator" content="@themepixels">
  <meta name="twitter:card" content="summary_large_image">
  <meta name="twitter:title" content="Slim">
  <meta name="twitter:description" content="Premium Quality and Responsive UI for Dashboard.">
  <meta name="twitter:image" content="http://themepixels.me/slim/img/slim-social.png">

  <!-- Facebook -->
  <meta property="og:url" content="http://themepixels.me/slim">
  <meta property="og:title" content="Slim">
  <meta property="og:description" content="Premium Quality and Responsive UI for Dashboard.">

  <meta property="og:image" content="http://themepixels.me/slim/img/slim-social.png">
  <meta property="og:image:secure_url" content="http://themepixels.me/slim/img/slim-social.png">
  <meta property="og:image:type" content="image/png">
  <meta property="og:image:width" content="1200">
  <meta property="og:image:height" content="600">

  <!-- Meta -->
  <meta name="description" content="Premium Quality and Responsive UI for Dashboard.">
  <meta name="author" content="ThemePixels">

  <title>Clinic Soft</title>

  <!-- vendor css -->
  <link href="{{asset('assets/backend/lib/font-awesome/css/font-awesome.css')}}" rel="stylesheet">
  <link href="{{asset('assets/backend/lib/Ionicons/css/ionicons.css')}}" rel="stylesheet">
  <link href="{{asset('assets/backend/lib/perfect-scrollbar/css/perfect-scrollbar.min.css')}}" rel="stylesheet">
  <link href="{{asset('assets/backend/lib/rickshaw/css/rickshaw.min.css')}}" rel="stylesheet">
  <link href="{{asset('assets/backend/lib/jquery.steps/css/jquery.steps.css')}}" rel="stylesheet">
  <link href="{{asset('assets/backend/lib/datatables/css/jquery.dataTables.css')}}" rel="stylesheet">
  <link href="{{asset('assets/backend/lib/jquery-toggles/css/toggles-full.css')}}" rel="stylesheet">
  <!-- Slim CSS -->
  <link rel="stylesheet" href="{{asset('assets/backend/css/slim.css')}}">
  <link rel="shortcut icon" href="{{ url('assets/backend/img/favv.png') }}">
  <link href="{{asset('assets/backend/lib/datepicker/css/bootstrap-datetimepicker.css')}}" rel="stylesheet">
  <link href="{{asset('assets/backend/lib/datepicker/css/bootstrap-datetimepicker.min.css')}}" rel="stylesheet">
  <link rel="stylesheet" href="{{asset('assets/backend/css/sweetalert.css')}}">
</head>
<body>


  <div class="slim-header with-sidebar">
    <div class="container-fluid">
      <div class="slim-header-left">
        <h2 class="slim-logo" style="color: #1B84E7;">@can('isSUPERADMIN')<a href="{{ url('/admin/dashboard') }}">@endcan <img src="{{ url('image/alqsrlogo1.png')}}"></a></h2>
        <a href="" id="slimSidebarMenu" class="slim-sidebar-menu"><span></span></a>

        {{-- <div class="search-box">
          <input type="text" class="form-control" placeholder="Search">
          <button class="btn btn-primary"><i class="fa fa-search"></i></button>
      </div><!-- search-box --> --}}

  </div><!-- slim-header-left -->
  <div class="slim-header-right">

    <div class="dropdown dropdown-c">
      <a href="#" class="logged-user" data-toggle="dropdown">
        <img src="http://via.placeholder.com/500x500" alt="">
        <span>{{Auth::user()->roll_name}}</span>
        <i class="fa fa-angle-down"></i>
    </a>
    <div class="dropdown-menu dropdown-menu-right">
        <nav class="nav">
                {{-- <a href="page-profile.html" class="nav-link"><i class="icon ion-person"></i> View Profile</a>
                <a href="page-edit-profile.html" class="nav-link"><i class="icon ion-compose"></i> Edit Profile</a>
                <a href="page-activity.html" class="nav-link"><i class="icon ion-ios-bolt"></i> Activity Log</a> --}}
                <a href="{{ url('/admin/password-change') }}" class="nav-link"><i class="icon ion-ios-gear"></i> Change Password</a>
                {{-- <a type="button" class="nav-link"><i class="icon ion-forward"></i> Sign Out</a>
                --}}
                {!!Form::open(['route' => 'admin.auth.logout', 'method' => 'get'])!!}
                <button style="width: 167px; background-color: white; color: #4662D4" type="submit" class="btn btn-default btn-flat">Sign out</button>
                {!!Form::close()!!}
            </nav>
        </div><!-- dropdown-menu -->
    </div><!-- dropdown -->
</div><!-- header-right -->
</div><!-- container-fluid -->
</div><!-- slim-header -->

<div class="slim-body">
  <div class="slim-sidebar">
    <label class="sidebar-label">Navigation</label>

    <ul class="nav nav-sidebar">
      <li class="sidebar-nav-item with-sub">
        
        {{-- @if($menu->mnu_title=="")
        <li class="sidebar-nav-item">
          <a href="{{ url('/admin/definition') }}" class="sidebar-nav-link {{ request()->is('admin/definition') ? 'active' : '' }}"> <i class="icon ion-compose"></i>{{ $menu->mnu_cat_category}}</a>
        </li>
        @else --}}

       {{--  @foreach($category as $key=> $categorys) 

            @if($categorys->mnu_cat_link==null)
        

            <li class="sidebar-nav-item with-sub">
                  <a href="" class="sidebar-nav-link {{ request()->is('/admin/'.$categorys->mnu_cat_link) ? 'active' : '' }}"><i class="{{ $categorys->mnu_cat_icon}}"></i>{{ $categorys->mnu_cat_category}}</a>
                
                <ul class="nav sidebar-nav-sub">
                    @foreach($menu as $key=> $menus)
                        @if($menus->mnu_category==$categorys->mnu_cat_pk_id)
                        <li class="nav-sub-item">             
                            <a href="{{url('/admin/'.$menus->mnu_link)}}" class="nav-sub-link {{ request()->is('/admin/'.$menus->mnu_link) ? 'active' : '' }}">{{ $menus->mnu_title}}</a>
                        </li>
                        @endif
                    @endforeach
                </ul>
                
            </li>

            @else

            <li class="sidebar-nav-item">
                <a href="{{url('/admin/'.$categorys->mnu_cat_link)}}" class="sidebar-nav-link {{ request()->is('/admin/'.$categorys->mnu_cat_link) ? 'active' : '' }}"><i class="{{ $categorys->mnu_cat_icon}}"></i>{{ $categorys->mnu_cat_category}}</a>
            </li>

            @endif
       
        @endforeach --}}

        @can('isSUPERADMIN')
        <li class="sidebar-nav-item">
          <a href="{{ url('/admin/definition') }}" class="sidebar-nav-link {{ request()->is('admin/definition') ? 'active' : '' }}"><i class="icon ion-information-circled"></i>Definitions</a>
        </li>
      @endcan
      @can('isSUPERADMIN')
      <li class="sidebar-nav-item with-sub">
          <a href="" class="sidebar-nav-link {{ request()->is('admin/roles') ? 'active' : '' }}  {{ request()->is('admin/users') ? 'active' : '' }} {{ request()->is('admin/classadd') ? 'active' : '' }}  {{ request()->is('admin/branches') ? 'active' : '' }} {{ request()->is('admin/doctor_list') ? 'active' : '' }} ">
            <i class="icon ion-ios-home"></i>Master</a>
            <ul class="nav sidebar-nav-sub">


              <li class="nav-sub-item">
                <a href="{{ url('/admin/roles') }}" class="nav-sub-link {{ request()->is('admin/roles') ? 'active' : '' }}">Roles</a>
            </li>
            <li class="nav-sub-item">
                <a href="{{ url('/admin/branches') }}" class="nav-sub-link {{ request()->is('admin/branches') ? 'active' : '' }}">Branches of Clinic</a>
            </li>               

            <li class="nav-sub-item">
                <a href="{{ url('/admin/classadd') }}" class="nav-sub-link {{ request()->is('admin/classadd') ? 'active' : '' }}">Category of Customer</a>
            </li>
            <li class="nav-sub-item">
                <a href="{{ url('/admin/users') }}" class="nav-sub-link {{ request()->is('admin/users') ? 'active' : '' }}">Add Users</a>
            </li>
           {{--  <li class="nav-sub-item">
                <a href="{{ url('/admin/doctor_list') }}" class="nav-sub-link {{ request()->is('admin/doctor_list') ? 'active' : '' }}">Doctors List</a>
            </li> --}}
                          
          
       </ul>
      </li>
      @endcan
  

        @can('isSUPERADMIN')  
            <li class="sidebar-nav-item with-sub">
              <a href="" class="sidebar-nav-link {{ request()->is('admin/treatment') ? 'active' : '' }} {{ request()->is('admin/normal') ? 'active' : '' }} {{ request()->is('admin/gov_list') ? 'active' : '' }} {{ request()->is('admin/student_list') ? 'active' : '' }}"><i class="icon ion-medkit"></i>Treatment Pricing</a>
                <ul class="nav sidebar-nav-sub">
                    <li class="nav-sub-item">
                        <a href="{{ url('/admin/treatment') }}" class="nav-sub-link {{ request()->is('admin/treatment') ? 'active' : '' }}">Add Treatments</a>
                    </li>
                    <li class="nav-sub-item">
                        <a href="{{ url('/admin/normal') }}" class="nav-sub-link {{ request()->is('admin/normal') ? 'active' : '' }}">Price for Normal</a>
                    </li>
                    <li class="nav-sub-item">
                        <a href="{{ url('/admin/gov_list') }}" class="nav-sub-link {{ request()->is('admin/gov_list') ? 'active' : '' }}">Price for Governments</a>
                    </li>
                    <li class="nav-sub-item">
                        <a href="{{ url('/admin/student_list') }}" class="nav-sub-link {{ request()->is('admin/student_list') ? 'active' : '' }}">Price for Students</a>
                    </li>
                    {{-- <li class="nav-sub-item">
                        <a href="{{ url('/admin/view_users') }}" class="nav-sub-link {{ request()->is('admin/view_users') ? 'active' : '' }}">List of Registration</a>
                    </li> --}}
                </ul>
            </li>
        @endcan
        

        @can('isSUPERADMIN')  
            <li class="sidebar-nav-item with-sub">
              <a href="" class="sidebar-nav-link {{ request()->is('admin/reg_user') ? 'active' : '' }} {{ request()->is('admin/reg_usr') ? 'active' : '' }} "><i class="icon ion-clipboard"></i>Costumer Registration</a>
                <ul class="nav sidebar-nav-sub">
                    <li class="nav-sub-item">             
                        <a href="{{ url('/admin/reg_user') }}" class="nav-sub-link {{ request()->is('admin/reg_user') ? 'active' : '' }}">Customer List</a>
                    </li>
                    <li class="nav-sub-item">             
                        <a href="{{ url('/admin/reg_usr') }}" class="nav-sub-link {{ request()->is('admin/reg_usr') ? 'active' : '' }}">Register</a>
                    </li>
                    {{-- <li class="nav-sub-item">
                        <a href="{{ url('/admin/view_users') }}" class="nav-sub-link {{ request()->is('admin/view_users') ? 'active' : '' }}">List of Registration</a>
                    </li> --}}
                </ul>
            </li>
        @endcan
        
        @can('isRECEPTIONIST')  
            <li class="sidebar-nav-item with-sub">
              <a href="" class="sidebar-nav-link {{ request()->is('admin/reg_user') ? 'active' : '' }} {{ request()->is('admin/reg_usr') ? 'active' : '' }}  "><i class="icon ion-clipboard"></i>Costumer Registration</a>
                <ul class="nav sidebar-nav-sub">
                    <li class="nav-sub-item">             
                        <a href="{{ url('/admin/reg_user') }}" class="nav-sub-link {{ request()->is('admin/reg_user') ? 'active' : '' }}">Customer List</a>
                    </li>
                    <li class="nav-sub-item">             
                        <a href="{{ url('/admin/reg_usr') }}" class="nav-sub-link {{ request()->is('admin/reg_usr') ? 'active' : '' }}">Register</a>
                    </li>
                    {{-- <li class="nav-sub-item">
                        <a href="{{ url('/admin/view_users') }}" class="nav-sub-link {{ request()->is('admin/view_users') ? 'active' : '' }}">List of Registration</a>
                    </li> --}}
                </ul>
            </li>
        @endcan
        @can('isSUPERADMIN')
          <li class="sidebar-nav-item with-sub">
              <a href="" class="sidebar-nav-link {{ request()->is('admin/menu_set_users') ? 'active' : '' }} {{ request()->is('admin/today_appointment') ? 'active' : '' }} "><i class="icon ion-compose"></i>Appointment</a>
              <ul class="nav sidebar-nav-sub">
                <li class="nav-sub-item">
                    <a href="{{ url('/admin/today_appointment') }}" class="nav-sub-link {{ request()->is('admin/today_appointment') ? 'active' : '' }}">Add / Appointment List</a>
                </li>
                {{-- <li class="nav-sub-item">
                  <a href="{{ url('/admin/menu_set_users') }}" class="nav-sub-link {{ request()->is('admin/menu_set_users') ? 'active' : '' }}">List</a>
              </li> --}}
          </ul>
        </li>
        @endcan
        @can('isRECEPTIONIST')
          <li class="sidebar-nav-item with-sub">
              <a href="" class="sidebar-nav-link {{ request()->is('admin/menu_set_users') ? 'active' : '' }} {{ request()->is('admin/today_appointment') ? 'active' : '' }} "><i class="icon ion-compose"></i></i>Appointment</a>
              <ul class="nav sidebar-nav-sub">
                <li class="nav-sub-item">
                    <a href="{{ url('/admin/today_appointment') }}" class="nav-sub-link {{ request()->is('admin/today_appointment') ? 'active' : '' }}">Add / Appointment List</a>
                </li>
                {{-- <li class="nav-sub-item">
                  <a href="{{ url('/admin/menu_set_users') }}" class="nav-sub-link {{ request()->is('admin/menu_set_users') ? 'active' : '' }}">List</a>
              </li> --}}
          </ul>
        </li>
        @endcan
        
        @can('isDOCTOR')
          <li class="sidebar-nav-item">
              <a href="{{ url('/admin/pending_cunsult') }}" class="sidebar-nav-link {{ request()->is('admin/pending_cunsult') ? 'active' : '' }} "><i class="icon ion-medkit"></i>My Appointment</a>
              {{-- <ul class="nav sidebar-nav-sub"> --}}
                {{-- <li class="nav-sub-item">
                    <a href="{{ url('/admin/pending_cunsult') }}" class="nav-sub-link {{ request()->is('admin/pending_cunsult') ? 'active' : '' }}">Pending Cunsultations</a>
                </li> --}}
                {{-- <li class="nav-sub-item">
                  <a href="{{ url('/admin/menu_set_users') }}" class="nav-sub-link {{ request()->is('admin/menu_set_users') ? 'active' : '' }}">List</a>
              </li> --}}
          {{-- </ul> --}}
        </li>
        @endcan
        @can('isRECEPTIONIST')
          <li class="sidebar-nav-item">
              <a href="{{ url('/admin/billing_list') }}" class="sidebar-nav-link {{ request()->is('admin/billing_list') ? 'active' : '' }} "><i class="fa fa-print"></i>Billing</a>
        </li>
        @endcan
        @can('isSUPERADMIN')
          <li class="sidebar-nav-item">
              <a href="{{ url('/admin/billing_list') }}" class="sidebar-nav-link {{ request()->is('admin/billing_list') ? 'active' : '' }} "><i class="fa fa-print"></i>Billing</a>
        </li>
        @endcan
        {{-- @can('isSUPERADMIN')
          <li class="sidebar-nav-item">
              <a href="{{ url('/admin/billing') }}" class="sidebar-nav-link {{ request()->is('admin/billing') ? 'active' : '' }} "><i class="icon ion-medkit"></i>Billing</a>
        </li>
        @endcan
        @can('isRECEPTIONIST')
          <li class="sidebar-nav-item">
              <a href="{{ url('/admin/pending_cunsult') }}" class="sidebar-nav-link {{ request()->is('admin/pending_cunsult') ? 'active' : '' }} "><i class="icon ion-medkit"></i>Billing</a>
        </li>
        @endcan --}}
       {{--  @can('isSUPERADMIN')
        <li class="sidebar-nav-item with-sub">
                <a href="" class="sidebar-nav-link {{ request()->is('admin/wallet') ? 'active' : '' }} {{ request()->is('admin/wallet_details') ? 'active' : '' }}"><i class="fa fa-credit-card "></i>Wallet Management</a>
            <ul class="nav sidebar-nav-sub">

                <li class="nav-sub-item">             
                    <a href="{{ url('/admin/wallet_details') }}" class="nav-sub-link {{ request()->is('admin/wallet_details') ? 'active' : '' }}">Recharge Wallet</a>
                </li>
                <li class="nav-sub-item">             
                    <a href="{{ url('/admin/wallet') }}" class="nav-sub-link {{ request()->is('admin/wallet') ? 'active' : '' }}">Wallet History</a>
                </li>
            </ul>
        </li>
        @endcan

        @can('isSUPERADMIN')
        <li class="sidebar-nav-item with-sub">
          <a href="" class="sidebar-nav-link {{ request()->is('admin/view_packing_purchase') ? 'active' : '' }}{{ request()->is('admin/purch_product') ? 'active' : '' }} {{ request()->is('admin/packing_purchase') ? 'active' : '' }} {{ request()->is('admin/product_purchase') ? 'active' : '' }} "><i class="fa fa-truck"></i>Purchase Items</a>
          <ul class="nav sidebar-nav-sub">

            <li class="nav-sub-item">
              <a href="{{url('/admin/view_packing_purchase')}}" class="nav-sub-link {{ request()->is('admin/view_packing_purchase') ? 'active' : '' }}">Bottle & others</a>
          </li>
          <li class="nav-sub-item">
              <a href="{{ url('/admin/purch_product') }}" class="nav-sub-link {{ request()->is('admin/purch_product') ? 'active' : '' }}">Product purchased</a>
          </li>

      </ul>
  </li>
  @endcan
      @can('isSUPERADMIN')
      <li class="sidebar-nav-item with-sub">
          <a href="" class="sidebar-nav-link {{ request()->is('admin/bottle_assign') ? 'active' : '' }} {{ request()->is('admin/assigned_user') ? 'active' : '' }} {{ request()->is('admin/assign_users') ? 'active' : '' }} "><i class="icon ion-compose"></i>Bottle assigning </a>
          <ul class="nav sidebar-nav-sub">
            <li class="nav-sub-item">
              <a href="{{ url('/admin/bottle_assign') }}" class="nav-sub-link {{ request()->is('admin/bottle_assign') ? 'active' : '' }}">Set Bottle Numbers</a>
          </li>
          <li class="nav-sub-item">
              <a href="{{ url('/admin/assign_users') }}" class="nav-sub-link {{ request()->is('admin/assign_users') ? 'active' : '' }}">Set Bottles to Customer</a>
          </li>
          <li class="nav-sub-item">
              <a href="{{ url('/admin/assigned_user') }}" class="nav-sub-link {{ request()->is('admin/assigned_user') ? 'active' : '' }}">Assigned Customers</a>
          </li>
      </ul>
    </li>
    @endcan


        @can('isSUPERADMIN')
        <li class="sidebar-nav-item with-sub">
          <a href="" class="sidebar-nav-link {{ request()->is('admin/bottle_broken') ? 'active' : '' }} {{ request()->is('admin/list_reassingn') ? 'active' : '' }} "><i class="icon ion-person-stalker "></i>Bottle Re-assigning</a>
          <ul class="nav sidebar-nav-sub">
            <li class="nav-sub-item">             
              <a href="{{ url('/admin/bottle_broken') }}" class="nav-sub-link {{ request()->is('admin/bottle_broken') ? 'active' : '' }}">Broken Bottle
              List</a>
          </li>
          <li class="nav-sub-item">
              <a href="{{ url('/admin/list_reassingn') }}" class="nav-sub-link {{ request()->is('admin/list_reassingn') ? 'active' : '' }}">List Re-assigning</a>
          </li>
      </ul>
    </li>
    @endcan



    @can('isSUPERADMIN')
    <li class="sidebar-nav-item with-sub">
      <a href="" class="sidebar-nav-link {{ request()->is('admin/view_product') ? 'active' : '' }} {{ request()->is('admin/add_product') ? 'active' : '' }}"><i class="fa fa-leaf"></i>Products</a>
      <ul class="nav sidebar-nav-sub">                
        <li class="nav-sub-item">
          <a href="{{ url('/admin/add_product') }}" class="nav-sub-link {{ request()->is('admin/add_product') ? 'active' : '' }}">Add Products</a>
      </li>
      <li class="nav-sub-item">
          <a href="{{ url('/admin/view_product') }}" class="nav-sub-link {{ request()->is('admin/view_product') ? 'active' : '' }}">Products List</a>
      </li>
  </ul>
</li>
@endcan


        @can('isCUSTOMER')
        <li class="sidebar-nav-item">
          <a href="{{ url('/admin/user_dashboard') }}" class="sidebar-nav-link {{ request()->is('admin/user_dashboard') ? 'active' : '' }}"> <i class="icon ion-ios-home"></i>Home</a>
        </li>
        @endcan
        @can('isCUSTOMER')
        <li class="sidebar-nav-item">
          <a href="{{ url('/admin/my_order') }}" class="sidebar-nav-link {{ request()->is('admin/my_order') ? 'active' : '' }} {{ request()->is('admin/place_order') ? 'active' : '' }}"> <i class="icon ion-ios-cart"></i>My Orders</a>
        </li>
        @endcan
        @can('isCUSTOMER')
        <li class="sidebar-nav-item">
          <a href="{{ url('/admin/my_wallet') }}" class="sidebar-nav-link {{ request()->is('admin/my_wallet') ? 'active' : '' }}"> <i class="fa fa-credit-card"></i>&nbsp;My Wallet</a>
        </li>
        @endcan
        @can('isSUPERADMIN')
        <li class="sidebar-nav-item with-sub">
          <a href="" class="sidebar-nav-link {{ request()->is('admin/order') ? 'active' : '' }} {{ request()->is('admin/todays_order') ? 'active' : '' }}"><i class="icon ion-navicon-round "></i>Order List</a>
          <ul class="nav sidebar-nav-sub">
            <li class="nav-sub-item">
              <a href="{{ url('/admin/order') }}" class="nav-sub-link {{ request()->is('admin/order') ? 'active' : '' }}">All Orders</a>
          </li>
          <li class="nav-sub-item">
              <a href="{{ url('/admin/todays_order') }}" class="nav-sub-link {{ request()->is('admin/todays_order') ? 'active' : '' }}">Today's Orders</a>
          </li>
        </ul>
        </li>
        @endcan
        @can('isSTAFF')
        <li class="sidebar-nav-item">
          <a href="{{ url('/admin/home_staff') }}" class="sidebar-nav-link  {{ request()->is('admin/home_staff') ? 'active' : '' }}"><i class="icon ion-ios-home"></i>Dashboard</a>
        </li>
        @endcan
        @can('isSTAFF')
        <li class="sidebar-nav-item">
          <a href="{{ url('/admin/view_order') }}" class="sidebar-nav-link {{ request()->is('admin/view_order') ? 'active' : '' }}"> <i class="icon ion-clipboard"></i>Today's Order</a>
        </li>
        @endcan
        @can('isSTAFF')
        <li class="sidebar-nav-item">
          <a href="{{ url('/admin/view_filld') }}" class="sidebar-nav-link {{ request()->is('admin/view_filld') ? 'active' : '' }}"> <i class="fa fa-pencil"></i>Verified Order</a>
        </li>
        @endcan
        @can('isDOCTOR')
        <li class="sidebar-nav-item">
          <a href="{{ url('/admin/delivery-boy') }}" class="sidebar-nav-link {{ request()->is('admin/delivery-boy') ? 'active' : '' }}"> <i class="icon ion-ios-home"></i>Dashboard</a>
        </li>
        @endcan
        @can('isDOCTOR')
        <li class="sidebar-nav-item">
          <a href="{{ url('/admin/deliveryboy_list') }}" class="sidebar-nav-link {{ request()->is('admin/deliveryboy_list') ? 'active' : '' }}"> <i class="icon ion-navicon-round "></i>Order List</a>
        </li>
        @endcan
        @can('isDOCTOR')
        <li class="sidebar-nav-item">
          <a href="{{ url('/admin/delivered_list') }}" class="sidebar-nav-link {{ request()->is('admin/delivered_list') ? 'active' : '' }}"> <i class="icon ion-clipboard"></i>Delivered List</a>
        </li>
        @endcan
        @can('isADMINISTRATOR')
        <li class="sidebar-nav-item">
          <a href="{{ url('/admin/vendor_home') }}" class="sidebar-nav-link {{ request()->is('admin/vendor_home') ? 'active' : '' }}"> <i class="icon ion-compose"></i>Dashboard</a>
        </li>
        @endcan
        @can('isSUPERADMIN')
        <li class="sidebar-nav-item">
          <a href="{{ url('/admin/delivered') }}" class="sidebar-nav-link {{ request()->is('admin/delivered') ? 'active' : '' }}"> <i class="icon ion-clipboard"></i>Today's Delivered list</a>
        </li>
        @endcan
        @can('isADMINISTRATOR')
        <li class="sidebar-nav-item">
          <a href="{{ url('/admin/purch_list') }}" class="sidebar-nav-link {{ request()->is('admin/purch_list') ? 'active' : '' }}"> <i class="icon ion-clipboard"></i>Purchase list</a>
        </li>
        @endcan
        @can('isDOCTOR')
        <li class="sidebar-nav-item">
          <a href="{{ url('/admin/deliver_report') }}" class="sidebar-nav-link {{ request()->is('admin/deliver_report') ? 'active' : '' }}"><i class="icon ion-compose"></i>Daily Order Report</a>
        </li>
        @endcan  --}}
                   {{--  @can('isDOCTOR')
            <li class="sidebar-nav-item">
              <a href="{{ url('/admin/report_commisn') }}" class="sidebar-nav-link {{ request()->is('admin/report_commisn') ? 'active' : '' }}"><i class="icon ion-compose"></i>Monthly Commission report</a>
            </li>
            @endcan --}}
           {{--  @can('isSUPERADMIN')
            <li class="sidebar-nav-item with-sub">
              <a href="" class="sidebar-nav-link {{ request()->is('admin/target') ? 'active' : '' }} {{ request()->is('admin/delivery_boy_commissions') ? 'active' : '' }} "><i class="fa fa-money"></i></i>Commission & Target</a>
              <ul class="nav sidebar-nav-sub">
                <li class="nav-sub-item">             
                  <a href="{{ url('/admin/target') }}" class="nav-sub-link {{ request()->is('admin/target') ? 'active' : '' }}">Commission & Target</a>
              </li>
              <li class="nav-sub-item">             
                  <a href="{{ url('/admin/delivery_boy_commissions') }}" class="nav-sub-link {{ request()->is('admin/delivery_boy_commissions') ? 'active' : '' }}">Delivery boy commission</a>
              </li>

          </ul>
      </li>
      @endcan
  </li> --}}

     {{--   @foreach($category as $key => $categories)
         <li class="sidebar-nav-item with-sub">
            <a href="" class="sidebar-nav-link">
              <i class="icon ion-ios-home-outline"></i> {{ $categories->mnu_cat_category }}</a>
              
              @foreach($menu as $key => $menus)
              <ul class="nav sidebar-nav-sub">    
                <li class="nav-sub-item">
                  <a href="{{$menus->mnu_link}}" class="nav-sub-link">{{ $menus->mnu_title }}</a>
                </li>  
              </ul>
              @endforeach --}}
     {{--    @foreach($category as $key => $categories)
         <li class="sidebar-nav-item with-sub">
            <a href="" class="sidebar-nav-link">
              <i class="icon ion-ios-home-outline"></i> {{ $categories->mnu_cat_category }}</a>
              <ul class="nav sidebar-nav-sub">
                <li class="nav-sub-item">
                  <a href="{{ url('/admin/roles') }}" class="nav-sub-link {{ request()->is('admin/roles') ? 'active' : '' }}">Add-Roles</a>
                </li>                
              </ul>
            </li>
            @endforeach --}}


        </ul>
      </div><!-- slim-sidebar -->