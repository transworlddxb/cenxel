<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
    </head>
    <style>
        td {
            border-bottom: 1px solid #ddd;
            margin: 5px;
        }
    </style>
    <body>
        <div>
            <div style="float: left;">
                <p>Al Qasr Al Wardi Medical Centre<br/>
                Office No: 906, Emirates Sail Tower, Buhaira Corniche<br>
                    Sharjah – United Arab Emirates<br>
                MOB No: (+971) 6 566 83 66<br>
                  Email: info@alqasercosme.com<br>
                TRN: 100443299100003</p>
            </div>
            <div style="float: right">
                <img src="{{ url('image/al_logo.png')}}" style="width: 270px;">
            </div>
        </div>
        <div>
            <div style="text-align: center; padding-top: 130px;">
                <h1>REPORT OF {{$date}}</h1>
            </div>
        </div>
        <div>
            <table cellspacing="0">
                <thead style="background-color: #eeeeee; border: none;">
                    <tr>
                        <th width="30px" height="35px">SI.NO</th>
                        <th width="75px">NAME</th>
                        <th width="75px">DOCTOR</th>
                        <th width="75px">DATE</th>
                        <th width="75px">TIME</th>
                        <th width="75px">AMOUNT</th>
                        <th width="75px">PRODUCT</th>
                        <th width="75px"> &nbsp;PAYED</th>
                        <th width="75px">BALANCE</th>
                    </tr>
                </thead>
                <tbody>
                     @foreach($appointment as $key => $report)
                    <tr>
                      <td>{{ $key+1 }}</td>
                      <td >{{ $report->first_name }}</td>
                      <td >{{ $report->roll_name }}</td>
                      <td >{{ $report->appointment_date }}</td>
                      <td >{{ $report->appointment_time }}</td>
                      <td >{{ $report->total_amount_to_pay}}</td>
                      <td >{{ $report->purchased_amount}}</td>
                      <td >{{ $report->payedbill_amount}}</td>
                      <td >{{ $report->duebill_amount}}</td>
                  </tr>
                  @endforeach 
                </tbody>
            </table>
        </div>
    </body>
</html>           
