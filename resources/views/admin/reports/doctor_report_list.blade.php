@include('admin.includes.head')
<link href="{{asset('assets/backend/css/darkbox.css')}}" rel="stylesheet">
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.21.0/moment.min.js" type="text/javascript"></script>
<style type="text/css">
.photogallery li{position: relative; overflow: hidden; min-height: 200px; background: #eee; border: 2px solid #fff;}
.photogallery li:nth-child(odd){background: #bbb;}
.photogallery li img{position: absolute; top: -9999px; right: -9999px; bottom: -9999px; left: -9999px; margin: auto; max-width: none; /*max-height: 100%;*/ width: 100%; height: auto;}
</style>

<div class="slim-mainpanel">
  <div class="container">
    <div class="slim-pageheader">
      <ol class="breadcrumb slim-breadcrumb">
        <li class="breadcrumb-item"><a href="#">Home</a></li>
        <li class="breadcrumb-item">Daily Report</li>
        {{-- <li class="breadcrumb-item active" aria-current="page">Form Wizards</li> --}}
      </ol>
      {{-- <h6 class="slim-pagetitle mb-2">Daily Report</h6> --}}
    </div><!-- slim-pageheader -->    


    <div class="section-wrapper">
    <h6 class="slim-pagetitle mb-2">Daily Report</h6>
      <div class="row">
        <div class="col-sm-12 col-lg-6"> 
          <form action="{{ url('/admin/datewise_doctor_pdf')}}" method="post">
            {{csrf_field()}}
            <div class="row  justify-content-md-end justify-content-center xs-p2">
              <input class="form-control form-input col-lg-3" id="example"  type="text" name="report_date" placeholder="Select date" required>
               <select class="form-control select2 col-lg-4" data-placeholder="Choose doctor" name="int_doctor_id" required>
                    <option label="Choose Doctor"></option>
                     @foreach($user as $key=>$doctors) 

                    <option value="{{$doctors->id}}">{{$doctors->roll_name}}</option>

                    @endforeach
                </select>
              <button type="submit" class="btn btn-primary icon ion-document-text" style="margin-right:15px;background-color: green; border-color: green;"> &nbsp;Print</button>
            </div>
          </form>
        </div>
        <div class="col-sm-12 col-lg-6">

          <form action="{{ url('/admin/doctor_search_pdf')}}" method="post">
            {{csrf_field()}}
            <div class="row  justify-content-md-end justify-content-center xs-p2">
              <input class="form-control form-input col-lg-3" id="example1"  type="text" name="report_date" placeholder="Select date" required>
               <select class="form-control select2 col-lg-4" data-placeholder="Choose doctor" name="int_doctor_id" required>
                    <option label="Choose Doctor"></option>
                     @foreach($user as $key=>$doctors) 

                    <option value="{{$doctors->id}}">{{$doctors->roll_name}}</option>

                    @endforeach
                </select>
              <button type="submit" class="btn btn-primary fa fa-search" style="margin-right:15px;"> &nbsp;</button>
            </div>
          </form>
        </div>
{{--           <div class="col-sm-12 col-lg-3">
            <div class="d-flex  justify-content-md-end justify-content-center">
              <a href="{{ url('/admin/treatment') }}" class="btn btn-primary" style="margin-bottom: 10px;"><i class="fa fa-plus"></i> Add new</a>
            </div>
          </div> --}}
        </div>
        <hr>
        <h6 class="slim-pagetitle mb-2">Monthly Report</h6>
      <div class="row">
        <div class="col-sm-12 col-lg-6"> 
          <form action="{{ url('/admin/monthwise_doctor_pdf')}}" method="post">
            {{csrf_field()}}
            <div class="row  justify-content-md-end justify-content-center xs-p2">
              <input class="form-control form-input col-lg-3" id="example2"  type="text" name="report_month" placeholder="Select Month" required>
               <select class="form-control select2 col-lg-4" data-placeholder="Choose doctor" name="int_doctor_id" required>
                    <option label="Choose Doctor"></option>
                     @foreach($user as $key=>$doctors) 

                    <option value="{{$doctors->id}}">{{$doctors->roll_name}}</option>

                    @endforeach
                </select>
              <button type="submit" class="btn btn-primary icon ion-document-text" style="margin-right:15px;background-color: green; border-color: green;"> &nbsp;Print</button>
            </div>
          </form>
        </div>
        <div class="col-sm-12 col-lg-6">

          <form action="{{ url('/admin/doctor_monthsearch_pdf')}}" method="post">
            {{csrf_field()}}
            <div class="row  justify-content-md-end justify-content-center xs-p2">
              <input class="form-control form-input col-lg-3" id="example3"  type="text" name="report_month" placeholder="Select month" required>
               <select class="form-control select2 col-lg-4" data-placeholder="Choose doctor" name="int_doctor_id" required>
                    <option label="Choose Doctor"></option>
                     @foreach($user as $key=>$doctors) 

                    <option value="{{$doctors->id}}">{{$doctors->roll_name}}</option>

                    @endforeach
                </select>
              <button type="submit" class="btn btn-primary fa fa-search" style="margin-right:15px;"> &nbsp;</button>
            </div>
          </form>
        </div>
{{--           <div class="col-sm-12 col-lg-3">
            <div class="d-flex  justify-content-md-end justify-content-center">
              <a href="{{ url('/admin/treatment') }}" class="btn btn-primary" style="margin-bottom: 10px;"><i class="fa fa-plus"></i> Add new</a>
            </div>
          </div> --}}
        </div>
        <hr>
        <br>


        <div class="table-wrapper">
          <table id="productlistListTable" class="table display" width='100%'>
            <thead>
              <tr>
                <th >SI.NO</th>
                <th >DOCTOR</th>
                <th >NAME</th>                
                <th >DATE</th>
                <th >TIME</th>
                <th >TOTAL AMOUNT</th>
                <th >TREATMENTS</th>
              </tr>
            </thead>
            <tbody>
               @foreach($appointment as $key => $report)
                    <tr>
                      <td>{{ $key+1 }}</td>
                      <td >{{ $report->roll_name }}</td>
                      <td >{{ $report->first_name }}</td>                     
                      <td >{{ $report->appointment_date }}</td>
                      <td >{{ $report->appointment_time }}</td>
                      <td >{{ $report->total_amount_to_pay}}</td>
                      <td >{{ $report->apo_treatment_one}}<br>{{ $report->apo_treatment_two}}<br>{{ $report->apo_treatment_three}}<br>{{ $report->apo_treatment_four}}<br>{{ $report->apo_treatment_five}}</td>
                      
                  </tr>
                  @endforeach 
            </tbody>
          </table>
        </div>
      </div><!-- section-wrapper -->
    </div>

    @include('admin.includes.footer')
    </script>
    <script src="{{asset('assets/backend/lib/datatables/js/jquery.dataTables.js')}}"></script>
    <script src="{{asset('assets/backend/lib/select2/js/select2.min.js')}}"></script>
    <script src="{{asset('assets/backend/js/darkbox.min.js')}}"></script>

    <script type="text/javascript">

      $('#productlistListTable').DataTable({
        scrollX:true,
        language: {
          searchPlaceholder: 'Search...',
          sSearch: '',
          lengthMenu: '_MENU_ items/page',
        },
        "ordering":false,
        "destroy": true,
        "processing": true,
      });
    </script>
    <script type="text/javascript">
        $(function () {
            $('#example').datetimepicker({
                format: 'YYYY-MM-DD'
            });      
        });
    </script>
    <script type="text/javascript">
        $(function () {
            $('#example1').datetimepicker({
                format: 'YYYY-MM-DD'
            });      
        });
    </script>
    <script type="text/javascript">
        $(function () {
            $('#example2').datetimepicker({
                format: 'MM'
            });      
        });
    </script>
    <script type="text/javascript">
        $(function () {
            $('#example3').datetimepicker({
                format: 'MM'
            });      
        });
    </script>