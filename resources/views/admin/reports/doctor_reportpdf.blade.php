<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
    </head>
    <style>
        td {
            border-bottom: 1px solid #ddd;
            margin: 5px;
        }
    </style>
    <body>
        <div>
            <div style="float: left;">
                <p>Al Qasr Al Wardi Medical Centre<br/>
                Office No: 906, Emirates Sail Tower, Buhaira Corniche<br>
                    Sharjah – United Arab Emirates<br>
                MOB No: (+971) 6 566 83 66<br>
                  Email: info@alqasercosme.com<br>
                TRN: 100443299100003</p>
            </div>
            <div style="float: right">
                <img src="{{ url('image/al_logo.png')}}" style="width: 270px;">
            </div>
        </div>
        <div>
            <div style="text-align: center; padding-top: 130px;">
                <h1>REPORT OF {{$doctor}}</h1>
            </div>
        </div>
        <div>
            <table cellspacing="0">
                <thead style="background-color: #eeeeee; border: none;">
                   <tr>
                    <th width="83px">SI.NO</th>
                    <th width="83px">DOCTOR</th>
                    <th width="83px">NAME</th>                
                    <th width="83px">DATE</th>
                    <th width="83px">TIME</th>
                    <th width="83px">TOTAL AMOUNT</th>
                    <th width="83px">TREATMENTS</th>
                  </tr>
                </thead>
                <tbody>
                    @foreach($appointment as $key => $report)
                    <tr>
                      <td>{{ $key+1 }}</td>
                      <td >{{ $report->roll_name }}</td>
                      <td >{{ $report->first_name }}</td>                     
                      <td >{{ $report->appointment_date }}</td>
                      <td >{{ $report->appointment_time }}</td>
                      <td >{{ $report->total_amount_to_pay}}</td>
                      <td >{{ $report->apo_treatment_one}}<br>{{ $report->apo_treatment_two}}<br>{{ $report->apo_treatment_three}}<br>{{ $report->apo_treatment_four}}<br>{{ $report->apo_treatment_five}}</td>
                      
                  </tr>
                  @endforeach 
                </tbody>
            </table>
        </div>
    </body>
</html>           
