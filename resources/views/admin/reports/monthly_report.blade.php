@include('admin.includes.head')
<link href="{{asset('assets/backend/css/darkbox.css')}}" rel="stylesheet">
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.21.0/moment.min.js" type="text/javascript"></script>
<style type="text/css">
.photogallery li{position: relative; overflow: hidden; min-height: 200px; background: #eee; border: 2px solid #fff;}
.photogallery li:nth-child(odd){background: #bbb;}
.photogallery li img{position: absolute; top: -9999px; right: -9999px; bottom: -9999px; left: -9999px; margin: auto; max-width: none; /*max-height: 100%;*/ width: 100%; height: auto;}
</style>

<div class="slim-mainpanel">
  <div class="container">
    <div class="slim-pageheader">
      <ol class="breadcrumb slim-breadcrumb">
        <li class="breadcrumb-item"><a href="#">Home</a></li>
        <li class="breadcrumb-item">MONTHLY REPORT</li>
        {{-- <li class="breadcrumb-item active" aria-current="page">Form Wizards</li> --}}
      </ol>
      <h6 class="slim-pagetitle mb-2">MONTHLY REPORT</h6>
    </div><!-- slim-pageheader -->    


    <div class="section-wrapper">

      <div class="row">
        <div class="col-sm-12 col-lg-3">
          <a href="{{ url('/admin/today_report') }}" class="btn btn-primary" style="margin-bottom: 10px;background-color: green; border-color: green;"><i class="icon ion-arrow-down-a"></i> Print Todays Report</a>
        </div>
        <div class="col-sm-12 col-lg-3"> 
          <form action="{{ url('/admin/datewise_pdf')}}" method="post">
            {{csrf_field()}}
            <div class="row  justify-content-md-end justify-content-center xs-p2">
              <input class="form-control form-input" id="example"  type="text" name="report_date" placeholder="Select date" required>
              <button type="submit" class="btn btn-primary icon ion-document-text" style="margin-right:15px;background-color: green; border-color: green;"> &nbsp;Print</button>
            </div>
          </form>
        </div>
        <div class="col-sm-12 col-lg-6">
          <form action="{{ url('/admin/monthly_pdf')}}" method="post">
            {{csrf_field()}}
            <div class="row  justify-content-md-end justify-content-center xs-p2">
              <input class="form-control form-input col-lg-3" id="example1"  type="text" name="report_month" placeholder="Select Month" required style="">
              <input class="form-control form-input col-lg-3" id="example2"  type="text" name="report_year" placeholder="Select Year" required style="">
              <button type="submit" class="btn btn-primary icon ion-document-text" style="margin-right:15px;background-color: green; border-color: green;"> &nbsp;Print</button>
            </div>
          </form>
        </div>
        </div>
        <hr>
        <div class="row" style="margin-top: 15px;">

        <div class="col-sm-12 col-lg-6">
          <form action="{{ url('/admin/bet_date_pdf')}}" method="post">
            {{csrf_field()}}
            <div class="row  justify-content-md-end justify-content-center xs-p2">
              <input class="form-control form-input col-lg-3" id="example5"  type="text" name="from_date" placeholder="From date" required>
              <input class="form-control form-input col-lg-3" id="example6"  type="text" name="to_date" placeholder="To date" required>
              <button type="submit" class="btn btn-primary icon ion-document-text" style="margin-right:15px; background-color: green;border-color: green;"> &nbsp;Print</button>
            </div>
          </form>
        </div>
        <div class="col-sm-12 col-lg-6">
          <form action="{{ url('/admin/month_year_search')}}" method="post">
            {{csrf_field()}}
            <div class="row  justify-content-md-end justify-content-center xs-p2">
                <label style="margin-top: 10px;">Search here : &nbsp;</label>
              <input class="form-control form-input col-lg-3" id="example3"  type="text" name="month" placeholder="Month" required style="">
              <input class="form-control form-input col-lg-3" id="example4"  type="text" name="year" placeholder="Year" required style="">
              <button type="submit" class="btn btn-primary fa fa-search" style="margin-right:15px;"> &nbsp;Search</button>
            </div>
          </form>
        </div>
{{--           <div class="col-sm-12 col-lg-3">
            <div class="d-flex  justify-content-md-end justify-content-center">
              <a href="{{ url('/admin/treatment') }}" class="btn btn-primary" style="margin-bottom: 10px;"><i class="fa fa-plus"></i> Add new</a>
            </div>
          </div> --}}
        </div>
        <hr>
        <br>


        <div class="table-wrapper">
          <table id="productlistListTable" class="table display" width='100%'>
            <thead>
              <tr>
                <th >SI.NO</th>
                <th >NAME</th>
                <th >DOCTOR</th>
                <th >DATE</th>
                <th >TIME</th>
                <th >TOTAL AMOUNT</th>
                <th >AMOUNT PAYED</th>
                <th >BALANCE AMOUNT</th>
              </tr>
            </thead>
            <tbody>
               @foreach($appointment as $key => $report)
                    <tr>
                      <td>{{ $key+1 }}</td>
                      <td >{{ $report->first_name }}</td>
                      <td >{{ $report->roll_name }}</td>
                      <td >{{ $report->appointment_date }}</td>
                      <td >{{ $report->appointment_time }}</td>
                      <td >{{ $report->total_amount_to_pay}}</td>
                      <td >{{ $report->payedbill_amount}}</td>
                      <td >{{ $report->duebill_amount}}</td>
                  </tr>
                  @endforeach 
            </tbody>
          </table>
        </div>
      </div><!-- section-wrapper -->
    </div>

    @include('admin.includes.footer')
    </script>
    <script src="{{asset('assets/backend/lib/datatables/js/jquery.dataTables.js')}}"></script>
    <script src="{{asset('assets/backend/lib/select2/js/select2.min.js')}}"></script>
    <script src="{{asset('assets/backend/js/darkbox.min.js')}}"></script>

    <script type="text/javascript">

      $('#productlistListTable').DataTable({
        scrollX:true,
        language: {
          searchPlaceholder: 'Search...',
          sSearch: '',
          lengthMenu: '_MENU_ items/page',
        },
        "ordering":false,
        "destroy": true,
        "processing": true,
      });
    </script>
    <script type="text/javascript">
        $(function () {
            $('#example').datetimepicker({
                format: 'YYYY-MM-DD'
            });      
        });
    </script>
    <script type="text/javascript">
        $(function () {
            $('#example1').datetimepicker({
                format: 'MM'
            });      
        });
    </script>
    <script type="text/javascript">
        $(function () {
            $('#example2').datetimepicker({
                format: 'YYYY'
            });      
        });
    </script>
    <script type="text/javascript">
        $(function () {
            $('#example3').datetimepicker({
                format: 'MM'
            });      
        });
    </script>
    <script type="text/javascript">
        $(function () {
            $('#example4').datetimepicker({
                format: 'YYYY'
            });      
        });
    </script>
    <script type="text/javascript">
        $(function () {
            $('#example5').datetimepicker({
                format: 'YYYY-MM-DD'
            });      
        });
    </script>
    <script type="text/javascript">
        $(function () {
            $('#example6').datetimepicker({
                format: 'YYYY-MM-DD'
            });      
        });
    </script>

