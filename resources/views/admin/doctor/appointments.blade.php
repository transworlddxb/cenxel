@include('admin.includes.head')
<link href="{{asset('assets/backend/css/darkbox.css')}}" rel="stylesheet">
<style type="text/css">
.photogallery li{position: relative; overflow: hidden; min-height: 200px; background: #eee; border: 2px solid #fff;}
.photogallery li:nth-child(odd){background: #bbb;}
.photogallery li img{position: absolute; top: -9999px; right: -9999px; bottom: -9999px; left: -9999px; margin: auto; max-width: none; /*max-height: 100%;*/ width: 100%; height: auto;}
</style>

<div class="slim-mainpanel">
  <div class="container">
    <div class="slim-pageheader">
      <ol class="breadcrumb slim-breadcrumb">
        <li class="breadcrumb-item"><a href="#">Home</a></li>
        <li class="breadcrumb-item">Today's appointments</li>
        {{-- <li class="breadcrumb-item active" aria-current="page">Form Wizards</li> --}}
      </ol>
      <h6 class="slim-pagetitle mb-2">Today's appointments</h6>
    </div><!-- slim-pageheader -->    


    <div class="section-wrapper">

      <div class="row">
        <div class="col-sm-12 col-lg-6">
          <label class="section-title"> Details</label>
          <p class="mg-b-20 mg-sm-b-40">List of Today's appointments</p></div>
          {{-- <div class="col-sm-12 col-lg-6">
            <div class="d-flex  justify-content-md-end justify-content-center">
              <a href="{{ url('/admin/reg_user') }}" class="btn btn-primary " style="margin-bottom: 10px;"><i class="fa fa-plus"></i> Click to Add new Appointment</a>
            </div>  
          </div> --}}
        </div>


        <div class="table-wrapper">
          <table id="productlistListTable" class="table display" width='100%'>
            <thead>
              <tr>
                <th >SI.NO</th>
                <th >FILE No.</th>
                <th >PATIENT NAME</th>
                <th >DOCTOR</th>
                <th >DATE</th>
                <th >TIME</th>
                <th >STATUS</th>
                <th >DISCOUNT REQUEST</th>
              </tr>
            </thead>
            <tbody>
                @foreach($appointment as $key => $appoint)
                    <tr>
                      <td >{{ $key+1 }}</td>
                      <td >{{ $appoint->file_no }}</td>
                      <td >{{ $appoint->appoint_patient }}</td>
                      <td >{{ $appoint->roll_name}}</td>
                      <td >{{ $appoint->appointment_date }}</td>
                      <td >{{ $appoint->appointment_time }}</td>
                      @if($appoint->complete_status==1)
                      <td style="color: red;">CUNSULTATION<br>NOT COMPLETED</td>
                      @endif
                      @if($appoint->complete_status==0)
                        <td style="color: green;">PENDING</td>
                        <td>
                            <a href="{{ url('/admin/open_appointment/'.$appoint->pk_int_appoint_id) }}" type="button" class="btn btn-primary" title="Open" value="Edit">Open</a>
                        </td>
                      @endif
                    {{--   @if($appoint->complete_status==1)
                      <td style="color: blue;text-align: center;">
                          <a type="button" class="edit-item btn btn-primary" style="color: white;" id="{{ $appoint->pk_int_appoint_id }}" data-toggle="modal" data-item-id=""><i class="fa fa-pencil"></i></a>
                        </td>
                      @endif --}}
                    {{-- <td> --}}
                        {{-- <a href="{{URL::to('/admin/delete-gst/'.$register->pk_int_reg_id)}}" type="button"  class="del btn btn-light waves-effect"><i class="fa fa-trash"></i></a> --}}
                        {{-- <a href="{{ url('/admin/edit_details/'.$appoint->pk_int_appoint_id) }}" type="button" class="btn btn-primary" title="Edit" value="Edit"><i class="fa fa-pencil"></i></a>&nbsp; --}}
                        {{-- <a href="{{ url('/admin/appointment/'.$appoint->pk_int_appoint_id) }}" type="button" class="btn btn-primary" title="Appointment" value="Edit">Appointment</a> --}}

                        {{-- <a type="button" class="edit-item btn btn-primary" style="color: white;" id="{{ $register->pk_int_reg_id }}" data-toggle="modal" data-item-id="" ><i class="fa fa-pencil"></i></a> --}}
                      
                    {{-- </td> --}}
                  </tr>
                  @endforeach
            </tbody>
          </table>
        </div>
      </div><!-- section-wrapper -->
    </div>

    @include('admin.includes.footer')
    </script>
    <script src="{{asset('assets/backend/lib/datatables/js/jquery.dataTables.js')}}"></script>
    <script src="{{asset('assets/backend/lib/select2/js/select2.min.js')}}"></script>
    <script src="{{asset('assets/backend/js/darkbox.min.js')}}"></script>
    
     @foreach ($appointment as $g) 
<div id="edit-modal" class="modal fade modal-min" >
  <div class="modal-dialog modal-dialog-vertical-center" role="document">
   <div class="modal-content bd-0 tx-14">
    <div class="modal-header pd-x-20">
      <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold">PAYMENT REMARKS</h6>
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <form role="form" action="{{ url('/admin/update_payment') }}" method="post">
      <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
      <div class="box-body col-md-12">
        <input type="hidden" name="id_id" id="id_id">
        <label>Total Amount</label>
        <div class="form-group"> 
          <input type="text" class="form-control" placeholder="Enter total amount" id="total_amount" name="total_amount" readonly>
        </div>
      </div>
      <div class="box-body col-md-12">
        <label>Previous due amount</label>
        <div class="form-group"> 
          <input type="number" class="form-control" placeholder="Enter discount amount" id="prev_due_amount" name="prev_due_amount" readonly>
        </div>
      </div>
      <div class="box-body col-md-12">
        <label>Discount Amount</label>
        <div class="form-group"> 
          <input type="number" class="form-control" placeholder="Enter discount amount" id="Discount_amount" name="Discount_amount">
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save changes</button>
      </div>
    </form>
  </div>
</div><!-- modal-dialog -->
</div><!-- modal -->
@endforeach

    <script type="text/javascript">
      $(document).on("click", ".del", function (event) {
    var result = confirm("Are you sure want to delete?");
    if (result) {
                  //Logic to delete the item
                  return true;
                }
                else
                {
                 return false;
               }
             });
  


      $('#productlistListTable').DataTable({
        scrollX:true,
        language: {
          searchPlaceholder: 'Search...',
          sSearch: '',
          lengthMenu: '_MENU_ items/page',
        },
        "ordering":false,
        "destroy": true,
        "processing": true,
      });
    </script>
    <script type="text/javascript">

     $(document).ready(function() {

       $(".edit-item").click(function()
       {
         var id=$(this).attr('id');
         $(this).attr('data-target','#edit-modal');
             // alert(id);
             jQuery.ajax({
              type: "get",
              url: 'paymnt_remrks/'+id,
                //data: {smsid:id},
                success: function(res)
                {
                   // alert(res);
                   var d1= res.split(',');
                   $("#id_id").val(d1[2]);
                   $("#prev_due_amount").val(d1[1]);
                   $("#total_amount").val(d1[0]);
                 }
               });
           });
     });
    </script>
