@include('admin.includes.head')
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.21.0/moment.min.js" type="text/javascript"></script>
<style type="text/css">
     input[type=number]::-webkit-inner-spin-button, 
   input[type=number]::-webkit-outer-spin-button { 
  -webkit-appearance: none; 
  margin: 0; 
}
</style>

<?php
$message=Session::get('message');
$msg=explode("|",$message);

?>




@if ($message = Session::get('message'))
<div  style='z-index:9999; position:absolute;width:30%;right:0px; padding:10px 15px 0px 15px;' id='msg'>
   <div class="alert alert-{{$msg[0]}}" role="alert">
     <button type="button" class="close" data-dismiss="alert" aria-label="Close">
       <span aria-hidden="true">&times;</span>
   </button>
   <strong>{{ ucfirst($msg[0]) }}!</strong>{{ $msg[1] }}
</div><!-- alert -->
</div>
@endif 
    <div class="slim-mainpanel">
        <div class="container">
            <div class="slim-pageheader">
                <ol class="breadcrumb slim-breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ url('/admin/dashboard') }}">Home</a></li>
                    <li class="breadcrumb-item">Consulting Data Entering</li>
                    {{-- <li class="breadcrumb-item active" aria-current="page">register</li> --}}
                </ol>
                <h6 class="slim-pagetitle mb-2">Consulting Data Entering</h6>
            </div><!-- slim-pageheader -->

            <div class="section-wrapper">
            <div class="row">
              <div class="col-sm-12 col-lg-6">
                <label class="section-title">Add Details</label>
                <p class="mg-b-20 mg-sm-b-40">The details of cunsultation</p></div>
                <div class="col-sm-12 col-lg-6">
                  <div class="d-flex  justify-content-md-end justify-content-center">
                    <a href="{{ url('/admin/pending_cunsult') }}" class="btn btn-primary " style="margin-bottom: 10px;"><i class="icon ion-arrow-left-a"></i> BACk</a>
                  </div>  
                </div>
              </div>
            <div class="form-layout">
            <form id="basic-form" method="post" action="{{url('/admin/store_cunsult_details/'.$appointment->pk_int_appoint_id)}}" enctype="multipart/form-data">
                            {{csrf_field()}}
               
            <div class="form-group">
                <div class="row">
                    <label class='col-md-3 control-label' style="margin-top:5px;text-align: right;">Discription if any<span class="tx-danger">*</span>: </label>
                        <div class="col-md-4">
                            <textarea class="form-control" type="text" name="consult_details" placeholder="enter consultation details"></textarea>

                        </div>
                </div>
            </div>
            <div class="form-group">
                <div class="row">
                         <label class='col-md-3 control-label' style="margin-top:5px;text-align: right;"> Treatment 1<span class="tx-danger">*</span>: </label>
                         <div class="col-md-4">
                             <select class="form-control select2" data-placeholder="Choose Doctor" name="treatment_one" >
                                    <option label="Choose Category"></option>
                                        @foreach($treatment as $key=>$cat) 
                                        <option value="{{$cat->pk_treatment_id}}">{{$cat->treatment_name}}</option>
                                        @endforeach
                                </select>
                             @if ($errors->has('treatment_one'))
                                <span class="help-block">
                                    <strong class="error">{{ $errors->first('treatment_one') }}</strong>
                                </span>
                            @endif

                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="row">
                         <label class='col-md-3 control-label' style="margin-top:5px;text-align: right;"> Treatment 2<span class="tx-danger">*</span>: </label>
                         <div class="col-md-4">
                             <select class="form-control select2" data-placeholder="Choose Doctor" name="treatment_two">
                                    <option label="Choose Category"></option>
                                       @foreach($treatment as $key=>$cat) 
                                        <option value="{{$cat->pk_treatment_id}}">{{$cat->treatment_name}}</option>
                                        @endforeach
                                </select>
                             @if ($errors->has('treatment_two'))
                                <span class="help-block">
                                    <strong class="error">{{ $errors->first('treatment_two') }}</strong>
                                </span>
                            @endif

                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="row">
                         <label class='col-md-3 control-label' style="margin-top:5px;text-align: right;"> Treatment 3<span class="tx-danger">*</span>: </label>
                         <div class="col-md-4">
                             <select class="form-control select2" data-placeholder="Choose Doctor" name="treatment_three" >
                                    <option label="Choose Category"></option>
                                       @foreach($treatment as $key=>$cat) 
                                        <option value="{{$cat->pk_treatment_id}}">{{$cat->treatment_name}}</option>
                                        @endforeach
                                </select>
                             @if ($errors->has('treatment_three'))
                                <span class="help-block">
                                    <strong class="error">{{ $errors->first('treatment_three') }}</strong>
                                </span>
                            @endif

                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="row">
                         <label class='col-md-3 control-label' style="margin-top:5px;text-align: right;"> Treatment 4<span class="tx-danger">*</span>: </label>
                         <div class="col-md-4">
                             <select class="form-control select2" data-placeholder="Choose Doctor" name="treatment_four" >
                                    <option label="Choose Category"></option>
                                       @foreach($treatment as $key=>$cat) 
                                        <option value="{{$cat->pk_treatment_id}}">{{$cat->treatment_name}}</option>
                                        @endforeach
                                </select>
                             @if ($errors->has('treatment_four'))
                                <span class="help-block">
                                    <strong class="error">{{ $errors->first('treatment_four') }}</strong>
                                </span>
                            @endif

                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="row">
                         <label class='col-md-3 control-label' style="margin-top:5px;text-align: right;"> Treatment 5<span class="tx-danger">*</span>: </label>
                         <div class="col-md-4">
                             <select class="form-control select2" data-placeholder="Choose Doctor" name="treatment_five">
                                    <option label="Choose Category"></option>
                                      @foreach($treatment as $key=>$cat) 
                                        <option value="{{$cat->pk_treatment_id}}">{{$cat->treatment_name}}</option>
                                        @endforeach
                                </select>
                             @if ($errors->has('treatment_five'))
                                <span class="help-block">
                                    <strong class="error">{{ $errors->first('treatment_five') }}</strong>
                                </span>
                            @endif

                    </div>
                </div>
            </div>
               {{-- <div class="form-group">
                    <div class="row">
                         <label class='col-md-3 control-label' style="margin-top:5px;text-align: right;"> Amount: </label>
                         <div class="col-md-4">
                             <input class="form-control"  type="number" name="amount" placeholder="Enter total amount" required>
                             @if ($errors->has('amount'))
                                <span class="help-block">
                                    <strong class="error">{{ $errors->first('amount') }}</strong>
                                </span>
                            @endif

                    </div>
                </div>
            </div> --}}

                <div class="form-group">
                <div class="row">
                         <label class='col-md-3 control-label' style="margin-top:5px;text-align: right;"></label>
                         <div class="col-md-4">
                           <button class="btn btn-primary bd-0">Submit</button>
                        <a href="{{ url('/admin/pending_cunsult') }}" class="btn btn-secondary bd-0">Back</a>

                    </div>
                </div>
            </div>

            </form>    
        </div><!-- section-wrapper -->
    </div>
    </div>


@include('admin.includes.footer')
{{-- <script type="text/javascript">
    $(function () {
      $('#example').datetimepicker({
        format: 'YYYY-MM-DD'
      });      
  });
     $(function () {
      $('#example1').datetimepicker({
        format: 'h:mm a'
      });      
  });
    setTimeout(function(){ $("#msg").fadeOut(2000); }, 2000);
</script> --}}
