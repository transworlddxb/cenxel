@include('admin.includes.head')

<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.21.0/moment.min.js" type="text/javascript"></script>

<style type="text/css">
     input[type=number]::-webkit-inner-spin-button, 
   input[type=number]::-webkit-outer-spin-button { 
  -webkit-appearance: none; 
  margin: 0; 
}
</style>

<?php
$message=Session::get('message');
$msg=explode("|",$message);
?>


@if ($message = Session::get('message'))
<div  style='z-index:9999; position:absolute;width:30%;right:0px; padding:10px 15px 0px 15px;' id='msg'>
   <div class="alert alert-{{$msg[0]}}" role="alert">
     <button type="button" class="close" data-dismiss="alert" aria-label="Close">
       <span aria-hidden="true">&times;</span>
   </button>
   <strong>{{ ucfirst($msg[0]) }}!</strong>{{ $msg[1] }}
</div><!-- alert -->
</div>
@endif


    <div class="slim-mainpanel">
        <div class="container">
            <div class="slim-pageheader">
                <ol class="breadcrumb slim-breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ url('/admin/dashboard') }}">Home</a></li>
                    <li class="breadcrumb-item">Users submission</li>
                    {{-- <li class="breadcrumb-item active" aria-current="page">Add-users</li> --}}
                </ol>
                <h6 class="slim-pagetitle mb-2">USERS</h6>
            </div><!-- slim-pageheader -->             

            <div class="section-wrapper">
                <div class="row">
                 <div class="col-sm-12 col-lg-6">
                   <label class="section-title">Add New</label>
                   <p class="mg-b-20 mg-sm-b-40">Please complete the form below,Fields marked with a <span class="tx-danger">*</span> are mandatory.</p></div>
                   <div class="col-sm-12 col-lg-6">
                     {{-- <div class="d-flex  justify-content-md-end justify-content-center">
                        <a href="{{ url('/admin/view_users') }}" class="btn btn-outline-primary mg-b-10" style="margin-bottom: 10px;"><i class="icon ion-person-stalker">&nbsp;&nbsp;List of user</i></a>
                    </div> --}}
                </div>
            </div>
            <form id="basic-form" method="post" action="{{url('/admin/add_users')}}" enctype="multipart/form-data">
            {{csrf_field()}}
                <div class="form-layout">
                    <div class="row mg-b-25">
                    
                        <div class="col-lg-3">
                            <div class="form-group mg-b-10-force">
                            <label class="form-control-label">Choose role: <span class="tx-danger">*</span></label>
                                <select class="form-control select2" data-placeholder="Choose role" name="users_role" required id="role_id">
                                    <option label="Choose role"></option>
                                    @foreach($role as $key=>$roles) 

                                    <option value="{{$roles->pk_int_role_id}}">{{$roles->roll_name}}</option>

                                    @endforeach

                                </select>
                                @if ($errors->has('users_role'))
                                <span class="help-block">
                                    <strong class="error">{{ $errors->first('users_role') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div><!-- col-8 -->
    
                        <div class="col-lg-3">
                            <div class="form-group">
                                <label class="form-control-label">Name: <span class="tx-danger">*</span></label>
                                <input class="form-control" type="text" name="user_name" placeholder="Enter name" required>
                                @if ($errors->has('user_name'))
                                <span class="help-block">
                                    <strong class="error">{{ $errors->first('user_name') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div><!-- col-4 -->

                        <div class="col-lg-3">
                            <div class="form-group">
                                <label class="form-control-label">Designation: <span class="tx-danger">*</span></label>
                                <input class="form-control" type="text" name="user_designation" placeholder="Enter designation" required>
                                @if ($errors->has('user_designation'))
                                <span class="help-block">
                                    <strong class="error">{{ $errors->first('user_designation') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div><!-- col-4 -->

                        {{-- <div class="col-lg-3">
                            <div class="form-group">
                                <label class="form-control-label">Email address: <span class="tx-danger">*</span></label>
                                <input class="form-control" type="email" name="users_email" placeholder="Enter email address" required>
                                @if ($errors->has('users_email'))
                                <span class="help-block">
                                    <strong class="error">{{ $errors->first('users_email') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div> --}}
                        <div class="col-lg-3">
                            <div class="form-group">
                                <label class="form-control-label">Password: <span class="tx-danger">*</span></label>
                                <input class="form-control" type="text" name="users_password" placeholder="Enter password" required>
                                @if ($errors->has('users_password'))
                                <span class="help-block">
                                    <strong class="error">{{ $errors->first('users_password') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div><!-- col-4 -->
                        

                    </div><!-- row -->

                    <div class="form-layout-footer">
                        <button type="submit" class="btn btn-primary bd-0">Submit</button>
                        {{-- <a href="{{ url('/admin/view_users') }}" class="btn btn-secondary bd-0">Back</a> --}}
                    </div><!-- form-layout-footer -->
                </div><!-- form-layout -->
            </form>    
<hr>
            <div class="table-wrapper" style="margin-top: 30px;">
                <table id="userListTables" class="table display" width='100%'>
                    <thead>
                        <tr>
                            <th>SI.NO</th>
                            <th>Roll NAME</th>
                            <th>DESIGNATION</th>
                            <th>ACTION</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($roles_show as $key => $user)
                        <tr>
                            <td>{{ $key+1 }}</td>
                            <td>{{ $user->roll_name }}</td>
                            <td>{{ $user->user_designation }}</td>
                                        <td>
                                            <div class="btn-group mb-2">
                                              @if($user->user_designation!="Admin")
                                                <a href="{{URL::to('/admin/delete_users/'.$user->id)}}"  type="button"  class="del btn btn-light waves-effect"><i class="fa fa-trash"></i></a>
                                                @endif

                                                <a type="button" class="edit-item btn btn-primary" style="color: white;" id="{{ $user->admin_role }}" data-toggle="modal" data-item-id="" {{-- data-target="#edit-modal" --}}><i class="fa fa-pencil"></i></a>

                                            </div>
                                        </td>
                                </tr>
                                @endforeach
                    </tbody>
                </table>
            </div>

        </div><!-- section-wrapper -->
    </div>

@include('admin.includes.footer')

<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
<script src="{{asset('assets/backend/lib/datatables/js/jquery.dataTables.js')}}"></script>
<script src="{{asset('assets/backend/lib/select2/js/select2.min.js')}}"></script>
<script type="text/javascript">
    $(function () {
      $('#example').datetimepicker({
        format: 'YYYY-MM-DD'
      });      
  });
    setTimeout(function(){ $("#msg").fadeOut(2000); }, 2000);
</script>

@foreach ($roles_show as $roles) 
<div id="edit-modal" class="modal fade modal-min" >
  <div class="modal-dialog modal-dialog-vertical-center" role="document">
   <div class="modal-content bd-0 tx-14">
    <div class="modal-header pd-x-20">
      <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold">Update Email Address</h6>
     <button type="button" class="close" data-dismiss="modal" aria-label="Close">
      <span aria-hidden="true">&times;</span>
    </button>
  </div>
  <form role="form" action="{{ url('/admin/update-user/'.$roles->id) }}" method="post">
    <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
    <div class="box-body col-md-12">
      <input type="hidden" name="user_id" id="user_id">
      <label></label>
      <div class="form-group"> 
        <input type="text" class="form-control" placeholder="Enter role" id="email" name="email">
      </div>
    </div>
    <div class="modal-footer">
      <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
      <button type="submit" class="btn btn-primary">Save changes</button>
    </div>
  </form>
</div>
</div><!-- modal-dialog -->
</div><!-- modal -->
@endforeach

<script type="text/javascript">

 $(document).ready(function() {

   $('#userListTables').on('click','.edit-item',function ()
   {
     var id=$(this).attr('id');
     $(this).attr('data-target','#edit-modal');
         // alert(id);
         jQuery.ajax({
          type: "get",
          url: 'edit-user/'+id,
            // data: {smsid:id},
            success: function(res)
            {
               // alert(res);
               var d1= res.split(',');
               $("#email").val(d1[1]);
               $("#user_id").val(d1[0]);
             }
           });
       });
 });
 $(document).on("click", ".del", function (event) {
    var result = confirm("Are you sure want to delete?");
    if (result) {
                  //Logic to delete the item
                  return true;
                }
                else
                {
                 return false;
               }
             });

  $('#userListTables').DataTable({
              scrollX:true,
              language: {
                searchPlaceholder: 'Search...',
                sSearch: '',
                lengthMenu: '_MENU_ items/page',
              },
              "ordering":false,
              "destroy": true,
              "processing": true,
            });
</script>


{{-- <script type="text/javascript">
    $("#role_id").change(function()
        {
            var rl = $("#role_id").val();
            // alert(rl);
            if(rl==1 || rl==3 || rl==4){
                $('#lat').hide();
                $('#long').hide();
                $('#sal').show();
            }
            else if(rl==2){
                $('#lat').hide();
                $('#long').hide();
                $('#sal').hide();
            }
            else{
                $('#lat').show();
                $('#long').show();
                $('#sal').hide();
            }
        });
</script> --}}