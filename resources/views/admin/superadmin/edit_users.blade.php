@include('admin.includes.head')
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.21.0/moment.min.js" type="text/javascript"></script>

<style type="text/css">
     input[type=number]::-webkit-inner-spin-button, 
   input[type=number]::-webkit-outer-spin-button { 
  -webkit-appearance: none; 
  margin: 0; 
}
</style>

<?php
$message=Session::get('message');
$msg=explode("|",$message);

?>


@if ($message = Session::get('message'))
<div  style='z-index:9999; position:absolute;width:30%;right:0px; padding:10px 15px 0px 15px;' id='msg'>
   <div class="alert alert-{{$msg[0]}}" role="alert">
     <button type="button" class="close" data-dismiss="alert" aria-label="Close">
       <span aria-hidden="true">&times;</span>
   </button>
   <strong>{{ ucfirst($msg[0]) }}!</strong>{{ $msg[1] }}
</div><!-- alert -->
</div>
@endif 


    <div class="slim-mainpanel">
        <div class="container">
            <div class="slim-pageheader">
                <ol class="breadcrumb slim-breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ url('/admin/dashboard') }}">Home</a></li>
                    <li class="breadcrumb-item"><a href="{{ url('/admin/view_users') }}">Users</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Edit</li>
                </ol>
                <h6 class="slim-pagetitle">USERS</h6>
            </div><!-- slim-pageheader -->           

            <div class="section-wrapper">
            <label class="section-title">Update users</label>
            <p class="mg-b-20 mg-sm-b-40">Please complete the form below.</p>
            <form id="basic-form" method="post" action="{{ url('/admin/update_users/'.$user->pk_int_users_id) }}" enctype="multipart/form-data">
                            {{csrf_field()}}
                <div class="form-layout">
                    <div class="row mg-b-25">
                        <div class="col-lg-4">
                            <div class="form-group mg-b-10-force">
                            <label class="form-control-label">Role: <span class="tx-danger">*</span></label>
                                {{-- <input class="form-control" type="text" name="street" placeholder="Enter street name" required value="{{ $user->users_street }}"> --}}
                                <select class="form-control select2" data-placeholder="Choose Role" name="users_role" required>
                                    <option label="Choose role"></option>
                                    @foreach($role as $key=>$roles) 

                                    <option value="{{$roles->pk_int_role_id}}" @if($user->users_role==$roles->pk_int_role_id){{  'selected' }} @endif>{{$roles->roll_name}}</option>

                                    @endforeach

                                </select>

                            </div>
                        </div><!-- col-4 -->
                        <div class="col-lg-4">
                            <div class="form-group">
                                <label class="form-control-label">Name: <span class="tx-danger">*</span></label>
                                <input class="form-control" type="text" name="users_name" placeholder="Enter full name" required="" value="{{ $user->users_name }}">
                                @if ($errors->has('users_name'))
                                <span class="help-block">
                                    <strong style="color:red">{{ $errors->first('users_name') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div><!-- col-4 -->
                        <div class="col-lg-4">
                            <div class="form-group">
                                <label class="form-control-label">Mobile number: <span class="tx-danger">*</span></label>
                                <input class="form-control" type="number" name="users_mobile" placeholder="Enter mobile number" required value="{{ $user->users_mobile }}" min="1000000000" max="9999999999" >
                                @if ($errors->has('users_mobile'))
                                <span class="help-block">
                                    <strong style="color:red">{{ $errors->first('users_mobile') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div><!-- col-4 -->
                       
                        
                        <div class="col-lg-12">
                            <div class="form-group mg-b-10-force">
                                <label class="form-control-label">Current address: <span class="tx-danger">*</span></label>
                                <textarea class="form-control" type="text" name="users_address" placeholder="Enter address" required >{{ $user->users_address }}</textarea>
                                @if ($errors->has('users_address'))
                                <span class="help-block">
                                    <strong style="color:red">{{ $errors->first('users_address') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div><!-- col-8 -->
                        <div class="col-lg-3">
                            <div class="form-group mg-b-10-force">
                            <label class="form-control-label">Area: <span class="tx-danger">*</span></label>
                                {{-- <input class="form-control" type="text" name="users_street" placeholder="Enter street name" required> --}}
                                <select class="form-control select2" data-placeholder="Choose Area" name="users_area_fk" required>
                                    <option label="Choose area"></option>
                                    @foreach($area as $key=>$ar)

                                    <option value="{{$ar->pk_int_area_id}}"  @if($user->users_area_fk==$ar->pk_int_area_id){{  'selected' }} @endif>{{$ar->vchr_area}}</option>

                                    @endforeach

                                </select>
                                @if ($errors->has('users_area_fk'))
                                <span class="help-block">
                                    <strong class="error">{{ $errors->first('users_area_fk') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div><!-- col-4 -->
                        <div class="col-lg-3">
                            <div class="form-group mg-b-10-force">
                            <label class="form-control-label">City: <span class="tx-danger">*</span></label>
                                <input class="form-control" type="text" name="users_city" placeholder="Enter city name" required value="{{ $user->users_city }}">
                            </div>
                        </div><!-- col-4 --> 
                         <div class="col-lg-3">
                            <div class="form-group mg-b-10-force">
                                <label class="form-control-label">Street: <span class="tx-danger">*</span></label>
                                <input class="form-control" type="text" name="users_street" placeholder="Enter street name" required value="{{ $user->users_street }}">
                                @if ($errors->has('users_street'))
                                <span class="help-block">
                                    <strong class="error">{{ $errors->first('users_street') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div><!-- col-8 -->
                        
                         
                         {{-- <div class="col-lg-4">
                            <div class="form-group">
                                <label class="form-control-label">Email address: <span class="tx-danger">*</span></label>
                                <input class="form-control" type="email" name="users_email" placeholder="Enter email address" required value="{{ $user->users_email }}">
                                @if ($errors->has('users_email'))
                                <span class="help-block">
                                    <strong style="color:red">{{ $errors->first('users_email') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div> --}}<!-- col-4 -->   
                        {{-- <div class="col-lg-4">
                            <div class="form-group">
                                <label class="form-control-label">Password: <span class="tx-danger">*</span></label>
                                <input class="form-control" type="text" name="users_password" placeholder="Enter password" required value="{{ $user->user_pass }}">
                                @if ($errors->has('users_password'))
                                <span class="help-block">
                                    <strong style="color:red">{{ $errors->first('users_password') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div> --}}<!-- col-4 -->                   
                        
                        <div class="col-lg-3">
                            <div class="form-group mg-b-10-force">
                            <label class="form-control-label">Choose SMS: <span class="tx-danger">*</span></label>
                                <select class="form-control select2" data-placeholder="Choose SMS" name="users_sms_group" required value="">
                                    <option label="Choose sms"></option>
                                    @foreach($smsgrp as $key=>$smsgrps) 

                                    <option value="{{$smsgrps->sgrp_pk_id}}"  @if($user->users_sms_group==$smsgrps->sgrp_pk_id){{  'selected' }} @endif >{{$smsgrps->sgrp_name}}</option>
                                    @endforeach
                                </select>
                                @if ($errors->has('users_sms_group'))
                                <span class="help-block">
                                    <strong style="color:red">{{ $errors->first('users_sms_group') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div><!-- col-4 -->
                 {{--        <div class="col-lg-4">
                            <div class="form-group">
                                <label class="form-control-label">Username: <span class="tx-danger">*</span></label>
                                <input class="form-control" type="text" name="username" placeholder="Enter username" required value="{{ $user->users_username }}">
                                @if ($errors->has('users_username'))
                                <span class="help-block">
                                    <strong style="color:red">{{ $errors->first('users_username') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div> --}}<!-- col-4 -->
                        
                       
                {{--         <div class="col-lg-6">
                            <div class="form-group mg-b-10-force">
                            <label class="form-control-label">date: <span class="tx-danger">*</span></label>
                                <input class="form-control" id="example"  type="text" name="date" placeholder="Select date"  value="{{ $user->users_date }}"> 
                                @if ($errors->has('users_date'))
                                <span class="help-block">
                                    <strong style="color:red">{{ $errors->first('users_date') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div> --}}<!-- col-4 -->
                        <div class="col-lg-4" id="lat">
                            <div class="form-group mg-b-10-force">
                            <label class="form-control-label">Latitude: <span class="tx-danger">*</span></label>
                                <input class="form-control" type="number" name="users_latitude" placeholder="Enter latitude" value="{{ $user->users_latitude }}">
                                @if ($errors->has('users_latitude'))
                                <span class="help-block">
                                    <strong style="color:red">{{ $errors->first('users_latitude') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div><!-- col-4 -->
                        <div class="col-lg-4" id="long">
                            <div class="form-group mg-b-10-force">
                            <label class="form-control-label">Longitude: <span class="tx-danger">*</span></label>
                                <input class="form-control" type="number" name="users_longitude" placeholder="Enter longitude" value="{{ $user->users_longitude }}">
                                @if ($errors->has('users_longitude'))
                                <span class="help-block">
                                    <strong style="color:red">{{ $errors->first('users_longitude') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div><!-- col-4 -->
                        <div class="col-lg-4" id="sal">
                            <div class="form-group mg-b-10-force">
                            <label class="form-control-label">Salary: <span class="tx-danger">*</span></label>
                                <input class="form-control" type="number" name="salary" placeholder="Enter salary" value="{{ $user->salary }}">
                                @if ($errors->has('salary'))
                                <span class="help-block">
                                    <strong class="error">{{ $errors->first('salary') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div><!-- col-4 -->
                    </div><!-- row -->

                    <div class="form-layout-footer">
                        <button type="submit" class="btn btn-primary bd-0">Submit</button>
                        <a href="{{ url('/admin/view_users') }}" class="btn btn-secondary bd-0">Back</a>
                    </div><!-- form-layout-footer -->
                </div><!-- form-layout -->
            </form>    
        </div><!-- section-wrapper -->
    </div>

@include('admin.includes.footer')

<script type="text/javascript">
    $(function () {
      $('#example').datetimepicker({
        format: 'YYYY-MM-DD'
      });      
  });
    setTimeout(function(){ $("#msg").fadeOut(2000); }, 2000);
</script>
<script type="text/javascript">
    var id = {{$user->users_role}};
    $(document).ready(function() {
        // alert(id);
        if(id==5){
            $('#lat').show();
            $('#long').show();
            $('#sal').hide();
        }
        else{
            $('#lat').hide();
            $('#long').hide();
            $('#sal').show();
        }
    });
</script>