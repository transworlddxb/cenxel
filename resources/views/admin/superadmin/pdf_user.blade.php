<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title></title>
  </head>
  <body>
    <center><h3>DETAILS OF {{$role}}</h3></center>

    <table id="userListTable" class="table display" width='100%' border="1">
          <thead>
            <tr>
              <th class="wd-10p">SI.NO</th>
              <th >PROFILE</th>
              <th >ADDRESS</th>
              <th class="wd-10p">SMS</th>
              <th>LOCATION</th>
            </tr>
          </thead>
          <tbody>
            @foreach($users as $key => $user)
            <tr>
                <td>{{ $key+1 }}</td>
                <td>{{ $user->users_name }}<br>
                    {{ $user->users_email }}<br>
                    {{ $user->users_mobile }}
                </td>
                <td>{{ $user->vchr_area }}<br>
                    {{ $user->users_address }}<br>
                  {{ $user->users_street }}<br>
                  {{ $user->users_city }}
                </td>
                <td>{{ $user->sgrp_name }}</td>
                <td>Lat:{{ $user->users_latitude }}<br>
                    Lng:{{ $user->users_longitude }}</td>
            </tr>
            @endforeach
          </tbody>
        </table>
  </body>
</html>