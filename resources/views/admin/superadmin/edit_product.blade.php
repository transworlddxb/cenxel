@include('admin.includes.head')
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.21.0/moment.min.js" type="text/javascript"></script>


<?php
$message=Session::get('message');
$msg=explode("|",$message);

?>


@if ($message = Session::get('message'))
<div  style='z-index:9999; position:absolute;width:30%;right:0px; padding:10px 15px 0px 15px;' id='msg'>
   <div class="alert alert-{{$msg[0]}}" role="alert">
     <button type="button" class="close" data-dismiss="alert" aria-label="Close">
       <span aria-hidden="true">&times;</span>
   </button>
   <strong>{{ ucfirst($msg[0]) }}!</strong>{{ $msg[1] }}
</div><!-- alert -->
</div>
@endif 
    <div class="slim-mainpanel">
        <div class="container">
            <div class="slim-pageheader">
                <ol class="breadcrumb slim-breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ url('/admin/dashboard') }}">Home</a></li>
                    <li class="breadcrumb-item"><a href="{{ url('/admin/view_product') }}">Products</a></li>
                    <li class="breadcrumb-item active" aria-current="page">edit</li>
                </ol>
                <h6 class="slim-pagetitle mb-2">Products</h6>
            </div><!-- slim-pageheader -->

            <div class="section-wrapper">
            <label class="section-title">Update Details</label>
            <p class="mg-b-20 mg-sm-b-40">Please complete the form below,Fields marked with a <span class="tx-danger">*</span> are mandatory.</p></p>

            <div class="form-layout">
            <form id="basic-form" method="post" action="{{ url('/admin/update_product/'.$prod->prod_pk_id) }}" enctype="multipart/form-data">
                            {{csrf_field()}}
                <div class="form-group">
                    <div class="row">
                         <label class='col-md-3 control-label' style="margin-top:5px;text-align: right;"><b> Date of Product: </b></label>
                         <div class="col-md-4">
                             <input class="form-control" id="example"  type="text" name="date" placeholder="Select date" required value="{{ $prod->prod_date }}">
                             @if ($errors->has('date'))
                                <span class="help-block">
                                    <strong class="error">{{ $errors->first('date') }}</strong>
                                </span>
                            @endif

                    </div>
                </div>
            </div>
                
                <div class="form-group">
                <div class="row">
                         <label class='col-md-3 control-label' style="margin-top:5px;text-align: right;"><b> Choose Product Category<span class="tx-danger">*</span>:</b></label>
                         <div class="col-md-4">
                             <select class="form-control select2" data-placeholder="Choose Category" name="category" required>
                                    {{-- <option label="Choose Category"></option> --}}
                                        <option label="Choose Category"></option>
                                    @foreach($prodcat as $key=>$pr) 

                                    <option value="{{$pr->prodcat_pk_id}}" @if($prod->prod_category==$pr->prodcat_pk_id){{  'selected' }} @endif >{{$pr->prodcat_name}}</option>

                                    @endforeach
                                </select>
                             @if ($errors->has('date'))
                                <span class="help-block">
                                    <strong class="error">{{ $errors->first('date') }}</strong>
                                </span>
                            @endif

                    </div>
                </div>
            </div>
                <div class="form-group">
                <div class="row">
                         <label class='col-md-3 control-label' style="margin-top:5px;text-align: right;"><b> Title of Product:<span class="tx-danger">*</span>: </b></label>
                         <div class="col-md-4">
                            <input class="form-control" type="text" name="title" placeholder="Enter title of product" required value="{{ $prod->prod_title }}">
                                @if ($errors->has('title'))
                                <span class="help-block">
                                    <strong class="error">{{ $errors->first('title') }}</strong>
                                </span>
                                @endif

                    </div>
                </div>
            </div>
                <div class="form-group">
                <div class="row">
                         <label class='col-md-3 control-label' style="margin-top:5px;text-align: right;"><b> Quantity:<span class="tx-danger">*</span>: </b></label>
                         <div class="col-md-4">
                            <input class="form-control" type="number" name="quantity" placeholder="Enter Quantity" required value="1" disabled>
                                @if ($errors->has('quantity'))
                                <span class="help-block">
                                    <strong class="error">{{ $errors->first('quantity') }}</strong>
                                </span>
                                @endif

                    </div>
                </div>
                </div>
                <div class="form-group">
                <div class="row">
                         <label class='col-md-3 control-label' style="margin-top:5px;text-align: right;"><b> Product Unit:<span class="tx-danger">*</span>: </b></label>
                         <div class="col-md-4">
                            <select class="form-control select2" data-placeholder="Choose unit" name="unit" required >

                                    <option label="Choose unit"></option>
                                        @foreach($units as $key=>$unit) 

                                    <option value="{{$unit->carry_unit_id}}" @if($prod->prod_unit==$unit->carry_unit_id){{  'selected' }} @endif>{{$unit->carry_units}}</option>

                                    @endforeach

                                    {{-- @endforeach --}}
                                </select>
                                {{-- <input class="form-control" type="text" name="unit" placeholder="Enter Unit" required> --}}
                                @if ($errors->has('unit'))
                                <span class="help-block">
                                    <strong class="error">{{ $errors->first('unit') }}</strong>
                                </span>
                                @endif

                    </div>
                </div>
            </div>  
                <div class="form-group">
                <div class="row">
                         <label class='col-md-3 control-label' style="margin-top:5px;text-align: right;"><b> Price of Product:<span class="tx-danger">*</span>: </b></label>
                         <div class="col-md-4">
                            <input class="form-control" type="number" name="price" placeholder="Enter Price" required value="{{ $prod->prod_price }}">
                                @if ($errors->has('price'))
                                <span class="help-block">
                                    <strong class="error">{{ $errors->first('price') }}</strong>
                                </span>
                                @endif

                    </div>
                </div>
            </div>
                <div class="form-group">
                <div class="row">
                         <label class='col-md-3 control-label' style="margin-top:5px;text-align: right;"><b> Upload Images  of Product:<span class="tx-danger">*</span>: </b></label>
                         <div class="col-md-4">

                            {{-- <input type="file" multiple class="custom-file-input" id="customFile" name="image[]"> --}}
                            <input type="file" id="exampleInputFile" name="image[]" multiple>
                                {{--  <table cellspacing="10">
                                <tr>
                                    <td>
                                        <img src="{{ asset('/uploads/product/'.$prod->prod_image1)}}" width="100"> 
                                    </td>       
                                    <td>    
                                        <img src="{{ asset('/uploads/product/'.$prod->prod_image2)}}" width="100">     
                                    </td>
                                </tr>
                            </table> --}}

                    </div>
                </div>
                </div>
                <div class="form-group">
                <div class="row">
                         <label class='col-md-3 control-label' style="margin-top:5px;text-align: right;"><b> Description of Product:<span class="tx-danger">*</span>: </b></label>
                         <div class="col-md-4">
                            <textarea class="form-control" type="text" name="description" placeholder="Enter product description" required>{{ $prod->prod_description }}</textarea>
                                @if ($errors->has('description'))
                                <span class="help-block">
                                    <strong class="error">{{ $errors->first('description') }}</strong>
                                </span>
                                @endif

                    </div>
                </div>
                </div>
                <div class="form-group">
                <div class="row">
                         <label class='col-md-3 control-label' style="margin-top:5px;text-align: right;"></label>
                         <div class="col-md-4">
                           <button class="btn btn-primary bd-0">Submit</button>
                        <a href="{{ url('/admin/view_product') }}" class="btn btn-secondary bd-0">Back</a>

                    </div>
                </div>
            </div>

            </form>    
        </div><!-- section-wrapper -->
    </div>
    </div>


@include('admin.includes.footer')

<script type="text/javascript">
    $(function () {
      $('#example').datetimepicker({
        format: 'YYYY-MM-DD'
      });      
  });
    setTimeout(function(){ $("#msg").fadeOut(2000); }, 2000);
</script>