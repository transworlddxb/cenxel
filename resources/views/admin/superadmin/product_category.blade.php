@include('admin.includes.head')
<?php
$message=Session::get('message');
$msg=explode("|",$message);

?>


@if ($message = Session::get('message'))
<div  style='z-index:9999; position:absolute;width:30%;right:0px; padding:10px 15px 0px 15px;' id='msg'>
 <div class="alert alert-{{$msg[0]}}" role="alert">
   <button type="button" class="close" data-dismiss="alert" aria-label="Close">
     <span aria-hidden="true">&times;</span>
   </button>
   <strong>{{ ucfirst($msg[0]) }}!</strong>{{ $msg[1] }}
 </div><!-- alert -->
</div>
@endif
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css">
<div class="slim-mainpanel">
  <div class="container">
    <div class="slim-pageheader">
      <ol class="breadcrumb slim-breadcrumb">
        <li class="breadcrumb-item"><a href="{{ url('/admin/dashboard') }}">Home</a></li>
        <li class="breadcrumb-item">Product Category</li>
        {{-- <li class="breadcrumb-item active" aria-current="page">Product Category</li> --}}
      </ol>
      <h6 class="slim-pagetitle">Product Category</h6>
    </div><!-- slim-pageheader -->

    <div class="section-wrapper mg-t-20">
      <div id="wizard2">
        <section>
          <div class="row">
            <div class="col-md-4">
              <label class="section-title">new category</label>

           <p class="mg-b-20 mg-sm-b-40">Add category of products</p>
                <div class="card-box">
                  <form class="form-horizontal" method="post" action="{{url('/admin/add_prod_cat')}}" enctype="multipart/form-data">
                    {{csrf_field()}}
                    {{-- <h4 class="header-title m-t-0 m-b-20">Add Product category</h4> --}}
                    <div>
                      <input type="text" id="autocomplete-ajax" class="form-control" placeholder="Enter Product category" required name="prodcat_name">
                      @if ($errors->has('prodcat_name'))
                      <span class="help-block">
                          <strong style="color:red">{{ $errors->first('prodcat_name') }}</strong>
                      </span>
                      @endif
                      
                      <select class="form-control select2" data-placeholder="Choose Role" name="prodcat_gst" required style="margin-top: 10px;">
                      <option label="Choose Gst value"></option>
                          @foreach($gst as $key=>$g) 

                            <option value="{{$g->gst_value}}">{{$g->gst_value}}</option>

                        @endforeach

                    </select>
                      @if ($errors->has('prodcat_gst'))
                      <span class="help-block">
                          <strong style="color:red">{{ $errors->first('prodcat_gst') }}</strong>
                      </span>
                      @endif

                  </div>
                  <button style="margin-top: 20px; background-color: #1B84E7; color: white; margin-left: 100px; margin-bottom: 10px;" class="btn btn-custom waves-light waves-effect" type="submit">Submit</button>
              </form>
          </div>
      </div>
      <div class="col-md-8">
        <div class="section-wrapper">
            <label class="section-title">Product Categories</label>
            <hr style="margin:5px 0px 12px 0px;background-color:#dee2e6;">

            <div class="table-wrapper">
              <table id="prodListTable" class="table display " width='100%'>
                <thead>
                  <tr>
                    <tr>
                        <th>SI.NO</th>
                        <th>Category</th>
                        <th>Product GST</th>
                        <th><center>ACTION</center></th>
                    </tr>
                </tr>
            </thead>
            <tbody>
                @foreach($prodcat as $key => $prod)
                <tr>
                  <td>{{ $key+1 }}</td>
                  <td>{{ $prod->prodcat_name }}</td>
                  <td>{{ $prod->prodcat_gst }}</td>
                  <td style="width: 200px;"><center>

                    {{-- <button data-id="{!! $prod->prodcat_pk_id !!}" type="button"  class="del btn btn-light waves-effect"><i class="fa fa-trash"></i></button> --}}
                    <a href="{{URL::to('/admin/delete_prod_cat/'.$prod->prodcat_pk_id)}}" type="button"  class="del btn btn-light waves-effect"><i class="fa fa-trash"></i></a>
                    {{-- <button type="button" class="btn btn-success edit-item" id="{{ $prod->pk_int_role_id }}" data-toggle="modal" data-item-id="">Edit</button> --}}

                    <a type="button" class="edit-item btn btn-primary" id="{{ $prod->prodcat_pk_id }}" data-toggle="modal" data-item-id="" style="color: white;"><i class="fa fa-pencil"></i></a>
                </center>
            </td>
        </tr>
        @endforeach             
    </tbody>
</table>
</div><!-- table-wrapper -->
</div><!-- section-wrapper -->    
</div>
</div>
</section>
</div>
</div><!-- section-wrapper -->

</div><!-- container -->


@include('admin.includes.footer')

@foreach ($prodcat as $prod) 
<div id="edit-modal" class="modal fade modal-min" >
  <div class="modal-dialog modal-dialog-vertical-center" role="document">
   <div class="modal-content bd-0 tx-14">
    <div class="modal-header pd-x-20">
     <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold">Update</h6>
     <button type="button" class="close" data-dismiss="modal" aria-label="Close">
      <span aria-hidden="true">&times;</span>
    </button>
  </div>
      <form role="form" action="{{ url('/admin/update-prod/'.$prod->prodcat_pk_id) }}" method="post">
        <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
        <div class="box-body col-md-12">
            <input type="hidden" name="user_id" id="user_id">
            <label></label>
            <div class="form-group"> 
                <input type="text" class="form-control" placeholder="Enter prod group" id="prodcat_name" name="prodcat_name">
                <select class="form-control select2" data-placeholder="Choose Role" id="prodcat_gst" name="prodcat_gst" required style="margin-top: 10px;">
                      <option label="Choose Gst value"></option>
                          @foreach($gst as $key=>$g) 

                            <option value="{{$g->gst_value}}">{{$g->gst_value}}</option>

                        @endforeach

                    </select>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary">Save changes</button>
        </div>
    </form>
</div>
</div><!-- modal-dialog -->
</div><!-- modal -->
@endforeach

<script type="text/javascript">

  $(document).ready(function() {

    // $(".edit-item").click(function()
    $('#prodListTable').on('click','.edit-item',function ()
   {
      var id=$(this).attr('id');
      $(this).attr('data-target','#edit-modal');
      // alert(id);
      jQuery.ajax({
       type: "get",
       url: 'prod-cat-update/'+id,
       // data: {smsid:id},
       success: function(res)
       {
          // alert(res);
          var d1= res.split(',');
          $("#prodcat_gst").val(d1[2]);
          $("#prodcat_name").val(d1[1]);
          $("#user_id").val(d1[0]);
        }
      });
    });
  })
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
<script src="{{asset('assets/backend/lib/datatables/js/jquery.dataTables.js')}}"></script>
<script src="{{asset('assets/backend/lib/select2/js/select2.min.js')}}"></script>
<script type="text/javascript">
  
  $('#prodListTable').DataTable({
    scrollX:true,
    language: {
      searchPlaceholder: 'Search...',
      sSearch: '',
      lengthMenu: '_MENU_ items/page',
    },
    "ordering":false,
    "destroy": true,
    "processing": true,
  });

  setTimeout(function(){ $("#msg").fadeOut(2000); }, 2000);


  $(document).on("click", ".del", function (event) {
    var result = confirm("Are you sure want to delete?");
    if (result) {
                  //Logic to delete the item
                  return true;
                }
                else
                {
                 return false;
               }
             });
           </script>