@include('admin.includes.head')
<meta name="csrf-token" content="{{ csrf_token() }}">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css">
<?php
$message=Session::get('message');
$msg=explode("|",$message);
?>
<style type="text/css">
.privilege_menu li{
    padding: 10px 0;
    border-bottom: 1px solid #ccc;

}
</style>

@if ($message = Session::get('message'))
<div  style='z-index:9999; position:absolute;width:30%;right:0px; padding:10px 15px 0px 15px;' id='msg'>
 <div class="alert alert-{{$msg[0]}}" role="alert">
   <button type="button" class="close" data-dismiss="alert" aria-label="Close">
     <span aria-hidden="true">&times;</span>
 </button>
 <strong>{{ ucfirst($msg[0]) }}!</strong>{{ $msg[1] }}
</div><!-- alert -->
</div>
@endif


<div class="slim-mainpanel">
  <div class="container">
    <div class="slim-pageheader">
      <ol class="breadcrumb slim-breadcrumb">
        <li class="breadcrumb-item"><a href="{{ url('/admin/dashboard') }}">Home</a></li>
        <li class="breadcrumb-item">Previlege Managment</li>
        {{-- <li class="breadcrumb-item active" aria-current="page">Form Wizards</li> --}}
    </ol>
    <h6 class="slim-pagetitle">Previlege Managment</h6>
</div><!-- slim-pageheader -->



<div class="section-wrapper mg-t-20">
  <section>
    <div class="row">
          <div class="col-md-4">
              <label class="section-title">Set Previlege</label>
              <p class="mg-b-20 mg-sm-b-40">Select role, menu and sub-menu. </p>

        <select class="form-control select2" data-placeholder="Choose role" name="role" required  id="role">
            <option label="Choose role"></option>
            @foreach($role as $key=>$roles) 

            <option value="{{$roles->pk_int_role_id}}">{{$roles->roll_name}}</option>

            @endforeach

        </select>
        <div class="card-box">
            <ul class="privilege_menu">
                <input type="hidden"  name="menu" id="menuid">
                @foreach($category as $key => $menu_user)
                <li><a href="#" id="{{ $menu_user->mnu_cat_pk_id }}" class="mnu">
                    {{ $menu_user->mnu_cat_name }}</a></li>                    
                    @endforeach     
                </ul>
            {{--  <select class="form-control select2 mnu2" data-placeholder="Choose role" name="menu" required id="menuid" style="margin-top: 10px;">
                    <option> -- Select Menu -- </option>
                    @foreach($category as $key => $menu_user)

                    <option value="{{ $menu_user->mnu_cat_pk_id }}">{{ $menu_user->mnu_cat_category }}</option>

                    @endforeach

                </select> --}}

            </div>
        </div>
        <div class="col-md-8">
            <div class="section-wrapper">
                <div class="row justify-content">
                    <div class="section-title col-md-6" >
                        <label class="secion-title">Sub-menu List</label>

                    </div>

                    <div class="col-md-6 text-right">

                      <button class="btn btn-primary mb-2" id="set_privilege" type="button" >Set</button>
                  </div>

              </div>
                  <hr style="margin:5px 0px 12px 0px;background-color:#dee2e6;">
              <div class="table-wrapper">
                <table id="userListTable" class="table display" width='100%'>
                  <thead>
                    <tr>
                      <tr>
                        <th>SI.NO</th>
                        <th>SI.NO</th>
                        <th>ROLE</th>
                        <th><center>ACTION</center></th>
                    </tr>
                </tr>
            </thead>
            <tbody>

            </tbody>
        </table>
    </div><!-- table-wrapper -->
</div><!-- section-wrapper -->    
</div>
</div>
</section>
</div>


</div><!-- container -->

@include('admin.includes.footer')

<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
<script src="{{asset('assets/backend/lib/datatables/js/jquery.dataTables.js')}}"></script>
<script src="{{asset('assets/backend/lib/select2/js/select2.min.js')}}"></script>



<script type="text/javascript">

    var  id="1";

    function get_datatable_data(id,roleid)
    {
        var url1="{{url('/')}}";

        $('#userListTable').DataTable({
        scrollX:true,
        language: {
          searchPlaceholder: 'Search...',
          sSearch: '',
          lengthMenu: '_MENU_ items/page',
        },
        "ordering":false,
        "destroy": true,
        "processing": true,



        "ajax": {
            url :url1+'/admin/submenu_list/'+id+"/"+roleid// json datasource
            },

            "columnDefs":[
            {"width":"45px","targets":0},],

            "columns": [
            {"data": "check"},
            { "data": "id"},
            { "data": "menu"},
            { "data": "submenu"},

            ],

        });
    }


    $('.mnu').click(function(event) {

        var id=$(this).attr('id');
        var roleid=$("#role").val();
        $('#menuid').val(id);
        get_datatable_data(id,roleid);
        // alert(roleid);
    });



    jQuery('#master').on('click', function(e) {
        if($(this).is(':checked',true))
        {
            $(".sub_chk").prop('checked', true);
            $(".sub_chk").closest('tr').toggleClass('selected');
        }
        else
        {
            $(".sub_chk").prop('checked',false);
            $(".sub_chk").closest('tr').removeClass('selected');
        }
    });


    $('#userListTable tbody').on( 'click', '.sub_chk', function ()
    {
        $(this).closest('tr').toggleClass('selected');
    });


    $.ajaxSetup({
        headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $("#set_privilege").click(function(e){
    
        e.preventDefault();

        var role = $("#role").val();

        var menu =  $('#menuid').val();

            var subid="";

            $.each($("#userListTable tr.selected"),function()
            {
              subid+=","+$(this).find('td').eq(1).text();

            });  
            // alert(menu);
              jQuery.ajax({
              type: "post",
              url: "{{url('/admin/users_privalege')}}",
              dataType: 'JSON',
              data: {role:role, menu:menu, submenu: subid},
              success: function(data)
              {
                swal("success !", "Menu set successfully","");
               
             }
            });
        });

         //    
    setTimeout(function(){ $("#msg").fadeOut(2000); }, 2000);
</script>