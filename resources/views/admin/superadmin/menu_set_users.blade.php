@include('admin.includes.head')

<?php
$message=Session::get('message');
$msg=explode("|",$message);
?>

@if ($message = Session::get('message'))
<div  style='z-index:9999; position:absolute;width:30%;right:0px; padding:10px 15px 0px 15px;' id='msg'>
   <div class="alert alert-{{$msg[0]}}" role="alert">
     <button type="button" class="close" data-dismiss="alert" aria-label="Close">
       <span aria-hidden="true">&times;</span>
   </button>
   <strong>{{ ucfirst($msg[0]) }}!</strong>{{ $msg[1] }}
</div><!-- alert -->
</div>
@endif 

<div class="slim-mainpanel">
	<div class="container">
		<div class="slim-pageheader">
			<ol class="breadcrumb slim-breadcrumb">
				<li class="breadcrumb-item"><a href="{{ url('/admin/dashboard') }}">Home</a></li>
				<li class="breadcrumb-item">Bottle Assigned</li>
				{{-- <li class="breadcrumb-item active" aria-current="page">Form Wizards</li> --}}
			</ol>
			<h6 class="slim-pagetitle mb-2">Bottle Assigned</h6>
		</div><!-- slim-pageheader -->		
		
		<div class="section-wrapper">
			<div class="row">
			   <div class="col-sm-12 col-lg-6">
			     <label class="section-title">Details</label>
			     <p class="mg-b-20 mg-sm-b-40">List of assigned details of bottles.</p></div>
			     
			</div>

			<div class="table-wrapper">
				<table id="userListTable" class="table display" width='100%'>
					<thead>
						<tr>
							<th class="wd-15p">SI.NO</th>
							<th class="wd-15p">CUSTOMER</th>
							<th class="wd-15p">AREA</th>
							<th class="wd-20p">BOTTLE NO:</th>
							{{-- <th class="wd-20p">UNIT</th> --}}
						</tr>
					</thead>
					<tbody>
					@foreach($menu_users as $key=> $menu_user) 
						<tr>
							<td>{{ $key+1 }}</td>
							<td>{{$menu_user->user_mnu_user_id}}</td>
							{{-- <td>{{$menu_user->user_mnu_category}}</td> --}}
							<td>{{$menu_user->user_mnu_sub}}</td>
						</tr>
					@endforeach
					</tbody>
				</table>
			</div>
		</div><!-- section-wrapper -->
	</div>

	@include('admin.includes.footer')


	<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
	<script src="{{asset('assets/backend/lib/datatables/js/jquery.dataTables.js')}}"></script>
	<script src="{{asset('assets/backend/lib/select2/js/select2.min.js')}}"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/alertify.js/0.5.0/alertify.min.js"></script>
	
	<script type="text/javascript">

		$(document).on("click", ".deleteImg", function (event) {
			var result = confirm("Are you sure want to delete?");
			if (result) {
			            //Logic to delete the item
			            return true;
			        }
			        else
			        {
			        	return false;
			        }
			    });

		$('#userListTable').DataTable({
						scrollX:true,
						   language: {
							searchPlaceholder: 'Search...',
							sSearch: '',
							lengthMenu: '_MENU_ items/page',
						  },
						"ordering":false,
						"destroy": true,
						"processing": true,
					});
				
				setTimeout(function(){ $("#msg").fadeOut(2000); }, 2000);

	</script>



