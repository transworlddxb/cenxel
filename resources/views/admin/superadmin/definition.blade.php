@include('admin.includes.head')

<?php
$message=Session::get('message');
$msg=explode("|",$message);
?>

@if ($message = Session::get('message'))
<div  style='z-index:9999; position:absolute;width:30%;right:0px; padding:10px 15px 0px 15px;' id='msg'>
	<div class="alert alert-{{$msg[0]}}" role="alert">
		<button type="button" class="close" data-dismiss="alert" aria-label="Close">
			<span aria-hidden="true">&times;</span>
		</button>
		<strong>{{ ucfirst($msg[0]) }}!</strong>{{ $msg[1] }}
	</div><!-- alert -->
</div>
@endif 

<div class="slim-mainpanel">
	<div class="container">
		<div class="slim-pageheader">
			<ol class="breadcrumb slim-breadcrumb">
				{{-- <li class="breadcrumb-item"><a href="{{ url('/admin/dashboard') }}">Home</a></li> --}}
				{{-- <li class="breadcrumb-item"><a href="#">Order</a></li> --}}
				{{-- <li class="breadcrumb-item active" aria-current="page">Form Wizards</li> --}}
			</ol>
			<h6 class="slim-pagetitle mb-2">Definitions to roles</h6>
		</div><!-- slim-pageheader -->		
		
		<div class="section-wrapper">
			<label class="slim-pagetitle mb-2">Super Admin</label>
			<p class="mg-b-20 mg-sm-b-40" style="font-size: 17px;">The super admin has the whole access to the daily process. Admin can only add a user, staff, delivery boy, etc.. so while the registration process is completed the admin should add their wallet amount. this wallet is only for users. Next step is to purchase the carrying bottles and other products. hence we can assign bottles to users as we purchased.  Then admin should add the product to view on the user screen for purchasing.
			also, admin can view every order and also they can view present-day orders also.  Admin can view the delivery list also.</p>

			<label class="slim-pagetitle mb-2">Staff</label>
			<p class="mg-b-20 mg-sm-b-40"style="font-size: 17px;">Staff can see the daily order which all are pending to pack. also, they should verify the packed items. then only the packed items will list in the delivery boy dashboard.</p>

			<label class="slim-pagetitle mb-2">Delivery boy</label>
			<p class="mg-b-20 mg-sm-b-40"style="font-size: 17px;">Delivery boy can view their current day orders which they should deliver. after each delivery, they should verify that they have delivered successfully and also they should mark if the received bottle is broken or not. then the delivered items can be viewed in the delivered menu list</p>

			<label class="slim-pagetitle mb-2">Vendor</label>
			<p class="mg-b-20 mg-sm-b-40"style="font-size: 17px;">Vendors can view the history of purchased items from them. and the report also is viewed there.</p>
			

			
		</div><!-- section-wrapper -->
	</div>

	@include('admin.includes.footer')

	<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
	<script src="{{asset('assets/backend/lib/datatables/js/jquery.dataTables.js')}}"></script>
	<script src="{{asset('assets/backend/lib/select2/js/select2.min.js')}}"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/alertify.js/0.5.0/alertify.min.js"></script>
	
	<script type="text/javascript">

		$(document).on("click", ".deleteImg", function (event) {
			var result = confirm("Are you sure want to delete?");
			if (result) {
			            //Logic to delete the item
			            return true;
			        }
			        else
			        {
			        	return false;
			        }
			    });

		$('#userListTable').DataTable({
			scrollX:true,
			language: {
				searchPlaceholder: 'Search...',
				sSearch: '',
				lengthMenu: '_MENU_ items/page',
			},
			"ordering":false,
			"destroy": true,
			"processing": true,
		});
		
		setTimeout(function(){ $("#msg").fadeOut(2000); }, 2000);

	</script>