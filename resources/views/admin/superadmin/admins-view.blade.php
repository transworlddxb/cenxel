@include('admin.includes.head')
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/alertify.js/0.5.0/alertify.core.css">
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/alertify.js/0.5.0/alertify.default.min.css">

<?php
$message=Session::get('message');
$msg=explode("|",$message);

?>


@if ($message = Session::get('message'))
<div  style='z-index:9999; position:absolute;width:30%;right:0px; padding:10px 15px 0px 15px;' id='msg'>
   <div class="alert alert-{{$msg[0]}}" role="alert">
     <button type="button" class="close" data-dismiss="alert" aria-label="Close">
       <span aria-hidden="true">&times;</span>
   </button>
   <strong>{{ ucfirst($msg[0]) }}!</strong>{{ $msg[1] }}
</div><!-- alert -->
</div>
@endif 




<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css">

<div class="slim-mainpanel">
	<div class="container">
		<div class="slim-pageheader">
			<ol class="breadcrumb slim-breadcrumb">
				<li class="breadcrumb-item"><a href="{{ url('/admin/dashboard') }}">Home</a></li>
				<li class="breadcrumb-item">Staffs</li>
				{{-- <li class="breadcrumb-item active" aria-current="page">Form Wizards</li> --}}
			</ol>
			<h6 class="slim-pagetitle mb-2">STAFFS</h6>
		</div><!-- slim-pageheader -->		
		
		<div class="section-wrapper">
			<div class="row">
			   <div class="col-sm-12 col-lg-6">
			     <label class="section-title">Staffs Details</label>
			     <p class="mg-b-20 mg-sm-b-40">Please complete the form below,Fields marked with a <span class="tx-danger">*</span> are mandatory.</p></div>
			     <div class="col-sm-12 col-lg-6">
			       <div class="d-flex  justify-content-md-end justify-content-center">
			         <a href="{{ url('/admin/users') }}" class="btn btn-primary" style="margin-bottom: 10px;">Add new</a>
			     </div>
			 </div>
			</div>

			<div class="table-wrapper">
				<table id="userListTable" class="table display" width='100%'>
					<thead>
						<tr>
							<th class="wd-15p">SI.NO</th>
							<th class="wd-15p">PROFILE</th>
							<th class="wd-20p">ADDRESS</th>
							<th class="wd-20p">role</th>
							<th class="wd-10p">SMS</th>
							<th class="wd-10p">DATE</th>
							<th class="wd-25p">ACTION</th>
						</tr>
					</thead>
					<tbody>
						@foreach($admins as $key => $admin)
						<tr>
							<td>{{ $key+1 }}</td>
							<td>{{ $admin->users_name }}<br>
								{{ $admin->users_email }}<br>
								{{ $admin->users_mobile }}</td>
								<td>{{ $admin->users_address }}<br>
									{{ $admin->users_street }}<br>
									{{ $admin->users_city }}</td>
									<td>{{ $admin->roll_name }}</td>
									<td>{{ $admin->sgrp_name }}</td>
									<td>{{ $admin->users_date }}</td>
										<td>
											<div class="btn-group mb-2">
												<a href="{{URL::to('/admin/del_admin/'.$admin->pk_int_users_id)}}"  type="button"  class="deleteImg btn btn-light waves-effect"><i class="icon ion-trash-a"></i>&nbsp;Delete</a>

												{{-- <a class="btn btn-danger" onclick="return confirm('Are you sure?')" href="{{ url('/admin/staffete_users', $admin->pk_int_users_id)}}"><i class="fa fa-trash"></i></a> --}}

												<a href="{{ url('/admin/edit_admin/'.$admin->pk_int_users_id) }}" type="button" class="btn btn-primary" title="Edit" value="Edit"><i class="icon ion-edit"></i>&nbsp;Edit</a>
											</div>
										</td>
								</tr>
								@endforeach
							</tbody>
						</table>
					</div>
				</div><!-- section-wrapper -->
			</div>


			@include('admin.includes.footer')


			<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
			<script src="{{asset('assets/backend/lib/datatables/js/jquery.dataTables.js')}}"></script>
			<script src="{{asset('assets/backend/lib/select2/js/select2.min.js')}}"></script>
			<script src="https://cdnjs.cloudflare.com/ajax/libs/alertify.js/0.5.0/alertify.min.js"></script>
			<script type="text/javascript">
 
			$(document).on("click", ".deleteImg", function (event) {
			        var result = confirm("Are you sure want to delete?");
			        if (result) {
			            //Logic to delete the item
			            return true;
			        }
			        else
			        {
			           return false;
			        }
			    });
				 
				

			
 $('#userListTable').DataTable({
            scrollX:true,
               language: {
              searchPlaceholder: 'Search...',
              sSearch: '',
              lengthMenu: '_MENU_ items/page',
              },
            "ordering":false,
            "destroy": true,
            "processing": true,
          });
				setTimeout(function(){ $("#msg").fadeOut(2000); }, 2000);
			</script>


