@include('admin.includes.head')
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/alertify.js/0.5.0/alertify.core.css">
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/alertify.js/0.5.0/alertify.default.min.css">

<?php
$message=Session::get('message');
$msg=explode("|",$message);

?>


@if ($message = Session::get('message'))
<div  style='z-index:9999; position:absolute;width:30%;right:0px; padding:10px 15px 0px 15px;' id='msg'>
   <div class="alert alert-{{$msg[0]}}" role="alert">
     <button type="button" class="close" data-dismiss="alert" aria-label="Close">
       <span aria-hidden="true">&times;</span>
   </button>
   <strong>{{ ucfirst($msg[0]) }}!</strong>{{ $msg[1] }}
</div><!-- alert -->
</div>
@endif 

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css">

<div class="slim-mainpanel">
	<div class="container">
		<div class="slim-pageheader">
			<ol class="breadcrumb slim-breadcrumb">
				<li class="breadcrumb-item"><a href="{{ url('/admin/dashboard') }}">Home</a></li>
				<li class="breadcrumb-item">Users</li>
				{{-- <li class="breadcrumb-item active" aria-current="page">Form Wizards</li> --}}
			</ol>
			<h6 class="slim-pagetitle mb-2">USERS</h6>
		</div><!-- slim-pageheader -->		
		<div class="section-wrapper">
			<div class="row" style='padding-bottom:0px;'>
			     <label class="col-md-3 section-title">Users Details</label>
			     <div class="col-md-3">
			     	<form action="{{ url('/admin/download_user_pdf')}}" method="post">
			       	{{csrf_field()}}
			     		<div class="input-group mb-3">
			     			<select name="user" class="form-control">
			     				<option value="">---select---</option>
			     				@foreach($role as $key => $user)
			     				<option value="{{ $user->pk_int_role_id }}">{{ $user->roll_name }}</option>
			     				@endforeach

						</select>
						<div class="input-group-append">
							<button type="submit" class="btn btn-primary icon ion-document-text"> &nbsp;Print</button>
						</div>
					</div>
				</form>
			</div>
				 <label class="col-md-1" style='margin-top:7px;'>Search :</label>
				 <div class="col-md-3">
				 	<form action="{{ url('/admin/user_list') }}" method="get">
				 		<div class="input-group mb-3">
				 			{{csrf_field()}}
				 			<select name="role" class="form-control">
				 				<option value="">choose role</option>
				 				@foreach($role as $key => $user)
				 				<option value="{{ $user->pk_int_role_id }}">{{ $user->roll_name }}</option>
				 				@endforeach

						</select>
						<div class="input-group-append">
							<button type="submit" class="btn btn-primary" style="padding:7px 17px 7px 17px;"><i class="icon ion-search"></i></button>
						</div>
					</div>
				</form>
			</div>
				
				<div class='col-md-2'>
				 	<a href="{{ url('/admin/users') }}" class="btn btn-primary" style="padding:7px 17px 7px 17px;">Add new</a>
				</div>
			     
			 </div>
	

			<hr style="margin:3px 0px 12px 0px;background-color:#dee2e6;">
			
			<div class="table-wrapper">
				<table id="userListTable" class="table display" width='100%'>
					<thead>
						<tr>
							<th class="wd-10p">SI.NO</th>
							<th >PROFILE</th>
							<th >ADDRESS</th>
							<th class="wd-15p">ROLE</th>
							<th class="wd-10p">SMS</th>
							{{-- <th class="wd-10p">DATE</th> --}}
							<th>LOCATION</th>
							<th class="wd-12p">ACTION</th>
						</tr>
					</thead>
					<tbody>
						@foreach($roles_show as $key => $user)
						<tr>
							<td>{{ $key+1 }}</td>
							<td>{{ $user->users_name }}<br>
								{{ $user->users_email }}<br>
								{{ $user->users_mobile }}</td>
								<td>{{ $user->vchr_area }}<br>
									{{ $user->users_address }}<br>
									{{ $user->users_street }}<br>
									{{ $user->users_city }}</td>
									<td>{{ $user->roll_name }}</td>
									<td>{{ $user->sgrp_name }}</td>
									{{-- <td>{{ $user->users_date }}</td> --}}
									<td>Lat:{{ $user->users_latitude }}<br>
										Lng:{{ $user->users_longitude }}</td>
										<td>
											<div class="btn-group mb-2">
												<a href="{{URL::to('/admin/delete_users/'.$user->pk_int_users_id)}}"  type="button"  class="deleteImg btn btn-light waves-effect"><i class="fa fa-trash"></i></a>

												{{-- <a class="btn btn-danger" onclick="return confirm('Are you sure?')" href="{{ url('/admin/delete_users', $user->pk_int_users_id)}}"><i class="fa fa-trash"></i></a> --}}

												<a href="{{ url('/admin/edit_users/'.$user->pk_int_users_id) }}" type="button" class="btn btn-primary" title="Edit" value="Edit"><i class="fa fa-pencil"></i></a>
											</div>
										</td>
								</tr>
								@endforeach
							</tbody>
						</table>
					</div>
				</div><!-- section-wrapper -->
			</div>


			@include('admin.includes.footer')


			<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
			<script src="{{asset('assets/backend/lib/datatables/js/jquery.dataTables.js')}}"></script>
			<script src="{{asset('assets/backend/lib/select2/js/select2.min.js')}}"></script>
			<script src="https://cdnjs.cloudflare.com/ajax/libs/alertify.js/0.5.0/alertify.min.js"></script>
			<script type="text/javascript">
 
			$(document).on("click", ".deleteImg", function (event) {
			        var result = confirm("Are you sure want to delete?");
			        if (result) {
			            //Logic to delete the item
			            return true;
			        }
			        else
			        {
			           return false;
			        }
			    });
				 
				

			
				$('#userListTable').DataTable({
						scrollX:true,
						   language: {
							searchPlaceholder: 'Search...',
							sSearch: '',
							lengthMenu: '_MENU_ items/page',
						  },
						"ordering":false,
						"destroy": true,
						"processing": true,
					});
				setTimeout(function(){ $("#msg").fadeOut(2000); }, 2000);
			</script>


