@include('admin.includes.head')
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.21.0/moment.min.js" type="text/javascript"></script>

<?php
$message=Session::get('message');
$msg=explode("|",$message);
?>

@if ($message = Session::get('message'))
<div  style='z-index:9999; position:absolute;width:30%;right:0px; padding:10px 15px 0px 15px;' id='msg'>
	<div class="alert alert-{{$msg[0]}}" role="alert">
		<button type="button" class="close" data-dismiss="alert" aria-label="Close">
			<span aria-hidden="true">&times;</span>
		</button>
		<strong>{{ ucfirst($msg[0]) }}!</strong>{{ $msg[1] }}
	</div><!-- alert -->
</div>
@endif 

<div class="slim-mainpanel">
	<div class="container">
		<div class="slim-pageheader">
			<ol class="breadcrumb slim-breadcrumb">
				<li class="breadcrumb-item"><a href="{{ url('/admin/dashboard') }}">Home</a></li>
				<li class="breadcrumb-item"><a href="#">Order</a></li>
				{{-- <li class="breadcrumb-item active" aria-current="page">Form Wizards</li> --}}
			</ol>
			<h6 class="slim-pagetitle mb-2">Orders To Deliver today</h6>
		</div><!-- slim-pageheader -->		
		
		<div class="section-wrapper">
			<div class="row">
				<div class="col-sm-12 col-lg-3">
					<label class="section-title">Order Details</label>
					<p class="mg-b-20 mg-sm-b-40">Here is the list of orders to deliver today.</p>
				</div>
				<div class="col-sm-12 col-lg-3"> 
					<form action="{{ url('/admin/download_delivery_pdf')}}" method="post">
						{{csrf_field()}}
						<div class="row  justify-content-md-end justify-content-center xs-p2">
							<input class="form-control form-input" id="example"  type="text" name="order_date" placeholder="Select date" required style="">
							<button type="submit" class="btn btn-primary icon ion-document-text" style="margin-right:15px;"> &nbsp;Print</button>
						</div>
					</form>
				</div>
				<div class="col-sm-12 col-lg-6">
					<form action="{{ url('/admin/download_delivery_month_pdf')}}" method="post">
						{{csrf_field()}}
						<div class="row  justify-content-md-end justify-content-center xs-p2">
							<input class="form-control form-input col-lg-3" id="example1"  type="text" name="order_month" placeholder="Select Month" required style="">
							<input class="form-control form-input col-lg-3" id="example2"  type="text" name="order_year" placeholder="Select Year" required style="">
							<button type="submit" class="btn btn-primary icon ion-document-text" style="margin-right:15px;"> &nbsp;Print</button>
						</div>
					</form>
				</div>
			</div>

			<div class="table-wrapper">
				<table id="userListTable" class="table display" width='100%'>
					<thead>
						<tr>
							<th class="wd-15p">SI.NO</th>
							<th class="wd-15p">AREA</th>
							<th class="wd-15p">customer name</th>
							<th class="wd-15p">PRODUCT</th>
							<th class="wd-15p">Quantity</th>
							{{-- <th class="wd-20p">Purchasing price</th> --}}
							<th class="wd-15p">Date</th>
							<th class="wd-15p">Bottle</th>
							{{-- <th class="wd-20p">Status</th> --}}
						</tr>
					</thead>
					<tbody>

						@foreach($delivery as $key => $orders)
						<tr>
							<td>{{ $key+1 }}</td>
							<td>{{ $orders->vchr_area }}</td>
							<td>{{ $orders->users_name }}</td>
							<td>{{ $orders->prod_title }}</td>
							<td>{{ $orders->stf_order_quant }} {{ $orders->stf_order_unit }}</td>							
							<td>{{ $orders->stf_order_date }}</td>							
							<td>{{ $orders->stf_order_botl }}</td>							
							{{-- <td><a type="button" class="edit-item btn btn-primary"  id="{{ $orders->stf_order_pk_id }}" data-toggle="modal" data-item-id="" ><i style="color: white;" class="fa fa-check"></i></a></td> --}}
						</tr>
						@endforeach 
					</tbody>
				</table>
			</div>
		</div><!-- section-wrapper -->
	</div>

	@include('admin.includes.footer')
	@foreach($delivery as $orders)
	<div id="edit-modal" class="modal fade modal-min" >
		<div class="modal-dialog modal-dialog-vertical-center" role="document">
			<div class="modal-content bd-0 tx-14">
				<div class="modal-header pd-x-20">
					<p class="slim-pagetitle">place your order here</p>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				

				<form method="post" action="{{url('/admin/delivery_confirm_submit')}}" enctype="multipart/form-data">
					{{csrf_field()}}
					<div class="card">

						<div class="card-body">
							<dl class="row">
								<dt class="col-sm-4 tx-inverse">Customer Name</dt>
								<dd class="col-sm-8" style="color: #5ECD62;font-weight: bold;" id="customer_name"></dd>
								<dt class="col-sm-4 tx-inverse">Customer Area</dt>
								<dd class="col-sm-8" style="color: #5ECD62;font-weight: bold;" id="vchr_area"></dd>
								<dt class="col-sm-4 tx-inverse">Ordered Product</dt>
								<dd class="col-sm-8" style="color: #5ECD62;font-weight: bold;" id="prod_title"></dd>
								<dt class="col-sm-4 tx-inverse">Quantity</dt>
								<dd class="col-sm-8" style="color: #5ECD62;font-weight: bold;" id="stf_order_quant"></dd>
								<dt class="col-sm-4 tx-inverse">Total Price</dt>
								<dd class="col-sm-8" style="color: #5ECD62;font-weight: bold;" id="stf_order_price"></dd>
								<dt class="col-sm-4 tx-inverse">Bottle Number</dt>
								<dd class="col-sm-8" style="color: #5ECD62;font-weight: bold;" id="stf_order_botl"></dd>
								
								<input type="hidden" name="order_prod" value="{{ $orders->stf_order_prod }}" id="order_prod">
								<input type="hidden" name="user_id" value="{{ $orders->stf_order_pk_id }}" id="user_id">
								<input type="hidden" name="order_area" value="{{ $orders->stf_order_area }}" id="order_area">
								<input type="hidden" name="order_id" value="{{ $orders->stf_order_area }}" id="order_id">
								<input type="hidden" name="stf_order_price" value="{{ $orders->stf_order_area }}" id="stf_order_price">


								<div class="col-sm-12 col-lg-6">
									<div class="d-flex  justify-content-md-end justify-content-center">
										<button type="submit" class="btn btn-primary bd-0" style="margin-top: 10px; border-radius: 3px;">Place Order</button>
										<button type="button" class="btn btn-default bd-0" data-dismiss="modal" style="margin-top: 10px; border-radius: 3px; margin-left: 10px;">Close</button>

									</div>
								</div>
							</dl>
						</div><!-- card-body -->
					</div><!-- card -->
				</form>
			</div>
		</div><!-- modal-dialog -->
	</div><!-- modal -->
	@endforeach

	<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
	<script src="{{asset('assets/backend/lib/datatables/js/jquery.dataTables.js')}}"></script>
	<script src="{{asset('assets/backend/lib/select2/js/select2.min.js')}}"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/alertify.js/0.5.0/alertify.min.js"></script>
	
	<script type="text/javascript">

		$(document).on("click", ".deleteImg", function (event) {
			var result = confirm("Are you sure want to delete?");
			if (result) {
			            //Logic to delete the item
			            return true;
			        }
			        else
			        {
			        	return false;
			        }
			    });

		$('#userListTable').DataTable({
			scrollX:true,
			language: {
				searchPlaceholder: 'Search...',
				sSearch: '',
				lengthMenu: '_MENU_ items/page',
			},
			"ordering":false,
			"destroy": true,
			"processing": true,
		});
		
		setTimeout(function(){ $("#msg").fadeOut(2000); }, 2000);

	</script>
	<script type="text/javascript">

		$(document).ready(function() {

			$(".edit-item").click(function()
			{
				var id=$(this).attr('id');
				$(this).attr('data-target','#edit-modal');
         // alert(id);
         jQuery.ajax({
         	type: "get",
         	url: 'delivery_confirm/'+id,
            //data: {smsid:id},
            success: function(res)
            {
            	// alert(res);
            	var d1= res.split(',');
            	$("#stf_order_price").html(d1[12]);
            	$("#order_id").html(d1[11]);
            	$("#order_area").html(d1[10]);
            	$("#prod_title").html(d1[9]);
            	$("#customer_name").html(d1[8]);
            	$("#vchr_area").html(d1[7]);
            	$("#stf_order_botl").html(d1[6]);
            	$("#stf_order_price").html(d1[5]);
            	$("#stf_order_unit").html(d1[4]);
            	$("#stf_order_quant").html(d1[3]);
            	$("#order_prod").html(d1[2]);
               // $("#customer_name").html(d1[1]);
               $("#user_id").val(d1[0]);
           }
       });
     });
		});
	</script>
	<script type="text/javascript">
		$(function () {
			$('#example').datetimepicker({
				format: 'YYYY-MM-DD'
			});      
		});
	</script>
	<script type="text/javascript">
		$(function () {
			$('#example1').datetimepicker({
				format: 'MM'
			});      
		});
	</script>
	<script type="text/javascript">
		$(function () {
			$('#example2').datetimepicker({
				format: 'YYYY'
			});      
		});
	</script>

