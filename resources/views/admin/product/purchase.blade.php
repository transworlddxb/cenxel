@include('admin.includes.head')
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.21.0/moment.min.js" type="text/javascript"></script>
<style type="text/css">
     input[type=number]::-webkit-inner-spin-button, 
   input[type=number]::-webkit-outer-spin-button { 
  -webkit-appearance: none; 
  margin: 0; 
}
</style>

<?php
$message=Session::get('message');
$msg=explode("|",$message);

?>




@if ($message = Session::get('message'))
<div  style='z-index:9999; position:absolute;width:30%;right:0px; padding:10px 15px 0px 15px;' id='msg'>
   <div class="alert alert-{{$msg[0]}}" role="alert">
     <button type="button" class="close" data-dismiss="alert" aria-label="Close">
       <span aria-hidden="true">&times;</span>
   </button>
   <strong>{{ ucfirst($msg[0]) }}!</strong>{{ $msg[1] }}
</div><!-- alert -->
</div>
@endif 
    <div class="slim-mainpanel">
        <div class="container">
            <div class="slim-pageheader">
                <ol class="breadcrumb slim-breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ url('/admin/dashboard') }}">Home</a></li>
                    <li class="breadcrumb-item">Product Purchace for Customer</li>
                    {{-- <li class="breadcrumb-item active" aria-current="page">register</li> --}}
                </ol>
                <h6 class="slim-pagetitle mb-2">Product Purchace for Customer</h6>
            </div><!-- slim-pageheader -->

            <div class="section-wrapper">
            <div class="row">
              <div class="col-sm-12 col-lg-6">
                <label class="section-title">Add Details</label>
                <p class="mg-b-20 mg-sm-b-40">The details of cunsultation</p></div>
                <div class="col-sm-12 col-lg-6">
                  <div class="d-flex  justify-content-md-end justify-content-center">
                    <a href="{{ url('/admin/today_appointment') }}" class="btn btn-primary " style="margin-bottom: 10px;"><i class="icon ion-arrow-left-a"></i> BACk</a>
                  </div>  
                </div>
              </div>
            <div class="form-layout">
            <form id="basic-form" method="post" action="{{url('/admin/purchased_prod/'.$appoint_id)}}" enctype="multipart/form-data">
                            {{csrf_field()}}
            <div class="form-group">
                <div class="row">
                         <label class='col-md-3 control-label' style="margin-top:5px;text-align: right;"> Need Product ?<span class="tx-danger">*</span>: </label>
                         <div class="col-md-4">
                             <select class="form-control select2" data-placeholder="Choose Doctor" name="prod_status" id="prod_choice">
                                    <option label="Choose Category"></option>
                                        <option value="1">Yes</option>
                                        <option value="0">No</option>
                                </select>
                             @if ($errors->has('prod_status'))
                                <span class="help-block">
                                    <strong class="error">{{ $errors->first('prod_status') }}</strong>
                                </span>
                            @endif

                    </div>
                </div>
            </div>
            <div class="form-group" id="product_one">
                <div class="row">
                         <label class='col-md-3 control-label' style="margin-top:5px;text-align: right;"> Product 1<span class="tx-danger">*</span>: </label>
                         <div class="col-md-4">
                             <select class="form-control select2" data-placeholder="Choose Doctor" name="product_one" >
                                    <option label="Choose Category"></option>
                                        @foreach($product as $key=>$cat) 
                                        <option value="{{$cat->product_id}}">{{$cat->prod_name}}</option>
                                        @endforeach
                                </select>
                             @if ($errors->has('product_one'))
                                <span class="help-block">
                                    <strong class="error">{{ $errors->first('product_one') }}</strong>
                                </span>
                            @endif

                    </div>
                </div>
            </div>
            <div class="form-group" id="product_two">
                <div class="row">
                         <label class='col-md-3 control-label' style="margin-top:5px;text-align: right;"> Product 2<span class="tx-danger">*</span>: </label>
                         <div class="col-md-4">
                             <select class="form-control select2" data-placeholder="Choose Doctor" name="product_two">
                                    <option label="Choose Category"></option>
                                       @foreach($product as $key=>$cat) 
                                        <option value="{{$cat->product_id}}">{{$cat->prod_name}}</option>
                                        @endforeach
                                </select>
                             @if ($errors->has('product_two'))
                                <span class="help-block">
                                    <strong class="error">{{ $errors->first('product_two') }}</strong>
                                </span>
                            @endif

                    </div>
                </div>
            </div>
            <div class="form-group" id="product_three">
                <div class="row">
                         <label class='col-md-3 control-label' style="margin-top:5px;text-align: right;"> Product 3<span class="tx-danger">*</span>: </label>
                         <div class="col-md-4">
                             <select class="form-control select2" data-placeholder="Choose Doctor" name="product_three" >
                                    <option label="Choose Category"></option>
                                       @foreach($product as $key=>$cat) 
                                        <option value="{{$cat->product_id}}">{{$cat->prod_name}}</option>
                                        @endforeach
                                </select>
                             @if ($errors->has('product_three'))
                                <span class="help-block">
                                    <strong class="error">{{ $errors->first('product_three') }}</strong>
                                </span>
                            @endif

                    </div>
                </div>
            </div>
            <div class="form-group" id="product_four">
                <div class="row">
                         <label class='col-md-3 control-label' style="margin-top:5px;text-align: right;"> Product 4<span class="tx-danger">*</span>: </label>
                         <div class="col-md-4">
                             <select class="form-control select2" data-placeholder="Choose Doctor" name="product_four" >
                                    <option label="Choose Category"></option>
                                       @foreach($product as $key=>$cat) 
                                        <option value="{{$cat->product_id}}">{{$cat->prod_name}}</option>
                                        @endforeach
                                </select>
                             @if ($errors->has('product_four'))
                                <span class="help-block">
                                    <strong class="error">{{ $errors->first('product_four') }}</strong>
                                </span>
                            @endif

                    </div>
                </div>
            </div>
            <div class="form-group" id="product_five">
                <div class="row">
                         <label class='col-md-3 control-label' style="margin-top:5px;text-align: right;"> Product 5<span class="tx-danger">*</span>: </label>
                         <div class="col-md-4">
                             <select class="form-control select2" data-placeholder="Choose Doctor" name="product_five">
                                    <option label="Choose Category"></option>
                                      @foreach($product as $key=>$cat) 
                                        <option value="{{$cat->product_id}}">{{$cat->prod_name}}</option>
                                        @endforeach
                                </select>
                             @if ($errors->has('product_five'))
                                <span class="help-block">
                                    <strong class="error">{{ $errors->first('product_five') }}</strong>
                                </span>
                            @endif

                    </div>
                </div>
            </div>

                <div class="form-group">
                <div class="row">
                         <label class='col-md-3 control-label' style="margin-top:5px;text-align: right;"></label>
                         <div class="col-md-4">
                           <button class="btn btn-primary bd-0">Submit</button>
                        <a href="{{ url('/admin/pending_cunsult') }}" class="btn btn-secondary bd-0">Back</a>

                    </div>
                </div>
            </div>

            </form>    
        </div><!-- section-wrapper -->
    </div>
    </div>


@include('admin.includes.footer')

<script type="text/javascript">
    $("#prod_choice").change(function()
        {
            var rl = $("#prod_choice").val();
            // alert(rl);
            if(rl==1){
                $('#product_one').show();
                $('#product_two').show();
                $('#product_three').show();
                $('#product_four').show();
                $('#product_five').show();
            }else{
              $('#product_one').hide();
              $('#product_two').hide();
              $('#product_three').hide();
              $('#product_four').hide();
              $('#product_five').hide();
            }
        });
</script>
