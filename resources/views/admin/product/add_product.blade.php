@include('admin.includes.head')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css">

<style type="text/css">
input[type=number]::-webkit-inner-spin-button, 
input[type=number]::-webkit-outer-spin-button { 
  -webkit-appearance: none; 
  margin: 0; 
}
</style>

<?php
$message=Session::get('message');
$msg=explode("|",$message);
?>

@if ($message = Session::get('message'))
<div  style='z-index:9999; position:absolute;width:30%;right:0px; padding:10px 15px 0px 15px;' id='msg'>
 <div class="alert alert-{{$msg[0]}}" role="alert">
   <button type="button" class="close" data-dismiss="alert" aria-label="Close">
     <span aria-hidden="true">&times;</span>
   </button>
   <strong>{{ ucfirst($msg[0]) }}!</strong>{{ $msg[1] }}
 </div><!-- alert -->
</div>
@endif


<div class="slim-mainpanel">
  <div class="container">
    <div class="slim-pageheader">
      <ol class="breadcrumb slim-breadcrumb">
        <li class="breadcrumb-item"><a href="{{ url('/admin/dashboard') }}">Home</a></li>
        <li class="breadcrumb-item">Add Product </li>
        {{-- <li class="breadcrumb-item active" aria-current="page">Form Wizards</li> --}}
      </ol>
      <h6 class="slim-pagetitle">Add Product</h6>
    </div><!-- slim-pageheader -->



    <div class="section-wrapper mg-t-20">
      <section>
        <div class="row">
          <div class="col-md-4">
            <label class="section-title">Add new</label>
            <p class="mg-b-20 mg-sm-b-40">add details of Products for sale</p>
            <div class="card-box">
              <form class="form-horizontal" method="post" action="{{url('/admin/add_product_list')}}" enctype="multipart/form-data">
                {{csrf_field()}}
                
                <div>
                    <label>Name:</label>
                  <input id="autocomplete-ajax" class="form-control" placeholder="Enter name of Product" required name="prod_name" >
                  @if ($errors->has('prod_name'))
                  <span class="help-block">
                    <p class="error">{{ $errors->first('prod_name') }}</p>
                    <strong style="color:red">{{ $errors->first('prod_name') }}</strong>
                  </span>
                  @endif
                </div>

                <div style="margin-top: 10px;">
                    <label>Price:</label>
                  <input id="autocomplete-ajax" type="number" class="form-control" placeholder="Enter price of Product" required name="prod_price" >
                  @if ($errors->has('prod_price'))
                  <span class="help-block">
                    <p class="error">{{ $errors->first('prod_price') }}</p>
                    <strong style="color:red">{{ $errors->first('prod_price') }}</strong>
                  </span>
                  @endif
                </div>

                <div style="margin-top: 10px;">
                    <label>Total Count:</label>
                  <input id="autocomplete-ajax" class="form-control" placeholder="Enter no. of stock" required name="prod_count"  type="number">
                  @if ($errors->has('prod_count'))
                  <span class="help-block">
                    <p class="error">{{ $errors->first('prod_count') }}</p>
                    <strong style="color:red">{{ $errors->first('prod_count') }}</strong>
                  </span>
                  @endif
                </div>

                <button style="margin-top: 20px; background-color: #1B84E7; color: white; margin-left: 100px; margin-bottom: 10px;" class="btn btn-custom waves-light waves-effect" type="submit">Submit</button>
              </form>
            </div>
          </div>
          <div class="col-md-8">
            <div class="section-wrapper">
              <label class="section-title">List of Products</label>
              <hr style="margin:5px 0px 12px 0px;background-color:#dee2e6;">
              <div class="table-wrapper">
                <table id="roleListTable" class="table display" width='100%'>
                  <thead>
                    <tr>
                      <tr>
                        <th>SI.NO</th>
                        <th>Product Name</th>
                        <th>Price</th>
                        <th>Total stock</th>
                        <th><center>ACTION</center></th>
                      </tr>
                    </tr>
                  </thead>
                  <tbody>
                       @foreach($product as $key => $prod)
                        <tr>
                          <td>{{ $key+1 }}</td>
                          <td>{{ $prod->prod_name }}</td>
                          <td>{{ $prod->prod_price }}.AED</td>
                          <td>{{ $prod->prod_count }}</td>
                          
                          <td style="width: 200px;"><center>

                            <a href="{{URL::to('/admin/delete_product/'.$prod->product_id)}}" type="button"  class="del btn btn-light waves-effect"><i class="fa fa-trash"></i></a>

                            <a type="button" class="edit-item btn btn-primary" style="color: white;" id="{{ $prod->product_id }}" data-toggle="modal" data-item-id=""><i class="fa fa-pencil"></i></a>
                          </center>
                        </td>
                      </tr>
                      @endforeach
                </tbody>
              </table>
            </div><!-- table-wrapper -->
          </div><!-- section-wrapper -->    
        </div>
      </div>
    </section>
  </div>


</div><!-- container -->

@include('admin.includes.footer')

<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
<script src="{{asset('assets/backend/lib/datatables/js/jquery.dataTables.js')}}"></script>
<script src="{{asset('assets/backend/lib/select2/js/select2.min.js')}}"></script>

{{-- model --}}
@foreach ($product as $g) 
<div id="edit-modal" class="modal fade modal-min" >
  <div class="modal-dialog modal-dialog-vertical-center" role="document">
   <div class="modal-content bd-0 tx-14">
    <div class="modal-header pd-x-20">
      <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold">Update Product details</h6>
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <form role="form" action="{{ url('/admin/update_product/'.$g->product_id) }}" method="post">
      <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
      <div class="box-body col-md-12">
        <input type="hidden" name="product_id" id="product_id">
        <label>Name</label>
        <div class="form-group"> 
          <input type="text" class="form-control" placeholder="Enter gst value" id="prod_name" name="prod_name">
        </div>
      </div>
      <div class="box-body col-md-12">
        <label>Price</label>
        <div class="form-group"> 
          <input type="text" class="form-control" placeholder="Enter gst value" id="prod_price" name="prod_price">
        </div>
      </div>
      <div class="box-body col-md-12">
        <label>Stock</label>
        <div class="form-group"> 
          <input type="text" class="form-control" placeholder="Enter gst value" id="prod_count" name="prod_count">
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save changes</button>
      </div>
    </form>
  </div>
</div><!-- modal-dialog -->
</div><!-- modal -->
@endforeach

<script type="text/javascript">

 // $(document).ready(function() {

    $('#roleListTable').on('click', '.edit-item', function()
   // $(".edit-item").click(function()
   {
     var id=$(this).attr('id');
     $(this).attr('data-target','#edit-modal');
         // alert(id);
         jQuery.ajax({
          type: "get",
          url: 'edit_product/'+id,
            //data: {smsid:id},
            success: function(res)
            {
               // alert(res);
               var d1= res.split(',');
               $("#prod_count").val(d1[3]);
               $("#prod_price").val(d1[2]);
               $("#prod_name").val(d1[1]);
               $("#product_id").val(d1[0]);
             }
           });
       });
 // });
</script>

<script type="text/javascript">

  setTimeout(function(){ $("#msg").fadeOut(2000); }, 2000);


  $(document).on("click", ".del", function (event) {
    var result = confirm("Are you sure want to delete?");
    if (result) {
                  //Logic to delete the item
                  return true;
                }
                else
                {
                 return false;
               }
             });
  
            $('#roleListTable').DataTable({
              scrollX:true,
              language: {
                searchPlaceholder: 'Search...',
                sSearch: '',
                lengthMenu: '_MENU_ items/page',
              },
              "ordering":false,
              "destroy": true,
              "processing": true,
            });
//   $(document).ready(function(){
//     $("#successMessage").delay(1700).slideUp(300);
// });
setTimeout(function(){ $("#msg").fadeOut(2000); }, 2000);
</script>
