@include('admin.includes.head')
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.21.0/moment.min.js" type="text/javascript"></script>
<style type="text/css">
     input[type=number]::-webkit-inner-spin-button, 
   input[type=number]::-webkit-outer-spin-button { 
  -webkit-appearance: none; 
  margin: 0; 
}
</style>

<?php
$message=Session::get('message');
$msg=explode("|",$message);

?>




@if ($message = Session::get('message'))
<div  style='z-index:9999; position:absolute;width:30%;right:0px; padding:10px 15px 0px 15px;' id='msg'>
   <div class="alert alert-{{$msg[0]}}" role="alert">
     <button type="button" class="close" data-dismiss="alert" aria-label="Close">
       <span aria-hidden="true">&times;</span>
   </button>
   <strong>{{ ucfirst($msg[0]) }}!</strong>{{ $msg[1] }}
</div><!-- alert -->
</div>
@endif 
    <div class="slim-mainpanel">
        <div class="container">
            <div class="slim-pageheader">
                <ol class="breadcrumb slim-breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ url('/admin/dashboard') }}">Home</a></li>
                    <li class="breadcrumb-item"><a href="{{ url('/admin/view_users') }}">customer registration</a></li>
                    <li class="breadcrumb-item active" aria-current="page">edit</li>
                </ol>
                <h6 class="slim-pagetitle mb-2">Edit Customer Registration Details</h6>
            </div><!-- slim-pageheader -->

            <div class="section-wrapper">
            <div class="row">
                <div class="col-sm-12 col-lg-6">
                  <label class="section-title"> Edit Customer Registration Details</label>
                  <p class="mg-b-20 mg-sm-b-40">List of Customers</p></div>
                  <div class="col-sm-12 col-lg-6">
                    <div class="d-flex  justify-content-md-end justify-content-center">
                      <a href="{{ url('/admin/reg_user') }}" class="btn btn-primary " style="margin-bottom: 10px;"><i class="icon ion-arrow-left-a"></i> BACK</a>
                    </div>  
                  </div>
                </div>
            <div class="form-layout">
            <form id="basic-form" method="post" action="{{url('/admin/store_edited_registration/'.$register->pk_int_reg_id) }}" enctype="multipart/form-data">
                            {{csrf_field()}}
                
                
                <div class="form-group">
                <div class="row">
                         <label class='col-md-3 control-label' style="margin-top:5px;text-align: right;"> Registration file number<span class="tx-danger">*</span>: </label>
                         <div class="col-md-4">
                            <input class="form-control" type="number" name="file_no" placeholder="Enter file no" required value="{{$register->file_no}}" readonly>
                                @if ($errors->has('file_no'))
                                <span class="help-block">
                                    <strong class="error">{{ $errors->first('file_no') }}</strong>
                                </span>
                                @endif

                    </div>
                </div>
                </div>


                <div class="form-group">
                <div class="row">
                         <label class='col-md-3 control-label' style="margin-top:5px;text-align: right;"> First Name<span class="tx-danger">*</span>:</label>
                         <div class="col-md-4">
                            <input class="form-control" type="text" name="first_name" placeholder="Enter first name" required="" value="{{$register->first_name}}">
                                @if ($errors->has('first_name'))
                                <span class="help-block">
                                    <strong class="error">{{ $errors->first('first_name') }}</strong>
                                </span>
                                @endif

                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="row">
                         <label class='col-md-3 control-label' style="margin-top:5px;text-align: right;"> Middle Name<span class="tx-danger">*</span>:</label>
                         <div class="col-md-4">
                            <input class="form-control" type="text" name="middle_name" placeholder="Enter middle name" value="{{$register->middle_name}}">
                                @if ($errors->has('middle_name'))
                                <span class="help-block">
                                    <strong class="error">{{ $errors->first('middle_name') }}</strong>
                                </span>
                                @endif

                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="row">
                         <label class='col-md-3 control-label' style="margin-top:5px;text-align: right;"> Last Name<span class="tx-danger">*</span>:</label>
                         <div class="col-md-4">
                            <input class="form-control" type="text" name="last_name" placeholder="Enter last name" value="{{$register->last_name}}">
                                @if ($errors->has('last_name'))
                                <span class="help-block">
                                    <strong class="error">{{ $errors->first('last_name') }}</strong>
                                </span>
                                @endif
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="row">
                         <label class='col-md-3 control-label' style="margin-top:5px;text-align: right;"> Last Name<span class="tx-danger">*</span>:</label>
                         <div class="col-md-4">
                            <input class="form-control" type="text" name="patient_place" placeholder="Enter last name" value="{{$register->patient_place}}">
                                @if ($errors->has('patient_place'))
                                <span class="help-block">
                                    <strong class="error">{{ $errors->first('patient_place') }}</strong>
                                </span>
                                @endif
                    </div>
                </div>
            </div>
            <div class="form-group">
                    <div class="row">
                         <label class='col-md-3 control-label' style="margin-top:5px;text-align: right;"> Date of Birth: </label>
                         <div class="col-md-4">
                             <input class="form-control" id="example"  type="text" name="reg_dob" placeholder="Select date" value="{{$register->reg_dob}}">
                             @if ($errors->has('reg_dob'))
                                <span class="help-block">
                                    <strong class="error">{{ $errors->first('reg_dob') }}</strong>
                                </span>
                            @endif

                    </div>
                </div>
            </div>
            <div class="form-group">
                    <div class="row">
                         <label class='col-md-3 control-label' style="margin-top:5px;text-align: right;"> Age: </label>
                         <div class="col-md-4">
                             <input class="form-control" id="example"  type="text" name="patient_age" placeholder="Age" value="{{$register->patient_age}}">
                             @if ($errors->has('patient_age'))
                                <span class="help-block">
                                    <strong class="error">{{ $errors->first('patient_age') }}</strong>
                                </span>
                            @endif

                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="row">
                         <label class='col-md-3 control-label' style="margin-top:5px;text-align: right;">Mobile Number:<span class="tx-danger">*</span>: </label>
                         <div class="col-md-4">
                            <input class="form-control" type="number" name="mob_number" placeholder="Enter number" value="{{$register->mob_number}}">
                                @if ($errors->has('mob_number'))
                                <span class="help-block">
                                    <strong class="error">{{ $errors->first('mob_number') }}</strong>
                                </span>
                                @endif

                    </div>
                </div>
            </div>
                <div class="form-group">
                <div class="row">
                         <label class='col-md-3 control-label' style="margin-top:5px;text-align: right;">Gender:<span class="tx-danger">*</span>:</label>
                         <div class="col-md-4">
                                @if($register->gender==Null)
                                <select class="form-control select2" data-placeholder="Choose unit" name="gender">
                                    <option value="">-- select one --</option>
                                    <option value="1">Male</option>
                                    <option value="2">Female</option>
                                </select>
                                @endif
                                 @if($register->gender==1)
                                <select class="form-control select2" data-placeholder="Choose unit" name="gender">
                                    <option value="">-- select one --</option>
                                    <option value="1" selected>Male</option>
                                    <option value="2">Female</option>
                                </select>
                                @endif
                                 @if($register->gender==2)
                                <select class="form-control select2" data-placeholder="Choose unit" name="gender">
                                    <option value="">-- select one --</option>
                                    <option value="1">Male</option>
                                    <option value="2" selected>Female</option>
                                </select>
                                @endif
                                @if ($errors->has('gender'))
                                <span class="help-block">
                                    <strong class="error">{{ $errors->first('gender') }}</strong>
                                </span>
                                @endif

                    </div>
                </div>
            </div>
          
               <div class="form-group">
                <div class="row">
                         <label class='col-md-3 control-label' style="margin-top:5px;text-align: right;">Marital status:<span class="tx-danger">*</span>:</label>
                         <div class="col-md-4">
                             @if($register->marital_status==Null)
                           <select class="form-control select2" data-placeholder="Choose unit" name="marital_status">
                                    <option value="">--select one--</option>
                                    <option value="1">Single</option>
                                    <option value="2">Married</option>
                                </select>
                                @endif
                                @if($register->marital_status==1)
                           <select class="form-control select2" data-placeholder="Choose unit" name="marital_status">
                                    <option value="">--select one--</option>
                                    <option value="1" selected>Single</option>
                                    <option value="2">Married</option>
                                </select>
                                @endif
                                @if($register->marital_status==2)
                           <select class="form-control select2" data-placeholder="Choose unit" name="marital_status">
                                    <option value="">--select one--</option>
                                    <option value="1">Single</option>
                                    <option value="2" selected>Married</option>
                                </select>
                                @endif
                            
                                {{-- <input class="form-control" type="text" name="unit" placeholder="Enter Unit" required> --}}
                                @if ($errors->has('marital_status'))
                                <span class="help-block">
                                    <strong class="error">{{ $errors->first('marital_status') }}</strong>
                                </span>
                                @endif

                    </div>
                </div>
            </div>  
           

                <div class="form-group">
                <div class="row">
                         <label class='col-md-3 control-label' style="margin-top:5px;text-align: right;"></label>
                         <div class="col-md-4">
                           <button class="btn btn-primary bd-0">Submit</button>
                        <a href="{{ url('/admin/view_product') }}" class="btn btn-secondary bd-0">Back</a>

                    </div>
                </div>
            </div>

            </form>    
        </div><!-- section-wrapper -->
    </div>
    </div>


@include('admin.includes.footer')
<script type="text/javascript">
    $(function () {
      $('#example').datetimepicker({
        format: 'YYYY-MM-DD'
      });      
  });
    setTimeout(function(){ $("#msg").fadeOut(2000); }, 2000);
</script>
<script type="text/javascript">
    $("#role_id").change(function()
        {
            var rl = $("#role_id").val();
            // alert(rl);
            if(rl==1){
                $('#percent_insurance').show();
                $('#expiry').show();
                $('#insurance').show();
            }else{
              $('#percent_insurance').hide();
              $('#expiry').hide();
              $('#insurance').hide();

            }
        });
</script>