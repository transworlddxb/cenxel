@include('admin.includes.head')
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.21.0/moment.min.js" type="text/javascript"></script>
<style type="text/css">
     input[type=number]::-webkit-inner-spin-button, 
   input[type=number]::-webkit-outer-spin-button { 
  -webkit-appearance: none; 
  margin: 0; 
}
</style>

<?php
$message=Session::get('message');
$msg=explode("|",$message);

?>




@if ($message = Session::get('message'))
<div  style='z-index:9999; position:absolute;width:30%;right:0px; padding:10px 15px 0px 15px;' id='msg'>
   <div class="alert alert-{{$msg[0]}}" role="alert">
     <button type="button" class="close" data-dismiss="alert" aria-label="Close">
       <span aria-hidden="true">&times;</span>
   </button>
   <strong>{{ ucfirst($msg[0]) }}!</strong>{{ $msg[1] }}
</div><!-- alert -->
</div>
@endif 
    <div class="slim-mainpanel">
        <div class="container">
            <div class="slim-pageheader">
                <ol class="breadcrumb slim-breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ url('/admin/dashboard') }}">Home</a></li>
                    <li class="breadcrumb-item"><a href="{{ url('/admin/view_users') }}">customer registration</a></li>
                    <li class="breadcrumb-item active" aria-current="page">register</li>
                </ol>
                <h6 class="slim-pagetitle mb-2">Registration</h6>
            </div><!-- slim-pageheader -->

            <div class="section-wrapper">
            <div class="row">
              <div class="col-sm-12 col-lg-6">
                <label class="section-title">Add Details</label>
                <p class="mg-b-20 mg-sm-b-40">Please complete the form below,Fields marked with a <span class="tx-danger">*</span> are mandatory.</p></div>
                <div class="col-sm-12 col-lg-6">
                  <div class="d-flex  justify-content-md-end justify-content-center">
                    <a href="{{ url('/admin/reg_user') }}" class="btn btn-primary " style="margin-bottom: 10px;"><i class="icon ion-arrow-left-a"></i> BACk</a>
                  </div>  
                </div>
              </div>
            <div class="form-layout">
            <form id="basic-form" method="post" action="{{url('/admin/store_registration')}}" enctype="multipart/form-data">
                            {{csrf_field()}}
                {{-- <div class="form-group">
                    <div class="row">
                         <label class='col-md-3 control-label' style="margin-top:5px;text-align: right;"> Date of Product: </label>
                         <div class="col-md-4">
                             <input class="form-control" id="example"  type="text" name="date" placeholder="Select date" required>
                             @if ($errors->has('date'))
                                <span class="help-block">
                                    <strong class="error">{{ $errors->first('date') }}</strong>
                                </span>
                            @endif

                    </div>
                </div>
            </div> --}}
                
               {{--  <div class="form-group">
                <div class="row">
                         <label class='col-md-3 control-label' style="margin-top:5px;text-align: right;"> Branch<span class="tx-danger">*</span>: </label>
                         <div class="col-md-4">
                             <select class="form-control select2" data-placeholder="Choose Category" name="int_branch" >
                                    <option label="Choose Branch"></option>
                                        @foreach($branches as $key=>$branch) 

                                    <option value="{{$branch->pk_branch_id}}">{{$branch->branch_name}}</option>

                                    @endforeach
                                </select>
                             @if ($errors->has('int_branch'))
                                <span class="help-block">
                                    <strong class="error">{{ $errors->first('int_branch') }}</strong>
                                </span>
                            @endif

                    </div>
                </div>
            </div> --}}
                
                <div class="form-group">
                <div class="row">
                         <label class='col-md-3 control-label' style="margin-top:5px;text-align: right;"> Registration file number<span class="tx-danger">*</span>: </label>
                         <div class="col-md-4">
                            <input class="form-control" type="number" name="file_no" placeholder="Enter file no" required value="{{$register}}" readonly>
                            {{-- <input class="form-control" type="number" name="file_no" placeholder="Enter file no" required> --}}
                                @if ($errors->has('file_no'))
                                <span class="help-block">
                                    <strong class="error">{{ $errors->first('file_no') }}</strong>
                                </span>
                                @endif

                    </div>
                </div>
                </div>

                {{-- <div class="form-group">
                <div class="row">
                         <label class='col-md-3 control-label' style="margin-top:5px;text-align: right;"> Category<span class="tx-danger">*</span>: </label>
                         <div class="col-md-4">
                             <select class="form-control select2" data-placeholder="Choose Category" name="int_reg_category" required>
                                    <option label="Choose Category"></option>
                                        @foreach($class as $key=>$class) 

                                    <option value="{{$class->class_id}}">{{$class->class_name}}</option>

                                    @endforeach
                                </select>
                             @if ($errors->has('int_reg_category'))
                                <span class="help-block">
                                    <strong class="error">{{ $errors->first('int_reg_category') }}</strong>
                                </span>
                            @endif

                    </div>
                </div>
            </div> --}}


                <div class="form-group">
                <div class="row">
                         <label class='col-md-3 control-label' style="margin-top:5px;text-align: right;"> First Name<span class="tx-danger">*</span>:</label>
                         <div class="col-md-4">
                            <input class="form-control" type="text" name="first_name" placeholder="Enter first name" required="">
                                @if ($errors->has('first_name'))
                                <span class="help-block">
                                    <strong class="error">{{ $errors->first('first_name') }}</strong>
                                </span>
                                @endif

                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="row">
                         <label class='col-md-3 control-label' style="margin-top:5px;text-align: right;"> Middle Name:</label>
                         <div class="col-md-4">
                            <input class="form-control" type="text" name="middle_name" placeholder="Enter middle name" >
                                @if ($errors->has('middle_name'))
                                <span class="help-block">
                                    <strong class="error">{{ $errors->first('middle_name') }}</strong>
                                </span>
                                @endif

                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="row">
                         <label class='col-md-3 control-label' style="margin-top:5px;text-align: right;"> Last Name<span class="tx-danger">*</span>:</label>
                         <div class="col-md-4">
                            <input class="form-control" type="text" name="last_name" placeholder="Enter last name" >
                                @if ($errors->has('last_name'))
                                <span class="help-block">
                                    <strong class="error">{{ $errors->first('last_name') }}</strong>
                                </span>
                                @endif

                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="row">
                         <label class='col-md-3 control-label' style="margin-top:5px;text-align: right;"> Place:<span class="tx-danger">*</span>:</label>
                         <div class="col-md-4">
                            <input class="form-control" type="text" name="patient_place" placeholder="Enter Place" >
                                @if ($errors->has('patient_place'))
                                <span class="help-block">
                                    <strong class="error">{{ $errors->first('patient_place') }}</strong>
                                </span>
                                @endif

                    </div>
                </div>
            </div>
            <div class="form-group">
                    <div class="row">
                         <label class='col-md-3 control-label' style="margin-top:5px;text-align: right;"> Date of Birth: </label>
                         <div class="col-md-4">
                             <input class="form-control" id="example"  type="text" name="reg_dob" placeholder="Select date">
                             @if ($errors->has('reg_dob'))
                                <span class="help-block">
                                    <strong class="error">{{ $errors->first('reg_dob') }}</strong>
                                </span>
                            @endif

                    </div>
                </div>
            </div>
            <div class="form-group">
                    <div class="row">
                         <label class='col-md-3 control-label' style="margin-top:5px;text-align: right;"> Age: </label>
                         <div class="col-md-4">
                             <input class="form-control" id="example"  type="text" name="patient_age" placeholder="Age">
                             @if ($errors->has('patient_age'))
                                <span class="help-block">
                                    <strong class="error">{{ $errors->first('patient_age') }}</strong>
                                </span>
                            @endif

                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="row">
                         <label class='col-md-3 control-label' style="margin-top:5px;text-align: right;">Mobile Number:<span class="tx-danger">*</span>: </label>
                         <div class="col-md-4">
                            <input class="form-control" type="number" name="mob_number" placeholder="ex:+971512345687">
                                @if ($errors->has('mob_number'))
                                <span class="help-block">
                                    <strong class="error">{{ $errors->first('mob_number') }}</strong>
                                </span>
                                @endif

                    </div>
                </div>
            </div>
                <div class="form-group">
                <div class="row">
                         <label class='col-md-3 control-label' style="margin-top:5px;text-align: right;">Gender:<span class="tx-danger">*</span>:</label>
                         <div class="col-md-4">
                            <select class="form-control select2" data-placeholder="Choose unit" name="gender" >
                                    <option value="">-- select one --</option>
                                    <option value="1">Male</option>
                                    <option value="2">Female</option>
                                </select>
                                @if ($errors->has('gender'))
                                <span class="help-block">
                                    <strong class="error">{{ $errors->first('gender') }}</strong>
                                </span>
                                @endif

                    </div>
                </div>
            </div>
            {{-- <div class="form-group">
                <div class="row">
                         <label class='col-md-3 control-label' style="margin-top:5px;text-align: right;">Nationality:<span class="tx-danger">*</span>:</label>
                         <div class="col-md-4">
                            <select class="form-control select2" data-placeholder="Choose unit" name="nationality" >
                                    <option value="">select</option>
                                      <option value="afghan">Afghan</option>
                                      <option value="albanian">Albanian</option>
                                      <option value="algerian">Algerian</option>
                                      <option value="american">American</option>
                                      <option value="andorran">Andorran</option>
                                      <option value="angolan">Angolan</option>
                                      <option value="antiguans">Antiguans</option>
                                      <option value="argentinean">Argentinean</option>
                                      <option value="armenian">Armenian</option>
                                      <option value="australian">Australian</option>
                                      <option value="austrian">Austrian</option>
                                      <option value="azerbaijani">Azerbaijani</option>
                                      <option value="bahamian">Bahamian</option>
                                      <option value="bahraini">Bahraini</option>
                                      <option value="bangladeshi">Bangladeshi</option>
                                      <option value="barbadian">Barbadian</option>
                                      <option value="barbudans">Barbudans</option>
                                      <option value="batswana">Batswana</option>
                                      <option value="belarusian">Belarusian</option>
                                      <option value="belgian">Belgian</option>
                                      <option value="belizean">Belizean</option>
                                      <option value="beninese">Beninese</option>
                                      <option value="bhutanese">Bhutanese</option>
                                      <option value="bolivian">Bolivian</option>
                                      <option value="bosnian">Bosnian</option>
                                      <option value="brazilian">Brazilian</option>
                                      <option value="british">British</option>
                                      <option value="bruneian">Bruneian</option>
                                      <option value="bulgarian">Bulgarian</option>
                                      <option value="burkinabe">Burkinabe</option>
                                      <option value="burmese">Burmese</option>
                                      <option value="burundian">Burundian</option>
                                      <option value="cambodian">Cambodian</option>
                                      <option value="cameroonian">Cameroonian</option>
                                      <option value="canadian">Canadian</option>
                                      <option value="cape verdean">Cape Verdean</option>
                                      <option value="central african">Central African</option>
                                      <option value="chadian">Chadian</option>
                                      <option value="chilean">Chilean</option>
                                      <option value="chinese">Chinese</option>
                                      <option value="colombian">Colombian</option>
                                      <option value="comoran">Comoran</option>
                                      <option value="congolese">Congolese</option>
                                      <option value="costa rican">Costa Rican</option>
                                      <option value="croatian">Croatian</option>
                                      <option value="cuban">Cuban</option>
                                      <option value="cypriot">Cypriot</option>
                                      <option value="czech">Czech</option>
                                      <option value="danish">Danish</option>
                                      <option value="djibouti">Djibouti</option>
                                      <option value="dominican">Dominican</option>
                                      <option value="dutch">Dutch</option>
                                      <option value="east timorese">East Timorese</option>
                                      <option value="ecuadorean">Ecuadorean</option>
                                      <option value="egyptian">Egyptian</option>
                                      <option value="emirian">Emirian</option>
                                      <option value="equatorial guinean">Equatorial Guinean</option>
                                      <option value="eritrean">Eritrean</option>
                                      <option value="estonian">Estonian</option>
                                      <option value="ethiopian">Ethiopian</option>
                                      <option value="fijian">Fijian</option>
                                      <option value="filipino">Filipino</option>
                                      <option value="finnish">Finnish</option>
                                      <option value="french">French</option>
                                      <option value="gabonese">Gabonese</option>
                                      <option value="gambian">Gambian</option>
                                      <option value="georgian">Georgian</option>
                                      <option value="german">German</option>
                                      <option value="ghanaian">Ghanaian</option>
                                      <option value="greek">Greek</option>
                                      <option value="grenadian">Grenadian</option>
                                      <option value="guatemalan">Guatemalan</option>
                                      <option value="guinea-bissauan">Guinea-Bissauan</option>
                                      <option value="guinean">Guinean</option>
                                      <option value="guyanese">Guyanese</option>
                                      <option value="haitian">Haitian</option>
                                      <option value="herzegovinian">Herzegovinian</option>
                                      <option value="honduran">Honduran</option>
                                      <option value="hungarian">Hungarian</option>
                                      <option value="icelander">Icelander</option>
                                      <option value="indian">Indian</option>
                                      <option value="indonesian">Indonesian</option>
                                      <option value="iranian">Iranian</option>
                                      <option value="iraqi">Iraqi</option>
                                      <option value="irish">Irish</option>
                                      <option value="israeli">Israeli</option>
                                      <option value="italian">Italian</option>
                                      <option value="ivorian">Ivorian</option>
                                      <option value="jamaican">Jamaican</option>
                                      <option value="japanese">Japanese</option>
                                      <option value="jordanian">Jordanian</option>
                                      <option value="kazakhstani">Kazakhstani</option>
                                      <option value="kenyan">Kenyan</option>
                                      <option value="kittian and nevisian">Kittian and Nevisian</option>
                                      <option value="kuwaiti">Kuwaiti</option>
                                      <option value="kyrgyz">Kyrgyz</option>
                                      <option value="laotian">Laotian</option>
                                      <option value="latvian">Latvian</option>
                                      <option value="lebanese">Lebanese</option>
                                      <option value="liberian">Liberian</option>
                                      <option value="libyan">Libyan</option>
                                      <option value="liechtensteiner">Liechtensteiner</option>
                                      <option value="lithuanian">Lithuanian</option>
                                      <option value="luxembourger">Luxembourger</option>
                                      <option value="macedonian">Macedonian</option>
                                      <option value="malagasy">Malagasy</option>
                                      <option value="malawian">Malawian</option>
                                      <option value="malaysian">Malaysian</option>
                                      <option value="maldivan">Maldivan</option>
                                      <option value="malian">Malian</option>
                                      <option value="maltese">Maltese</option>
                                      <option value="marshallese">Marshallese</option>
                                      <option value="mauritanian">Mauritanian</option>
                                      <option value="mauritian">Mauritian</option>
                                      <option value="mexican">Mexican</option>
                                      <option value="micronesian">Micronesian</option>
                                      <option value="moldovan">Moldovan</option>
                                      <option value="monacan">Monacan</option>
                                      <option value="mongolian">Mongolian</option>
                                      <option value="moroccan">Moroccan</option>
                                      <option value="mosotho">Mosotho</option>
                                      <option value="motswana">Motswana</option>
                                      <option value="mozambican">Mozambican</option>
                                      <option value="namibian">Namibian</option>
                                      <option value="nauruan">Nauruan</option>
                                      <option value="nepalese">Nepalese</option>
                                      <option value="new zealander">New Zealander</option>
                                      <option value="ni-vanuatu">Ni-Vanuatu</option>
                                      <option value="nicaraguan">Nicaraguan</option>
                                      <option value="nigerien">Nigerien</option>
                                      <option value="north korean">North Korean</option>
                                      <option value="northern irish">Northern Irish</option>
                                      <option value="norwegian">Norwegian</option>
                                      <option value="omani">Omani</option>
                                      <option value="pakistani">Pakistani</option>
                                      <option value="palauan">Palauan</option>
                                      <option value="panamanian">Panamanian</option>
                                      <option value="papua new guinean">Papua New Guinean</option>
                                      <option value="paraguayan">Paraguayan</option>
                                      <option value="peruvian">Peruvian</option>
                                      <option value="polish">Polish</option>
                                      <option value="portuguese">Portuguese</option>
                                      <option value="qatari">Qatari</option>
                                      <option value="romanian">Romanian</option>
                                      <option value="russian">Russian</option>
                                      <option value="rwandan">Rwandan</option>
                                      <option value="saint lucian">Saint Lucian</option>
                                      <option value="salvadoran">Salvadoran</option>
                                      <option value="samoan">Samoan</option>
                                      <option value="san marinese">San Marinese</option>
                                      <option value="sao tomean">Sao Tomean</option>
                                      <option value="saudi">Saudi</option>
                                      <option value="scottish">Scottish</option>
                                      <option value="senegalese">Senegalese</option>
                                      <option value="serbian">Serbian</option>
                                      <option value="seychellois">Seychellois</option>
                                      <option value="sierra leonean">Sierra Leonean</option>
                                      <option value="singaporean">Singaporean</option>
                                      <option value="slovakian">Slovakian</option>
                                      <option value="slovenian">Slovenian</option>
                                      <option value="solomon islander">Solomon Islander</option>
                                      <option value="somali">Somali</option>
                                      <option value="south african">South African</option>
                                      <option value="south korean">South Korean</option>
                                      <option value="spanish">Spanish</option>
                                      <option value="sri lankan">Sri Lankan</option>
                                      <option value="sudanese">Sudanese</option>
                                      <option value="surinamer">Surinamer</option>
                                      <option value="swazi">Swazi</option>
                                      <option value="swedish">Swedish</option>
                                      <option value="swiss">Swiss</option>
                                      <option value="syrian">Syrian</option>
                                      <option value="taiwanese">Taiwanese</option>
                                      <option value="tajik">Tajik</option>
                                      <option value="tanzanian">Tanzanian</option>
                                      <option value="thai">Thai</option>
                                      <option value="togolese">Togolese</option>
                                      <option value="tongan">Tongan</option>
                                      <option value="trinidadian or tobagonian">Trinidadian or Tobagonian</option>
                                      <option value="tunisian">Tunisian</option>
                                      <option value="turkish">Turkish</option>
                                      <option value="tuvaluan">Tuvaluan</option>
                                      <option value="ugandan">Ugandan</option>
                                      <option value="ukrainian">Ukrainian</option>
                                      <option value="uruguayan">Uruguayan</option>
                                      <option value="uzbekistani">Uzbekistani</option>
                                      <option value="venezuelan">Venezuelan</option>
                                      <option value="vietnamese">Vietnamese</option>
                                      <option value="welsh">Welsh</option>
                                      <option value="yemenite">Yemenite</option>
                                      <option value="zambian">Zambian</option>
                                      <option value="zimbabwean">Zimbabwean</option>
                                </select> --}}
{{--                                 @if ($errors->has('nationality'))
                                <span class="help-block">
                                    <strong class="error">{{ $errors->first('nationality') }}</strong>
                                </span>
                                @endif

                    </div>
                </div>
            </div>  --}} 
               <div class="form-group">
                <div class="row">
                         <label class='col-md-3 control-label' style="margin-top:5px;text-align: right;">Marital status:<span class="tx-danger">*</span>:</label>
                         <div class="col-md-4">
                            <select class="form-control select2" data-placeholder="Choose unit" name="marital_status" >
                                    <option label="Choose marital status"></option>
                                        {{-- @foreach($units as $key=>$unit)  --}}

                                    <option value="1">Single</option>
                                    <option value="2">Married</option>

                                    {{-- @endforeach --}}
                                </select>
                                {{-- <input class="form-control" type="text" name="unit" placeholder="Enter Unit" required> --}}
                                @if ($errors->has('marital_status'))
                                <span class="help-block">
                                    <strong class="error">{{ $errors->first('marital_status') }}</strong>
                                </span>
                                @endif

                    </div>
                </div>
            </div>  
            {{-- <div class="form-group">
                <div class="row">
                         <label class='col-md-3 control-label' style="margin-top:5px;text-align: right;"> Emirates ID<span class="tx-danger">*</span>:</label>
                         <div class="col-md-4">
                            <input class="form-control" type="text" name="emirates_id" placeholder="Enter emirates id" >
                                @if ($errors->has('emirates_id'))
                                <span class="help-block">
                                    <strong class="error">{{ $errors->first('emirates_id') }}</strong>
                                </span>
                                @endif

                    </div>
                </div>
            </div>
            
                <div class="form-group">
                <div class="row">
                         <label class='col-md-3 control-label' style="margin-top:5px;text-align: right;"> Upload File of Emirates ID: <span class="tx-danger">*</span>:</label>
                         <div class="col-md-4">

                            <input type="file" id="exampleInputFile" name="emites_id_file">

                    </div>
                </div>
                </div> --}}
                {{-- <div class="form-group">
                <div class="row">
                         <label class='col-md-3 control-label' style="margin-top:5px;text-align: right;">Insurance Status:<span class="tx-danger">*</span>:</label>
                         <div class="col-md-4">
                            <select class="form-control select2" data-placeholder="Choose unit" name="insurance_status" id="role_id">
                                    <option value="">-- select one --</option>
                                    <option value="1">Yes</option>
                                    <option value="0">No</option>
                                </select>
                                @if ($errors->has('insurance_status'))
                                <span class="help-block">
                                    <strong class="error">{{ $errors->first('insurance_status') }}</strong>
                                </span>
                                @endif

                    </div>
                </div>
            </div> --}}
           {{--  <div class="form-group"  id="insurance">
                <div class="row">
                         <label class='col-md-3 control-label' style="margin-top:5px;text-align: right;"> Insurance Name<span class="tx-danger">*</span>:</label>
                         <div class="col-md-4">
                            <input class="form-control" type="text" name="insurance_name" placeholder="Enter emirates id">
                                @if ($errors->has('insurance_name'))
                                <span class="help-block">
                                    <strong class="error">{{ $errors->first('insurance_name') }}</strong>
                                </span>
                                @endif

                    </div>
                </div>
            </div> --}}
            {{-- <div class="form-group" id="expiry">
                <div class="row">
                         <label class='col-md-3 control-label' style="margin-top:5px;text-align: right;"> Expiry Date<span class="tx-danger">*</span>:</label>
                         <div class="col-md-4">
                            <input class="form-control" type="text" name="expiry_date" placeholder="Enter expiry date"  >
                                @if ($errors->has('expiry_date'))
                                <span class="help-block">
                                    <strong class="error">{{ $errors->first('expiry_date') }}</strong>
                                </span>
                                @endif

                    </div>
                </div>
            </div> --}}
           {{--  <div class="form-group" id="percent_insurance">
                <div class="row">
                         <label class='col-md-3 control-label' style="margin-top:5px;text-align: right;"> Insurance Percentage<span class="tx-danger">*</span>:</label>
                         <div class="col-md-4">
                            <input class="form-control" type="text" name="percent_insurance" placeholder="Enter Insurance percentage" >
                                @if ($errors->has('percent_insurance'))
                                <span class="help-block">
                                    <strong class="error">{{ $errors->first('percent_insurance') }}</strong>
                                </span>
                                @endif

                    </div>
                </div>
            </div> --}}

                <div class="form-group">
                <div class="row">
                         <label class='col-md-3 control-label' style="margin-top:5px;text-align: right;"></label>
                         <div class="col-md-4">
                           <button class="btn btn-primary bd-0">Submit</button>
                        <a href="{{ url('/admin/view_product') }}" class="btn btn-secondary bd-0">Back</a>

                    </div>
                </div>
            </div>

            </form>    
        </div><!-- section-wrapper -->
    </div>
    </div>


@include('admin.includes.footer')
<script type="text/javascript">
    $(function () {
      $('#example').datetimepicker({
        format: 'YYYY-MM-DD'
      });      
  });
    setTimeout(function(){ $("#msg").fadeOut(2000); }, 2000);
</script>
<script type="text/javascript">
    $("#role_id").change(function()
        {
            var rl = $("#role_id").val();
            // alert(rl);
            if(rl==1){
                $('#percent_insurance').show();
                $('#expiry').show();
                $('#insurance').show();
            }else{
              $('#percent_insurance').hide();
              $('#expiry').hide();
              $('#insurance').hide();

            }
        });
</script>