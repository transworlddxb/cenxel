@include('admin.includes.head')
<link href="{{asset('assets/backend/css/darkbox.css')}}" rel="stylesheet">
<style type="text/css">
.photogallery li{position: relative; overflow: hidden; min-height: 200px; background: #eee; border: 2px solid #fff;}
.photogallery li:nth-child(odd){background: #bbb;}
.photogallery li img{position: absolute; top: -9999px; right: -9999px; bottom: -9999px; left: -9999px; margin: auto; max-width: none; /*max-height: 100%;*/ width: 100%; height: auto;}
</style>

<div class="slim-mainpanel">
  <div class="container">
    <div class="slim-pageheader">
      <ol class="breadcrumb slim-breadcrumb">
        <li class="breadcrumb-item"><a href="#">Home</a></li>
        <li class="breadcrumb-item">Customer list</li>
        {{-- <li class="breadcrumb-item active" aria-current="page">Form Wizards</li> --}}
      </ol>
      <h6 class="slim-pagetitle mb-2">Customer List</h6>
    </div><!-- slim-pageheader -->    


    <div class="section-wrapper">

      <div class="row">
        <div class="col-sm-12 col-lg-6">
          <label class="section-title"> Details</label>
          <p class="mg-b-20 mg-sm-b-40">List of Customers</p></div>
          <div class="col-sm-12 col-lg-6">
            <div class="d-flex  justify-content-md-end justify-content-center">
              <a href="{{ url('/admin/reg_usr') }}" class="btn btn-primary " style="margin-bottom: 10px;"><i class="fa fa-plus"></i> Click to Register new customer</a>
            </div>  
          </div>
        </div>


        <div class="table-wrapper">
          <table id="productlistListTable" class="table display" width='100%'>
            <thead>
              <tr>
                <th >SI.NO</th>
                <th >FILE No.</th>
                <th >FULL NAME</th>
                <th >DATE OF BIRTH</th>
                <th >MOBILE</th>
                <th >DUE AMOUNT</th>
                <th >GENDER</th>
                <th >ACTION</th>
              </tr>
            </thead>
            <tbody>
              @foreach($register as $key => $register)
                    <tr>
                      <td >{{ $key+1 }}</td>
                      <td >{{ $register->file_no }}</td>
                      <td >{{ $register->first_name}} {{ $register->middle_name}} {{ $register->last_name}}</td>
                      <td >{{ $register->reg_dob }}</td>
                      <td >{{ $register->mob_number }}</td>
                      <td >{{ $register->final_due_amount }} AED</td>
                        @if($register->gender==1)
                      <td>Male </td>
                      @endif
                       @if($register->gender==2)
                      <td >female</td>
                      @endif
                      <td >

                        <a href="{{ url('/admin/edit_details/'.$register->pk_int_reg_id) }}" type="button" class="btn btn-primary" title="Edit" value="Edit"><i class="fa fa-pencil"></i></a>&nbsp;
                        <a href="{{ url('/admin/appointment/'.$register->pk_int_reg_id) }}" type="button" class="btn btn-primary" title="Appointment" value="Edit">Appointment</a>

                      
                    </td>
                    
                  </tr>
                  @endforeach
            </tbody>
          </table>
        </div>
      </div><!-- section-wrapper -->
    </div>

    @include('admin.includes.footer')
    </script>
    <script src="{{asset('assets/backend/lib/datatables/js/jquery.dataTables.js')}}"></script>
    <script src="{{asset('assets/backend/lib/select2/js/select2.min.js')}}"></script>
    <script src="{{asset('assets/backend/js/darkbox.min.js')}}"></script>

    <script type="text/javascript">
      $(document).on("click", ".del", function (event) {
    var result = confirm("Are you sure want to delete?");
    if (result) {
                  //Logic to delete the item
                  return true;
                }
                else
                {
                 return false;
               }
             });
  


      $('#productlistListTable').DataTable({
        scrollX:true,
        language: {
          searchPlaceholder: 'Search...',
          sSearch: '',
          lengthMenu: '_MENU_ items/page',
        },
        "ordering":false,
        "destroy": true,
        "processing": true,
      });
    </script>
