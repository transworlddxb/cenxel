@extends('beautymail::templates.minty')

@section('content')

	@include('beautymail::templates.minty.contentStart')
		<tr>
			<td class="title">
				There is a enquiry from your websites Real Estate Section.
			</td>
		</tr>
		<tr>
			<td class="paragraph">
				Details are here.
			</td>
		</tr>
		<tr>
			<td class="title">
				Name : {{$name}}
			</td>
		</tr>
		<tr>
			<td class="title">
				Email : {{$email}}
			</td>
		</tr>
		<tr>
			<td class="title">
				Contact : {{$phn_num}}
			</td>
		</tr>
		<tr>
			<td class="title">
				Message : {{$mesge}}
			</td>
		</tr>
		<tr>
			<td width="100%" height="10"></td>
		</tr>
		<tr>
			<td width="100%" height="10"></td>
		</tr>
		{{-- <tr>
			<td class="paragraph">
				More paragraph text.
			</td>
		</tr> --}}
		<tr>
			<td width="100%" height="25"></td>
		</tr>
		{{-- <tr>
			<td>
				@include('beautymail::templates.minty.button', ['text' => 'Sign in', 'link' => '#'])
			</td>
		</tr> --}}
		<tr>
			<td width="100%" height="25"></td>
		</tr>
	@include('beautymail::templates.minty.contentEnd')

@stop