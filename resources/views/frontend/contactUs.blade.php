@include('includes.header')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css">

<style type="text/css">
input[type=number]::-webkit-inner-spin-button,
input[type=number]::-webkit-outer-spin-button {
  -webkit-appearance: none;
  margin: 0;
}
</style>

<?php
$message=Session::get('message');
$msg=explode("|",$message);
?>

@if ($message = Session::get('message'))
<div  style='z-index:9999; position:absolute;width:30%;right:0px; padding:10px 15px 0px 15px;' id='msg'>
 <div class="alert alert-{{$msg[0]}}" role="alert">
   <button type="button" class="close" data-dismiss="alert" aria-label="Close">
     <span aria-hidden="true">&times;</span>
   </button>
   {{ $msg[1] }}
 </div><!-- alert -->
</div>
@endif
<!----------- Start of our services--------------------->
<section>
<div class="container-fluid cntclr">
	
	<div class="row cntHdr ">
		<h4 data-aos="zoom-in" data-aos-delay="700" data-aos-duration="1000">Feel free to drop us a message</h4>
	</div>

	<div class="row contactUsMsg">
		<div class="col-md-6 col-md-offset-3" data-aos="fade-up" data-aos-delay="0"
    data-aos-duration="1000">
        <div class="well well-sm">
			<form class="form-horizontal" method="post" action="{{url('/submit_contact_form')}}" enctype="multipart/form-data">
            {{csrf_field()}}
          	<fieldset>
           		<div class="form-group">
				    <div class="col-md-12">
				        <input id="name" name="name" type="text" placeholder="Name" class="form-control">
				    </div>
				</div>
	     		<div class="form-group">
			        <div class="col-md-12">
			            <input id="email" name="email" type="email" placeholder="Email" class="form-control">
			        </div>
			    </div>
			    <!-- Phone number input-->
			    <div class="form-group">
			        <div class="col-md-12">
			            <input id="phn_num" name="phn_num" type="number" placeholder="Phone" class="form-control">
			        </div>
			    </div>
            <!-- Message body -->
			    <div class="form-group">
			        <div class="col-md-12">
			            <textarea class="form-control" id="message" name="message" placeholder="message " rows="5"></textarea>
			        </div>
			    </div>
			    <div class="form-group">
			        <div class="col-md-12 subtn text-right">
			            <button type="submit" class="btn btn-success ">Submit</button>
			        </div>
			    </div>
			</fieldset>
			</form>
		</div>
		</div>

		<div class="col-md-6 cntImg" data-aos="zoom-in" data-aos-delay="500"
    data-aos-duration="1000">
			<img class="img-fluid" src="images/ALPHA BRAIN - FINAL(3).png"   >
		</div>
	</div>
	<div class="row contactUsMsg">
		<div class="col-md-12">
			        <div class="addrListgrp" data-aos="fade-up" data-aos-delay="300" data-aos-duration="1000">
					<div class="list-group">
					<ul class="cntusTxt">
						<li class="sptxt2"><img src="images/PHONE ICON.png" alt="phnIcon" ><span>&nbsp; +(971) 55 758 6218</span></li>
						<li class="sptxt2 txt2"><img src="images/MAIL.png" alt="phnIcon" ><span>&nbsp; alphahrdxb@gmail.com</span></i></li>

						<li><img src="images/ADDRESS ICON.png" alt="phnIcon" ><span > &nbsp;OFFICE 240, Unique World Business centre, Karama, Dubai, UAE<br><span class="sptxt1">LT-03105 VILNIUS</span></span></i></li>
					</ul>
					</div>
					</div>
				</div>	          
	</div>
	</div>
	</section>

<!----------- Start of Quality policy--------------------->
<section>
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12 locationMap" data-aos="zoom-in" data-aos-delay="500" data-aos-duration="1000">
				<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d14434.305774109222!2d55.3084917!3d25.2511872!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3e5f432958555551%3A0x19d59190950ee7aa!2sUnique%20World%20Business%20Center%20LLC!5e0!3m2!1sen!2sae!4v1694783908039!5m2!1sen!2sae" width="600" height="450" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
			</div>
		</div>
	</div>
</section>

@include('includes.footer')
