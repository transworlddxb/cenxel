<!DOCTYPE html>
<html>
<head>
  <title>Alpha Brain HR Consultancy LLC </title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  
  <link rel="stylesheet" type="text/css" href="{{asset('assets/frontend/css/bootstrap.min.css')}}">
  <link rel="stylesheet" type="text/css" href="{{asset('assets/frontend/css/stylesMain.css')}}">
  <link rel="stylesheet" type="text/css" href="{{asset('assets/frontend/css/all.css')}}">
  <link rel="stylesheet" type="text/css" href="{{asset('assets/frontend/css/all.min.css')}}">
   <link rel="stylesheet" type="text/css" href="{{asset('assets/frontend/css/aos.css')}}">
  <link rel="stylesheet" type="text/css" href="{{asset('assets/frontend/css/svg-with-js.css')}}">
  <link rel="shortcut icon" href="{{asset('assets/frontend/images/finarsalpha tab logo.png')}}" type="image/x-icon">
  {{-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous"> --}}
  <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@400;500;600;700&display=swap" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@500;600;700&display=swap" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css2?family=Lato:wght@400;700&display=swap" rel="stylesheet">

</head>
<body  >
<!------------------------Start of header------------>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css">

<style type="text/css">
input[type=number]::-webkit-inner-spin-button,
input[type=number]::-webkit-outer-spin-button {
  -webkit-appearance: none;
  margin: 0;
}
</style>

<?php
$message=Session::get('message');
$msg=explode("|",$message);
?>

@if ($message = Session::get('message'))
<div  style='z-index:9999; position:absolute;width:30%;right:0px; padding:10px 15px 0px 15px;' id='msg'>
 <div class="alert alert-{{$msg[0]}}" role="alert">
   <button type="button" class="close" data-dismiss="alert" aria-label="Close">
     <span aria-hidden="true">&times;</span>
   </button>
   {{ $msg[1] }}
 </div><!-- alert -->
</div>
@endif

<section>
	<div class="container-fluid">
		<div class="row hdrRw">
	<div class="d-flex flex-row-reverse finrasHeader fixed-top">
  <div class="p-2 headerItem"><i class="far fa-clock"></i>
                        <span>Mon-Sat:9am to 6pm</span></div>
  <div class="p-2 headerItem"><i class="far fa-envelope"></i>
                        <span>alphahrdxb@gmail.com</span>
    </div>

    	
    	<div class="p-2 headerItem"><span class="fa-stack" style="vertical-align: top;">
  <i class="far fa-circle fa-stack-2x"></i>
  <i class="fas fa-phone-alt fa-stack-1x"></i>
</span><span>+(971) 55 758 6218</span></div>

  </div>
</div>
</div>
</section>
<!---------------------Menu-------------------------------------->
<section>
	<div class="container-fluid">
	<div class="row no-padding">
	<div class="col-lg-12 col-md-12 " >
	<nav class="navbar navbar-expand-md bg-dark fixed-top fin_nav" style="height:150px">
		
		<a class="navbar-brand" href="{{ url('/') }}"><img src="images/ALPHA BRAIN logo.png" style="width:220px !important; height:220px !important;"></a>
		  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"><i class="fa fa-bars" aria-hidden="true"></i></span>
		  </button>

		  	<div class="collapse navbar-collapse" id="navbarSupportedContent">
		    	<ul class="navbar-nav ml-auto">
		      		<li class="nav-item active">
		        		<a class="nav-link" href="{{ url('/') }}">Home <span class="sr-only">(current)</span></a>
		      		</li>
				    <li class="nav-item">
				        <a class="nav-link" href="{{ url('/'.'#aboutUsSection') }}">About Us</a>
				    </li>
		      		<li class="nav-item dropdown">
		        		<a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-hover="dropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Services</a>
		       
       
        				<div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
							<a class="dropdown-item" href="{{ url('/residency_permit') }}">Invest & Get EU Residency Permit</a>
							<a class="dropdown-item" href="{{ url('/agriculture') }}">Agriculture</a>
							<a class="dropdown-item" href="{{ url('/foodNbeverage') }}">Food & Beverage</a>
							<a class="dropdown-item" href="{{ url('/hospitality') }}">Hospitality</a>
							<a class="dropdown-item" href="{{ url('/manpowerSupply') }}">Manpower Supply</a>
          					
        				</div>
      				</li>
		       
		      		<li class="nav-item">
						<a class="nav-link" href="{{ url('/contact') }}">Contact Us</a>
					  </li>
					  <li class="nav-item">
						<a class="nav-link" href="{{ url('/blog') }}">Blogs</a>
					  </li>
		      		<li class="nav-item">
				
          				<form action="" class="search-form" autocomplete="off">
                			<div class="form-group has-feedback">
            					<label for="search" class="sr-only">Search</label>
            					<input type="text" class="form-control" name="search" id="search" placeholder="search">
              					<span class="form-control-feedback"><i class="fa fa-search"></i></span>
            				</div>
            			</form>        
					</li>
				</ul>
		  	</div>
		
		</nav>
</div>
</div>
</div>
</section>
<!------------------------End of Menu----------------------------------->


<section>
		
	<div class="container-fluid rbntop">
		<div class="lmdevice"><img src="images/europe-permit-page-head-3@2x.jpg" ></div>
		<div class="smdevice"><img src="images/europe-permit-page-head-3@4x.jpg" ></div>
		
		<div class="row rbn">
			
		
			<div class="col-md-1 bnnrDet" data-aos="fade-right" data-aos-delay="0"
    		data-aos-duration="1000">
				<div id="orSeparator">
				<div class="row" id="socialSeparatorTop"></div>
				<div class="row" id="or" class="list-group">
					<ul class="orSptr">
						<li><a href=""><i class="fab fa-facebook-f zoom" style="color: #A7FCFF"></i></a></li>
						<li><a href=""><i class="fab fa-twitter zoom" style="color: #A7FCFF"></i></a></li>
						<li><a href=""><i class="fab fa-linkedin-in zoom" style="color: #A7FCFF"></i></a></li>
						<li><a href=""><i class="fab fa-instagram zoom" style="color: #A7FCFF"></i></a></li>
					</ul>
				</div>
				<div class="row" id="socialSeparatorBottom"></div>
				</div>
			</div>

		<div class="col-md-7 bnnrHeadingServices mdHdg">
				<h3 data-aos="fade-up" data-aos-delay="100" data-aos-duration="1000">Agriculture</h3>
		</div>
		<div class="col-md-10 bnnrHeadingServices smHdg">
				<h3 data-aos="fade-up" data-aos-delay="100" data-aos-duration="1000">Agriculture</h3>
		</div>

		</div>
	</div>
	

	</section>



<!----------- Start of about us--------------------->
<section>
	<div class="container">
		<div class="row r1 servDet ">
		<div class="col-md-6 servImg" data-aos="fade-up" data-aos-delay="100" data-aos-duration="1000" style="margin-top: -30px;">
			<img src="images/agriculture4.jpg" height="auto" alt="" >	
		</div>
		<div class="col-md-6 servCont" data-aos="fade-up" data-aos-delay="100" data-aos-duration="1000">
			{{-- <h4 style="color:#1296B7;">Commercial Property </h4> --}}
				  {{-- <h4 class="sptxt3" style="color:#1296B7;"> (Buying & Selling) </h4> --}}
				  {{-- <h6 style="color:#1296B7;">Investing in property in Europe is an open door to a lot of opportunities. </h6> --}}
		  <div>
			<p style="text-align: justify;text-justify: inter-word;">Agriculture always plays an essential role in the economy of every country. Not only because it tends to provide foods for the entire population of a country but agriculture helps to connect and interact with all the related industries of that country. It is one of the main pillars of the European Union also. Expensive machinery, the absence of infrastructure and lack of knowledge are some of the challenges the Agriculture sector has to overcome today.<br> What is the appropriate regulatory framework to encourage the take-up of new technologies? How can traditional farming be combined with technologies? What role is left for farmers in precision agriculture? What are the funding requirements and investments needed? What solutions are needed to improve farmers’ knowledge of new technologies?  Answers to these questions enable farmers to be more precise, efficient and helps them to improve productivity and sustainability while remaining competitive. The benefits of this new approach to farming apply to all farmers, regardless of their size.</p>
		</div>
			
			</div>
			<div class="row r1 servDet" style="margin-top: -30px;">
				<div class="col-md-12" data-aos="fade-up" data-aos-delay="100" data-aos-duration="1000">
				  <div>
					<p style="text-align: justify;text-justify: inter-word;"> A stable agricultural industry plays an important role to ensure the food security of a country. Food security is considered as one of the basic requirements of any nation. Lithuania is a massive country blessed with abundant natural resources and resourceful people, moulded by a harsh physical environment and challenging trading conditions. Alpha Brain HR Consultancy LLC develops and manages its large-scale farms and it is expanding in other areas of the country. On the other side, we are offering farmers in Lithuania and the surrounding region quality products and services for a wide variety of agricultural activities from land preparation to harvesting.<br> We strive to provide our clients and partners agricultural solutions tailored to the region that increases productivity and cost-effectiveness. We help farmers who face difficulties in embracing the latest technologies’ advantages of new-age farming methods. We believe that agriculture needs topmost priority because the world would fail to succeed if agriculture could not be successful.</p>
					
				  </div>
					
					</div>
		
			
				</div>
		{{-- <div class="col-md-6 servCont" data-aos="fade-up" data-aos-delay="600" data-aos-duration="1000">

			<div class="row justify-content-center updownBttn" >
				
			<button onclick="myFunction()"><i class="fas fa-chevron-up"></i></button>
		
		</div>
		<div class="row justify-content-center">
			<div id="myDIV" class="example contHeight2" >
			<div id="content">
			<p>Agriculture always plays an essential role in the economy of every country. Not only because it tends to provide foods for the entire population of a country but agriculture helps to connect and interact with all the related industries of that country. It is one of the main pillars of the European Union also. Expensive machinery, the absence of infrastructure and lack of knowledge are some of the challenges the Agriculture sector has to overcome today.<br> What is the appropriate regulatory framework to encourage the take-up of new technologies? How can traditional farming be combined with technologies? What role is left for farmers in precision agriculture? What are the funding requirements and investments needed? What solutions are needed to improve farmers’ knowledge of new technologies?  Answers to these questions enable farmers to be more precise, efficient and helps them to improve productivity and sustainability while remaining competitive. The benefits of this new approach to farming apply to all farmers, regardless of their size.<br> A stable agricultural industry plays an important role to ensure the food security of a country. Food security is considered as one of the basic requirements of any nation. Lithuania is a massive country blessed with abundant natural resources and resourceful people, moulded by a harsh physical environment and challenging trading conditions. Alpha Brain HR Consultancy LLC develops and manages its large-scale farms and it is expanding in other areas of the country. On the other side, we are offering farmers in Lithuania and the surrounding region quality products and services for a wide variety of agricultural activities from land preparation to harvesting.<br> We strive to provide our clients and partners agricultural solutions tailored to the region that increases productivity and cost-effectiveness. We help farmers who face difficulties in embracing the latest technologies’ advantages of new-age farming methods. We believe that agriculture needs topmost priority because the world would fail to succeed if agriculture could not be successful.</p>

		</div>
			
		</div>
			</div>
			<div class="row justify-content-center updownBttn">
			<button onclick="myFunction1()"><i class="fas fa-chevron-down"></i></button>
		</div>
		</div> --}}
		</div>
	</div>
</section>
<!---------------Enquiry Form---------------------------------------->
<section>
		<div class="container-fluid">
			<div class="row justify-content-center enqMsg">
				<div class="col-md-6 col-md-offset-3 enqCol" data-aos="fade-up" data-aos-delay="100" data-aos-duration="1000">
        			<div class="well well-sm">
						<form class="form-horizontal" method="post" action="{{url('/submit_agricul_form')}}" enctype="multipart/form-data">
							{{csrf_field()}}
          			<fieldset>
           			 <legend >Enquiry</legend>
    
			            
			            <div class="form-group">
			             
			              <div class="col-md-12">
			                <input id="name" name="name" type="text" placeholder="Name" class="form-control">
			              </div>
			            </div>
     
			          
			            <div class="form-group">
			              
			              <div class="col-md-12">
			                <input id="email" name="email" type="text" placeholder="Email" class="form-control">
			              </div>
			            </div>
			    
			            <div class="form-group">
			              
			              <div class="col-md-12">
			                <input id="phn_num" name="phn_num" type="text" placeholder="Phone" class="form-control">
			              </div>
			            </div>
          
			            <div class="form-group">
			              
			              <div class="col-md-12">
			                <textarea class="form-control" id="message" name="message" placeholder="message " rows="5"></textarea>
			              </div>
			            </div>
			    
			          
			            <div class="form-group">
			              <div class="col-md-12 subtn text-right">
			                <button type="submit" class="btn btn-success ">Submit</button>
			              </div>
			            </div>
			          </fieldset>
			          </form>
			        </div>
			      </div>

				<div class="col-md-5">
				<div class="clr1" data-aos="zoom-in" data-aos-delay="500" data-aos-duration="1000">
						<a class="zoom1" href="{{ url('/'.'#ourServicesdd') }}" style="text-decoration: none;"><h3>Other Services<br> > </h3></a>
					</div>
					</div>
				</div>
		</div>
	</div>
	</section>


	@include('includes.footer')
<script>
	setTimeout(function(){ $("#msg").fadeOut(2000); }, 2000);
</script>