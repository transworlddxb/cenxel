@include('includes.header')

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css">

<style type="text/css">
input[type=number]::-webkit-inner-spin-button,
input[type=number]::-webkit-outer-spin-button {
  -webkit-appearance: none;
  margin: 0;
}
</style>

<?php
$message=Session::get('message');
$msg=explode("|",$message);
?>

@if ($message = Session::get('message'))
<div  style='z-index:9999; position:absolute;width:30%;right:0px; padding:10px 15px 0px 15px;' id='msg'>
 <div class="alert alert-{{$msg[0]}}" role="alert">
   <button type="button" class="close" data-dismiss="alert" aria-label="Close">
     <span aria-hidden="true">&times;</span>
   </button>
   {{ $msg[1] }}
 </div><!-- alert -->
</div>
@endif
<section>
		
	<div class="container-fluid rbntop">
		

		<div class="lmdevice"><img src="images/europe-permit-page-head-4@2x.jpg" ></div>
		<div class="smdevice"><img src="images/europe-permit-page-head-4@4x.jpg" ></div>
		<div class="row rbn">
			
		
			<div class="col-md-1 bnnrDet " data-aos="fade-right" data-aos-delay="0"
    		data-aos-duration="1000">
				<div id="orSeparator">
				<div class="row" id="socialSeparatorTop"></div>
				<div class="row" id="or" class="list-group">
					<ul class="orSptr">
						<li><a href=""><i class="fab fa-facebook-f zoom" style="color: #A7FCFF"></i></a></li>
						<li><a href=""><i class="fab fa-twitter zoom" style="color: #A7FCFF"></i></a></li>
						<li><a href=""><i class="fab fa-linkedin-in zoom" style="color: #A7FCFF"></i></a></li>
						<li><a href=""><i class="fab fa-instagram zoom" style="color: #A7FCFF"></i></a></li>
					</ul>
				</div>
				<div class="row" id="socialSeparatorBottom"></div>
				</div>
			</div>
			<div class="col-md-7 bnnrHeadingServices mdHdg">
				<h3 data-aos="fade-up" data-aos-delay="100" data-aos-duration="1000">Food And</h3>
				<h3 class="txtMrgn1" data-aos="fade-up" data-aos-delay="200" data-aos-duration="1000">Beverages</h3>
				
		</div>
		<div class="col-md-10 bnnrHeadingServices smHdg">
				<h3 data-aos="fade-up" data-aos-delay="100" data-aos-duration="1000">Food And</h3>
				<h3 class="txtMrgn1" data-aos="fade-up" data-aos-delay="200" data-aos-duration="1000">Beverages</h3>
				
		</div>
		
		</div>
	</div>
	

	</section>



<!----------- Start of about us--------------------->
<section>
	<div class="container">
		<div class="row r1 justify-content-center servDet ">
		<div class="col-md-5 servImg" data-aos="fade-up" data-aos-delay="100" data-aos-duration="1000">
			<img src="images/food-n-bvg.jpg"  height="auto" alt="" >	
		</div>
		<div class="col-md-6 servCont" data-aos="fade-up" data-aos-delay="100" data-aos-duration="1000" style="margin-top:-20px;">
			{{-- <h4 style="color:#1296B7;">Commercial Property </h4> --}}
				  {{-- <h4 class="sptxt3" style="color:#1296B7;"> (Buying & Selling) </h4> --}}
				  {{-- <h6 style="color:#1296B7;">Investing in property in Europe is an open door to a lot of opportunities. </h6> --}}
		  <div>
			<p style="text-align: justify;text-justify: inter-word;">Today there are a variety of opportunities for growing food and beverage businesses. But the industry is continuously changing and evolving. Staying on the top of emerging technologies, along with changing consumer preferences and understanding the major business trends is essential to meet the challenges in the industry. Consumers want fortified and functional foods that promote health, fuel their brains and benefit their physical appearance. They have a strong interest in not only what the product is but how it is made and by whom. With today’s instant access to information, if a consumer can’t understand or find out where and how a product is made and what’s in it, they’ll be more inclined to leave it behind. As consumers demand to know more about the food they eat, food and safety issues become more important than ever. With so many different products available for purchase in a multitude of channels- from in-store farmer’s markets, online, social media and more product innovation and differentiation becomes prominent.  Our food and beverages professional use their industry bench strength to bring innovative and insightful answers to our clients. We offer customized planning, implementation and business solutions. Where should your products be located? How should it be displayed? How it is being promoted? These are some details that will make a difference between you and your competitors. On-time delivery, finding the right distributor and a good advertising strategy are essential for the success of the business. We will give you industry-tailored solutions for each situation you are facing. </p>
		</div>
			
			</div>
		{{-- <div class="col-md-6 servCont" data-aos="fade-up" data-aos-delay="100" data-aos-duration="1000">

			<div class="row justify-content-center updownBttn" >
				
			<button onclick="myFunction()"><i class="fas fa-chevron-up"></i></button>
		
		</div>
		<div class="row justify-content-center">
			<div id="myDIV" class="example contHeight1" >

			<div id="content">
				
			<p>Today there are a variety of opportunities for growing food and beverage businesses. But the industry is continuously changing and evolving. Staying on the top of emerging technologies, along with changing consumer preferences and understanding the major business trends is essential to meet the challenges in the industry. Consumers want fortified and functional foods that promote health, fuel their brains and benefit their physical appearance. They have a strong interest in not only what the product is but how it is made and by whom. With today’s instant access to information, if a consumer can’t understand or find out where and how a product is made and what’s in it, they’ll be more inclined to leave it behind. As consumers demand to know more about the food they eat, food and safety issues become more important than ever. With so many different products available for purchase in a multitude of channels- from in-store farmer’s markets, online, social media and more product innovation and differentiation becomes prominent.  Our food and beverages professional use their industry bench strength to bring innovative and insightful answers to our clients. We offer customized planning, implementation and business solutions. Where should your products be located? How should it be displayed? How it is being promoted? These are some details that will make a difference between you and your competitors. On-time delivery, finding the right distributor and a good advertising strategy are essential for the success of the business. We will give you industry-tailored solutions for each situation you are facing. </p>

		</div>
			
		</div>
			</div>
			<div class="row justify-content-center updownBttn">
			<button onclick="myFunction1()"><i class="fas fa-chevron-down"></i></button>
		</div>
		</div> --}}
		</div>
	</div>
</section>
<!---------------Enquiry Form---------------------------------------->
<section>
		<div class="container-fluid">
			<div class="row justify-content-center enqMsg">
				<div class="col-md-6 col-md-offset-3 enqCol" data-aos="fade-up" data-aos-delay="100" data-aos-duration="1000">
        			<div class="well well-sm">
						<form class="form-horizontal" method="post" action="{{url('/submit_food_form')}}" enctype="multipart/form-data">
							{{csrf_field()}}
          			<fieldset>
           			 <legend >Enquiry</legend>
    
			            
			            <div class="form-group">
			             
			              <div class="col-md-12">
			                <input id="name" name="name" type="text" placeholder="Name" class="form-control">
			              </div>
			            </div>
     
			          
			            <div class="form-group">
			              
			              <div class="col-md-12">
			                <input id="email" name="email" type="text" placeholder="Email" class="form-control">
			              </div>
			            </div>
			    
			            <div class="form-group">
			              
			              <div class="col-md-12">
			                <input id="phn_num" name="phn_num" type="text" placeholder="Phone" class="form-control">
			              </div>
			            </div>
          
			            <div class="form-group">
			              
			              <div class="col-md-12">
			                <textarea class="form-control" id="message" name="message" placeholder="message " rows="5"></textarea>
			              </div>
			            </div>
			    
			          
			            <div class="form-group">
			              <div class="col-md-12 subtn text-right">
			                <button type="submit" class="btn btn-success ">Submit</button>
			              </div>
			            </div>
			          </fieldset>
			          </form>
			        </div>
			      </div>

				<div class="col-md-5">
				<div class="clr1" data-aos="zoom-in" data-aos-delay="500" data-aos-duration="1000">
						<a class="zoom1" href="{{ url('/'.'#ourServicesdd') }}" style="text-decoration: none;"><h3>Other Services<br> > </h3></a>
					</div>
					</div>
				</div>
		</div>
	</div>
	</section>

@include('includes.footer')

