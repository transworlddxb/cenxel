<!DOCTYPE html>
<html>
<head>
  <title>OpenC4S</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <link rel="stylesheet" type="text/css" href="{{asset('assets/frontend/css/bootstrap.min.css')}}">
  <link rel="stylesheet" type="text/css" href="{{asset('assets/frontend/css/stylesMain.css')}}">
  <link rel="stylesheet" type="text/css" href="{{asset('assets/frontend/css/all.css')}}">
  <link rel="stylesheet" type="text/css" href="{{asset('assets/frontend/css/all.min.css')}}">
  <link rel="shortcut icon" href="{{ url('assets/frontend/images/cenexl logo color.svg') }}">
<link rel="preconnect" href="https://fonts.gstatic.com">
<link href="https://fonts.googleapis.com/css2?family=Nunito+Sans:wght@200;300;400;600;700;800;900&display=swap" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="{{asset('assets/frontend/css/svg-with-js.css')}}">

</head>
<body  >
<!------------------------Start of header------------>
<section>
  <div class="container-fluid">
    <div class="row fList">

      <div class="col-md-6 ">
        <a href="{{ url('/') }}"><img src="{{asset('assets/frontend/images/c4s logo.svg')}}" width="40%"></a>
        <div class="fList_det">
        <h1>Features</h1>
        <br>
        <br>
      <h6>Administration.</h6>
      <div class="list-group">
            <ul>
              <li>Registration of candidates.</li>
              <li>Marking attendance.</li>
              <li>Reporting.</li>
              <li>Remittance of fees.</li>
              <li>Downloading notes.</li>
              <li>Class level tests.</li>
            </ul>
          </div>
          <br>

      <h6>Accounts.</h6>
      <div class="list-group">
            <ul>
              <li>Add a new user.</li>
              <li>Archive users.</li>
              <li>Browse list of users.</li>
              <li>Bulk user actions.</li>
              <li>User profile fields.</li>
              <li>Upload users.</li>
            </ul>
          </div>
          <br>

      <h6>Activity grading.</h6>
      <div class="list-group">
            <ul>
              <li>Course history</li>
              <li>Grade book.</li>
              <li>Grade book audit trail.</li>
              <li>Grade book comments.</li>
              <li>Manual grading.</li>
              <li>Multiple grading scales.</li>
            </ul>
          </div>
          <br>

      <h6>Authentication.</h6>
      <div class="list-group">
            <ul>
              <li>Active directory.</li>
              <li>LDAP Integration.</li>
              <li>Custom user Login.</li>
              <li>manual accounts.</li>
              <li>Login.</li>
            </ul>
          </div>
          <br>

      <h6>Categories.</h6>
      <div class="list-group">
            <ul>
              <li>Assign courses to categories.</li>
              <li>Create new categories.</li>
              <li>Manage categories.</li>
              <li>Priced categories.</li>
            </ul>
          </div>
          <br>

      <h6>Certificate management.</h6>
      <div class="list-group">
            <ul>
              <li>Certification life cycle.</li>
              <li>Manage certification templates.</li>
              <li>Pre-defined certification templates.</li>
              <li>Unique certification by each course.</li>
              <li>Unique certification by curriculum.</li>
            </ul>
          </div>
          <br>

      <h6>Compliance management.</h6>
      <div class="list-group">
            <ul>
              <li>Certificate expiration notifications.</li>
              <li>Certificate expiration management.</li>
              <li>Due date notification.</li>
              <li>Soft\hard stop due dates.</li>
            </ul>
          </div>
          <br>

      <h6>Creation.</h6>
      <div class="list-group">
            <ul>
              <li>Assignments engine.</li>
              <li>Built in authority tool.</li>
              <li>Can reuse PPTs, PDFs ,videos.</li>
              <li>Changing course default settings.</li>
              <li>Consume online video content.</li>
              <li>Course backup options.</li>
              <li>Learning paths(curriculums).</li>
              <li>Scheduling live events.</li>
              <li>Survey engine.</li>
              <li>Text engine.</li>
              <li>Upload courses.</li>
            </ul>
          </div>
          <br>

      <h6>Customization.</h6>
      <div class="list-group">
            <ul>
              <li>Mobile learning support.</li>
              <li>Personalized views.</li>
            </ul>
          </div>
          <br>

      <h6>Enrolment.</h6>
      <div class="list-group">
            <ul>
              <li>Attendance tracking.</li>
              <li>Automated enrolment(based on user dates).</li>
              <li>Guest access settings.</li>
              <li>Manual-enrolment.</li>
              <li>Self-enrolment.</li>
              <li>Self-enrolment (with pin #).</li>
              <li>Survey enrolment (based on response).</li>
            </ul>
          </div>
          <br>

      <h6>Format. </h6>
      <div class="list-group">
            <ul>
              <li>Course discussions.</li>
              <li>Gamification format.</li>
              <li>Learner upload.</li>
              <li>LIVE Chat format.</li>
              <li>Live Video conferencing\webinar.</li>
              <li>Social format.</li>
              <li>Topics format.</li>
              <li>Weekly format.</li>
            </ul>
          </div>
          <br>

      <h6>Gamification.</h6>
      <div class="list-group">
            <ul>
              <li>Badge customization.</li>
              <li>Badges.</li>
              <li>Gamification mechanics (Customizable).</li>
              <li>Leader boards.</li>
              <li>Levels.</li>
              <li>Points and rewards.</li>
            </ul>
          </div>
          <br>

      <h6>Interface.</h6>
      <div class="list-group">
            <ul>
              <li>Additional external pages.</li>
              <li>Block Management.</li>
              <li>Calendar settings.</li>
              <li>Language settings.</li>
              <li>Location settings.</li>
              <li>Media embedding settings.</li>
              <li>Multi-language support.</li>
              <li>Ready-made themes.</li>
            </ul>
          </div>
          <br>

      <h6>Learning types.</h6>
      <div class="list-group">
            <ul>
              <li>Asynchronous Instructor led.</li>
              <li>Asynchronous self paced.</li>
              <li>Blended learning.</li>
              <li>Synchronous virtual class rooms.</li>
            </ul>
          </div>
          <br>

      <h6>Mobile learning.</h6>
      <div class="list-group">
            <ul>
              <li>Offline (disconnected mobile app).</li>
              <li>Online (Internet connected).</li>
              <li>Supported for offline access to content.</li>
            </ul>
          </div>
          <br>

      <h6>Reports.</h6>
      <div class="list-group">
            <ul>
              <li>Automated report scheduling.</li>
              <li>Canned reports.</li>
              <li>Dashboard and graphic reports.</li>
              <li>Email delivery of reports.</li>
              <li>Exporting reports in variety of formats.</li>
              <li>Grading report settings.</li>
              <li>Training report maintenance.</li>
            </ul>
          </div>
          <br>

      <h6>Roles.</h6>
      <div class="list-group">
            <ul>
              <li>Role definition.</li>
              <li>Role assignments.</li>
              <li>System permissions per role.</li>
              <li>Teams and team hierarchies.</li>
            </ul>
          </div>
          <br>

      <h6>Security.</h6>
      <div class="list-group">
            <ul>
              <li>Anti spam.</li>
              <li>Antivirus.</li>
              <li>IP Blocker.</li>
              <li>Restrict registration to specific domains.</li>
              <li>Strong passwords.</li>
            </ul>
          </div>
          <br>

      <h6>Dynamic text editor (Server access for):</h6>
      <div class="list-group">
            <ul>
              <li>Uploading any type of question.</li>
              <li>Uploading content of any subject.</li>
              <li>Easy upload of symbol\equation\sign rich questions.</li>
            </ul>
          </div>
          <br>

      <h6>Dynamic Text setup manager.</h6>
      <div class="list-group">
            <ul>
              <li>Generation of test by the lead instructor.</li>
              <li>Release by the Head Instructor.</li>
            </ul>
          </div>
          <br>

      <h6>Personalised test for participants.</h6>
      <div class="list-group">
            <ul>
              <li>Facility for participants to set.</li>
            </ul>
          </div>
          <br>

      <h6>Personalised content delivery Tools.</h6>
      <div class="list-group">
            <ul>
              <li>Personalized Video-audio- text animation.</li>
              <li>Auto grouping.</li>
              <li>Auto ranking.</li>
              <li>Dynamic assignment Reports.</li>
              <li>Assignment reports for the students.</li>
            </ul>
      </div>
      <br>
      <a href="{{ url('/') }}"><i class="fas fa-arrow-left"></i> Back</a>

        </div>
          </div>
    </div>
  </div>
</section>

<!------------------------End of footer----------------------->


<script src="{{asset('assets/frontend/js/jquery-3.3.1.slim.min.js')}}" ></script>
<script type="text/javascript" src="{{asset('assets/frontend/js/bootstrap.min.js')}}"></script>

<script type="text/css" href="{{asset('assets/frontend/js/all.js')}}"></script>
<script type="text/css" href="{{asset('assets/frontend/js/all.min.js')}}"></script>
</body>
</html>
