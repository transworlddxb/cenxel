@include('includes.header')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css">

<style type="text/css">
input[type=number]::-webkit-inner-spin-button,
input[type=number]::-webkit-outer-spin-button {
  -webkit-appearance: none;
  margin: 0;
}
</style>

<?php
$message=Session::get('message');
$msg=explode("|",$message);
?>

@if ($message = Session::get('message'))
<div  style='z-index:9999; position:absolute;width:30%;right:0px; padding:10px 15px 0px 15px;' id='msg'>
 <div class="alert alert-{{$msg[0]}}" role="alert">
   <button type="button" class="close" data-dismiss="alert" aria-label="Close">
     <span aria-hidden="true">&times;</span>
   </button>
   {{ $msg[1] }}
 </div><!-- alert -->
</div>
@endif
<!--------------------------Banner------------------------------->

<section>
		
	<div class="container-fluid rbntop">
		
		<div class="lmdevice"><img src="images/europe-permit-page-head-5@2x.jpg" ></div>
		<div class="smdevice"><img src="images/europe-permit-page-head-5@4x.jpg" ></div>
		<div class="row rbn">
			
		
			<div class="col-md-1 bnnrDet " data-aos="fade-right" data-aos-delay="0"
    		data-aos-duration="1000">
				<div id="orSeparator">
				<div class="row" id="socialSeparatorTop"></div>
				<div class="row" id="or" class="list-group">
					<ul class="orSptr">
						<li><a href=""><i class="fab fa-facebook-f zoom" style="color: #A7FCFF"></i></a></li>
						<li><a href=""><i class="fab fa-twitter zoom" style="color: #A7FCFF"></i></a></li>
						<li><a href=""><i class="fab fa-linkedin-in zoom" style="color: #A7FCFF"></i></a></li>
						<li><a href=""><i class="fab fa-instagram zoom" style="color: #A7FCFF"></i></a></li>
					</ul>
				</div>
				<div class="row" id="socialSeparatorBottom"></div>
				</div>
			</div>

		<div class="col-md-7 bnnrHeadingServices mdHdg">
				<h3 data-aos="fade-up" data-aos-delay="100" data-aos-duration="1000">Hospitality</h3>
		</div>
		<div class="col-md-10 bnnrHeadingServices smHdg">
				<h3 data-aos="fade-up" data-aos-delay="100" data-aos-duration="1000">Hospitality</h3>
		</div>

		</div>
	</div>
	

	</section>



<!----------- info about hospitality--------------------->
<section>
	<div class="container">
		<div class="row r1 servDet ">
		<div class="col-md-6 servImg" data-aos="fade-up" data-aos-delay="100" data-aos-duration="1000" style="margin-top: -30px;">
			<img src="images/hospitality@2x.jpg" height="auto" alt="" >	
		</div>

		<div class="col-md-6 servCont" data-aos="fade-up" data-aos-delay="100" data-aos-duration="1000">
			{{-- <h4 style="color:#1296B7;">Commercial Property </h4> --}}
				  {{-- <h4 class="sptxt3" style="color:#1296B7;"> (Buying & Selling) </h4> --}}
				  {{-- <h6 style="color:#1296B7;">Investing in property in Europe is an open door to a lot of opportunities. </h6> --}}
		  <div>
			<p style="text-align: justify;text-justify: inter-word;">In the hospitality business the slogan “Customer is God” should be practised in all its senses. Treating customers like the most valued persons in the world and making them feel they are getting the best service and facilities are most important in this business. In short customer satisfaction is everything. Provide them luxury, pleasure, enjoyment and a never-before experience. With an increasingly competitive environment and rapidly changing trends, the need to maximize hospitality assets is critical.</p>
			<p style="text-align: justify;text-justify: inter-word;">At Alpha Brain HR Consultancy LLC, you get the best service and customized facilities, while focusing on the day-to-day running of your business.  We deliver a comprehensive range of services throughout Europe. Our primary concentrations include Food & Beverage, Facilities Management, Sales, Marketing & e-commerce Finance, Revenue Management, Purchasing and Information Technology related to the Hospitality Industry.  The consulting services we provide are unique for each project we undertake.</p>  
		</div>
			
			</div>
			<div class="row r1 servDet" style="margin-top: -30px;">
				<div class="col-md-12" data-aos="fade-up" data-aos-delay="100" data-aos-duration="1000">
				  <div>
					<p style="text-align: justify;text-justify: inter-word;">Our experience makes us ideally suited to assist you with your hospitality development project.  Whether you are building a new business, renovating, re-branding an existing hotel, or simply improving your day-to-day performance, we offer the ability to ensure the operational efficiencies and productivity levels to an improved stage.  If you are overseeing a 500+ room hotel, its concept restaurant, room service, meeting events or social catering, excellent guest and owner experience is of paramount importance as we pursue our mission; Keeping our clients happy, and their clients happier. At Alpha Brain HR Consultancy LLC, we do not sell a product, but we propagate a perception, wherein we modify your business in exactly the way you specify.</p>
					
				  </div>
					
					</div>
		
			
				</div>

	
		</div>
	</div>
</section>
{{-- <section>
	<div class="container">
		<div class="row r1 servDet ">
		<div class="col-md-12 servCont" data-aos="fade-up" data-aos-delay="100" data-aos-duration="1000">
		  <div>
			<p style="color: #8998B4;text-align: justify;text-justify: inter-word;">Our experience makes us ideally suited to assist you with your hospitality development project.  Whether you are building a new business, renovating, re-branding an existing hotel, or simply improving your day-to-day performance, we offer the ability to ensure the operational efficiencies and productivity levels to an improved stage.  If you are overseeing a 500+ room hotel, its concept restaurant, room service, meeting events or social catering, excellent guest and owner experience is of paramount importance as we pursue our mission; Keeping our clients happy, and their clients happier. At Alpha Brain HR Consultancy LLC, we do not sell a product, but we propagate a perception, wherein we modify your business in exactly the way you specify.</p>
			
		  </div>
			
			</div>

	
		</div>
	</div>
</section> --}}
<!---------------Enquiry Form---------------------------------------->
<section>
		<div class="container-fluid">
			<div class="row justify-content-center enqMsg">
				<div class="col-md-6 col-md-offset-3 enqCol" data-aos="fade-up" data-aos-delay="100" data-aos-duration="1000">
        			<div class="well well-sm">
						<form class="form-horizontal" method="post" action="{{url('/submit_hospitality_form')}}" enctype="multipart/form-data">
							{{csrf_field()}}
          			<fieldset>
           			 <legend >Enquiry</legend>
    
			            
			            <div class="form-group">
			             
			              <div class="col-md-12">
			                <input id="name" name="name" type="text" placeholder="Name" class="form-control">
			              </div>
			            </div>
     
			          
			            <div class="form-group">
			              
			              <div class="col-md-12">
			                <input id="email" name="email" type="text" placeholder="Email" class="form-control">
			              </div>
			            </div>
			    
			            <div class="form-group">
			              
			              <div class="col-md-12">
			                <input id="phn_num" name="phn_num" type="text" placeholder="Phone" class="form-control">
			              </div>
			            </div>
          
			            <div class="form-group">
			              
			              <div class="col-md-12">
			                <textarea class="form-control" id="message" name="message" placeholder="message " rows="5"></textarea>
			              </div>
			            </div>
			    
			          
			            <div class="form-group">
			              <div class="col-md-12 subtn text-right">
			                <button type="submit" class="btn btn-success ">Submit</button>
			              </div>
			            </div>
			          </fieldset>
			          </form>
			        </div>
			      </div>

				<div class="col-md-5">
				<div class="clr1" data-aos="zoom-in" data-aos-delay="500" data-aos-duration="1000">
						<a class="zoom1" href="{{ url('/'.'#ourServicesdd') }}" style="text-decoration: none;"><h3>Other Services<br> > </h3></a>
					</div>
					</div>
				</div>
		</div>
	</div>
	</section>



	@include('includes.footer')