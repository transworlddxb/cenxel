<!DOCTYPE html>
<html>
<head>
  <title>Alpha Brain HR Consultancy LLC </title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
  <link rel="stylesheet" type="text/css" href="{{asset('assets/frontend/css/stylesMain.css')}}">
  
  <link rel="stylesheet" type="text/css" href="{{asset('assets/frontend/css/all.min.css')}}">
  <link rel="stylesheet" type="text/css" href="{{asset('assets/frontend/css/svg-with-js.css')}}">
  <link rel="stylesheet" type="text/css" href="{{asset('assets/frontend/css/aos.css')}}">
  <link rel="shortcut icon" href="{{asset('assets/frontend/images/alpha tab logo.png')}}" type="image/x-icon">
  <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@400;500;600;700&display=swap" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@500;600;700&display=swap" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css2?family=Lato:wght@400;700&display=swap" rel="stylesheet">



</head>
<body >
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css">

<style type="text/css">
input[type=number]::-webkit-inner-spin-button,
input[type=number]::-webkit-outer-spin-button {
  -webkit-appearance: none;
  margin: 0;
}
</style>

<?php
$message=Session::get('message');
$msg=explode("|",$message);
?>

@if ($message = Session::get('message'))
<div  style='z-index:9999; position:absolute;width:30%;right:0px; padding:10px 15px 0px 15px;' id='msg'>
 <div class="alert alert-{{$msg[0]}}" role="alert">
   <button type="button" class="close" data-dismiss="alert" aria-label="Close">
     <span aria-hidden="true">&times;</span>
   </button>
   {{ $msg[1] }}
 </div><!-- alert -->
</div>
@endif
<!------------------------Start of header------------>
		

<section>
	<div class="container-fluid">
		<div class="row hdrRw">
	<div class="d-flex flex-row-reverse finrasHeader fixed-top">
  <div class="p-2 headerItem"><i class="far fa-clock"></i>
                        <span>Mon-Sat:9am to 6pm</span></div>
  <div class="p-2 headerItem"><i class="far fa-envelope"></i>
                        <span>alphahrdxb@gmail.com</span>
    </div>

    	
    	<div class="p-2 headerItem"><span class="fa-stack" style="vertical-align: top;">
  <i class="far fa-circle fa-stack-2x"></i>
  <i class="fas fa-phone-alt fa-stack-1x"></i>
</span><span>+(971) 55 758 6218</span></div>

  </div>
</div>
</div>
</section>
<!---------------------Menu-------------------------------------->
<section>
	<div class="container-fluid">
	<div class="row no-padding">
	<div class="col-lg-12 col-md-12 " >
	<nav class="navbar navbar-expand-md bg-dark fixed-top fin_nav" style="height:150px">
		
		<a class="navbar-brand" href="{{ url('/') }}"><img src="images/ALPHA BRAIN logo.png" style="width:220px !important; height:220px !important;"></a>
		  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"><i class="fa fa-bars" aria-hidden="true"></i></span>
		  </button>

		  	<div class="collapse navbar-collapse" id="navbarSupportedContent">
		    	<ul class="navbar-nav ml-auto">
		      		<li class="nav-item active">
		        		<a class="nav-link" href="{{ url('/') }}">Home <span class="sr-only">(current)</span></a>
		      		</li>
				    <li class="nav-item">
				        <a class="nav-link" href="{{ url('/'.'#aboutUsSection') }}">About Us</a>
				    </li>
		      		<li class="nav-item dropdown">
		        		<a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-hover="dropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Services</a>
		       
       
        				<div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
							<a class="dropdown-item" href="{{ url('/residency_permit') }}">Invest & Get EU Residency Permit</a>
							<a class="dropdown-item" href="{{ url('/agriculture') }}">Agriculture</a>
							<a class="dropdown-item" href="{{ url('/foodNbeverage') }}">Food & Beverage</a>
							<a class="dropdown-item" href="{{ url('/hospitality') }}">Hospitality</a>
							<a class="dropdown-item" href="{{ url('/manpowerSupply') }}">Manpower Supply</a>          					
        				</div>
      				</li>
		       
		      		<li class="nav-item">
						<a class="nav-link" href="{{ url('/contact') }}">Contact Us</a>
					  </li>
					  <li class="nav-item">
						<a class="nav-link" href="{{ url('/blog') }}">Blogs</a>
					  </li>
		      		<li class="nav-item">
				
          				{{-- <form action="" class="search-form" autocomplete="off">
                			<div class="form-group has-feedback">
            					<label for="search" class="sr-only">Search</label>
            					<input type="text" class="form-control" name="search" id="search" placeholder="search">
              					<span class="form-control-feedback"><i class="fa fa-search"></i></span>
            				</div>
            			</form>         --}}
					</li>
				</ul>
		  	</div>
		
		</nav>
</div>
</div>
</div>
</section>
<!------------------------End of Menu----------------------------------->




<section>
		
	<div class="container-fluid rbntop">
		
		<div class="lmdevice"><img src="images/europe-permit-page-head-2@2x.jpg" ></div>
		<div class="smdevice"><img src="images/europe-permit-page-head-2@4x.jpg" ></div>
		<div class="row rbn">
			
		
			<div class="col-md-1 bnnrDet " data-aos="fade-right" data-aos-delay="0"
    		data-aos-duration="1000">
				<div id="orSeparator">
				<div class="row" id="socialSeparatorTop"></div>
				<div class="row" id="or" class="list-group">
					<ul class="orSptr">
						<li><a href=""><i class="fab fa-facebook-f zoom" style="color: #A7FCFF"></i></a></li>
						<li><a href=""><i class="fab fa-twitter zoom" style="color: #A7FCFF"></i></a></li>
						<li><a href=""><i class="fab fa-linkedin-in zoom" style="color: #A7FCFF"></i></a></li>
						<li><a href=""><i class="fab fa-instagram zoom" style="color: #A7FCFF"></i></a></li>
					</ul>
				</div>
				<div class="row" id="socialSeparatorBottom"></div>
				</div>
			</div>
			<div class="col-md-7 bnnrHeadingServices mdHdg">
				<h3 data-aos="fade-up" data-aos-delay="100" data-aos-duration="1000">Human</h3>
				<h3 class="txtMrgn1" data-aos="fade-up" data-aos-delay="200" data-aos-duration="1000">Resources</h3>
		</div>
		<div class="col-md-10 bnnrHeadingServices smHdg">
				<h3 data-aos="fade-up" data-aos-delay="100" data-aos-duration="1000">Human</h3>
				<h3 class="txtMrgn1" data-aos="fade-up" data-aos-delay="200" data-aos-duration="1000">Resources</h3>
		</div>
		
		</div>
	</div>
	

	</section>



<!----------- Start of about us--------------------->
<section>
	<div class="container">
		<div class="row r1 justify-content-center servDet ">
		<div class="col-md-5 servImg" data-aos="fade-up" data-aos-delay="100" data-aos-duration="1000">
			<img src="images/hr.jpg" width="100%" height="auto" alt="" >	
		</div>
		<div class="col-md-6 servCont" data-aos="fade-up" data-aos-delay="100" data-aos-duration="1000">
		<h2 data-aos="fade-right"
      data-aos-delay="0"
	data-aos-duration="1000" style="text-align:center;color:#5EABC0;">Investing in property in Europe is an open door to a lot of opportunities.</h2>
	<div>
		<p style="color: #8998B4;text-align: justify;text-justify: inter-word;">A skilled and committed workforce is the backbone of every organization. Having the best talent is a competitive advantage in today’s world. Remember that success relies upon talent. Acquiring the most talented people is very important. We are the one-stop destination for all your manpower supply needs in various trades and businesses. We find solutions for the complex workforce challenges various types of organizations face today.<br>Built upon our deep recruitment expertise and based on rigorous processed, our innovative approach ensures the best result you need. We know that all organizations are different from others and the requirements are never alike. So we go deep to understand your organization and the job skills necessary for your success. Our proven sourcing strategies, long experience, and expertise accelerate the acquisition of the right candidates for you. We provide highly qualified professionals and skilled labours to hasten the growth of your organization.</p>
	</div>
		
		</div>
		</div>
	</div>
</section>

<section>
	<div class="container manpowerDet" style="margin-top: -40px">
		<div class="row r1 justify-content-center servDet ">
		<div class="col-md-6" data-aos="fade-up" data-aos-delay="100" data-aos-duration="1000">
			<h2 data-aos="fade-right"
      data-aos-delay="0"
	data-aos-duration="1000" style="text-align:center;">Jobseekers</h2>
	<div>
		<p style="color: #8998B4;text-align: justify;text-justify: inter-word;">You may register your profile and upload along with your supporting documents on our website. All applications will be reviewed and will undergo a screening process. Applicants shortlisted may submit further supporting documents if required.</p>
	</div>

		</div>
		<div class="col-md-6" data-aos="fade-up" data-aos-delay="100" data-aos-duration="1000">
			<h2 data-aos="fade-right"
			data-aos-delay="0"
		  data-aos-duration="1000" style="text-align:center;">Employers</h2>
		  <div>
			  <p style="color: #8998B4;text-align: justify;text-justify: inter-word;">You may submit your list of requirements through our website. We have a very large database of talents. We can easily identify the candidates meeting your project requirements. We provide professional, well-groomed and trained staff that has a strong background in the industry.</p>
		  </div>
		</div>
		</div>
		<div class="mdetLine" style="margin-top: 35px;"></div>
	</div>
</section>


<section>
	<div class="container manpowerDet2  " >
		{{-- <div class="mdetLine"></div> --}}
		
		<div class="row manpowerDetLogo justify-content-center" data-aos="fade-up"
      data-aos-delay="500"
    data-aos-duration="1000">
			<div class="col-lg-3 col-md-3 col-sm-6 ">
				<div class="row justify-content-center mdetails2">
			<img src="{{ url('assets/frontend/property/certificate-icon.png')}}" >	
			<p style="font-size: 13px;">We supply trained and certified to the industry standard to ensure a zero harm work place.</p>
		</div>
			</div>
			<div class="col-lg-3 col-md-3 col-sm-6 ">
					<div class="row justify-content-center mdetails2">
			<img src="{{ url('assets/frontend/property/emploayee.png')}}" >	
			<p style="font-size: 13px;">We provide team or individuals who can meet your requirements and labor shortages.</p></div>
			
			</div>
			<div class="col-lg-3 col-md-3 col-sm-6 ">
					<div class="row justify-content-center mdetails2">
			<img src="{{ url('assets/frontend/property/multi-user-icon.png')}}" >
				<p style="font-size: 13px;">We supply manpower for several job positions.</p></div>
			</div>
			<div class="col-lg-3 col-md-3 col-sm-6 ">
					<div class="row justify-content-center mdetails2">
			<img src="{{ url('assets/frontend/property/jobserach.png')}}" >	
			<p style="font-size: 13px;">Flexible and cost-effective hiring options includes a wide spread of categories available for both short-term and long-term jobs.</p></div>
			</div>
			
			
			
		</div>
			{{-- <div class="mdetLine"></div> --}}
	</div>
</section>

<section>
	<div class="container manpowerDet" style="margin-top: -40px">
		<div class="row r1 justify-content-center servDet ">
		<div class="col-md-6" data-aos="fade-up" data-aos-delay="100" data-aos-duration="1000">
			<h2 data-aos="fade-right"
      data-aos-delay="0"
	data-aos-duration="1000" style="text-align:center;"><span style="color: black">OUR</span> MISSION</h2>
	<div>
		<h6 style="color: #8998B4;text-align:center;">To provide staffing solutions to our customers with exceptional quality of service.</h6>
	</div>

		</div>
		<div class="col-md-6" data-aos="fade-up" data-aos-delay="100" data-aos-duration="1000">
			<h2 data-aos="fade-right"
			data-aos-delay="0"
		  data-aos-duration="1000" style="text-align:center;"><span style="color: black">OUR</span> VISION</h2>
		  <div>
			  <h6 style="color: #8998B4;text-align:center;">To be recognized as the preferred Manpower supplier in the entire Europe by establishing long term relationship with our clients and customers.</h6>
		  </div>
		</div>
		</div>
		<div class="mdetLine" style="margin-top: 35px;"></div>
	</div>
</section>

<!------------------------------------------------------------------------>
<section>
	<div class="container manpowerDet" >
		<h2 data-aos="fade-right"
      data-aos-delay="0"
    data-aos-duration="1000" style="text-align:center;">Categories</h2>
		<div class="row manpowerDetLogo justify-content-center" data-aos="fade-up"
      data-aos-delay="500"
    data-aos-duration="1000">
			<div class="col-lg-3 col-md-3 col-sm-6 imageBordr mlogo">
			<img src="{{ url('assets/frontend/property/finras_icon_3.png')}}" >	
			<p>Electrician</p>
			</div>
			<div class="col-lg-3 col-md-3 col-sm-6 imageBordr mlogo">
			<img src="{{ url('assets/frontend/property/finras_icon_6.png')}}" >
			<p>Tile mason</p>
			</div>
			<div class="col-lg-3 col-md-3 col-sm-6 imageBordr mlogo">
			<img src="{{ url('assets/frontend/property/finras_icon_2.png')}}" >
				<p>Brick Mason</p>
			</div>
			<div class="col-lg-3 col-md-3 col-sm-6 imageBordr mlogo">
			<img src="{{ url('assets/frontend/property/finras_icon_4.png')}}" >	
			<p>Plumber</p>
			</div>
			<div class="col-lg-3 col-md-3 col-sm-6 imageBordr mlogo">
			<img src="{{ url('assets/frontend/property/gas pipe fitter.png')}}" >
				<p>Gas Pipe Fitter</p>
			</div>
			<div class="col-lg-3 col-md-3 col-sm-6 imageBordr mlogo">
			<img src="{{ url('assets/frontend/property/finras_icon_8.png')}}" >
			<p>Truck Driver</p>	
			</div>
			<div class="col-lg-3 col-md-3 col-sm-6 imageBordr mlogo">
			<img src="{{ url('assets/frontend/property/finras_icon_1.png')}}" >
			<p>Butcher</p>
			</div>
			<div class="col-lg-3 col-md-3 col-sm-6 imageBordr mlogo">
			<img src="{{ url('assets/frontend/property/finras_icon_5.png')}}" >
			<p>Tailor</p>	
			</div>
		</div>
		
	</div>
</section>
<!------------------------------------------------------------------------>

<!---------------Enquiry Form---------------------------------------->
<section>
		<div class="container-fluid">
			<div class="row justify-content-center enqMsg">
				<div class="col-md-6 col-md-offset-3 enqCol" data-aos="fade-up" data-aos-delay="100" data-aos-duration="1000">
        			<div class="well well-sm">
						<form class="form-horizontal" method="post" action="{{url('/submit_manpower_form')}}" enctype="multipart/form-data">
							{{csrf_field()}}
          			<fieldset>
           			 <legend >Enquiry</legend>
    
			            
			            <div class="form-group">
			             
			              <div class="col-md-12">
			                <input id="name" name="name" type="text" placeholder="Name" class="form-control">
			              </div>
			            </div>
     
			          
			            <div class="form-group">
			              
			              <div class="col-md-12">
			                <input id="email" name="email" type="text" placeholder="Email" class="form-control">
			              </div>
			            </div>
			    
			            <div class="form-group">
			              
			              <div class="col-md-12">
			                <input id="phn_num" name="phn_num" type="text" placeholder="Phone" class="form-control">
			              </div>
			            </div>
          
			            <div class="form-group">
			              
			              <div class="col-md-12">
			                <textarea class="form-control" id="message" name="message" placeholder="message " rows="5"></textarea>
			              </div>
			            </div>
			    
			          
			            <div class="form-group">
			              <div class="col-md-12 subtn text-right">
			                <button type="submit" class="btn btn-success ">Submit</button>
			              </div>
			            </div>
			          </fieldset>
			          </form>
			        </div>
			      </div>

				<div class="col-md-5">
				<div class="clr1" data-aos="zoom-in" data-aos-delay="500" data-aos-duration="1000">
						<a href="{{ url('/'.'#ourServicesdd') }}" style="text-decoration: none"><h3>Other Services<br> > </h3></a>
					</div>
					</div>
				</div>
		</div>
	</div>
	</section>

	@include('includes.footer')