<!DOCTYPE html>
<html>
<head>
  <title>OpenC4S</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <link rel="stylesheet" type="text/css" href="{{asset('assets/frontend/css/bootstrap.min.css')}}">
  <link rel="stylesheet" type="text/css" href="{{asset('assets/frontend/css/stylesMain.css')}}">
  <link rel="stylesheet" type="text/css" href="{{asset('assets/frontend/css/all.css')}}">
  <link rel="stylesheet" type="text/css" href="{{asset('assets/frontend/css/all.min.css')}}">
  <link rel="shortcut icon" href="{{ url('assets/frontend/images/cenexl logo color.svg') }}">
<link rel="preconnect" href="https://fonts.gstatic.com">
<link href="https://fonts.googleapis.com/css2?family=Nunito+Sans:wght@200;300;400;600;700;800;900&display=swap" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="{{asset('assets/frontend/css/svg-with-js.css')}}">

</head>
<body  >
<!------------------------Start of header------------>
<section>
  <div class="container-fluid">
    <div class="row cnxl">
      <div class="col-md-6 cnxl_det ">
     <a href="{{ url('/') }}"><img src="{{asset('assets/frontend/images/c4s logo.svg')}}" width="40%"></a>
     <div class="cnxl_det_pdg">
     <h1>Cenxel</h1>
     <h6>In a world with knowledge on the fingertips, Cenxel Technologies launches Open C4S-</h6>
     <p>the easiest, most comfortable and effective way of knowledge building and dissemination for the benefit of educational institutions and corporate companies or organizations which need to educate or train students or employees through a massive open online course Platform. This platform is the brainchild of a group of brilliant Industrial specialists, educators and subject specialists who have a deep understanding of the traditional methods of education and the possibilities, effectiveness, modalities and challenges in the online system. The effort of this ideal association has resulted in forging Open C4S, a widely applicable platform or software solution deployable by all types of institutions for the purpose of instruction and education. While designing and programming the platform, our experts have paid utmost diligence to mitigate all the challenges faced at the level of content creation, course differences, patter diversity of organizations and management and administration of institutions. In short, the universality of application in the field of education is the hallmark of Open C4S. A comfortable web or mobile learning interface, variety of content types, easy delivery features, integration with the internal system and corporate database, repeating and analytic features such as gamified and adaptive learning techniques etc define the platform and they make the whole system effective, perfect and comfortable. And no wonder that the client is satisfied and feel aligned with the brand!Open Edx – The foundation of Open C4S</p>
      <a href="{{ url('/') }}"><i class="fas fa-arrow-left"></i> Back</a>
    </div>
    </div>
    <div class="col-md-6 cnxl_img">
      <img src="{{asset('assets/frontend/images/Mask_Group_7@11.png')}}" width="100%">
    </div>
    </div>
  </div>
</section>


<!------------------------End of footer----------------------->


<script src="{{asset('assets/frontend/js/jquery-3.3.1.slim.min.js')}}" ></script>
<script type="text/javascript" src="{{asset('assets/frontend/js/bootstrap.min.js')}}"></script>

<script type="text/css" href="{{asset('assets/frontend/js/all.js')}}"></script>
<script type="text/css" href="{{asset('assets/frontend/js/all.min.js')}}"></script>
</body>
</html>
