@include('includes.header')

<!----------- Start of --------------------->
<section>
	<div class="container-fluid">
			<div class="row pptyPage">
			</div>
		</div>
</section>
<!----------- Start of --------------------->
<section>
	
<div class="container">

   <div class="row pptyDet"> 
   	<div class="col-md-6" data-aos="fade-up" data-aos-delay="100" data-aos-duration="1000">
   		<h5 class="txt3">Kebab Shop – 1 person Residency With Family</h5>
   	</div>
   	
   	<div class="col-md-6 shrPrnt" data-aos="fade-left" data-aos-delay="300" data-aos-duration="1000">
					<div class="d-flex flex-row-reverse bd-highlight">
	  					<div class="p-2 bd-highlight"><img src="images/print@2x.jpg" ></div>
	  					<div class="p-2 bd-highlight"><img src="images/plus@2x.jpg" ></div>
	  					<div class="p-2 bd-highlight"><img src="images/share@2x.jpg" ></div>
					</div>
					
	</div>
   		 
   </div>
   	<div class="row">
   	<div class="col-md-7 pptyCarousel" data-aos="fade-up" data-aos-delay="100" data-aos-duration="1000">
    
    <h5 class="txt5">Price On Call</h5>
    <!--Carousel Wrapper-->
    <div id="carousel-thumb" class="carousel slide carousel-fade carousel-thumbnails" data-ride="carousel">
      <!--Slides-->
      <div class="carousel-inner" role="listbox">
        <div class="carousel-item active">
          <img class="d-block w-100" style="height: 500px;" src="{{ url('assets/frontend/property/prop31.jpeg')}}" alt="First slide">
        </div>
        <div class="carousel-item">
          <img class="d-block w-100" style="height: 500px;" src="{{ url('assets/frontend/property/prop32.jpeg')}}" alt="Second slide">
        </div>
        <div class="carousel-item">
          <img class="d-block w-100" style="height: 500px;" src="{{ url('assets/frontend/property/prop33.jpeg')}}" alt="Third slide">
        </div>
      </div>
      <!--/.Slides-->
      <!--Controls-->
      <a class="carousel-control-prev" href="#carousel-thumb" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
      </a>
      <a class="carousel-control-next" href="#carousel-thumb" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
      </a>
      <!--/.Controls-->
      
      <ol class="carousel-indicators">
        <li data-target="#carousel-thumb" data-slide-to="0" class="active"> <img style="height: 60px;" class="d-block w-100" src="{{ url('assets/frontend/property/prop31.jpeg')}}"
            class="img-fluid"></li>
        <li data-target="#carousel-thumb" data-slide-to="1"><img style="height: 60px;" class="d-block w-100" src="{{ url('assets/frontend/property/prop32.jpeg')}}"
            class="img-fluid"></li>
        <li data-target="#carousel-thumb" data-slide-to="2"><img style="height: 60px;" class="d-block w-100" src="{{ url('assets/frontend/property/prop33.jpeg')}}"
            class="img-fluid"></li>
              
      </ol>

    </div>
    <!--/.Carousel Wrapper-->
</div>

<div class="col-md-5 pptySlctdDet" data-aos="fade-up" data-aos-delay="100" data-aos-duration="1000">
	<h5 class="txt5">Property ID 499</h5>
	<h4 class="txt6">Description <span class="hrLine2"></span></h4>
	<p class="txt9">Here is an exceptional opportunity to own a kabab shop and takeaway in the heart of the booming city of Vilniaus in Lithuania.<br>
    This Trendy, Modern style cafe is for sale<br>
    1 (one) person  for residency with family can apply for this offer<br>
    Property rental price –  € 120/- per month<br>
    Hypermarket parking is available near the shop<br>
    Great profit return clientele and highly reputable<br>
    The Shop is very popular among the resident of the city. The favorable location of the Shop provides a large number of regular visitors.<br>
    Shop premises are rented for a symbolic amount. The owners of the premises are interested in and help to develop and advertise the Shop. The shop has its own trademark, all the necessary equipment, and furniture, and there are also opportunities to use an additional part of the premises</p>
</div>
</div>
<div class="row pptydet1">
	<div class="col-md-6" >
		<h5 class="txt10">Address <span class="hrLine2"></span></h5>
		<p class="txt8">Country : Lithuania</p>
		<img src="images/placeholder (1).jpg" > <span >
			<a href="#" class="txt7">Open On Google Maps</a>
		</span>
		<h5 class="txt10">Overview <span class="hrLine2"></span></h5>

<table class="table table-striped" data-aos="zoom-in" data-aos-delay="500" data-aos-duration="1000">
  
    <tr>
      <th scope="col">Property ID</th>
      <td scope="col">498</td>
      
    </tr>
  
 
    <tr>
    <th scope="col">Price</th>
      <td scope="col">Price On Call</td>
    </tr>
   <tr>
    <th scope="col">Property Type</th>
      <td scope="col">Property for Sale</td>
    </tr>
     <tr>
    <th scope="col">Rental Price</th>
      <td scope="col">120 &euro;</td>
    </tr>
     <tr>
    <th scope="col">Residency Permit for</th>
      <td scope="col">1 persons with family</td>
    </tr>
  
</table>
<h5 class="txt10">File Attachment <span class="hrLine2"></span></h5>
<div class="row">
	<div class="col-md-1">
		<img src="images/photo.jpg" >
	</div>
	<div class="col-md-3">
		<p class="txtMrgn">498.jpg</p>
		<a href="{{ url('assets/frontend/images/498.png')}}" target="_blank" class="txt7">Download</a>
	</div>
</div>
	</div>
</div>
<div class="row">
	<div class="col-md-12 locationMap1" data-aos="zoom-in" data-aos-delay="500" data-aos-duration="1000">
		<h5 class="txt10">Get Direction <span class="hrLine2"></span></h5>
		<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d4676121.739789051!2d19.399052488360773!3d55.091166293830334!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x46dd94140f33be13%3A0xf30a54d3a55dbab9!2sLithuania!5e0!3m2!1sen!2sin!4v1588620178155!5m2!1sen!2sin" width="600" height="450" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
</div>
</div>
<div class="row">
	<div class="col-md-12 " data-aos="fade-up" data-aos-delay="100" data-aos-duration="1000">
<h5 class="txt10">Contact <span class="hrLine2"></span></h5>
<h6>Finras Office</h6>
<p>+(971) 43555110</p>
<p>contact@uabfinras.com</p>

<div class="well well-sm">

          			<form class="form-horizontal" action="" method="post">
          			<fieldset>
           			 <!--<legend >Feel free to drop us a message</legend>-->
    					<div class="row">
			            <!-- Name input-->
			            <div class="col-md-4">
			            <div class="form-group">
			             
			              
			                <input id="name" name="name" type="text" placeholder="Full Name*" class="form-control formBckgd">
			              </div>
			            </div>
     
			            <!-- Email input-->
			            <div class="col-md-4">
			            <div class="form-group">
			              
			              
			                <input id="email" name="email" type="text" placeholder="Email Address*" class="form-control formBckgd">
			              </div>
			            </div>
			    <!-- Phone number input-->
			    <div class="col-md-4">
			            <div class="form-group">
			              
			              
			                <input id="phn_num" name="phn_num" type="text" placeholder="Phone Number*" class="form-control formBckgd">
			              </div>
			            </div>
            <!-- Message body -->
            <div class="col-md-12">
			            <div class="form-group">
			              
			              
			                <textarea class="form-control formBckgd" id="message" name="message" placeholder="Message* " rows="5"></textarea>
			              </div>
			            </div>
			    
			            <!-- Form actions -->

			            <div class="col-md-12 submtReq">

			            <div class="form-group">
			              
			              	<label>Submit Request</label>
			                <a href="#"><img src="images/upload@2x.jpg"></a>
			              </div>
      

			            </div>

			            <div class="col-md-12 d-flex justify-content-center">
			            	
			            	<div class="p-2"><img src="images/calendar icon.jpg" ></div>
			            	<div class="p-2"><p id="demo"></p></div>
			            	<div class="p-2"><img src="images/views icon.jpg" ></div>
			            	<div class="p-2"><p>204 views</p></div>

			            </div>

			           
			        	

			        </div>
			          </fieldset>
			          </form>
			        </div>




</div>
</div>



  </div>
</section>
<!----------- Start of --------------------->

<!----------- Start of --------------------->
<section>
  <div class="swiper-container" data-aos="fade-up" data-aos-delay="0" data-aos-duration="1000">
  
      <div class="col-md-12 cardHdg">
      <h5 class="txt4">Business For Sale</h5>
    </div>
      <div class="swiper-wrapper pptyCard">
        <div class="swiper-slide" >
          <div class="card ">
      <img class="card-img-top" src="{{ url('assets/frontend/property/prop16.png')}}" alt="Card image cap">
      
      <div class="card-body">
        
        <div class="card-title">
        <a href="{{ url('/single_prop') }}"><h6>Restaurant for Sale – 1 Person Residency with Family</h6></a></div>
  
        <p class="card-text"><mark>Price on call</mark></p>
      </div>
  
      <div class="card-footer">
        <small class="text-muted"><i class="fas fa-user-tie"></i>Finras</small>
      </div>
  
    </div>
  
  </div>
        <div class="swiper-slide" >
          <div class="card">
      <img class="card-img-top" src="{{ url('assets/frontend/property/prop21.jpeg')}}" alt="Card image cap">
      
      <div class="card-body">
        
        <div class="card-title">
        <a href="{{ url('/single_prop2') }}"><h6>Restaurant – 2 Persons Residency with family</h6></a></div>
  
        <p class="card-text"><mark>Price on call</mark></p>
      </div>
  
      <div class="card-footer">
        <small class="text-muted"><i class="fas fa-user-tie"></i>Finras</small>
      </div>
  
    </div>
  
  </div>
        <div class="swiper-slide" >
          <div class="card">
      <img class="card-img-top" src="{{ url('assets/frontend/property/prop31.jpeg')}}" alt="Card image cap">
      
      <div class="card-body">
        
        <div class="card-title">
        <a href="{{ url('/single_prop3') }}"><h6>Kebab Shop – 1 person Residency With Family</h6></a></div>
  
        <p class="card-text"><mark>Price on call</mark></p>
      </div>
  
      <div class="card-footer">
        <small class="text-muted"><i class="fas fa-user-tie"></i>Finras</small>
      </div>
  
    </div>
  
  </div>
        <div class="swiper-slide" >
          <div class="card ">
      <img class="card-img-top" src="{{ url('assets/frontend/property/prop42.jpeg')}}" alt="Card image cap">
      
      <div class="card-body">
        
        <div class="card-title">
        <a href="{{ url('/single_prop4') }}"><h6>Coffee Shop – 2 Person Residency with Family</h6></a></div>
  
        <p class="card-text"><mark>Price on call</mark></p>
      </div>
  
      <div class="card-footer">
        <small class="text-muted"><i class="fas fa-user-tie"></i>Finras</small>
      </div>
  
    </div>
  
  </div>
        <div class="swiper-slide" >
          <div class="card ">
        <img class="card-img-top" src="{{ url('assets/frontend/property/prop21.jpeg')}}" alt="Card image cap">
      
        <div class="card-body">
          
          <div class="card-title">
          <a href="{{ url('/single_prop2') }}"><h6>Restaurant – 2 Persons Residency with family</h6></a></div>
      
          <p class="card-text"><mark>Price on call</mark></p>
        </div>
      
        <div class="card-footer">
          <small class="text-muted"><i class="fas fa-user-tie"></i>Finras</small>
        </div>
  
    </div>
  
  </div>
        <div class="swiper-slide" >
          <div class="card ">
        <img class="card-img-top" src="{{ url('assets/frontend/property/prop16.png')}}" alt="Card image cap">
      
        <div class="card-body">
          
          <div class="card-title">
          <a href="{{ url('/single_prop') }}"><h6>Restaurant for Sale – 1 Person Residency with Family</h6></a></div>
      
          <p class="card-text"><mark>Price on call</mark></p>
        </div>
      
        <div class="card-footer">
          <small class="text-muted"><i class="fas fa-user-tie"></i>Finras</small>
        </div>
  
      </div>
  
    </div>
    
        <div class="swiper-slide" >
          <div class="card ">
        <img class="card-img-top" src="{{ url('assets/frontend/property/prop31.jpeg')}}" alt="Card image cap">
      
        <div class="card-body">
          
          <div class="card-title">
          <a href="{{ url('/single_prop3') }}"><h6>Kebab Shop – 1 person Residency With Family</h6></a></div>
      
          <p class="card-text"><mark>Price on call</mark></p>
        </div>
      
        <div class="card-footer">
          <small class="text-muted"><i class="fas fa-user-tie"></i>Finras</small>
        </div>
  
    </div>
  
  </div>
      </div>
    </div>
  
  </section>

@include('includes.footer')