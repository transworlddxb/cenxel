@include('includes.header')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css">

<style type="text/css">
input[type=number]::-webkit-inner-spin-button,
input[type=number]::-webkit-outer-spin-button {
  -webkit-appearance: none;
  margin: 0;
}
</style>

<?php
$message=Session::get('message');
$msg=explode("|",$message);
?>

@if ($message = Session::get('message'))
<div  style='z-index:9999; position:absolute;width:30%;right:0px; padding:10px 15px 0px 15px;' id='msg'>
 <div class="alert alert-{{$msg[0]}}" role="alert">
   <button type="button" class="close" data-dismiss="alert" aria-label="Close">
     <span aria-hidden="true">&times;</span>
   </button>
   {{ $msg[1] }}
 </div><!-- alert -->
</div>
@endif

<section>
		
	<div class="container-fluid rbntop">
	
    <div class="lmdevice"><img src="images/europe-permit-page-head@2x.jpg" ></div>
    <div class="smdevice"><img src="images/europe-permit-page-head@4x.jpg" ></div>
		<div class="row rbn">
			
		
			<div class="col-md-1 bnnrDet " data-aos="fade-right" data-aos-delay="0"
    		data-aos-duration="1000">
				<div id="orSeparator">
				<div class="row" id="socialSeparatorTop"></div>
				<div class="row" id="or" class="list-group">
					<ul class="orSptr">
						<li><a href=""><i class="fab fa-facebook-f zoom" style="color: #A7FCFF"></i></a></li>
						<li><a href=""><i class="fab fa-twitter zoom" style="color: #A7FCFF"></i></a></li>
						<li><a href=""><i class="fab fa-linkedin-in zoom" style="color: #A7FCFF"></i></a></li>
						<li><a href=""><i class="fab fa-instagram zoom" style="color: #A7FCFF"></i></a></li>
					</ul>
				</div>
				<div class="row" id="socialSeparatorBottom"></div>
				</div>
			</div>

		<div class="col-md-7 bnnrHeadingServices mdHdg">
			<h3 data-aos="fade-up" data-aos-delay="100" data-aos-duration="1000">INVESTMENT & EU RESIDENCY PERMIT</h3>
		</div>
		<div class="col-md-10 bnnrHeadingServices smHdg">
				<h3 data-aos="fade-up" data-aos-delay="100" data-aos-duration="1000">INVESTMENT & EU RESIDENCY PERMIT</h3>
		</div>


		</div>
	</div>
	

	</section>


	

<!----------- Start of services content--------------------->
<section>
	<div class="container">
		<div class="row justify-content-center r1 servDet ">
		<div class="col-md-5 servImg" data-aos="fade-up" data-aos-delay="0" data-aos-duration="1000">
			<img src="images/commercial-img2@2x.jpg" height="auto" alt="" >	
    </div>
    
    <div class="col-md-6 servCont" data-aos="fade-up" data-aos-delay="100" data-aos-duration="1000">
      <h4 style="color:#1296B7;">Commercial Property </h4>
			<h4 class="sptxt3" style="color:#1296B7;"> (Buying & Selling) </h4>
			<h6 style="color:#1296B7;">Investing in property in Europe is an open door to a lot of opportunities. </h6>
    <div>
      <p style="color: #8998B4;text-align: justify;text-justify: inter-word;">We, at Alpha Brain HR Consultancy LLC, are focusing on the country of Lithuania where it has the largest economy among the Baltic States. Lithuania is a member of the European Union and Schengen States and its GDP per capita is the highest in the Baltic States. It is the right time to invest in this country as you can also grow together with its steadily growing economy.    *Buyers For non-European citizens, it would be a wise option to invest in the commercial property that guarantees a return of investment. The attraction about Lithuania is, it offers a low investment cost to qualify you for a permanent residency. It is advisable to take advantage while it is a developing country because very soon it will become fully developed, just like any other European country. And it will surely require a much higher investment cost then.</p>
    </div>
      
      </div>

		{{-- <div class="col-md-6 servCont" data-aos="fade-up" data-aos-delay="0" data-aos-duration="1000">
			
			<div class="row justify-content-center updownBttn" >
				
			<button onclick="myFunction()"><i class="fas fa-chevron-up"></i></button>
		
		</div>
		<div class="row justify-content-center">
			<div id="myDIV" class="example contHeight1" >
			<div id="content">
				<div class="headingRE">
			<h4>A) Commercial Property </h4>
			<h4 class="sptxt3"> (Buying & Selling) </h4>
			<h6>Investing in property in Europe is an open door to a lot of opportunities. </h6>
			</div>
			<p>We, at Alpha Brain HR Consultancy LLC, are focusing on the country of Lithuania where it has the largest economy among the Baltic States. Lithuania is a member of the European Union and Schengen States and its GDP per capita is the highest in the Baltic States. It is the right time to invest in this country as you can also grow together with its steadily growing economy.    *Buyers For non-European citizens, it would be a wise option to invest in the commercial property that guarantees a return of investment. The attraction about Lithuania is, it offers a low investment cost to qualify you for a permanent residency. It is advisable to take advantage while it is a developing country because very soon it will become fully developed, just like any other European country. And it will surely require a much higher investment cost then. </p>

		</div>
			
		</div>
			</div>
			<div class="row justify-content-center updownBttn">
			<button onclick="myFunction1()"><i class="fas fa-chevron-down"></i></button>
		</div>
		</div> --}}
		</div>
	</div>
</section>



	<!---------------------------->
<section>
<div class="swiper-container" data-aos="fade-up" data-aos-delay="0" data-aos-duration="1000">

		<div class="col-md-12 cardHdg">
		<h5 class="txt4">Business For Sale</h5>
	</div>
    <div class="swiper-wrapper pptyCard">
      <div class="swiper-slide" >
      	<div class="card ">
    <img class="card-img-top" src="{{ url('assets/frontend/property/prop16.png')}}" alt="Card image cap">
    
    <div class="card-body">
      
      <div class="card-title">
      <a href="{{ url('/single_prop') }}"><h6>Restaurant for Sale – 1 Person Residency with Family</h6></a></div>

      <p class="card-text"><mark>Price on call</mark></p>
    </div>

    <div class="card-footer">
      <small class="text-muted"><i class="fas fa-user-tie"></i>Alpha Brain HR Consultancy LLC</small>
    </div>

  </div>

</div>
      <div class="swiper-slide" >
      	<div class="card">
    <img class="card-img-top" src="{{ url('assets/frontend/property/prop21.jpeg')}}" alt="Card image cap">
    
    <div class="card-body">
      
      <div class="card-title">
      <a href="{{ url('/single_prop2') }}"><h6>Restaurant – 2 Persons Residency with family</h6></a></div>

      <p class="card-text"><mark>Price on call</mark></p>
    </div>

    <div class="card-footer">
      <small class="text-muted"><i class="fas fa-user-tie"></i>Alpha Brain HR Consultancy LLC</small>
    </div>

  </div>

</div>
      <div class="swiper-slide" >
      	<div class="card">
    <img class="card-img-top" src="{{ url('assets/frontend/property/prop31.jpeg')}}" alt="Card image cap">
    
    <div class="card-body">
      
      <div class="card-title">
      <a href="{{ url('/single_prop3') }}"><h6>Kebab Shop – 1 person Residency With Family</h6></a></div>

      <p class="card-text"><mark>Price on call</mark></p>
    </div>

    <div class="card-footer">
      <small class="text-muted"><i class="fas fa-user-tie"></i>Alpha Brain HR Consultancy LLC</small>
    </div>

  </div>

</div>
      <div class="swiper-slide" >
      	<div class="card ">
    <img class="card-img-top" src="{{ url('assets/frontend/property/prop42.jpeg')}}" alt="Card image cap">
    
    <div class="card-body">
      
      <div class="card-title">
      <a href="{{ url('/single_prop4') }}"><h6>Coffee Shop – 2 Person Residency with Family</h6></a></div>

      <p class="card-text"><mark>Price on call</mark></p>
    </div>

    <div class="card-footer">
      <small class="text-muted"><i class="fas fa-user-tie"></i>Alpha Brain HR Consultancy LLC</small>
    </div>

  </div>

</div>
      <div class="swiper-slide" >
      	<div class="card ">
			<img class="card-img-top" src="{{ url('assets/frontend/property/prop21.jpeg')}}" alt="Card image cap">
    
			<div class="card-body">
			  
			  <div class="card-title">
			  <a href="{{ url('/single_prop2') }}"><h6>Restaurant – 2 Persons Residency with family</h6></a></div>
		
			  <p class="card-text"><mark>Price on call</mark></p>
			</div>
		
			<div class="card-footer">
			  <small class="text-muted"><i class="fas fa-user-tie"></i>Alpha Brain HR Consultancy LLC</small>
			</div>

  </div>

</div>
      <div class="swiper-slide" >
      	<div class="card ">
			<img class="card-img-top" src="{{ url('assets/frontend/property/prop16.png')}}" alt="Card image cap">
    
			<div class="card-body">
			  
			  <div class="card-title">
			  <a href="{{ url('/single_prop') }}"><h6>Restaurant for Sale – 1 Person Residency with Family</h6></a></div>
		
			  <p class="card-text"><mark>Price on call</mark></p>
			</div>
		
			<div class="card-footer">
			  <small class="text-muted"><i class="fas fa-user-tie"></i>Alpha Brain HR Consultancy LLC</small>
			</div>

		</div>

	</div>
  
      <div class="swiper-slide" >
      	<div class="card ">
			<img class="card-img-top" src="{{ url('assets/frontend/property/prop31.jpeg')}}" alt="Card image cap">
    
			<div class="card-body">
			  
			  <div class="card-title">
			  <a href="{{ url('/single_prop3') }}"><h6>Kebab Shop – 1 person Residency With Family</h6></a></div>
		
			  <p class="card-text"><mark>Price on call</mark></p>
			</div>
		
			<div class="card-footer">
			  <small class="text-muted"><i class="fas fa-user-tie"></i>Alpha Brain HR Consultancy LLC</small>
			</div>

  </div>

</div>
    </div>
  </div>

</section>
<!----------------------------------------------->
<section class="sec3">
	<div class="container">
		<div class="row r1 servDet ">
		
		<div class="col-md-6 servCont" data-aos="fade-up" data-aos-delay="0" data-aos-duration="1000">
			<div class="headingRE2 "  >
			<h4 style="color:#1296B7;text-align:center;">Property Management</h4></div>
			{{-- <div class="row justify-content-center updownBttn" >
				
			<button onclick="myFunction()"><i class="fas fa-chevron-up"></i></button>
		
		</div> --}}
		<div class="row justify-content-center">
			{{-- <div id="myDIV" class="example contHeight2" > --}}
			<div>
			<p style="color: #5f646d;text-align: justify;text-justify: inter-word;">We, at Alpha Brain HR Consultancy LLC, are focusing on the country of Lithuania where it has the largest economy among the Baltic States. Lithuania is a member of the European Union and Schengen States and its GDP per capita is the highest in the Baltic States. It is the right time to invest in this country as you can also grow together with its steadily growing economy.    *Buyers For non-European citizens, it would be a wise option to invest in the commercial property that guarantees a return of investment. The attraction about Lithuania is, it offers a low investment cost to qualify you for a permanent residency. It is advisable to take advantage while it is a developing country because very soon it will become fully developed, just like any other European country. And it will surely require a much higher investment cost then.  </p>

		{{-- </div>   --}}
			
		</div>
			</div>
			{{-- <div class="row justify-content-center updownBttn">
			<button onclick="myFunction1()"><i class="fas fa-chevron-down"></i></button>
		</div> --}}
		</div>
		<div class="col-md-6 servImg " data-aos="fade-up" data-aos-delay="0" data-aos-duration="1000">
			<img src="{{ url('assets/frontend/property/europe-permit-page.jpg')}}" height="auto" alt="" >	
		</div>
		</div>
	</div>


</section>
<!------- Start of our services--------------------->
<!---------------Enquiry Form---------------------------------------->
<section>
		<div class="container-fluid">
			<div class="row justify-content-center enqMsg">
				<div class="col-md-6 col-md-offset-3 enqCol" data-aos="fade-up" data-aos-delay="0" data-aos-duration="1000">
        			<div class="well well-sm">
						<form class="form-horizontal" method="post" action="{{url('/submit_realestate_form')}}" enctype="multipart/form-data">
							{{csrf_field()}}
          			<fieldset>
           			 <legend >Enquiry</legend>
    
			            
			            <div class="form-group">
			             
			              <div class="col-md-12">
			                <input id="name" name="name" type="text" placeholder="Name" class="form-control">
			              </div>
			            </div>
     
			          
			            <div class="form-group">
			              
			              <div class="col-md-12">
			                <input id="email" name="email" type="text" placeholder="Email" class="form-control">
			              </div>
			            </div>
			    
			            <div class="form-group">
			              
			              <div class="col-md-12">
			                <input id="phn_num" name="phn_num" type="text" placeholder="Phone" class="form-control">
			              </div>
			            </div>
          
			            <div class="form-group">
			              
			              <div class="col-md-12">
			                <textarea class="form-control" id="message" name="message" placeholder="message " rows="5"></textarea>
			              </div>
			            </div>
			    
			          
			            <div class="form-group">
			              <div class="col-md-12 subtn text-right">
			                <button type="submit" class="btn btn-success ">Submit</button>
			              </div>
			            </div>
			          </fieldset>
			          </form>
			        </div>
			      </div>

				<div class="col-md-5">
				<div class="clr1" data-aos="zoom-in" data-aos-delay="500" data-aos-duration="1000">
						<a class="zoom1" href="{{ url('/'.'#ourServicesdd') }}" style="text-decoration: none;"><h3>Other Services<br> > </h3></a>
					</div>
					</div>
				</div>
		</div>
	</div>
	</section>


  @include('includes.footer')