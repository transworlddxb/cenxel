<!DOCTYPE html>
<html>
<head>
  <title>Alpha Brain HR Consultancy LLC </title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  
  <link rel="stylesheet" type="text/css" href="{{asset('assets/frontend/css/bootstrap.min.css')}}">
  {{-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous"> --}}
  <link rel="stylesheet" type="text/css" href="{{asset('assets/frontend/css/stylesMain.css')}}">
  <link rel="stylesheet" type="text/css" href="{{asset('assets/frontend/css/all.css')}}">
  <link rel="stylesheet" type="text/css" href="{{asset('assets/frontend/css/all.min.css')}}">
   <link rel="stylesheet" type="text/css" href="{{asset('assets/frontend/css/aos.css')}}">
  <link rel="stylesheet" type="text/css" href="{{asset('assets/frontend/css/svg-with-js.css')}}">
  <link rel="shortcut icon" href="{{asset('assets/frontend/images/alpha tab logo.png')}}" type="image/x-icon">
  <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@400;500;600;700&display=swap" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@500;600;700&display=swap" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css2?family=Lato:wght@400;700&display=swap" rel="stylesheet">

</head>
<body  >
<!------------------------Start of header------------>


<section>
	<div class="container-fluid">
		<div class="row hdrRw">
	<div class="d-flex flex-row-reverse finrasHeader fixed-top">
  <div class="p-2 headerItem"><i class="far fa-clock"></i>
                        <span>Mon-Sat:9am to 6pm</span></div>
  <div class="p-2 headerItem"><i class="far fa-envelope"></i>
                        <span>alphahrdxb@gmail.com</span>
    </div>

    	
    	<div class="p-2 headerItem"><span class="fa-stack" style="vertical-align: top;">
  <i class="far fa-circle fa-stack-2x"></i>
  <i class="fas fa-phone-alt fa-stack-1x"></i>
</span><span>+(971) 43555110</span></div>

  </div>
</div>
</div>
</section>
<!---------------------Menu-------------------------------------->
<section>
	<div class="container-fluid">
	<div class="row no-padding">
	<div class="col-lg-12 col-md-12 " >
		<nav class="navbar navbar-expand-md bg-dark fixed-top fin_nav" style="height:150px">
		
			<a class="navbar-brand" href="{{ url('/') }}"><img src="images/ALPHA BRAIN logo.png" style="width:220px !important; height:220px !important;"></a>
		  	<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
		    	<span class="navbar-toggler-icon"><i class="fa fa-bars" aria-hidden="true"></i></span>
		  	</button>

		  	<div class="collapse navbar-collapse" id="navbarSupportedContent">
		    	<ul class="navbar-nav ml-auto">
		      		<li class="nav-item active">
		        		<a class="nav-link" href="{{ url('/') }}">Home <span class="sr-only">(current)</span></a>
		      		</li>
				    <li class="nav-item">
				        <a class="nav-link" href="{{ url('/'.'#aboutUsSection') }}">About Us</a>
				    </li>
		      		<li class="nav-item dropdown">
		        		<a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-hover="dropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Services</a>
		       
       
        				<div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
							<a class="dropdown-item" href="{{ url('/residency_permit') }}">Property Buy And Sell</a>
							<a class="dropdown-item" href="{{ url('/agriculture') }}">Agriculture</a>
							<a class="dropdown-item" href="{{ url('/foodNbeverage') }}">Food & Beverage</a>
							<a class="dropdown-item" href="{{ url('/hospitality') }}">Hospitality</a>
							<a class="dropdown-item" href="{{ url('/manpowerSupply') }}">Manpower Supply</a>
          					
        				</div>
      				</li>
		       
		      		<li class="nav-item">
						<a class="nav-link" href="{{ url('/contact') }}">Contact Us</a>
					  </li>
					  <li class="nav-item">
						<a class="nav-link" href="{{ url('/blog') }}">Blogs</a>
					  </li>
		      		<li class="nav-item">
				
          				{{-- <form action="" class="search-form" autocomplete="off">
                			<div class="form-group has-feedback">
            					<label for="search" class="sr-only">Search</label>
            					<input type="text" class="form-control" name="search" id="search" placeholder="search">
              					<span class="form-control-feedback"><i class="fa fa-search"></i></span>
            				</div>
            			</form>         --}}
					</li>
				</ul>
		  	</div>
		
		</nav>
</div>
</div>
</div>
</section>
<!------------------------End of Menu----------------------------------->

<!--------------------------Banner------------------------------->
<section>
		
	<div class="container-fluid fb">
		<div class="ldevice"><img src="images/top-page-1@2x.jpg"><div class="icon-scroll"></div></div>
		<div class="mdevice"><img src="images/top-page-1@4x.jpg"><div class="icon-scroll"></div></div>
		<div class="sdevice"><img src="images/top-page-1@5x.jpg"><div class="icon-scroll"></div></div>
		<div class="row no-padding rbn">
				
		<div class="col-md-1 bnnrDet" data-aos="fade-right" data-aos-delay="0"
    		 data-aos-duration="1000">
				<div id="orSeparator">
				<div class="row" id="socialSeparatorTop"></div>
				<div class="row" id="or" class="list-group">
					<ul class="orSptr">
						<li><a href=""><i class="fab fa-facebook-f zoom" style="color: #A7FCFF"></i></a></li>
						<li><a href=""><i class="fab fa-twitter zoom" style="color: #A7FCFF"></i></a></li>
						<li><a href=""><i class="fab fa-linkedin-in zoom" style="color: #A7FCFF"></i></a></li>
						<li><a href=""><i class="fab fa-instagram zoom" style="color: #A7FCFF"></i></a></li>
					</ul>
				</div>
				<div class="row" id="socialSeparatorBottom"></div>
				</div>
		</div>

		<div class="col-md-7 bnnrHeading lmdevice"  >
			<h3 data-aos="fade-right" data-aos-delay="0" data-aos-duration="1000">Empowering you to </h3>
			<h3 data-aos="fade-right" data-aos-delay="0" data-aos-duration="1000">meet the challenges ahead</h3>
			<p>Alpha Brain HR Consultancy LLC is a leading company that provides and offer services in the field of Property and Commercial Business Buy and Sell, Manpower Supply, Food and Beverage, Hospitality and Agriculture.</p>
	 	</div>
	 	<div class="col-md-7 bnnrHeading smdevice">
			<h3 data-aos="fade-right" data-aos-delay="0" data-aos-duration="1000">Empowering you to meet </h3>
			<h3 data-aos="fade-right" data-aos-delay="0" data-aos-duration="1000">the challenges ahead </h3>
			
	 	</div>

		</div>
	</div>
	

	</section>


<!---------------------------our ventures--------------------------->
<section>
	<div class="container ourVentures" >
		<h2 data-aos="fade-right"
      data-aos-delay="0"
    data-aos-duration="1000"> Our Ventures</h2>
		<div class="row venturesLogo justify-content-center" data-aos="fade-up"
      data-aos-delay="500"
    data-aos-duration="1000">
			<div class="col-lg-3 col-md-3 imageBordr logo">
			<img src="images/TRANSORLD LOGO@2x.jpg" >	
			{{-- <div class="septr"></div> --}}
			</div>
			<div class="col-lg-3 col-md-3 imageBordr logo">
			<img src="images/EVEREST LOGO@2x.jpg" >	
			{{-- <div class="septr"></div> --}}
			</div>
			<div class="col-lg-2 col-md-2 imageBordr logo">
			<img src="{{ url('assets/frontend/property/LOGO.png')}}" >
			{{-- <div class="septr"></div>	 --}}
			</div>
			<div class="col-lg-4 col-md-4 imageBordr logo" id="aboutUsSection">
			<img src="{{ url('assets/frontend/property/al_logo1.png')}}" >	
			</div>
		</div>
		
	</div>
</section>
<section>
	<div class="container ourVentures" >
		<div class="row venturesLogo justify-content-center" data-aos="fade-up"
      data-aos-delay="500"
    data-aos-duration="1000">
			<div class="col-lg-3 col-md-3 imageBordr logo">
			<img src="{{ url('assets/frontend/property/memo_logo.png')}}" >	
			{{-- <div class="septr"></div> --}}
			</div>
			<div class="col-lg-2 col-md-2 imageBordr logo">
			<img src="{{ url('assets/frontend/property/ofw2.png')}}" >	
			{{-- <div class="septr"></div> --}}
			</div>
			<div class="col-lg-2 col-md-2 imageBordr logo">
			<img src="{{ url('assets/frontend/property/kb.jpg')}}" >
			{{-- <div class="septr"></div>	 --}}
			</div>
			<div class="col-lg-2 col-md-2 imageBordr logo" id="aboutUsSection">
			<img src="{{ url('assets/frontend/property/tidytown.png')}}" >	
			</div>
		</div>
		
	</div>
</section>
<!-------------------------- Start of about us-------------------------->
<section>
	
	<div class="container" >
		
		<div class="row r1">

		<div class="col-md-6" data-aos="fade-up"
    data-aos-delay="0"
    data-aos-duration="1000">
			<img src="images/office@2x.jpg" width="550" height="365" alt="" >	
		</div>
		<div class="col-md-6 abtUs" data-aos="fade-up"
    
    data-aos-delay="500"
    data-aos-duration="1000">
			<h2>
			who we are
			</h2>
			<p style="color: #8a95aa;text-align: justify;text-justify: inter-word;">Alpha Brain HR Consultancy LLC is a leading company that offer services to get  EU Residency permit through Investment and  Manpower supply services. Our services are designed to help maximize the value of your asset, and to help you every step of the way of the transaction process. Finras has a real commitment to build strong, proactive and long term relationships with both corporate and private clients, for whom we always strive to deliver the best results and total satisfaction.<br>Finras provides skilled and semi-skilled labor force and resources to offer a comprehensive suite of services for our clients with efficient and cost-effective solutions that guarantee long term asset value and enable uninterrupted business operations. Through our well trained and enthusiastic staffs we maintain comprehensive trust and confidence of our clients by seeking to serve their needs better for future development and success. </p>
			<div id="ourServices"></div>
			</div>
	</div>
	</div>
</section>
<!-------------------------- Start of our services-------------------------->

<section class="sec1"  id="ourServicesdd">
	<div class="container ">
		<div class="row justify-content-center">
			<div class="col-md-10">

				<h2 data-aos="fade-up"
    data-aos-delay="0"
    data-aos-duration="1000">Our services</h2>
		</div>
		</div>
		
		<div class="row justify-content-center serv1" data-aos="fade-up"
    data-aos-delay="500"
    data-aos-duration="1000">


			<div class="col-md-4 cardCol">
					<div class="card text-center">
						<img class="card-img-top" src="images/eu-permit1.png" >
						<div class="card-body">
							<h6>EU Residency Permit</h6>
							<p>The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested.</p>
							 <a href="{{ url('/residency_permit') }}">Read More..</a>
						</div>
					</div>
				</div>
				<div class="col-md-4 cardCol">
					<div class="card text-center">
						<img class="card-img-top" src="images/property1.png" >
						<div class="card-body">
							<h6>Commercial Property Buy & Sell</h6>
							<p>Investing in property in Europe is an open door to a lot of opportunities.</p>
							 <a href="{{ url('/residency_permit') }}">Read More..</a>
						</div>
					</div>
				</div>
				<div class="col-md-4 cardCol">
					<div class="card text-center">
						<img class="card-img-top" src="images/manpower_supply1.png" >
						<div class="card-body">
							<h6>Manpower Supply</h6>
							<p>We are the one-stop destination for all your manpower supply needs in various trades and businesses.</p>
							 <a href="{{ url('/manpowerSupply') }}">Read More..</a>
						</div>
					</div>
				</div>

		</div>

		<div class="row justify-content-center serv1" data-aos="fade-up"
    data-aos-delay="500"
    data-aos-duration="1000">
			<div class="col-md-4 cardCol">
					<div class="card text-center">
						<img class="card-img-top" src="images/hospitality1.png">
						<div class="card-body">
							<h6>Hospitality</h6>
							<p>Finras is known as a diversified company operating in industries including Real Estate.</p>
							 <a href="{{ url('/hospitality') }}">Read More..</a>
						</div>
					</div>
				</div>
				<div class="col-md-4 cardCol">
					<div class="card text-center">
						<img class="card-img-top" width="50%" src="images/food_n_bvg1.png" >
						<div class="card-body">
							<h6>Food & Beverage</h6>
							<p>Finras is known as a diversified company operating in industries including Real Estate.</p>
							 <a href="{{ url('/foodNbeverage') }}">Read More..</a>
						</div>
					</div>
				</div>
					<div class="col-md-4 cardCol">
					<div class="card text-center">
						<img class="card-img-top" width="50%" src="images/real_estate1.png" >
						<div class="card-body">
							<h6>Property Buy & Sell</h6>
							<p>Finras is known as a diversified company operating in industries including Real Estate.</p>
							 <a href="{{ url('/residency_permit') }}">Read More..</a>
						</div>
					</div>
				</div>
		</div>


<div class="row justify-content-center serv1" data-aos="fade-up"
    data-aos-delay="500"
    data-aos-duration="1000">
			<div class="col-md-4 cardCol">
					<div class="card text-center">
						<img class="card-img-top" src="images/agriculture1.png" >
						<div class="card-body">
							<h6>Agriculture</h6>
							<p>Agriculture always plays an essential role in the economy of every country.</p>
							 <a href="{{ url('/agriculture') }}">Read More..</a>
						</div>
					</div>
				</div>

		
	<div class="col-lg-5 col-md-4 hd1">
		<h4 data-aos="zoom-in"
    data-aos-delay="500"
    data-aos-duration="1000">Heading About Helping In Success</h4>
	</div>
	<div class="col-lg-3 col-md-4">
	<h6></h6>
			
	</div>
</div>
			<div class="climb" data-aos="fade-up"
			    data-aos-delay="900"
			    data-aos-duration="1000">
			<img src="images/climb22.png" >
			</div>
	</div>
	
</section>

<!--------------------------Start of Quality policy-------------------------->
<section class="sec2">
	<div class="container ">
		<div class="row">
		<div class="col-lg-3 col-md-4 col-sm-5 qltyImg" data-aos="zoom-in"
    data-aos-delay="500"
    data-aos-duration="1000">
		<img src="images/thumbsup.png" height="200px" >
	</div>
	<div class="col-md-6 qltyplcy" >
		<h1 data-aos="fade-up"
    data-aos-delay="0"
    data-aos-duration="1000">Finras</h1>
		<h1 data-aos="fade-up"
    data-aos-delay="0"
    data-aos-duration="1000">Quality policy</h1>
		<p data-aos="fade-up"
    data-aos-delay="500"
    data-aos-duration="1000">Our mission is to bring affordable quality service within the reach of our clients. We are committed to provide each client with the facility of the highest quality services with cost-effectiveness. We have a zest for delivering the best, and to help our clients to adopt appropriate strategies and tactical plans in right time.</p>
		<a href="#ourServices" data-aos="zoom-in"
    data-aos-delay="800"
    data-aos-duration="1000"
    data-aos-anchor-placement="bottom-bottom">Our Services</a>
	</div>
	</div>
	</div>
</section>

@include('includes.footer')