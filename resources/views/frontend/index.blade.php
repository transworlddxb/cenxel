<!DOCTYPE html>
<html>
<head>
  <title>OpenC4S</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <link rel="stylesheet" type="text/css" href="{{asset('assets/frontend/css/bootstrap.min.css')}}">
  <link rel="stylesheet" type="text/css" href="{{asset('assets/frontend/css/stylesMain.css')}}">
  <link rel="stylesheet" type="text/css" href="{{asset('assets/frontend/css/all.css')}}">
  <link rel="stylesheet" type="text/css" href="{{asset('assets/frontend/css/all.min.css')}}">
  <link rel="stylesheet" type="text/css" href="{{ url('assets/frontend/css/aos.css') }}">
  <link rel="stylesheet" href="{{asset('assets/backend/css/sweetalert.css')}}">
  <link rel="shortcut icon" href="{{ url('assets/frontend/images/cenexl logo color.svg') }}">
<link rel="preconnect" href="https://fonts.gstatic.com">
<link href="https://fonts.googleapis.com/css2?family=Nunito+Sans:wght@200;300;400;600;700;800;900&display=swap" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="{{asset('assets/frontend/css/svg-with-js.css')}}">
</head>
<body  >


<style type="text/css">
     input[type=number]::-webkit-inner-spin-button,
   input[type=number]::-webkit-outer-spin-button {
  -webkit-appearance: none;
  margin: 0;
}
</style>

<?php
$message=Session::get('message');
$msg=explode("|",$message);

?>

@if ($message = Session::get('message'))
<div  style='z-index:9999; position:absolute;width:200px;right:0px; padding:10px 15px 0px 15px;' id='msg'>
   <div class="alert alert-{{$msg[0]}}" role="alert">
     <button type="button" class="close" data-dismiss="alert" aria-label="Close">
       <span aria-hidden="true">&times;</span>
   </button>
   {{ $msg[1] }}
</div><!-- alert -->
</div>
@endif

<!------------------------Start of header------------>
<section>
	<div class="c4s_menu">
	<nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
  <a class="navbar-brand" href="{{ url('/') }}"><img src="{{asset('assets/frontend/images/open c4 logo color 1.svg')}}"><img src="{{asset('assets/frontend/images/open c4 logo color2.svg')}}"></a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav ml-auto">
      <li class="nav-item active">
        <a class="nav-link" href="#techSectn">Technology <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#learnEasySectn">Learn easy</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#yOpenc4sSectn">Why open c4s</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#featuresSectn">Features</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#cntusSectn">Contact</a>
      </li>
         <li class="nav-item cenxel_logo">
        <a class="nav-link" href="{{ url('/cenxel') }}"><img src="{{asset('assets/frontend/images/cenexl logo color.svg')}}"></a>
      </li>

    </ul>

  </div>
</nav>



</section>
<!----------------------------------------------------------------->


<!--------------Main banner------------------>

<section>
	<div class="container-fluid ">
		<div class="row c4s_index_bnr">

			<div class="col-lg-6 col-md-8 bnnr_cont">
        <div class="row">
          <div class="col-2"></div>
          <div class="col-10 p1">
            <div class="bnrhdg">
				<h2>Open C4S</h2>
        <h1 data-aos="fade-right" data-aos-delay="200"
        data-aos-duration="1000">WIDENING</h1><h1 data-aos="fade-right" data-aos-delay="200"
        data-aos-duration="1000"> THE WORLD OF </h1><h1 data-aos="fade-right" data-aos-delay="200"
        data-aos-duration="1000">E-LEARNING</h1>
				<p data-aos="zoom-in" data-aos-delay="400"
        data-aos-duration="1000">In a world with knowledge on the fingertips, Cenxel Technologies launches Open C4S - the easiest, most comfortable and effective way of knowledge building and dissemination for the benefit of educational institutions and corporate companies or organizations which need to educate or train students or employees through a massive open online course Platform.</p>
        <a href="#sectn1" class="fltRight" data-aos="fade-right" data-aos-delay="500"
        data-aos-duration="1000">
          <img src="{{asset('assets/frontend/images/arrow yellow right.svg')}}"></a>
          </div>
          {{-- <!--<a href="#" ><img src="{{asset('assets/frontend/images/download.svg')}}"><span class="dwnldBrochure">Download Brochure</span></a>--> --}}
          <div id="hero">
            <div id="scroll">
              <span class="arrow-bounce">↓</span>
            </div>
          </div>
			</div>
    </div>

	</div>
  <div class="col-lg-6 col-md-4 bnnrSMedia" data-aos="fade-left" data-aos-delay="0"
        data-aos-duration="1000">
            <div class="list-group">
            <ul>
              <li><a href="https://www.facebook.com/Cenxel-190071032548291"><img src="{{asset('assets/frontend/images/facebook(1).svg')}}"></a></li>
              <li><a href="https://twitter.com/Cenxel1"><img src="{{asset('assets/frontend/images/twitter(1).svg')}}"></a></li>
              <li><a href="https://www.instagram.com/cenxel_tvm/"><img src="{{asset('assets/frontend/images/instagram(1).svg')}}"></i></a></li>
            </ul>
          </div>
        </div>
</div>
</div>
</section>
<!----------------------bannerFtr------------------------------>

<section>
	<div class="container-fluid bnr_ftr">
   <div class="row justify-content-center">
    <h3 data-aos="flip-up" data-aos-delay="200"
        data-aos-duration="1000">Open C4S is squarely based on the Open Edx technology, the most advanced open learning destination founded by Harvard and MIT in 2012 for the biggest online portal in the world.</h3>
   </div>
  </div>
</section>

<!--------------------------section1-------------------------->

<section>
<div class="container-fluid" id="sectn1">
  <div class="row justify-content-center sec1 pnk">
    <div class="col-lg-6 col-md-6 sec1_det">
      <div class="row">
        <div class="col-2"></div>
        <div class="col-10 p1 ">
      <h2>Open C4S</h2>
      <h1 data-aos="fade-right" data-aos-delay="200"
        data-aos-duration="1000">Technology for Teaching better, Learning better</h1>
      <div id="summary">
        <p class="collapse" id="collapseSummary" data-aos="zoom-in" data-aos-delay="200"
        data-aos-duration="1000">
          In a world with knowledge on the fingertips, Cenxel Technologies launches Open C4S - the easiest, most comfortable and effective way of knowledge building and dissemination for the benefit of educational institutions and corporate companies or organizations which need to educate or train students or employees through a massive open online course Platform. This platform is the brainchild of a group of brilliant Industrial specialists, educators and subject specialists who have a deep understanding of the traditional methods of education and the possibilities, effectiveness, modalities and challenges in the online system. The effort of this ideal association has resulted in forging Open C4S, a widely applicable platform or software solution deployable by all types of institutions for the purpose of instruction and education. While designing and programming the platform, our experts have paid utmost diligence to mitigate all the challenges faced at the level of content creation, course differences, patter diversity of organizations and management and administration of institutions. In short, the universality of application in the field of education is the hallmark of Open C4S. A comfortable web or mobile learning interface, variety of content types, easy delivery features, integration with the internal system and corporate database, repeating and analytic features such as gamified and adaptive learning techniques etc define the platform and they make the whole system effective, perfect and comfortable. And no wonder that the client is satisfied and feel aligned with the brand!Open Edx – The foundation of Open C4S.
        </p>
        <a class="collapsed" data-toggle="collapse" href="#collapseSummary" aria-expanded="false" aria-controls="collapseSummary"></a>
      </div>
    </div>
  </div>
</div>
      <div class="col-lg-6 col-md-6 bnr_img " data-aos="zoom-in" data-aos-delay="300"
        data-aos-duration="1000">
        <img src="{{asset('assets/frontend/images/illustration 1.svg')}}" width="100%">
      </div>
    </div></div>
      </section>
<!----------------------section technology------------------------>

<section>
  <div class="container-fluid" id="techSectn">
  <div class="row justify-content-center techRow sec1 ylw">
    <div class="col-lg-6 col-md-6 sec1_det br_bnr">
       <div class="row">
        <div class="col-2"></div>
        <div class="col-10 p1 ">
      <h1 data-aos="fade-right" data-aos-delay="200"
        data-aos-duration="1000">Built it on the rock</h1>
      <div id="summary">
        <p >
          Open C4S is squarely based on the Open Edx technology, the most advanced open learning destination founded by Harvard and MIT in 2012 for the biggest online portal in the world. While exploring the possibilities of delivering the best available technologies to serve high-quality education, Cenxel Technologies have realized the superiority of Open Edx and our team of online learning experts has been working exclusively in open edX related projects with reference to providing software as a service hosting solution, consulting services, Technology integration, content creation, and professional e-learning solutions and infrastructure training and assessment initiatives. Open edX is so flexible and diverse that it fulfils the diverse instructional design needs of different organizations to create engaging experiences of different topics to the pursuers of different goals, and users of different learning scenarios. Open edX enables even small organizations to create high quality and customized content for instructing the personnel and enhancing their skills. World’s largest organizations like Google, Microsoft Lass, Mc Kinsey, Johnson and Johnson are only a few among the beneficiaries of Open Edx platform who use it for educating employees, onboarding customers etc. Harvard, MIT, Arizona State University and other eminent educational institutions also use open edX to engage a huge amount of students for interaction, content dissemination and tracking educational progress etc.
        </p>
        <a class="collapsed" data-toggle="collapse" href="#collapseSummary" aria-expanded="false" aria-controls="collapseSummary"></a>
      </div>
    </div>
  </div>
</div>
      <div class="col-lg-6 col-md-6 bnr_img lmDevc">
        <img src="{{asset('assets/frontend/images/built_on_rock@1x.png')}}" width="100%">
      </div>

    </div></div>
</section>
<!--------------------section learn easy------------------------>

<section>
  <div class="container-fluid" id="learnEasySectn">
  <div class="row justify-content-center sec1">
    <div class="col-md-6 bnr_img" data-aos="zoom-in" data-aos-delay="300" data-aos-duration="1000">
        <img src="{{asset('assets/frontend/images/illustration 2.svg')}}" width="100%">
      </div>
    <div class="col-md-6 sec1_det">

      <h1 data-aos="fade-right" data-aos-delay="200"
        data-aos-duration="1000">Learn with</h1><h1 data-aos="fade-right" data-aos-delay="200"
        data-aos-duration="1000"> the leader</h1>

        <p >
          There are numerous learning platforms available around you and the number keeps on mounting up. Still, Open C4S justifies its keep by amazingly growing popularity as it is more advanced, comfortable, easy, user- friendly and of course, more useful. All needs of the learning and knowledge dissemination system are covered by it. Powered by Open edXand built using Ubuntu, Django, Docker and React, Open C4S  is a superb learning platform which institutions can make use of to conduct classes. Open C4S has two models: the on-premises model and cloud model. To educational institutions, the on-premises model is more adaptive and useful. Educators can extend this platform to build learning tools that precisely meet their specific needs. Open C4S supports Video, HTML text and discussion elements. Course authors are also able to upload include their own Wiki, or textbook style of elements as PDF file.<br>There are two sides to the open C4S platform. LMS – The LMS is the learner facing side of the platform where learners can access and complete courses. Studio- Studio is the other side of the platform accessed through a browser by course administrators, instructors and course authors to create, design and manage courses.
        </p>

    </div>

    </div></div>
</section>
<!--------------------section why open c4s------------------------>
<section>
  <div class="container-fluid" id="yOpenc4sSectn">

  <div class="row yOpenC4S sec1 whu_c4_bnr">

    <div class="col-lg-7 col-md-7 yOpenC4S_det sec1_det">
      <h1 data-aos="fade-right" data-aos-delay="200"
        data-aos-duration="1000">Why choose C4S LMS platform?- Love the ultimate!</h1>

        <p data-aos="zoom-in" data-aos-delay="200"
        data-aos-duration="1000" >
          Open C4S is an open-source LMS written in Python and supported by a huge international community and has a proven track record of successful operation. To put it briefly, the wide acceptance of C4S as the best LMS platform owing to the following factors:
        </p>

        <!--------------------------->
    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
        <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="headingOne">
                <h4 class="panel-title">
                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
                  Wide range of functionality
                </a>
                 </h4>
            </div>
            <div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                  <div class="panel-body">
                      <p>C4S includes both the studio CMS for online course development and LMS for the learning process Management. A comprehensive coverage at both ends!</p>
                  </div>
            </div>
          </div>
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="headingTwo">
                                    <h4 class="panel-title">
                                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                        Content and integration into the existing eco-system
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                                    <div class="panel-body">
                                        <p>As Open C4S is a Shareable Content Object Reference Model, the company can easily transform existing online materials into an Open C4S course T. Hence, the clients who have the same kind of in house training programs for employees can blend and use these materials for further training programs.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="headingThree">
                                    <h4 class="panel-title">
                                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                            Content Authoring and Management in clicks
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                                    <div class="panel-body">
                                        <p>Open C4S uses x blocks content structure – minor complete course elements that can be easily managed, adjusted and reorganized if need be, empowering intense reuse and diversification to meet the business goals set. </p>
                                    </div>
                                </div>
                            </div>


                        <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="headingFour">
                <h4 class="panel-title">
                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                  Convenient Learner enrolment and management.
                </a>
                 </h4>
            </div>
            <div id="collapseFour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFour">
                  <div class="panel-body">
                      <p>Open C4S utilizes multiple ways of learner enrolment (self- enrolment via a platform, e-mail invitations, CSV import etc.)All the learners enrolled for a course can be split into groups and easily rearranged to form new groups which are especially important for corporate training. </p>
                  </div>
            </div>
          </div>
          <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="headingFive">
                <h4 class="panel-title">
                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                  Diverse Learning models for different courses.
                </a>
                 </h4>
            </div>
            <div id="collapseFive" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFive">
                  <div class="panel-body">
                      <p>Open C4S supports synchronous, asynchronous and blended learning models. The learners can study fully online or have their exams in class (on-campus Proctoring) They gain access to the new stages in groups(synchronous, instructor- paced ) or on their own as they advance ( asynchronous, self-paced ). This approach has a lot of value in corporate training programs. </p>
                  </div>
            </div>
          </div>
          <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="headingSix">
                <h4 class="panel-title">
                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseSix" aria-expanded="false" aria-controls="collapseSix">
                  Vast options for communication and collaboration.
                </a>
                 </h4>
            </div>
            <div id="collapseSix" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingSix">
                  <div class="panel-body">
                      <p>The platform offers a variety of engaging channels for communication and collaboration between educators and learners, discussion forums for all learners of courses, separate groups or cohorts of learners for working on different projects, chats and push notifications and even telegram bot – all these channels help augment the learning process greatly. By implementing the learning by teaching approach and collaboration help learners comprehend new knowledge and improve their skills better than the passive learners.</p>
                  </div>
            </div>
          </div>
          <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="headingSeven">
                <h4 class="panel-title">
                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseSeven" aria-expanded="false" aria-controls="collapseSeven">
                  Ample selection of exam proctoring models and tools.
                </a>
                 </h4>
            </div>
            <div id="collapseSeven" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingSeven">
                  <div class="panel-body">
                      <p>Open C4S allows multiple approaches to the exam proctoring process. The on-campus proctoring system helps monitor the students passing exams in class. Any campus or corporation can choose amongst several proctoring modules or models that fit their goals best. </p>
                  </div>
            </div>
          </div>
          </div>

        <!------------------------------>

    </div>
      <div class="col-lg-5 col-md-5 bnr_img lmDevc">
        <img src="{{asset('assets/frontend/images/whu_c4s@22x.png')}}" width="100%">
      </div>
    </div>
  </div>
</section>
<!--------------------------section features--------------------->
<section>

  <div class="container-fluid" id="featuresSectn">
  <div class="row justify-content-center sec1">
    <div class="col-md-6 bnr_img" data-aos="zoom-in" data-aos-delay="200"
        data-aos-duration="1000">
        <img src="{{asset('assets/frontend/images/illustration 3.svg')}}" width="100%">
      </div>
    <div class="col-md-6 sec1_det">
      <h1 data-aos="fade-right" data-aos-delay="200"
        data-aos-duration="1000">Features- Amazingly</h1>
        <h1 data-aos="fade-right" data-aos-delay="200"
        data-aos-duration="1000"> Complete and Total</h1>

        <p>
          All inclusiveness helps the platform stand out from the rest. That Is why it is so popular among the educational institutions and corporate companies and many of them have installed Open  C4S as their educational platform for teaching and training. It is widely acclaimed as the most convenient, comfortable, productive and easy to use educational platform. The platform is dynamic and it contains Dynamic text editor, Dynamic Text set up manager, Personalized test for participants, personalized content delivery, reports facility for dynamic assignment etc. On a normal LMS, Mathematics and science instructors are facing a lot of problems to put equations, symbols and other signs to set questions. Here- everything –symbols, equations, signs can be brought to the page, for uploading even the most symbol\equation\sign, rich question faster than on any other LMS. All other normal features of an LMS will be in the system in a PLE manner.  The features are so comprehensive to cover all the functions and aspects required for a learning platform. Clients can choose from the numerous tools and customize their product according to their need and requirement.
        </p>
        <a href="{{ url('/featuresList') }}" data-aos="zoom-in" data-aos-delay="400" data-aos-duration="1000">Features List <i class="fas fa-arrow-right"></i></a>
    </div>

    </div></div>
</section>
<!--------------------section contact us------------------------>
<section>
  <div class="container-fluid" id="cntusSectn">
  <div class="row justify-content-center cntus_frm">

    <div class="col-lg-5 col-md-6">
      <h1 data-aos="fade-right" data-aos-delay="200"
        data-aos-duration="1000">Contact us</h1>
      <div class="list-group" data-aos="zoom-in" data-aos-delay="200"
        data-aos-duration="1000">
            <ul>
              <li>Cenxel Technologies Pvt.Ltd,</li>
              <li>2nd Floor, CM Mathew Brothers Arcade,</li>
              <li>Near Westway Hotel,</li>
              <li>Chakkorathkulam,</li>
              <li> Kozhikode-673006,Kerala.</li>
              <li>info@cenxel.com</li>

            </ul>
          </div>
    </div>
    <div class="col-lg-6 col-md-6 cntc_frm_det">
      <div class="col-lg-12 col-lg-offset-2">


        <form class="form-horizontal" method="post" action="{{url('/contact_us')}}" enctype="multipart/form-data">
            {{csrf_field()}}

        <div class="messages"></div>

        <div class="controls">

          <div class="row">
            <div class="col-md-12 p2">
              <div class="form-group">

                <input id="form_name" type="text" name="name" class="form-control" placeholder="Name" required="required" data-error="Name is required.">
                <div class="help-block with-errors"></div>
              </div>
            </div>

          </div>
          <div class="row">
            <div class="col-md-6 p2">
              <div class="form-group">
                <input id="form_phone" type="tel" name="phone" class="form-control" placeholder="Phone">
                <div class="help-block with-errors"></div>
              </div>
            </div>
            <div class="col-md-6 p2">
              <div class="form-group">
                <input id="form_email" type="email" name="email" class="form-control" placeholder="Email" required="required" data-error="Valid email is required.">
                <div class="help-block with-errors"></div>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12 p2">
              <div class="form-group">
                <textarea id="form_message" name="messages" class="form-control" placeholder="Message" rows="4" required data-error="Please,leave us a message."></textarea>
                <div class="help-block with-errors"></div>
              </div>
            </div>
            <div class="col-md-12 p2">

              <button type="submit" class="btn btn-defult bttn1">Submit <i class="fas fa-arrow-right"></i></button>
            </div>
          </div>

        </div>

        </form>

      </div>

    </div>
  </div>
</div>
</section>

<!--------------------------Start of footer------------------------>
<section>

	<div class="container-fluid">
	<div class="row ftr">
		<!---------Main ftr row separation---------------------->
		<div class="col-md-4">
     <div class="row">

      <div class="col-7 vrtLine logo1">
        <img src="{{asset('assets/frontend/images/open c4s grey logo.svg')}}" width="80%">
      </div>



      <div class="col-4 logo2">
        <img src="{{asset('assets/frontend/images/cenexl grey logo.svg')}}" width="80%">
      </div>

     </div>
    </div>
	<!-----------Main ftr row separation-------------------->
    <div class="col-md-4">
      <div class="row">



       <div class="col-11 ftrDscrptn vrtLine1">
      <p >
        All inclusiveness helps the platform stand out from the rest. That Is why it is so popular among the educational institutions and corporate companies and many of them havinstalled Open  C4S as their educational platform for teaching and training. It is widely acclaimed as the most convenient, comfortable, productive and easy to use educational platform.
      </p>
      <p>Copyrights © 2021. All Rights Reserved</p>
    </div>

    </div>
    </div>
<!------------Main ftr row separation------------------->
    <div class="col-md-4">
    <div class="row">
      <!------->


      <div class="col-6 ftrLinks">
      <div class="row">

        <div class="col-8 footerMainLinks ">
          <div class="list-group">
            <ul>
              <li><a href="#techSectn">Technology</a></li>
              <li><a href="#yOpenc4sSectn">Why open c4s</a></li>
              <li><a href="#learnEasySectn">Learn easy</a></li>
              <li><a href="#featuresSectn">Features</a></li>
              <li><a href="#cntusSectn">Contact</a></li>
            </ul>
          </div>
        </div>

          <div class="col-2 footerSocialMedia vrtLine">
            <div class="list-group">
            <ul>
              <li><a href="https://www.facebook.com/Cenxel-190071032548291"><i class="fab fa-facebook-f"></i></a></li>
              <li><a href="https://twitter.com/Cenxel1"><i class="fab fa-twitter"></i></a></li>
              <li><a href="https://www.instagram.com/cenxel_tvm/"><i class="fab fa-instagram"></i></a></li>

            </ul>
          </div>
          </div>

        </div>
      </div>



            <!----->
      <div class="col-6 footerMainLinks p1">

        <div class="list-group">
            <ul>
              <li>Cenxel Technologies Pvt.Ltd,</li>
              <li>2nd Floor, CM Mathew Brothers Arcade,</li>
              <li>Near Westway Hotel,</li>
              <li>Chakkorathkulam,</li>
              <li> Kozhikode-673006,Kerala.</li>
              <li>info@cenxel.com</li>

            </ul>
          </div>
      </div>
      <!------->
    </div>
  </div>
<!--------------------------->
	</div>
	</div>

</section>

<!------------------------End of footer----------------------->


<script type="text/javascript">
    $(document).ready(function() {
        setTimeout(function(){
            $("#msg").fadeOut(2000); }, 2000)
        })
    });
</script>
<script src="{{asset('assets/frontend/js/jquery-3.3.1.slim.min.js')}}" ></script>
<script type="text/javascript" src="{{asset('assets/frontend/js/bootstrap.min.js')}}"></script>
<script src="{{asset('assets/frontend/js/aos.js')}}" ></script>
<script src="{{asset('assets/backend/js/sweetalert.min.js')}}"></script>

<script>
    AOS.init();
  </script>
<script type="text/css" href="{{asset('assets/frontend/js/all.js')}}"></script>
<script type="text/css" href="{{asset('assets/frontend/js/all.min.js')}}"></script>

</body>
</html>
