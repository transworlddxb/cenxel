<!--------------------------Start of footer-------------------------->
<section>
	<div class="ftr">
	<div class="container">
	<div class="row">
		<div class="col-md-6">
			<div class="footerMainCol1" >
			  <div class="clearfix"></div>
			  <img src="{{ url('images/ALPHA BRAIN logo.png')}}" width="200" height="150" alt="" data-aos="zoom-in" data-aos-delay="0" data-aos-duration="1000">
			  <p>UAB Alpha Brain HR Consultancy LLC is a leading company that provides and offer services in the field of Agriculture, Food and Beverages,   Hospitality, Property and Commercial Business Buy and Sell and Manpower Supply.  </p>
			  <div class="hrLine"></div>
			</div>
		</div>
		
		<div class="col-md-3">
			<div class="footerMainLinks" data-aos="zoom-in" data-aos-delay="100"
    		  data-aos-duration="1000">
			  <div class="clearfix"></div>
			  <h6>Our Services</h6>
			  <div class="list-group">
				<ul>
					<li class="zoom1"><a href="{{ url('/residency_permit') }}">EU Permanent Residence</a></li>
					<li class="zoom1"><a href="{{ url('/residency_permit') }}">commercial Property Buy & Sell</a></li>
					<li class="zoom1"><a href="{{ url('/manpowerSupply') }}">Manpower Supply</a></li>
					<li class="zoom1"><a href="{{ url('/hospitality') }}">Hospitality</a></li>
					<li class="zoom1"><a href="{{ url('/foodNbeverage') }}">Food and Beverages</a></li>
					<li class="zoom1"><a href="{{ url('/residency_permit') }}">Property Buy & Sell</a></li>
					<li class="zoom1"><a href="{{ url('/agriculture') }}">Agriculture</a></li>
				</ul>	
			  </div>
			</div>
		</div>


		<div class="col-md-3">
			<div class="footerMainLinks" data-aos="zoom-in" data-aos-delay="100"
    			data-aos-duration="1000">
			  <div class="clearfix"></div>
			  <h6>Get In Touch</h6>
			  <div class="list-group">
				<ul>
					<li class="imgSz"><img src="images/ADDRESS ICON.png" alt="phnIcon" ><span > &nbsp;Alpha Brain HR Consultancy LLC</span></i></li>
					<li><span class="sptxt1">  Offic 240, Ground Floor,</span></li>
					<li><span class="sptxt1"> Unique world Business Centre, Dubai</span></li>
					<li class="sptxt2 imgSz"><img src="images/PHONE ICON.png" alt="phnIcon" ><span>&nbsp; +(971) 55 758 6218</span></li>
					<li class="sptxt2 txt2 imgSz"><img src="images/MAIL.png" alt="phnIcon" ><span>&nbsp; alphahrdxb@gmail.com</span></i></li>
					<li class="sptxt2"><span class="mediaIcons">
					 <i class="fab fa-facebook-f zoom1"><span>&nbsp;&nbsp;</span></i>
					 <i class="fab fa-twitter zoom1"><span>&nbsp;&nbsp;</span></i>
					 <i class="fab fa-linkedin-in zoom1"><span>&nbsp;&nbsp;</span></i>
					 <i class="fab fa-instagram zoom1"><span></i>
					</span></li>
				</ul>	
			  </div>
			
			</div>
		</div>
	</div>
	</div>
	</div>
</section>

<!------------------------End of footer----------------------->

<script type="text/javascript" src="{{asset('assets/frontend/js/swiper.min.js')}}"></script>	
<script src="{{asset('assets/frontend/js/jquery-3.3.1.slim.min.js')}}" ></script>
<script src="{{asset('assets/frontend/js/aos.js')}}" ></script>
	<script type="text/javascript" src="{{asset('assets/frontend/js/bootstrap.min.js')}}"></script>
	<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
	<script>
    AOS.init();
  </script>
	<script>
		$(document).ready(function() {
     
		    $(".fa-search").click(function() {
		       $(".search-box").toggle();
		       $("input[type='text']").focus();
		     });
 
 		});
	</script>
	
	<script>
		
$(window).scroll(function(){
 $('nav').toggleClass('scrolled', $(this).scrollTop() > 10);
 });


	</script>
	<script>
		var swiper = new Swiper('.swiper-container', {
		  effect: 'coverflow',
		  grabCursor: true,
		  centeredSlides: true,
		  slidesPerView: 'auto',
		  loop: true,
		  coverflowEffect: {
			rotate: 10,
			stretch: 0,
			depth: 200,
			modifier: 1,
			slideShadows : true,
	
		  },
		  pagination: {
			el: '.swiper-pagination',
		  },
		});
	  </script>

<script>

	function myFunction() {
	  var elmnt = document.getElementById("myDIV");
	 
	  elmnt.scrollTop -= 30;
	}
	
	</script>
	<script>
	
	function myFunction1() {
	  var elmnt = document.getElementById("myDIV");
	 
	  elmnt.scrollTop += 30;
	}
	
	</script>
	<script>
    var swiper = new Swiper('.swiper-container', {
      effect: 'coverflow',
      grabCursor: true,
      centeredSlides: true,
      slidesPerView: 'auto',
      loop: true,
      coverflowEffect: {
        rotate: 10,
        stretch: 0,
        depth: 200,
        modifier: 1,
        slideShadows : true,

      },
      pagination: {
        el: '.swiper-pagination',
      },
    });

	setTimeout(function(){ $("#msg").fadeOut(2000); }, 2000);
  </script>

<script>
	var dat=Date();
	var arr=dat.split(" ")
	document.getElementById('demo').innerHTML="-"+arr[1]+" "+arr[2]+", "+arr[3];
	</script>

  <script type="text/css" href="{{asset('assets/frontend/js/all.js')}}"></script>
  <script type="text/css" href="{{asset('assets/frontend/js/all.min.js')}}"></script>
</body>
</html>
