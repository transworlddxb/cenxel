<!DOCTYPE html>
<html>
<head>
  <title>Alpha Brain HR Consultancy LLC </title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  
  <link rel="stylesheet" type="text/css" href="{{asset('assets/frontend/css/bootstrap.min.css')}}">
  <link rel="stylesheet" type="text/css" href="{{asset('assets/frontend/css/stylesMain.css')}}">
  <link rel="stylesheet" type="text/css" href="{{asset('assets/frontend/css/all.css')}}">
  <link rel="stylesheet" type="text/css" href="{{asset('assets/frontend/css/all.min.css')}}">
   <link rel="stylesheet" type="text/css" href="{{asset('assets/frontend/css/aos.css')}}">
  <link rel="stylesheet" type="text/css" href="{{asset('assets/frontend/css/svg-with-js.css')}}">
  <link rel="stylesheet" type="text/css" href="{{asset('assets/frontend/css/swiper.min.css')}}">
  <link rel="shortcut icon" href="{{asset('assets/frontend/images/alpha tab logo.png')}}" type="image/x-icon">
  {{-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous"> --}}
  <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@400;500;600;700&display=swap" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@500;600;700&display=swap" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css2?family=Lato:wght@400;700&display=swap" rel="stylesheet">

</head>
<body  >
<!------------------------Start of header------------>


<section>
	<div class="container-fluid">
		<div class="row hdrRw">
	<div class="d-flex flex-row-reverse finrasHeader fixed-top">
  <div class="p-2 headerItem"><i class="far fa-clock"></i>
                        <span>Mon-Sat:9am to 6pm</span></div>
  <div class="p-2 headerItem"><i class="far fa-envelope"></i>
                        <span>alphahrdxb@gmail.com</span>
    </div>

    	
    	<div class="p-2 headerItem"><span class="fa-stack" style="vertical-align: top;">
  <i class="far fa-circle fa-stack-2x"></i>
  <i class="fas fa-phone-alt fa-stack-1x"></i>
</span><span>+(971) 55 758 6218</span></div>

  </div>
</div>
</div>
</section>
<!---------------------Menu-------------------------------------->
<section>
	<div class="container-fluid">
	<div class="row no-padding">
	<div class="col-lg-12 col-md-12 " >
	<nav class="navbar navbar-expand-md bg-dark fixed-top fin_nav" style="height:150px">
		
		<a class="navbar-brand" href="{{ url('/') }}"><img src="images/ALPHA BRAIN logo.png" style="width:220px !important; height:220px !important;"></a>
		  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"><i class="fa fa-bars" aria-hidden="true"></i></span>
		  </button>

		  	<div class="collapse navbar-collapse" id="navbarSupportedContent">
		    	<ul class="navbar-nav ml-auto">
		      		<li class="nav-item active">
		        		<a class="nav-link" href="{{ url('/') }}">Home <span class="sr-only">(current)</span></a>
		      		</li>
				    <li class="nav-item">
				        <a class="nav-link" href="{{ url('/'.'#aboutUsSection') }}">About Us</a>
				    </li>
		      		<li class="nav-item dropdown">
		        		<a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-hover="dropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Services</a>
		       
       
        				<div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
							<a class="dropdown-item" href="{{ url('/residency_permit') }}">Invest & Get EU Residency Permit</a>
							<a class="dropdown-item" href="{{ url('/agriculture') }}">Agriculture</a>
							<a class="dropdown-item" href="{{ url('/foodNbeverage') }}">Food & Beverage</a>
							<a class="dropdown-item" href="{{ url('/hospitality') }}">Hospitality</a>
							<a class="dropdown-item" href="{{ url('/manpowerSupply') }}">Manpower Supply</a>
          					
        				</div>
      				</li>
		       
		      		<li class="nav-item">
						<a class="nav-link" href="{{ url('/contact') }}">Contact Us</a>
					  </li>
					  <li class="nav-item">
						<a class="nav-link" href="{{ url('/blog') }}">Blogs</a>
					  </li>
		      		<li class="nav-item">
				
          				{{-- <form action="" class="search-form" autocomplete="off">
                			<div class="form-group has-feedback">
            					<label for="search" class="sr-only">Search</label>
            					<input type="text" class="form-control" name="search" id="search" placeholder="search">
              					<span class="form-control-feedback"><i class="fa fa-search"></i></span>
            				</div>
            			</form>         --}}
					</li>
				</ul>
		  	</div>
		
		</nav>
</div>
</div>
</div>
</section>
<!------------------------End of Menu----------------------------------->