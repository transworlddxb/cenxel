@extends('beautymail::templates.minty')

@section('content')

	@include('beautymail::templates.minty.contentStart')
		<tr>
			<td class="title">
			Hello admin
			</td>
		</tr>
		<tr>
			<td width="100%" height="10"></td>
		</tr>
		<tr>
			<td class="paragraph">
				This is the Billing Statement for Costumer : {{$firstname}} {{$middlename}} {{$lastname}}
			</td>
		</tr>
		<tr>
			<td width="100%" height="10"></td>
		</tr>
		<tr>
			<td class="paragraph">
				Treatment Taken By:{{$doctor_name}}
			</td>
		</tr>
		<tr>
			<td width="100%" height="10"></td>
		</tr>
		<tr>
			<td class="paragraph">
				Date: {{$appointment_date}}
			</td>
		</tr>
		<tr>
			<td width="100%" height="10"></td>
		</tr>
		<tr>
			<td class="paragraph">
				Time: {{$appointment_time}}
			</td>
		</tr>
		<tr>
			<td width="100%" height="10"></td>
		</tr>
		<table cellspacing="0">
                <tbody>
                      <td colspan="2" rowspan="9" class="valign-middle">
                        <div class="invoice-notes">
                          <label class="section-label-sm tx-gray-500"></label>
                          {{-- <p>{{$cunsultdata->consult_details}}</p> --}}
                          <p></p>
                        </div><!-- invoice-notes -->
                      </td>
                      @if($file->insurance_status==0)
                      <td class="tx-right">Insurance</td>
                      <td colspan="2" class="tx-right" style="color: red;">Not Applicable</td>
                      @endif
                       @if($file->insurance_status==1)
                      <td class="tx-right">Insurance</td>
                      <td colspan="2" class="tx-right" style="color: green;">{{$file->percent_insurance}} %</td>
                      @endif
                    
                    </tr>
                     <tr>
                      <td class="tx-right">total Treatment amount</td>
                      <td colspan="2"  class="tx-right">{{$amount}}</td>
                    </tr>
                    <tr>
                      <td class="tx-right">total Purchased amount</td>
                      <td colspan="2"  class="tx-right">{{$total_purch_price}}</td>
                    </tr>
                    <tr>
                      <td class="tx-right"> discount</td>
                      <td colspan="2"  class="tx-right">{{$discount}}</td>
                    </tr>
                    <tr>
                      <td class="tx-right">Previous Due amount</td>
                      <td colspan="2"  class="tx-right">{{$due}}</td>
                    </tr>     
                    <tr>
                      <td class="tx-right">VAT amount</td>
                      <td colspan="2"  class="tx-right">{{$vat_amount}}</td>
                    </tr>
                    <tr>
                      <td class="tx-right tx-uppercase tx-bold tx-inverse">NET Amount</td>
                      <td colspan="2" class="tx-right"><h4 class="tx-primary tx-bold tx-lato">{{$grand_total}}</h4></td>
                    </tr>
                    
                    <tr>
                      <td class="tx-right tx-uppercase tx-bold tx-inverse">Paid Amount</td>
                      <td colspan="2" class="tx-right"><h4 class="tx-primary tx-bold tx-lato">{{$payed}}</h4></td>
                    </tr>
                </tbody>
            </table>
		<tr>
			{{-- <td>
				@include('beautymail::templates.minty.button', ['text' => 'Sign in', 'link' => 'http://codersbench.com/farmse_new/public/login'])
			</td> --}}
		</tr>
		<tr>
			<td width="100%" height="25"></td>
		</tr>
	@include('beautymail::templates.minty.contentEnd')

@stop