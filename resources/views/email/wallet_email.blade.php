@extends('beautymail::templates.minty')

@section('content')

	@include('beautymail::templates.minty.contentStart')
		<tr>
			<td class="title">
			Hello {{$name}}
			</td>
		</tr>
		<tr>
			<td width="100%" height="10"></td>
		</tr>
		<tr>
			<td class="paragraph">
				Your account wallet is credited successfully with ₹ {{$amount}}.Thank you!
			</td>
		</tr>
		{{-- <tr>
			<td width="100%" height="25"></td>
		</tr>
		<tr>
			<td class="title">
				This is a heading
			</td>
		</tr> --}}
		<tr>
			<td width="100%" height="10"></td>
		</tr>
{{-- 		<tr>
			<td class="paragraph">
				Username:{{$user_name}}
			</td>
		</tr>
		<tr>
			<td class="paragraph">
				Password:{{$password}}
			</td>
		</tr> --}}
		<tr>
			<td width="100%" height="25"></td>
		</tr>
		<tr>
			<td>
				@include('beautymail::templates.minty.button', ['text' => 'Sign in', 'link' => 'http://codersbench.com/farmse_new/public/login'])
			</td>
		</tr>
		<tr>
			<td width="100%" height="25"></td>
		</tr>
	@include('beautymail::templates.minty.contentEnd')

@stop