@extends('beautymail::templates.minty')

@section('content')

	@include('beautymail::templates.minty.contentStart')
		<tr>
			<td class="title">
			Hello Admin
			</td>
		</tr>
		<tr>
			<td width="100%" height="10"></td>
		</tr>
		<tr>
			<td class="paragraph">
				There is a enquiry from your website!
			</td>
		</tr>
		<tr>
			<td width="100%" height="10"></td>
		</tr>
		<tr>
			<td class="paragraph">
				Name:<b style="text-transform: uppercase"> {{ $name }} </b>
			</td>
		</tr>
		<tr>
			<td class="paragraph">
				Phone:<b style="text-transform: uppercase"> {{ $phone }} </b>
			</td>
		</tr>
		<tr>
			<td class="paragraph">
				Email:<b style="text-transform: uppercase"> {{ $email }} </b>
			</td>
		</tr>
		<tr>
			<td class="paragraph">
				Message:<b style="text-transform: uppercase"> {{ $messages }} </b>
			</td>
		</tr>
		<tr>
			<td width="100%" height="25"></td>
		</tr>
		<tr>
			{{-- <td>
				@include('beautymail::templates.minty.button', ['text' => 'Sign in', 'link' => 'http://codersbench.com/farmse_new/public/login'])
			</td> --}}
		</tr>
		<tr>
			<td width="100%" height="25"></td>
		</tr>
	@include('beautymail::templates.minty.contentEnd')

@stop
