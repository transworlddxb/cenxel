@include('admin.includes.header_account')


    <body class="account-pages">

        <!-- Begin page -->
        <div class="accountbg" style="background: url('assets/images/bg-2.jpg');background-size: cover;background-position: center;"></div>

        <div class="wrapper-page account-page-full">

            <div class="card">
                <div class="card-block">

                    <div class="account-box">

                        <div class="card-box p-5">
                            <h2 class="text-uppercase text-center pb-4">
                                <a href="index.php" class="text-success">
                                    <span><img src="{{asset('assets/images/logo.png')}}" alt="" height="26"></span>
                                </a>
                            </h2>
                            @include('flash::message')
                            <form action="{{url('/reg-user')}}" method="post" class="form-horizontal">
                                @csrf
                                <div class="form-group row m-b-20">
                                    <div class="col-12">
                                        <label for="username">User Name</label>
                                        <input class="form-control" type="name" id="username" required="" placeholder="Your name" name="name">
                                    </div>
                                </div>

                                <div class="form-group row m-b-20">
                                    <div class="col-12">
                                        <label for="emailaddress">Email address</label>
                                        <input class="form-control" type="email" id="emailaddress" required="" placeholder="Your Email" name="email">
                                        @if ($errors->has('email'))
                                        <span class="help-block">
                                            <strong style="color:red">{{ $errors->first('email') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group row m-b-20">
                                    <div class="col-12">
                                        <label for="password">Password</label>
                                        <input class="form-control" type="password" required="" id="password" placeholder="Enter your password" name="password">
                                        @if ($errors->has('password'))
                                        <span class="help-block">
                                            <strong style="color:red">{{ $errors->first('password') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group row m-b-20">
                                    <div class="col-12">
                                        <label for="password">Confirm Password</label>
                                        <input class="form-control" type="password" required="" id="password" placeholder="Enter your password" name="confirm-password">
                                
                                    </div>
                                </div>


                                <div class="form-group row text-center m-t-10">
                                    <div class="col-12">
                                        <button class="btn btn-block btn-custom waves-effect waves-light" type="submit">Sign Up Free</button>
                                    </div>
                                </div>

                            </form>

                            <div class="row m-t-50">
                                <div class="col-sm-12 text-center">
                                    <p class="text-muted">Already have an account?  <a href="page-login.php" class="text-dark m-l-5"><b><a style="color: #283d54;" href="{{ url('/login') }}">Sign In</a></b></a></p>
                                </div>
                            </div>

                        </div>
                    </div>

                </div>
            </div>

            <div class="m-t-40 text-center">
                <p class="account-copyright">2018 © Highdmin. - Coderthemes.com</p>
            </div>

        </div>

      @include('admin.includes.footer_account')