<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Twitter -->
    <meta name="twitter:site" content="@themepixels">
    <meta name="twitter:creator" content="@themepixels">
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:title" content="Slim">
    <meta name="twitter:description" content="Premium Quality and Responsive UI for Dashboard.">
    <meta name="twitter:image" content="http://themepixels.me/slim/img/slim-social.png">

    <!-- Facebook -->
    <meta property="og:url" content="http://themepixels.me/slim">
    <meta property="og:title" content="Slim">
    <meta property="og:description" content="Premium Quality and Responsive UI for Dashboard.">

    <meta property="og:image" content="http://themepixels.me/slim/img/slim-social.png">
    <meta property="og:image:secure_url" content="http://themepixels.me/slim/img/slim-social.png">
    <meta property="og:image:type" content="image/png">
    <meta property="og:image:width" content="1200">
    <meta property="og:image:height" content="600">

    <!-- Meta -->
    <meta name="description" content="Premium Quality and Responsive UI for Dashboard.">
    <meta name="author" content="ThemePixels">

    <title>Doctors Portal</title>

    <!-- Vendor css -->
    <link rel="shortcut icon" href="{{ url('image/dcsymbol.jpg') }}">
    <link href="{{asset('assets/backend/lib/font-awesome/css/font-awesome.css')}}" rel="stylesheet">
    <link href="{{asset('assets/backend/lib/Ionicons/css/ionicons.css')}}" rel="stylesheet">

    <!-- Slim CSS -->
    <link rel="stylesheet" href="{{asset('assets/backend/css/slim.css')}}">

</head>
<body>

    <div class="d-md-flex flex-row-reverse">
      <div class="signin-right">
        <form class="" action="{{url('/login')}}" method="post">
            @csrf
            <div class="signin-box signin-box-mob">
              <h2 class="signin-title-primary">Welcome back!</h2>
              <h3 class="signin-title-secondary">Sign in to continue.</h3>

              <div class="form-group">
                <input class="form-control" type="email" id="emailaddress" required="" placeholder="Enter your email" name="email">
                @if ($errors->has('email'))
                <span class="help-block">
                    <strong style="color:red">{{ $errors->first('email') }}</strong>
                </span>
                @endif
            </div><!-- form-group -->
            <div class="form-group mg-b-50">
                <input class="form-control" type="password" required="" id="password" placeholder="Enter your password" name="password">
                @if ($errors->has('password'))
                <span class="help-block">
                    <strong style="color:red">{{ $errors->first('password') }}</strong>
                </span>
                @endif
            </div><!-- form-group -->
            <button type="submit" class="btn btn-primary btn-block btn-signin">Sign In</button>
            {{-- <p class="mg-b-0">Don't have an account? <a href="page-signup2.html">Sign Up</a></p> --}}
        </div>
    </form>

</div><!-- signin-right -->
<div class="signin-left">
    <div class="signin-box">
      <h2 class="slim-logo"><img src="{{ url('image/doctor.png')}}"></h2>

{{--       <p>We are excited to launch our new company and product Slim. After being featured in too many magazines to mention and having created an online stir, we know that ThemePixels is going to be big. We also hope to win Startup Fictional Business of the Year this year.</p>

      <p>Browse our site and see for yourself why you need Slim.</p> --}}

      <p><a href="#" class="btn btn-outline-secondary pd-x-25">Learn More</a></p>

      <p class="tx-12">&copy; Copyright 2020. All Rights Reserved.</p>
  </div>
</div><!-- signin-left -->
</div><!-- d-flex -->

<script src="{{asset('assets/backend/lib/jquery/js/jquery.js')}}"></script>
<script src="{{asset('assets/backend/lib/popper.js/js/popper.js')}}"></script>
<script src="{{asset('assets/backend/lib/bootstrap/js/bootstrap.js')}}"></script>

<script src="{{asset('assets/backend/js/slim.js')}}"></script>

</body>
</html>