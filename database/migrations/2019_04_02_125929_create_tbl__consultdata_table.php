<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblConsultdataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl__consultdata', function (Blueprint $table) {
            $table->increments('pk_consult_id');
            $table->string('customer_id');         
            $table->string('patient_name');         
            $table->string('appointment_id');            
            $table->string('doctor_id');            
            $table->string('height_weight_check')->nullable();     
            $table->string('blood_pressure_check')->nullable();     
            $table->string('cholestrol_check')->nullable();     
            $table->string('blood_sugar_test')->nullable();     
            $table->string('throat_check')->nullable();
            $table->string('ear_check')->nullable();
            $table->string('eye_check')->nullable();
            $table->string('electrocardiogram_check')->nullable();
            $table->text('height_weight_check_descrip')->nullable();     
            $table->text('blood_pressure_check_descrip')->nullable();     
            $table->text('cholestrol_check_descrip')->nullable();     
            $table->text('blood_sugar_test_descrip')->nullable();     
            $table->text('throat_check_descrip')->nullable();
            $table->text('ear_check_descrip')->nullable();
            $table->text('eye_check_descrip')->nullable();
            $table->text('electrocardiogram_check_descrip')->nullable();
            $table->Integer('status')->nullable();      
            $table->rememberToken();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl__consultdata');
    }
}
