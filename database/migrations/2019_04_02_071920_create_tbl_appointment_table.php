<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblAppointmentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_appointment', function (Blueprint $table) {
            $table->increments('pk_int_appoint_id');
            $table->string('inv_nmbr');
            $table->Integer('fk_customer_reg_id');            
            $table->Integer('fk_doctor');
            $table->string('appointment_date');
            $table->string('appointment_time');
            $table->string('appoint_patient');
            $table->string('prod_purch_status');
            $table->string('trt_total_amount')->nullable();
            $table->text('consult_description')->nullable();
            $table->string('total_amount_to_pay')->nullable();
            $table->string('payedbill_amount')->nullable();
            $table->string('duebill_amount')->nullable();
            $table->string('vat_amount')->nullable();
            $table->string('prev_due_amount')->nullable();
            $table->string('prod_total_amount')->nullable();
            $table->string('product_one')->nullable();
            $table->string('product_two')->nullable();
            $table->string('product_three')->nullable();
            $table->string('product_four')->nullable();
            $table->string('product_five')->nullable();
            $table->string('product_six')->nullable();
            $table->string('product_one_count')->nullable();
            $table->string('product_two_count')->nullable();
            $table->string('product_three_count')->nullable();
            $table->string('product_four_count')->nullable();
            $table->string('product_five_count')->nullable();
            $table->string('product_six_count')->nullable();
            $table->Integer('consult_status');       
            $table->Integer('other_details_status')->default('0');       
            $table->Integer('payment_status');       
            $table->Integer('complete_status');       
            $table->rememberToken();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_appointment');
    }
}
