<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblProductPurchasedTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_product_purchased', function (Blueprint $table) {
            $table->increments('purchased_id');
            $table->Integer('purch_appointment_id');
            $table->string('product_one')->nullable();
            $table->string('product_two')->nullable();
            $table->string('product_three')->nullable();
            $table->string('product_four')->nullable();
            $table->string('product_five')->nullable();
            $table->string('purchased_date')->nullable();
            $table->string('purchased_amount')->nullable();
            $table->Integer('purchased_status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_product_purchased');
    }
}
