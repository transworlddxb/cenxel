<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblBillingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl__billing', function (Blueprint $table) {
            $table->increments('billing_id');
            $table->Integer('costumer_id');
            $table->Integer('doctor_id');
            $table->Integer('fk_bill_appointment_id');
            $table->string('date');
            $table->string('total_amount');
            $table->string('discount_amount')->nullable();
            $table->string('prev_due_amount')->nullable();
            $table->Integer('status');
            $table->Integer('approval_status')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl__billing');
    }
}
