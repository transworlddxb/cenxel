<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblRegistrationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_registration', function (Blueprint $table) {
            $table->increments('pk_int_reg_id');
            $table->string('doctor_id');
            $table->string('file_no')->nullable();
            $table->Integer('int_reg_category')->nullable();
            $table->string('first_name')->nullable();
            $table->string('middle_name')->nullable();
            $table->string('last_name')->nullable();
            $table->string('patient_place')->nullable();
            $table->string('age')->nullable();
            $table->string('patient_age')->nullable();
            $table->string('reg_dob')->nullable();
            $table->bigInteger('mob_number')->nullable();
            $table->string('gender')->nullable();
            $table->string('marital_status')->nullable();                 
            $table->string('adv_amount')->nullable();         
            $table->string('final_due_amount');         
            $table->Integer('status');         
            $table->rememberToken();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_registration');
    }
}
