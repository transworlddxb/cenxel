<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\User::create([
            'roll_name'          => 'Super Admin',
			'user_designation' 	     => 'Admin',
			'email'		 => 'admin@doctorsportal.com',
            'password'   => bcrypt('doctorsportal@110'),
			'pass'   => 'doctorsportal@110',
            'admin_role' =>User::SUPERADMIN,
            'status'     =>User::ACTIVATE
		]);	
	
    }
}
