<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         DB::table('tbl_role')->insert([
            'pk_int_role_id' => '1',
            'roll_name' => 'Super Admin',
            'roll_status' => '1',
        ]);
		DB::table('tbl_role')->insert([
            'pk_int_role_id' => '2',
            'roll_name' => 'Administrator',
            'roll_status' => '1',
        ]);
        DB::table('tbl_role')->insert([
            'pk_int_role_id' => '3',
            'roll_name' => 'Receptionist',
            'roll_status' => '1',
        ]);
        DB::table('tbl_role')->insert([
            'pk_int_role_id' => '4',
            'roll_name' => 'Doctor',
            'roll_status' => '1',
        ]);
        DB::table('tbl_role')->insert([
            'pk_int_role_id' => '5',
            'roll_name' => 'Accounts',
            'roll_status' => '1',
        ]);
    }
}
