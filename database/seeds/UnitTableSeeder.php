<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class UnitTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         DB::table('tbl_units_carrying')->insert([
            'class_id' => '1',
            'class_name' => 'Normal',
        ]);
		DB::table('tbl_units_carrying')->insert([
            'class_id' => '2',
            'class_name' => 'Government',
        ]);
        DB::table('tbl_units_carrying')->insert([
            'class_id' => '3',
            'class_name' => 'Student',
        ]);
    }
}
