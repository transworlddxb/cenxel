<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    protected $dates = ['deleted_at'];

    protected $primaryKey ='pk_int_role_id';

    protected $table = 'tbl_role';

    protected $fillable = ['roll_name','roll_status',];

    public static $rule=[
    	'roll_name'=>'required',
	];
	public static $message=[
		'roll_name.required'=>'Role is required',
    ];
}
