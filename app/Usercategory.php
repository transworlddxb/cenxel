<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Usercategory extends Model
{
    protected $dates = ['deleted_at'];

    protected $primaryKey ='ucat_pk_id';

    protected $table = 'tbl_user_category';

    protected $fillable = ['fk_ucat_id','ucat_name','ucat_status',];

    public static $rule=[
    	'ucat_name'=>'required',
	];

	public static $message=[
		'ucat_name.required'=>'Role is required',
    ];
}
