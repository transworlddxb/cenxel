<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Mail;
use Snowfire\Beautymail\Beautymail;
use App\Mail\SendMailable;
use App\Users;
use App\Units_carry;
use App\User;
use App\Role;
use App\Treatment;

use Validator;
use Flash;
use DataTables;
use DB;
use Auth;
use Session;
use Response;
use PDF;
use Carbon;
use \File;

class TreatmentController extends Controller
{
    public function add()
	{				
		// $list_treatment = Treatment::all();	
		$list_treatment = Units_carry::all();
		$treatment = DB::table('treatment')
        // ->join('tbl_units_carrying', 'tbl_units_carrying.class_id', '=', 'treatment.roll_status')
        ->select('*')
        ->get();
        // dd($treatment);die(); 
		return view('admin.treatment.add_treatment',compact("treatment","list_treatment"));
	}

	public function normal_price()
	{				
		// $list_treatment = Treatment::all();	
		// $treatment = Units_carry::all();
		$normal_list = DB::table('treatment')
		->where('roll_status',1)
        ->join('tbl_units_carrying', 'tbl_units_carrying.class_id', '=', 'treatment.roll_status')
        ->select('*')
        ->get();
        // dd($treatment);die(); 
		return view('admin.treatment.normal_price_list',compact("normal_list"));
	}

	public function gov_list()
	{				
		// $list_treatment = Treatment::all();	
		// $treatment = Units_carry::all();
		$gov_list = DB::table('treatment')
		->where('roll_status',2)
        ->join('tbl_units_carrying', 'tbl_units_carrying.class_id', '=', 'treatment.roll_status')
        ->select('*')
        ->get();
        // dd($treatment);die(); 
		return view('admin.treatment.gov_price',compact("gov_list"));
	}

	public function stud_list()
	{				
		// $list_treatment = Treatment::all();	
		// $treatment = Units_carry::all();
		$stud_list = DB::table('treatment')
		->where('roll_status',3)
        ->join('tbl_units_carrying', 'tbl_units_carrying.class_id', '=', 'treatment.roll_status')
        ->select('*')
        ->get();
        // dd($treatment);die(); 
		return view('admin.treatment.student',compact("stud_list"));
	}

	public function store(Request $request)
	{ 
		// return 1;
		$input=$request->all();
		// dd($input);die();
			$treatment = new Treatment();

			$treatment->treatment_name = $input['treatment_name'];
			$treatment->treatment_price = 0;
			$treatment->roll_status = 0;

			$flag = $treatment->save();            

			if ($flag) {
				Session::flash('message', 'success|Added successfully ');
				return redirect()->back();
			}
			else {
				Session::flash('message', 'danger|Failed, Try again');
				return redirect()->back();
			}
	}

	public function edit(Request $request,$id)
	{
		$sms = Treatment::where('pk_treatment_id', $id)->first();
		// print_r($sms); die();
				// $sms->sgrp_name = $input['sgrp_name'];
		$sg1 = $sms->pk_treatment_id.','.$sms->treatment_name.','.$sms->treatment_price;	
		echo $sg1;

	}
	public function update(Request $request,$id)
	{
		$input = $request->all();
		$treatment =Treatment::find($input['pk_treatment_id']);
		$treatment->fill($input);
		$treatment->treatment_name = $input['treatment_name'];
		// $treatment->treatment_price = $input['treatment_price'];
		$flag=$treatment->save();

		if ($flag) {
				Session::flash('message', 'success|Updated successfully ');
				return redirect()->back();
			}
			else {
				Session::flash('message', 'danger|Failed, Try again');
				return redirect()->back();
			}
	}

	public function destroy($id)
	{

		$treatment = Treatment::find($id);
		if($treatment!==Null) {

			$flag = $treatment->delete();

			if ($flag) {
			Session::flash('message', 'success|Deleted successfully ');
			return redirect()->back();
			}
			else {
				Session::flash('message', 'danger|Failed, Try again');
				return redirect()->back();
			}
		}
	} 
}
