<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Mail;
use Snowfire\Beautymail\Beautymail;
use App\Mail\SendMailable;
use Illuminate\Support\Facades\App;
//use Barryvdh\DomPDF\Facade as PDF;
use App\Users;
use App\User;
use App\Role;
use App\Register;
use App\Branches;
use App\Doctor;
use App\Units_carry;
use App\Appointment;
use App\Billing;
use Validator;
use Flash;
use DataTables;
use DB;
use Auth;
use Session;
use Response;
use PDF;
use Carbon;
use \File;

class ReportController extends Controller
{
    public function daily_report()
	{			
		// $role = Role::all();	
		// $register = Register::where('pk_int_reg_id', $id)->first();
		$date = Carbon::today()->toDateString();
        // dd($date);die();
		$branches = Branches::all();
		$doctor = Doctor::all();
		$class = Units_carry::all();
        
        $appointment = DB::table('tbl_appointment')
        ->join('tbl_registration', 'tbl_registration.pk_int_reg_id', '=', 'tbl_appointment.fk_customer_reg_id')
        ->join('users', 'users.id', '=', 'tbl_appointment.fk_doctor')
        // ->join('tbl__billing', 'tbl__billing.fk_bill_appointment_id', '=', 'tbl_appointment.pk_int_appoint_id')
        ->join('tbl_product_purchased', 'tbl_product_purchased.purch_appointment_id', '=', 'tbl_appointment.pk_int_appoint_id')
        ->where('complete_status','4')
        ->where('appointment_date',$date)
        ->select('*')
        ->get();

        // $total_to_pay=$appointment->amount+$appointment->purchased_amount;
        // dd($total_to_pay);die();
        // return 1;
		return view('admin.reports.daily_report',compact("appointment","branches","doctor","class","total_to_pay"));
	}
	public function monthly_report()
	{				
		// return 1;
		// $role = Role::all();	
		// $register = Register::where('pk_int_reg_id', $id)->first();
		// $date = Carbon::today()->toDateString();
		$now = Carbon::now();
		// dd($now->month);die();
		$branches = Branches::all();
		$doctor = Doctor::all();
		$class = Units_carry::all();
        $appointment = DB::table('tbl_appointment')
        ->join('tbl_registration', 'tbl_registration.pk_int_reg_id', '=', 'tbl_appointment.fk_customer_reg_id')
        ->join('users', 'users.id', '=', 'tbl_appointment.fk_doctor')
        // ->join('tbl__billing', 'tbl__billing.fk_bill_appointment_id', '=', 'tbl_appointment.pk_int_appoint_id')
        ->join('tbl_product_purchased', 'tbl_product_purchased.purch_appointment_id', '=', 'tbl_appointment.pk_int_appoint_id')
        ->where('complete_status','4')
        ->whereMonth('tbl_appointment.created_at',$now->month)
        ->select('*')
        ->get();
        // return 1;
		return view('admin.reports.monthly_report',compact("appointment","branches","doctor","class"));
	}
	public function weekly_report()
	{				
		// return 1;
		// $role = Role::all();	
		// $register = Register::where('pk_int_reg_id', $id)->first();
		// $date = Carbon::today()->toDateString();
		$now = Carbon::now();
		// dd($now->month);die();
		$branches = Branches::all();
		$doctor = Doctor::all();
		$class = Units_carry::all();
        $appointment = DB::table('tbl_appointment')
        ->join('tbl_registration', 'tbl_registration.pk_int_reg_id', '=', 'tbl_appointment.fk_customer_reg_id')
        ->join('users', 'users.id', '=', 'tbl_appointment.fk_doctor')
        // ->join('tbl__billing', 'tbl__billing.fk_bill_appointment_id', '=', 'tbl_appointment.pk_int_appoint_id')
        ->join('tbl_product_purchased', 'tbl_product_purchased.purch_appointment_id', '=', 'tbl_appointment.pk_int_appoint_id')
        ->where('complete_status','4')
        ->whereMonth('appointment_date',$now->month)
        ->select('*')
        ->get();
        // return 1;
		return view('admin.reports.weekly_report',compact("appointment","branches","doctor","class"));
	}
	public function date_search(Request $request)
	{				
		// return 1;
		$input = $request->all();
    	$from_date = $input['from_date'];
    	$to_date = $input['to_date'];
		$now = Carbon::now();
		// dd($now->month);die();
		$branches = Branches::all();
		$doctor = Doctor::all();
		$class = Units_carry::all();
        $appointment = DB::table('tbl_appointment')
        ->join('tbl_registration', 'tbl_registration.pk_int_reg_id', '=', 'tbl_appointment.fk_customer_reg_id')
        ->join('users', 'users.id', '=', 'tbl_appointment.fk_doctor')
        // ->join('tbl__billing', 'tbl__billing.fk_bill_appointment_id', '=', 'tbl_appointment.pk_int_appoint_id')
        ->join('tbl_product_purchased', 'tbl_product_purchased.purch_appointment_id', '=', 'tbl_appointment.pk_int_appoint_id')
        ->where('complete_status','4')
        ->where('appointment_date','>=', $from_date)
        ->where('appointment_date','<=', $to_date)
        ->select('*')
        ->get();
        // return 1;
		return view('admin.reports.weekly_report',compact("appointment","branches","doctor","class"));
	}
	public function date_daily_search(Request $request)
	{				
		// return 1;
		$input = $request->all();
    	$from_date = $input['from_date'];
    	$to_date = $input['to_date'];
		$now = Carbon::now();
		// dd($now->month);die();
		$branches = Branches::all();
		$doctor = Doctor::all();
		$class = Units_carry::all();
        $appointment = DB::table('tbl_appointment')
        ->join('tbl_registration', 'tbl_registration.pk_int_reg_id', '=', 'tbl_appointment.fk_customer_reg_id')
        ->join('users', 'users.id', '=', 'tbl_appointment.fk_doctor')
        // ->join('tbl__billing', 'tbl__billing.fk_bill_appointment_id', '=', 'tbl_appointment.pk_int_appoint_id')
        ->join('tbl_product_purchased', 'tbl_product_purchased.purch_appointment_id', '=', 'tbl_appointment.pk_int_appoint_id')
        ->where('complete_status','4')
        ->where('appointment_date','>=', $from_date)
        ->where('appointment_date','<=', $to_date)
        ->select('*')
        ->get();
        // return 1;
		return view('admin.reports.daily_report',compact("appointment","branches","doctor","class"));
	}
	public function search_date(Request $request)
	{				
		// return 1;
		$input = $request->all();
    	$search_date = $input['search_date'];
		// $now = Carbon::now();
		// dd($search_date);die();
		$branches = Branches::all();
		$doctor = Doctor::all();
		$class = Units_carry::all();
        $appointment = DB::table('tbl_appointment')
        ->join('tbl_registration', 'tbl_registration.pk_int_reg_id', '=', 'tbl_appointment.fk_customer_reg_id')
        ->join('users', 'users.id', '=', 'tbl_appointment.fk_doctor')
        // ->join('tbl__billing', 'tbl__billing.fk_bill_appointment_id', '=', 'tbl_appointment.pk_int_appoint_id')
        ->join('tbl_product_purchased', 'tbl_product_purchased.purch_appointment_id', '=', 'tbl_appointment.pk_int_appoint_id')
        ->where('complete_status','4')
        ->whereDate('appointment_date',$search_date)
        ->select('*')
        ->get();
        // dd($appointment);die();
        // return 1;
		return view('admin.reports.daily_report',compact("appointment","branches","doctor","class"));
	}
	public function month_year_search(Request $request)
	{				
		$input = $request->all();
    	$month = $input['month'];
    	$year = $input['year'];

		$branches = Branches::all();
		$doctor = Doctor::all();
		$class = Units_carry::all();
        $appointment = DB::table('tbl_appointment')
        ->join('tbl_registration', 'tbl_registration.pk_int_reg_id', '=', 'tbl_appointment.fk_customer_reg_id')
        ->join('users', 'users.id', '=', 'tbl_appointment.fk_doctor')
        // ->join('tbl__billing', 'tbl__billing.fk_bill_appointment_id', '=', 'tbl_appointment.pk_int_appoint_id')
        ->join('tbl_product_purchased', 'tbl_product_purchased.purch_appointment_id', '=', 'tbl_appointment.pk_int_appoint_id')
        ->where('complete_status','4')
        ->whereMonth('tbl_appointment.created_at',$month)
        ->whereYear('tbl_appointment.created_at',$year)
        ->select('*')
        ->get();
        // return 1;
		return view('admin.reports.monthly_report',compact("appointment","branches","doctor","class"));
	}

	public function reportPDF(Request $request)
    {
        $date = Carbon::today()->toDateString();
		$branches = Branches::all();
		$doctor = Doctor::all();
		$class = Units_carry::all();
        $appointment = DB::table('tbl_appointment')
        ->join('tbl_registration', 'tbl_registration.pk_int_reg_id', '=', 'tbl_appointment.fk_customer_reg_id')
        ->join('users', 'users.id', '=', 'tbl_appointment.fk_doctor')
        // ->join('tbl__billing', 'tbl__billing.fk_bill_appointment_id', '=', 'tbl_appointment.pk_int_appoint_id')
        ->join('tbl_product_purchased', 'tbl_product_purchased.purch_appointment_id', '=', 'tbl_appointment.pk_int_appoint_id')
        ->where('complete_status','4')
        ->where('appointment_date',$date)
        ->select('*')
        ->get();

        $pdf = PDF::loadView('admin.reports.reportpdf',compact("appointment","branches","doctor","file","date","billcount","treatment_one","treatment_two","treatment_three","treatment_four","treatment_five","amount","date"));
        return $pdf->download('report.pdf');

    }
    public function datewisePDF(Request $request)
    {
    	// return 1;
    	$input = $request->all();
    	$report_date = $input['report_date'];
        $date = Carbon::today()->toDateString();
		$branches = Branches::all();
		$doctor = Doctor::all();
		$class = Units_carry::all();
        $appointment = DB::table('tbl_appointment')
        ->join('tbl_registration', 'tbl_registration.pk_int_reg_id', '=', 'tbl_appointment.fk_customer_reg_id')
        ->join('users', 'users.id', '=', 'tbl_appointment.fk_doctor')
        // ->join('tbl__billing', 'tbl__billing.fk_bill_appointment_id', '=', 'tbl_appointment.pk_int_appoint_id')
        ->join('tbl_product_purchased', 'tbl_product_purchased.purch_appointment_id', '=', 'tbl_appointment.pk_int_appoint_id')
        ->where('complete_status','4')
        ->where('appointment_date',$report_date)
        ->select('*')
        ->get();

        $pdf = PDF::loadView('admin.reports.datewisepdf',compact("appointment","branches","doctor","file","date","billcount","treatment_one","treatment_two","treatment_three","treatment_four","treatment_five","amount","date","report_date"));
        return $pdf->download('report_datewise.pdf');

    }
    public function monthlyPDF(Request $request)
    {
    	// return 1;
    	$input = $request->all();
    	$report_month = $input['report_month'];
    	$report_year = $input['report_year'];
        $date = Carbon::today()->toDateString();
		$branches = Branches::all();
		$doctor = Doctor::all();
		$class = Units_carry::all();
        $appointment = DB::table('tbl_appointment')
        ->join('tbl_registration', 'tbl_registration.pk_int_reg_id', '=', 'tbl_appointment.fk_customer_reg_id')
        ->join('users', 'users.id', '=', 'tbl_appointment.fk_doctor')
        // ->join('tbl__billing', 'tbl__billing.fk_bill_appointment_id', '=', 'tbl_appointment.pk_int_appoint_id')
        ->join('tbl_product_purchased', 'tbl_product_purchased.purch_appointment_id', '=', 'tbl_appointment.pk_int_appoint_id')
        ->whereMonth('tbl_appointment.created_at', $report_month)
        ->whereYear('tbl_appointment.created_at', $report_year)
        ->where('complete_status','4')
        ->select('*')
        ->get();

        $pdf = PDF::loadView('admin.reports.monthlypdf',compact("appointment","branches","doctor","file","date","report_month","treatment_one","treatment_two","treatment_three","treatment_four","treatment_five","amount","date"));
        return $pdf->download('report_datewise.pdf');

    }
    public function bet_date_pdf(Request $request)
    {               
        // return 1;
        $input = $request->all();
        $from_date = $input['from_date'];
        $to_date = $input['to_date'];

        $branches = Branches::all();
        $doctor = Doctor::all();
        $class = Units_carry::all();
        $appointment = DB::table('tbl_appointment')
        ->join('tbl_registration', 'tbl_registration.pk_int_reg_id', '=', 'tbl_appointment.fk_customer_reg_id')
        ->join('users', 'users.id', '=', 'tbl_appointment.fk_doctor')
        // ->join('tbl__billing', 'tbl__billing.fk_bill_appointment_id', '=', 'tbl_appointment.pk_int_appoint_id')
        ->join('tbl_product_purchased', 'tbl_product_purchased.purch_appointment_id', '=', 'tbl_appointment.pk_int_appoint_id')
        ->where('complete_status','4')
        ->where('appointment_date','>=', $from_date)
        ->where('appointment_date','<=', $to_date)
        ->select('*')
        ->get();
        // return 1;
        $pdf = PDF::loadView('admin.reports.date_betweenpdf',compact("appointment","branches","doctor","file","date","report_month","treatment_one","treatment_two","treatment_three","treatment_four","treatment_five","from_date","to_date"));
        return $pdf->download('report.pdf');
    }
    public function doctor_report()
    {           
        // $role = Role::all(); 
        // $register = Register::where('pk_int_reg_id', $id)->first();
        $date = Carbon::today()->toDateString();
        $user = User::where('admin_role', '4')->get();
        $branches = Branches::all();
        $class = Units_carry::all();
        
        $appointment = DB::table('tbl_appointment')
        ->join('tbl_registration', 'tbl_registration.pk_int_reg_id', '=', 'tbl_appointment.fk_customer_reg_id')
        ->join('users', 'users.id', '=', 'tbl_appointment.fk_doctor')
        // ->join('tbl__billing', 'tbl__billing.fk_bill_appointment_id', '=', 'tbl_appointment.pk_int_appoint_id')
        ->join('tbl__consultdata', 'tbl__consultdata.appointment_id', '=', 'tbl_appointment.pk_int_appoint_id')
        ->where('complete_status','4')
        ->orderBy('tbl_appointment.created_at', 'desc')
        ->select('*')
        ->get();


        // $total_to_pay=$appointment->amount+$appointment->purchased_amount;
        // dd($treatment_one);die();
        // return 1;

        return view('admin.reports.doctor_report_list',compact("appointment","branches","user","class","total_to_pay","treatment_one","treatment_two","treatment_three"));
    }
    public function datewise_doctor_pdf(Request $request)
    {               
        // return 1;
        $input = $request->all();
        $report_date = $input['report_date'];
        $doctor_id = $input['int_doctor_id'];

        $branches = Branches::all();
        $class = Units_carry::all();
        $appointment = DB::table('tbl_appointment')
        ->join('tbl_registration', 'tbl_registration.pk_int_reg_id', '=', 'tbl_appointment.fk_customer_reg_id')
        ->join('users', 'users.id', '=', 'tbl_appointment.fk_doctor')
        // ->join('tbl__billing', 'tbl__billing.fk_bill_appointment_id', '=', 'tbl_appointment.pk_int_appoint_id')
        ->join('tbl_product_purchased', 'tbl_product_purchased.purch_appointment_id', '=', 'tbl_appointment.pk_int_appoint_id')
        ->where('complete_status','4')
        ->where('appointment_date', $report_date)
        ->where('fk_doctor', $doctor_id)
        ->select('*')
        ->get();

        $doctor = User::where('id', $doctor_id)
        ->pluck('roll_name')
        ->first();


        // return 1;
        $pdf = PDF::loadView('admin.reports.doctor_reportpdf',compact("appointment","branches","doctor","file","date","report_month","treatment_one","treatment_two","treatment_three","treatment_four","treatment_five","from_date","to_date"));
        return $pdf->download('doctor_report.pdf');
    }
    public function doctor_search_pdf(Request $request)
    {               
         $date = Carbon::today()->toDateString();
         $input = $request->all();
        $report_date = $input['report_date'];
        $doctor_id = $input['int_doctor_id'];
        $branches = Branches::all();
        $class = Units_carry::all();
        $user = User::where('admin_role', '4')->get();
        $doctor = User::where('id', $doctor_id)
        ->pluck('roll_name')
        ->first();

        $appointment = DB::table('tbl_appointment')
        ->join('tbl_registration', 'tbl_registration.pk_int_reg_id', '=', 'tbl_appointment.fk_customer_reg_id')
        ->join('users', 'users.id', '=', 'tbl_appointment.fk_doctor')
        // ->join('tbl__billing', 'tbl__billing.fk_bill_appointment_id', '=', 'tbl_appointment.pk_int_appoint_id')
        ->join('tbl__consultdata', 'tbl__consultdata.appointment_id', '=', 'tbl_appointment.pk_int_appoint_id')
        ->where('appointment_date', $report_date)
        ->where('fk_doctor', $doctor_id)
        ->where('complete_status','4')
        ->orderBy('tbl_appointment.created_at', 'desc')
        ->select('*')
        ->get();


        return view('admin.reports.doctor_report_list',compact("appointment","branches","user","class","total_to_pay","treatment_one","treatment_two","treatment_three","doctor"));
    }
    public function monthwise_doctor_pdf(Request $request)
    {               
        // return 1;
        $input = $request->all();
        $report_month = $input['report_month'];
        $doctor_id = $input['int_doctor_id'];

        $branches = Branches::all();
        $class = Units_carry::all();
        $appointment = DB::table('tbl_appointment')
        ->join('tbl_registration', 'tbl_registration.pk_int_reg_id', '=', 'tbl_appointment.fk_customer_reg_id')
        ->join('users', 'users.id', '=', 'tbl_appointment.fk_doctor')
        // ->join('tbl__billing', 'tbl__billing.fk_bill_appointment_id', '=', 'tbl_appointment.pk_int_appoint_id')
        ->where('complete_status','4')
        ->whereMonth('appointment_date', $report_month)
        ->where('fk_doctor', $doctor_id)
        ->select('*')
        ->get();

        $doctor = User::where('id', $doctor_id)
        ->pluck('roll_name')
        ->first();


        // return 1;
        $pdf = PDF::loadView('admin.reports.doctor_reportpdf',compact("appointment","branches","doctor","file","date","report_month","treatment_one","treatment_two","treatment_three","treatment_four","treatment_five","from_date","to_date"));
        return $pdf->download('doctor_report.pdf');
    }
    public function doctor_monthsearch_pdf(Request $request)
    {               
         $date = Carbon::today()->toDateString();
         $input = $request->all();
        $report_month = $input['report_month'];
        $doctor_id = $input['int_doctor_id'];

        $branches = Branches::all();
        $class = Units_carry::all();
        $user = User::where('admin_role', '4')->get();
        $doctor = User::where('id', $doctor_id)
        ->pluck('roll_name')
        ->first();

        $appointment = DB::table('tbl_appointment')
        ->join('tbl_registration', 'tbl_registration.pk_int_reg_id', '=', 'tbl_appointment.fk_customer_reg_id')
        ->join('users', 'users.id', '=', 'tbl_appointment.fk_doctor')
        // ->join('tbl__billing', 'tbl__billing.fk_bill_appointment_id', '=', 'tbl_appointment.pk_int_appoint_id')
        ->join('tbl__consultdata', 'tbl__consultdata.appointment_id', '=', 'tbl_appointment.pk_int_appoint_id')
        ->whereMonth('appointment_date', $report_month)
        ->where('fk_doctor', $doctor_id)
        ->where('complete_status','4')
        ->orderBy('tbl_appointment.created_at', 'desc')
        ->select('*')
        ->get();


        return view('admin.reports.doctor_report_list',compact("appointment","branches","user","class","total_to_pay","treatment_one","treatment_two","treatment_three","doctor"));
    }
    
    
}
