<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Mail;
use Snowfire\Beautymail\Beautymail;
use App\Mail\SendMailable;
use App\Users;
use App\User;
use App\Role;
use App\Register;
use App\Branches;

use Validator;
use Flash;
use DataTables;
use DB;
use Auth;
use Session;
use Response;
use PDF;
use Carbon;
use \File;

class BranchController extends Controller
{
    public function index()
	{				
		// $role = Role::all();	
		$branches = Branches::all();
		return view('admin.branches.add_branch',compact("branches"));
	}
	public function add_branches(Request $request)
	{ 
		// return 1;
		$input=$request->all();
		// dd($input);die();
			$branch = new Branches();

			$branch->branch_name = $input['branch_name'];
			$branch->status = '1';

			$flag = $branch->save();            

			if ($flag) {
				Session::flash('message', 'success|Added successfully ');
				return redirect()->back();
			}
			else {
				Session::flash('message', 'danger|Failed, Try again');
				return redirect()->back();
			}
	}
	public function edit(Request $request,$id)
	{
		$sms = Branches::where('pk_branch_id', $id)->first();
		// print_r($sms); die();
				// $sms->sgrp_name = $input['sgrp_name'];
		$sg1 = $sms->pk_branch_id.','.$sms->branch_name;	
		echo $sg1;

	}
	public function update_branch(Request $request,$id)
	{
		$input = $request->all();
		$branches =Branches::find($input['pk_branch_id']);
		$branches->fill($input);
		$branches->branch_name = $input['branch_name'];
		// $branches->sgrp_pk_id = $input['user_id'];
		$flag=$branches->save();

		if ($flag) {
				Session::flash('message', 'success|Updated successfully ');
				return redirect()->back();
			}
			else {
				Session::flash('message', 'danger|Failed, Try again');
				return redirect()->back();
			}
	}
	public function destroy($id)
	{

		$branches = Branches::find($id);
		if($branches!==Null) {

			$flag = $branches->delete();

			if ($flag) {
			Session::flash('message', 'success|Deleted successfully ');
			return redirect()->back();
			}
			else {
				Session::flash('message', 'danger|Failed, Try again');
				return redirect()->back();
			}
		}
	} 
	
}
