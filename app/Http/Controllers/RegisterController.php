<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Mail;
use Snowfire\Beautymail\Beautymail;
use App\Mail\SendMailable;
use App\Users;
use App\User;
use App\Role;
use App\Register;
use App\Branches;
use App\Units_carry;

use Validator;
use Flash;
use DataTables;
use DB;
use Auth;
use Session;
use Response;
use PDF;
use Carbon;
use \File;

class RegisterController extends Controller
{
    public function add()
	{				

        $register = DB::table('tbl_registration')
        // ->join('tbl_units_carrying', 'tbl_units_carrying.class_id', '=', 'tbl_registration.int_reg_category')
        ->select('*')
        ->get();
		return view('admin.register.register',compact("register"));
	}

	public function reg_usr()
	{		
		// return 1;
		$count=Register::count();
		$branches = Branches::all();
		$class = Units_carry::all();
		$register = $count+1;
		return view('admin.register.register_user',compact("register","branches","class"));
	}

	public function store(Request $request)
  	{
  		// return 1;     
      $logged=Auth::user()->id;
  		$input = $request->all(); 
        $filenumber = $input['file_no'];

        $register = new Register();  
        if (Register::where('file_no', '=', Input::get('file_no'))->exists()) {
               Session::flash('message', 'info|File number is already exist');
                            return redirect()->back();
            }

        $register->file_no = $input['file_no'];
        $register->doctor_id = $logged;
     	$register->first_name = $input['first_name'];
     	$register->middle_name = $input['middle_name'];
     	$register->last_name = $input['last_name'];
     	$register->reg_dob = $input['reg_dob'];
     	$register->mob_number = $input['mob_number'];
     	$register->gender = $input['gender'];
        $register->marital_status = $input['marital_status'];
      $register->patient_place = $input['patient_place'];
     	$register->patient_age = $input['patient_age'];
     	$register->final_due_amount = '0';
     	$register->status = '1';    
	     
	     if ($request->hasFile('emites_id_file')) {
            $image = $request->file('emites_id_file');
            $name = str_slug($request->emites_id_file).'.'.$image->getClientOriginalExtension();
            $destinationPath = public_path('uploads/emiratesid');
            $imagePath = $destinationPath. "/".  $name;
            $image->move($destinationPath, $name);
            $register->emites_id_file = $name;
          } 
        
        $flag = $register->save();

      	if ($flag) {
				Session::flash('message', 'success|Added successfully ');
				return redirect('/admin/reg_user');
			}
			else {
				Session::flash('message', 'danger|Failed, Try again');
				return redirect()->back();
		}
	}

    public function edit_details($id)
    {
        // return 1;
        $count=Register::count();
        $branches = Branches::all();
        $class = Units_carry::all();
        $register = Register::where('pk_int_reg_id', $id)->first();
        // $register = $count+1;
        return view('admin.register.edit_details',compact("register","branches","class"));

    }

    public function update(Request $request,$id)
    {
        // return 1;
        $input = $request->all();

        $register = Register::find($id);
        $register->fill($input);
            
        $register->file_no = $input['file_no'];
        $register->first_name = $input['first_name'];
        $register->middle_name = $input['middle_name'];
        $register->last_name = $input['last_name'];
        $register->reg_dob = $input['reg_dob'];
        $register->mob_number = $input['mob_number'];
        $register->patient_age = $input['patient_age'];
        $register->gender = $input['gender'];
        $register->patient_place = $input['patient_place'];
        $register->marital_status = $input['marital_status'];
        $register->status = '1';    
         
        $flag=$register->save();

        if ($flag) {
            Session::flash('message', 'success|Added successfully ');
            return redirect('/admin/reg_user');
        }
        else {
            Session::flash('message', 'danger|Failed, Try again');
            return redirect()->back();
        }
    }
}
