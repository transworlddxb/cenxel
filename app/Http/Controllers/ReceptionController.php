<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Mail;
use Snowfire\Beautymail\Beautymail;
use App\Mail\SendMailable;
use App\Users;
use App\User;
use App\Role;
use App\Register;
use App\Branches;
use App\Units_carry;
use App\Appointment;

use Validator;
use Flash;
use DataTables;
use DB;
use Auth;
use Session;
use Response;
use PDF;
use Carbon;
use \File;

class ReceptionController extends Controller
{
    public function index()
	{				
		// $role = Role::all();	
		// $register = Register::all();
		$customer=Register::where('status', 1)->count();
		$dt = Carbon::today()->toDateString();
		$apoointment=Appointment::where('appointment_date', $dt)->count();
		return view('admin.reception.receptionhome',compact('customer','apoointment'));
	}
}
