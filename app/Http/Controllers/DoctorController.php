<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Mail;
use Snowfire\Beautymail\Beautymail;
use App\Mail\SendMailable;
use App\Users;
use App\User;
use App\Role;
use App\Register;
use App\Branches;
use App\Doctor;
use App\Appointment;

use Validator;
use Flash;
use DataTables;
use DB;
use Auth;
use Session;
use Response;
use PDF;
use Carbon;
use \File;

class DoctorController extends Controller
{
    public function index()
	{				
		// $role = Role::all();	
		$doctor = Doctor::all();
		return view('admin.doctor.add_doctorlist',compact("doctor"));
	}
	public function home()
	{				
		$logged=Auth::user()->id;
		$dt = Carbon::today()->toDateString();
		// $role = Role::all();	
		$doctor = Doctor::all();
		$appointment = Appointment::where('fk_doctor',$logged)->where('appointment_date',$dt)->where('complete_status','0')->count();
		$appointment1 = Appointment::where('fk_doctor',$logged)->where('appointment_date',$dt)->where('complete_status','4')->count();
		return view('admin.doctor.home',compact("appointment","appointment1"));
	}

	public function store(Request $request)
	{ 
		// return 1;
		$input=$request->all();
		// dd($input);die();
			$doctor = new Doctor();

			$doctor->doctor_name = $input['doctor_name'];
			$doctor->status = '1';

			$flag = $doctor->save();            

			if ($flag) {
				Session::flash('message', 'success|Added successfully ');
				return redirect()->back();
			}
			else {
				Session::flash('message', 'danger|Failed, Try again');
				return redirect()->back();
			}
	}
	public function edit(Request $request,$id)
	{
		$sms = Doctor::where('pk_doctor_id', $id)->first();
		// print_r($sms); die();
				// $sms->sgrp_name = $input['sgrp_name'];
		$sg1 = $sms->pk_doctor_id.','.$sms->doctor_name;	
		echo $sg1;

	}
	public function update(Request $request,$id)
	{
		$input = $request->all();
		$doctor =Doctor::find($input['pk_doctor_id']);
		$doctor->fill($input);
		$doctor->doctor_name = $input['doctor_name'];
		// $doctor->sgrp_pk_id = $input['user_id'];
		$flag=$doctor->save();

		if ($flag) {
				Session::flash('message', 'success|Updated successfully ');
				return redirect()->back();
			}
			else {
				Session::flash('message', 'danger|Failed, Try again');
				return redirect()->back();
			}
	}

	public function destroy($id)
	{

		$doctor = Doctor::find($id);
		if($doctor!==Null) {

			$flag = $doctor->delete();

			if ($flag) {
			Session::flash('message', 'success|Deleted successfully ');
			return redirect()->back();
			}
			else {
				Session::flash('message', 'danger|Failed, Try again');
				return redirect()->back();
			}
		}
	} 
}
