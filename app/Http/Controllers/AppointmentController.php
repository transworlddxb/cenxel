<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Mail;
use Snowfire\Beautymail\Beautymail;
use App\Mail\SendMailable;
use App\Users;
use App\User;
use App\Role;
use App\Register;
use App\Branches;
use App\Doctor;
use App\Units_carry;
use App\Appointment;
use App\Billing;
use App\Reference;
use App\Product;

use Validator;
use Flash;
use DataTables;
use DB;
use Auth;
use Session;
use Response;
use PDF;
use Carbon;
use \File;

class AppointmentController extends Controller
{
    public function index($id)
	{				
		// $role = Role::all();	
		$register = Register::where('pk_int_reg_id', $id)->first();
		$branches = Branches::all();
		$reference = Reference::all();
		$product = Product::all();
        $doctor = User::where('admin_role',4)->get();

		return view('admin.appointment.add_appointment',compact("register","branches","doctor","product","reference"));
	}
	public function today_list()
	{				
		// $role = Role::all();	
		// $register = Register::where('pk_int_reg_id', $id)->first();
        $date = Carbon::today()->toDateString();
        $logged=Auth::user()->id;
        $appointment = DB::table('tbl_appointment')
        ->where('appointment_date',$date)
        ->where('fk_doctor',$logged)
        ->join('tbl_registration', 'tbl_registration.pk_int_reg_id', '=', 'tbl_appointment.fk_customer_reg_id')
        ->join('users', 'users.id', '=', 'tbl_appointment.fk_doctor')
        ->orderBy('tbl_appointment.created_at', 'desc')
        ->select('*')
        ->get();
        $product = DB::table('tbl_product')
        ->select('*')
        ->get();

        // return 1;
		return view('admin.appointment.today_appointment',compact("appointment","branches","doctor","class","product"));
	}

    public function search_date_appo(Request $request)
    {               
        // $role = Role::all(); 
        // $register = Register::where('pk_int_reg_id', $id)->first();
        $input = $request->all();
        $date = $input['search_date'];
        $logged=Auth::user()->id;
        // $date = Carbon::today()->toDateString();

        $appointment = DB::table('tbl_appointment')
        ->join('tbl_registration', 'tbl_registration.pk_int_reg_id', '=', 'tbl_appointment.fk_customer_reg_id')
        ->join('users', 'users.id', '=', 'tbl_appointment.fk_doctor')
        ->where('appointment_date',$date)
        ->where('fk_doctor',$logged)
        ->orderBy('tbl_appointment.created_at', 'desc')
        ->select('*')
        ->get();

        $product = DB::table('tbl_product')
        ->select('*')
        ->get();

        // return 1;
        return view('admin.appointment.today_appointment',compact("appointment","branches","doctor","class","product"));
    }

    public function customer_consultation_list($id)
    {               
        // $role = Role::all(); 
        // $register = Register::where('pk_int_reg_id', $id)->first();
        $date = Carbon::today()->toDateString();
        $appointment = DB::table('tbl_appointment')
        ->where('fk_customer_reg_id',$id)
        ->join('tbl_registration', 'tbl_registration.pk_int_reg_id', '=', 'tbl_appointment.fk_customer_reg_id')
        ->join('users', 'users.id', '=', 'tbl_appointment.fk_doctor')
        ->orderBy('tbl_appointment.created_at', 'desc')
        ->select('*')
        ->get();
        $product = DB::table('tbl_product')
        ->select('*')
        ->get();
        $patient = DB::table('tbl_registration')
        ->where('pk_int_reg_id',$id)
        ->first();

        // return 1;
        return view('admin.appointment.patient_trt_list',compact("appointment","patient","doctor","class","product"));
    }

    public function add_other_deatils($id)
    {               
        // $role = Role::all(); 
        // $register = Register::where('pk_int_reg_id', $id)->first();
        $date = Carbon::today()->toDateString();
        $logged=Auth::user()->id;
        $appointment = DB::table('tbl_appointment')
        ->join('tbl_registration', 'tbl_registration.pk_int_reg_id', '=', 'tbl_appointment.fk_customer_reg_id')
        ->where('pk_int_appoint_id',$id)
        ->first();

        // return 1;
        return view('admin.appointment.other_details',compact("logged","appointment"));
    }

    public function add_other_deatils_edit($id)
    {               
        // $role = Role::all(); 
        // $register = Register::where('pk_int_reg_id', $id)->first();
        $date = Carbon::today()->toDateString();
        $logged=Auth::user()->id;
        $appointment = DB::table('tbl_appointment')
        ->join('tbl_registration', 'tbl_registration.pk_int_reg_id', '=', 'tbl_appointment.fk_customer_reg_id')
        ->join('tbl__consultdata', 'tbl__consultdata.appointment_id', '=', 'tbl_appointment.pk_int_appoint_id')
        ->where('pk_int_appoint_id',$id)
        ->first();

        // return 1;
        return view('admin.appointment.other_details_edit',compact("logged","appointment"));
    }

    public function view_full_report($id)
    {               
        // $role = Role::all(); 
        // $register = Register::where('pk_int_reg_id', $id)->first();
        $date = Carbon::today()->toDateString();
        $logged=Auth::user()->id;
        $appointment = DB::table('tbl_appointment')
        ->join('tbl_registration', 'tbl_registration.pk_int_reg_id', '=', 'tbl_appointment.fk_customer_reg_id')
        // ->join('tbl__consultdata', 'tbl__consultdata.appointment_id', '=', 'tbl_appointment.pk_int_appoint_id')
        ->where('pk_int_appoint_id',$id)
        ->first();

         $data = DB::table('tbl_appointment')
         ->join('users', 'users.id', '=', 'tbl_appointment.fk_doctor')
        ->where('fk_customer_reg_id',$appointment->pk_int_reg_id)
        ->select('*')
        ->get();
        $count = DB::table('tbl_appointment')
         ->join('users', 'users.id', '=', 'tbl_appointment.fk_doctor')
        ->where('fk_customer_reg_id',$appointment->pk_int_reg_id)
        ->count();
        $product = DB::table('tbl_product')
        ->select('*')
        ->get();

        // return 1;
        return view('admin.appointment.profile_customer',compact("logged","appointment","data","product","count"));
    }
    

    public function search_time(Request $request)
    {               
        // $role = Role::all(); 
        // $register = Register::where('pk_int_reg_id', $id)->first();
        $input = $request->all();
        $time = $input['search_time'];
        $branches = Branches::all();
        $doctor = Doctor::all();
        $class = Units_carry::all();
        $appointment = DB::table('tbl_appointment')
        ->join('tbl_registration', 'tbl_registration.pk_int_reg_id', '=', 'tbl_appointment.fk_customer_reg_id')
        ->join('users', 'users.id', '=', 'tbl_appointment.fk_doctor')
        ->where('appointment_time',$time)
        ->select('*')
        ->get();
        // return 1;
        return view('admin.appointment.today_appointment',compact("appointment","branches","doctor","class"));
    }


	public function store(Request $request,$id)
  	{
  		// return 1;
        $logged=Auth::user()->id;
  		$input = $request->all();   
        $appointment = new Appointment();  
        $appointment->fk_customer_reg_id = $id;
     	$appointment->fk_doctor = $logged;
     	$appointment->appointment_date = $input['appointment_date'];
     	$appointment->appointment_time = $input['appointment_time'];
     	$appointment->appoint_patient = $input['appoint_patient'];
     	$appointment->payment_status = '0';
        $appointment->complete_status = '1';

        $product_one = $input['product_one'];
        $product_two = $input['product_two'];
        $product_three = $input['product_three'];
        $product_four = $input['product_four'];
        $product_five = $input['product_five'];
        $product_six = $input['product_six'];

        if($product_one!=null){
            $amount_one = DB::table('tbl_product')
            ->where('product_id',$product_one)
            ->first();
            $product_one_amount = $amount_one->prod_price;
        }
        else{
            $product_one_amount = 0;
        }
        if($product_two!=null){
            $amount_two = DB::table('tbl_product')
            ->where('product_id',$product_two)
            ->first();
            $product_two_amount = $amount_two->prod_price;
        }
        else{
            $product_two_amount = 0;
        }
        if($product_three!=null){
            $amount_three = DB::table('tbl_product')
            ->where('product_id',$product_three)
            ->first();
            $product_three_amount = $amount_three->prod_price;
        }
        else{
            $product_three_amount = 0;
        }
        if($product_four!=null){
            $amount_four = DB::table('tbl_product')
            ->where('product_id',$product_four)
            ->first();
            $product_four_amount = $amount_four->prod_price;
        }
        else{
            $product_four_amount = 0;
        }
        if($product_five!=null){
            $amount_five = DB::table('tbl_product')
            ->where('product_id',$product_five)
            ->first();
            $product_five_amount = $amount_five->prod_price;
        }
        else{
            $product_five_amount = 0;
        }
        if($product_six!=null){
            $amount_six = DB::table('tbl_product')
            ->where('product_id',$product_six)
            ->first();
            $product_six_amount = $amount_six->prod_price;
        }
        else{
            $product_six_amount = 0;
        }
        

        $prod_amount=$product_one_amount+$product_two_amount+$product_three_amount+$product_four_amount+$product_five_amount+$product_six_amount;

        $appointment->prod_total_amount = $prod_amount;

        if($product_one!=null){
            $appointment->prod_purch_status = '1';
        }
        else{
            $appointment->prod_purch_status = '0';
        }
        
     	$appointment->consult_status = '1';
        $getdate = $input['appointment_date'];
        $appointment->consult_description = $input['consult_description'];
        $appointment->product_one = $input['product_one'];
        $appointment->product_two = $input['product_two'];
        $appointment->product_three = $input['product_three'];
        $appointment->product_four = $input['product_four'];
        $appointment->product_five = $input['product_five'];
        $appointment->product_six = $input['product_six'];
        $appointment->product_one_count = $input['product_count_one'];
        $appointment->product_two_count = $input['product_count_two'];
        $appointment->product_three_count = $input['product_count_three'];
        $appointment->product_four_count = $input['product_count_four'];
        $appointment->product_five_count = $input['product_count_five'];
        $appointment->product_six_count = $input['product_count_six'];
        
        $d = date_parse_from_format("Y-m-d", $getdate);
        $month = $d["month"];
        $month_val = str_pad($month,2,'0',STR_PAD_LEFT);
        // dd($month);die();
        $billcount = DB::table('tbl_appointment')
        ->whereMonth('appointment_date',$month)
        ->count();

        $in_nm = $billcount+1;
        $invoice_number = str_pad($in_nm,4,'0',STR_PAD_LEFT);

        $bill_inv = ['DP-',$month_val,'-',$invoice_number];
        $inv_bill = implode($bill_inv);
        $appointment->inv_nmbr = $inv_bill;
        // dd($inv_bill);die();

        $flag = $appointment->save();

      	if ($flag) {
				Session::flash('message', 'success|Added successfully ');
				return redirect('/admin/today_appointment');
			}
			else {
				Session::flash('message', 'danger|Failed, Try again');
				return redirect()->back();
		}
	}
    public function billing($id)
    {               
        // $role = Role::all(); 
        // $bill=Billing::count();
        // $billcount = $bill+1;
        $date = Carbon::today()->toDateString();
        $branches = Branches::all();
        $doctor = Doctor::all();
        $file = DB::table('tbl_registration')
        ->join('tbl_appointment', 'tbl_appointment.fk_customer_reg_id', '=', 'tbl_registration.pk_int_reg_id')
        ->where('pk_int_appoint_id',$id)
        ->first();
        // dd($file);die();

        $cunsultdata = DB::table('tbl__consultdata')
        ->join('tbl_appointment', 'tbl_appointment.pk_int_appoint_id', '=', 'tbl__consultdata.appointment_id')
        ->join('users', 'users.id', '=', 'tbl__consultdata.doctor_id')
        // ->join('tbl__billing', 'tbl__billing.fk_bill_appointment_id', '=', 'tbl__consultdata.appointment_id')
        ->join('tbl_registration', 'tbl_registration.pk_int_reg_id', '=', 'tbl__consultdata.customer_id')
        ->join('tbl_product_purchased', 'tbl_product_purchased.purch_appointment_id', '=', 'tbl__consultdata.appointment_id')
        ->where('appointment_id',$id)
        ->first();

        $appointment_trt_data = DB::table('tbl_appointment')
        ->where('pk_int_appoint_id',$id)
        ->first();

        $billcount = DB::table('tbl_appointment')
        ->where('pk_int_appoint_id',$id)
        ->pluck('pk_int_appoint_id')
        ->first();
        // dd($appointment_trt_data->trt_one_amount);die();

        // dd($cunsultdata);die();
        // $one=$cunsultdata->treatment_one;
        // $two=$cunsultdata->treatment_two;
        // $three=$cunsultdata->treatment_three;
        // $four=$cunsultdata->treatment_four;
        // $five=$cunsultdata->treatment_five;

        $prod_one=$cunsultdata->product_one;
        $prod_two=$cunsultdata->product_two;
        $prod_three=$cunsultdata->product_three;
        $prod_four=$cunsultdata->product_four;
        $prod_five=$cunsultdata->product_five;
        // dd($prod_two);die();

        $product_one = DB::table('tbl_product')
        ->join('tbl_product_purchased', 'tbl_product_purchased.product_one', '=', 'tbl_product.product_id')
        ->where('purch_appointment_id',$id)
        ->first();
        $product_two = DB::table('tbl_product')
        ->join('tbl_product_purchased', 'tbl_product_purchased.product_two', '=', 'tbl_product.product_id')
        ->where('purch_appointment_id',$id)
        ->first();
        $product_three = DB::table('tbl_product')
        ->join('tbl_product_purchased', 'tbl_product_purchased.product_three', '=', 'tbl_product.product_id')
        ->where('purch_appointment_id',$id)
        ->first();
        $product_four = DB::table('tbl_product')
        ->join('tbl_product_purchased', 'tbl_product_purchased.product_four', '=', 'tbl_product.product_id')
        ->where('purch_appointment_id',$id)
        ->first();
        $product_five = DB::table('tbl_product')
        ->join('tbl_product_purchased', 'tbl_product_purchased.product_five', '=', 'tbl_product.product_id')
        ->where('purch_appointment_id',$id)
        ->first();

        if($prod_one==Null){
            $prod_amount_one=0;
        }else{
            $prod_amount_one=$product_one->prod_price;
        }
        if($prod_two==Null){
            $prod_amount_two=0;
        }else{
            $prod_amount_two=$product_two->prod_price;
        }
        if($prod_three==Null){
            $prod_amount_three=0;
        }else{
            $prod_amount_three=$product_three->prod_price;
        }
        if($prod_four==Null){
            $prod_amount_four=0;
        }else{
            $prod_amount_four=$product_four->prod_price;
        }
        if($prod_five==Null){
            $prod_amount_five=0;
        }else{
            $prod_amount_five=$product_five->prod_price;
        }

        $total_purch_price=$prod_amount_one+$prod_amount_two+$prod_amount_three+$prod_amount_four+$prod_amount_five;

        $bills = DB::table('tbl__billing')
        ->join('tbl_appointment', 'tbl_appointment.fk_customer_reg_id', '=', 'tbl__billing.costumer_id')
        ->where('fk_bill_appointment_id',$id)
        ->first();
        // dd($cunsultdata->treatment_one);die();

        $treatment_one = DB::table('treatment')
        ->join('tbl__consultdata', 'tbl__consultdata.treatment_one', '=', 'treatment.pk_treatment_id')
        ->where('appointment_id',$id)
        ->first();
        // dd($treatment_one);die();
        $treatment_two = DB::table('treatment')
        ->join('tbl__consultdata', 'tbl__consultdata.treatment_two', '=', 'treatment.pk_treatment_id')
        ->where('appointment_id',$id)
        ->first();
        $treatment_three = DB::table('treatment')
        ->join('tbl__consultdata', 'tbl__consultdata.treatment_three', '=', 'treatment.pk_treatment_id')
        ->where('appointment_id',$id)
        ->first();
        $treatment_four = DB::table('treatment')
        ->join('tbl__consultdata', 'tbl__consultdata.treatment_four', '=', 'treatment.pk_treatment_id')
        ->where('appointment_id',$id)
        ->first();
        $treatment_five = DB::table('treatment')
        ->join('tbl__consultdata', 'tbl__consultdata.treatment_five', '=', 'treatment.pk_treatment_id')
        ->where('appointment_id',$id)
        ->first();
        // if($cunsultdata->approval_status==1){
        // $discount = $cunsultdata->discount_amount;
        // }
        // else{
        // $discount = 0;
        // }
        $due = $cunsultdata->final_due_amount;
        //amount
    
            $amount_one=$appointment_trt_data->trt_one_amount;
            $amount_two=$appointment_trt_data->trt_two_amount;
            $amount_three=$appointment_trt_data->trt_three_amount;
            $amount_four=$appointment_trt_data->trt_four_amount;
            $amount_five=$appointment_trt_data->trt_five_amount;

        $amount=$amount_one+$amount_two+$amount_three+$amount_four+$amount_five;

        $percent = $file->percent_insurance;
        // dd($file->percent_insurance);die();

        if($file->insurance_status==1){
            $total_amount=(($percent*$amount)/100)+$total_purch_price;
            $vat_amount=(5*($amount))/100;
            $grand_total=$vat_amount+$total_amount;
            // dd($total_amount);die();
        }
        if($file->insurance_status==0){
            $total=$amount_one+$amount_two+$amount_three+$amount_four+$amount_five;
            $total_amount=($total)+$cunsultdata->final_due_amount+$total_purch_price;
            // dd($total_amount);die();
            $vat_amount=(5*($total))/100;
            $grand_total=$vat_amount+$total_amount;
        }

        // return 1;
        return view('admin.billing.billing',compact("cunsultdata","branches","doctor","file","date","billcount","treatment_one","treatment_two","treatment_three","treatment_four","treatment_five","amount","total_amount","discount","due","prod_one","prod_two","prod_three","prod_four","prod_five","product_one","product_two","product_three","product_four","product_five","total_purch_price","vat_amount","grand_total","amount_one","amount_two","amount_three","amount_four","amount_five","appointment_trt_data"));
    }
    public function billPDF(Request $request,$id)
    {
        // return 1;
        $usr = Appointment::where('pk_int_appoint_id','=',$id)->update(['complete_status' => '4']);
         $bill=Billing::count();
        $billcount = $bill+1;
        $date = Carbon::today()->toDateString();
        $branches = Branches::all();
        $doctor = Doctor::all();
        $file = DB::table('tbl_registration')
        ->join('tbl_appointment', 'tbl_appointment.fk_customer_reg_id', '=', 'tbl_registration.pk_int_reg_id')
        ->where('pk_int_appoint_id',$id)
        ->first();
        $cunsultdata = DB::table('tbl__consultdata')
        ->join('tbl_appointment', 'tbl_appointment.fk_customer_reg_id', '=', 'tbl__consultdata.customer_id')
        ->join('users', 'users.id', '=', 'tbl__consultdata.doctor_id')
        ->join('tbl__billing', 'tbl__billing.fk_bill_appointment_id', '=', 'tbl__consultdata.appointment_id')
        ->join('tbl_registration', 'tbl_registration.pk_int_reg_id', '=', 'tbl__consultdata.customer_id')
        ->join('tbl_product_purchased', 'tbl_product_purchased.purch_appointment_id', '=', 'tbl__consultdata.appointment_id')
        // ->join('treatment', 'treatment.pk_treatment_id', '=', 'tbl__consultdata.treatment_one')
        ->where('pk_int_appoint_id',$id)
        ->where('fk_bill_appointment_id',$id)
        ->first();

        $one=$cunsultdata->treatment_one;
        $two=$cunsultdata->treatment_two;
        $three=$cunsultdata->treatment_three;
        $four=$cunsultdata->treatment_four;
        $five=$cunsultdata->treatment_five;
        

        $prod_one=$cunsultdata->product_one;
        $prod_two=$cunsultdata->product_two;
        $prod_three=$cunsultdata->product_three;
        $prod_four=$cunsultdata->product_four;
        $prod_five=$cunsultdata->product_five;
        // dd($prod_two);die();

        $product_one = DB::table('tbl_product')
        ->join('tbl_product_purchased', 'tbl_product_purchased.product_one', '=', 'tbl_product.product_id')
        ->first();
        $product_two = DB::table('tbl_product')
        ->join('tbl_product_purchased', 'tbl_product_purchased.product_two', '=', 'tbl_product.product_id')
        ->first();
        $product_three = DB::table('tbl_product')
        ->join('tbl_product_purchased', 'tbl_product_purchased.product_three', '=', 'tbl_product.product_id')
        ->first();
        $product_four = DB::table('tbl_product')
        ->join('tbl_product_purchased', 'tbl_product_purchased.product_four', '=', 'tbl_product.product_id')
        ->first();
        $product_five = DB::table('tbl_product')
        ->join('tbl_product_purchased', 'tbl_product_purchased.product_five', '=', 'tbl_product.product_id')
        ->first();

        if($prod_one==Null){
            $prod_amount_one=0;
        }else{
            $prod_amount_one=$product_one->prod_price;
        }
        if($prod_two==Null){
            $prod_amount_two=0;
        }else{
            $prod_amount_two=$product_two->prod_price;
        }
        if($prod_three==Null){
            $prod_amount_three=0;
        }else{
            $prod_amount_three=$product_three->prod_price;
        }
        if($prod_four==Null){
            $prod_amount_four=0;
        }else{
            $prod_amount_four=$product_four->prod_price;
        }
        if($prod_five==Null){
            $prod_amount_five=0;
        }else{
            $prod_amount_five=$product_five->prod_price;
        }

        $total_purch_price=$prod_amount_one+$prod_amount_two+$prod_amount_three+$prod_amount_four+$prod_amount_five;

        $treatment_one = DB::table('treatment')
        ->join('tbl__consultdata', 'tbl__consultdata.treatment_one', '=', 'treatment.pk_treatment_id')
        ->first();
        $treatment_two = DB::table('treatment')
        ->join('tbl__consultdata', 'tbl__consultdata.treatment_two', '=', 'treatment.pk_treatment_id')
        ->first();
        $treatment_three = DB::table('treatment')
        ->join('tbl__consultdata', 'tbl__consultdata.treatment_three', '=', 'treatment.pk_treatment_id')
        ->first();
        $treatment_four = DB::table('treatment')
        ->join('tbl__consultdata', 'tbl__consultdata.treatment_four', '=', 'treatment.pk_treatment_id')
        ->first();
        $treatment_five = DB::table('treatment')
        ->join('tbl__consultdata', 'tbl__consultdata.treatment_five', '=', 'treatment.pk_treatment_id')
        ->first();

        $discount = $cunsultdata->discount_amount;
        $due = $cunsultdata->final_due_amount;

        //amount
        if($one==Null){
            $amount_one=0;
        }else{
            $amount_one=$treatment_one->treatment_price;
        }
        if($two==Null){
            $amount_two=0;
        }else{
            $amount_two=$treatment_two->treatment_price;
        }
        if($three==Null){
            $amount_three=0;
        }else{
            $amount_three=$treatment_three->treatment_price;
        }
        if($four==Null){
            $amount_four=0;
        }else{
            $amount_four=$treatment_four->treatment_price;
        }
        if($five==Null){
            $amount_five=0;
        }else{
            $amount_five=$treatment_five->treatment_price;
        }

         $amount=$amount_one+$amount_two+$amount_three+$amount_four+$amount_five;

        $percent = $file->percent_insurance;
        // dd($file->percent_insurance);die();

        if($file->insurance_status==1){
            $total_amount=(($percent*$amount)/100)+$total_purch_price;
            $vat_amount=(5*$amount)/100;
            $grand_total=$vat_amount+$total_amount;
            // dd($total_amount);die();
        }
        if($file->insurance_status==0){
            $total=$amount_one+$amount_two+$amount_three+$amount_four+$amount_five;
            $total_amount=($total-$cunsultdata->discount_amount)+$cunsultdata->final_due_amount+$total_purch_price;

            $vat_amount=(5*$total)/100;
            $grand_total=$vat_amount+$total_amount;
            // dd($total_amount);die();
        }
        $pdf = PDF::loadView('admin.billing.billingpdf',compact("cunsultdata","branches","doctor","file","date","billcount","treatment_one","treatment_two","treatment_three","treatment_four","treatment_five","amount","total_amount","discount","due","one","two","three","four","five","prod_one","prod_two","prod_three","prod_four","prod_five","product_one","product_two","product_three","product_four","product_five","total_purch_price","grand_total","vat_amount"));
        return $pdf->download('Consulting_Bill.pdf');

    }
    public function statementPDF()
    {
        return 1;
        
        
        $input = $request->all();
        $payed_amounts = $input['payed_amounts'];


        $bill=Billing::count();
        $billcount = $bill+1;
        $date = Carbon::today()->toDateString();
        $branches = Branches::all();
        $doctor = Doctor::all();
        $file = DB::table('tbl_registration')
        ->join('tbl_appointment', 'tbl_appointment.fk_customer_reg_id', '=', 'tbl_registration.pk_int_reg_id')
        ->where('pk_int_appoint_id',$id)
        ->first();
        $cunsultdata = DB::table('tbl__consultdata')
        ->join('tbl_appointment', 'tbl_appointment.fk_customer_reg_id', '=', 'tbl__consultdata.customer_id')
        ->join('users', 'users.id', '=', 'tbl__consultdata.doctor_id')
        ->join('tbl__billing', 'tbl__billing.fk_bill_appointment_id', '=', 'tbl__consultdata.appointment_id')
        ->join('tbl_registration', 'tbl_registration.pk_int_reg_id', '=', 'tbl__consultdata.customer_id')
        // ->join('treatment', 'treatment.pk_treatment_id', '=', 'tbl__consultdata.treatment_one')
        ->where('pk_int_appoint_id',$id)
        ->where('fk_bill_appointment_id',$id)
        ->first();

        $treatment_one = DB::table('treatment')
        ->join('tbl__consultdata', 'tbl__consultdata.treatment_one', '=', 'treatment.pk_treatment_id')
        ->first();
        $treatment_two = DB::table('treatment')
        ->join('tbl__consultdata', 'tbl__consultdata.treatment_two', '=', 'treatment.pk_treatment_id')
        ->first();
        $treatment_three = DB::table('treatment')
        ->join('tbl__consultdata', 'tbl__consultdata.treatment_three', '=', 'treatment.pk_treatment_id')
        ->first();
        $treatment_four = DB::table('treatment')
        ->join('tbl__consultdata', 'tbl__consultdata.treatment_four', '=', 'treatment.pk_treatment_id')
        ->first();
        $treatment_five = DB::table('treatment')
        ->join('tbl__consultdata', 'tbl__consultdata.treatment_five', '=', 'treatment.pk_treatment_id')
        ->first();

        $discount = $cunsultdata->discount_amount;
        $due = $cunsultdata->final_due_amount;

        //amount
        if($cunsultdata->treatment_one==Null){
            $amount_one=0;
        }else{
            $amount_one=$treatment_one->treatment_price;
        }
        if($cunsultdata->treatment_two==Null){
            $amount_two=0;
        }else{
            $amount_two=$treatment_two->treatment_price;
        }
        if($cunsultdata->treatment_three==Null){
            $amount_three=0;
        }else{
            $amount_three=$treatment_three->treatment_price;
        }
        if($cunsultdata->treatment_four==Null){
            $amount_four=0;
        }else{
            $amount_four=$treatment_four->treatment_price;
        }
        if($cunsultdata->treatment_five==Null){
            $amount_five=0;
        }else{
            $amount_five=$treatment_five->treatment_price;
        }

         $amount=$amount_one+$amount_two+$amount_three+$amount_four+$amount_five;

        $percent = $file->percent_insurance;
        // dd($file->percent_insurance);die();

        if($file->insurance_status==1){
            $total_amount=($percent*$amount)/100;
            // dd($total_amount);die();
        }
        if($file->insurance_status==0){
            $total=$amount_one+$amount_two+$amount_three+$amount_four+$amount_five;
            $total_amount=($total-$cunsultdata->discount_amount)+$cunsultdata->final_due_amount;
            // dd($total_amount);die();
        }
        $pdf = PDF::loadView('admin.billing.billingpdf',compact("cunsultdata","branches","doctor","file","date","billcount","treatment_one","treatment_two","treatment_three","treatment_four","treatment_five","amount","total_amount","discount","due"));
        return $pdf->download('Consulting_Bill.pdf');

    }
    public function approval_list()
    {               
        $date = Carbon::today()->toDateString();
        $logged=Auth::user()->id;
        $branches = Branches::all();
        $doctor = Doctor::all();
        $class = Units_carry::all();
        $appointment = DB::table('tbl_appointment')
        ->join('tbl_registration', 'tbl_registration.pk_int_reg_id', '=', 'tbl_appointment.fk_customer_reg_id')
        ->join('users', 'users.id', '=', 'tbl_appointment.fk_doctor')
        // ->where('appointment_date',$date)
        ->where('complete_status','11')
        ->select('*')
        ->get();
        // dd($appointment);die();
        return view('admin.appointment.approval_list',compact("appointment","branches","doctor","class"));
    }

    public function approval_remrks($id)
    {
        // $sms = Consultdata::where('pk_doctor_id', $id)->first();
        $cunsultdata = DB::table('tbl__consultdata')
        ->join('tbl_appointment', 'tbl_appointment.pk_int_appoint_id', '=', 'tbl__consultdata.appointment_id')
        ->join('users', 'users.id', '=', 'tbl__consultdata.doctor_id')
        ->join('tbl_registration', 'tbl_registration.pk_int_reg_id', '=', 'tbl__consultdata.customer_id')
        ->join('tbl__billing', 'tbl__billing.fk_bill_appointment_id', '=', 'tbl__consultdata.appointment_id')
        ->where('appointment_id',$id)
        ->first();
        // print_r($sms); die();
                // $sms->sgrp_name = $input['sgrp_name'];
        $sg1 = $cunsultdata->discount_amount;    
        echo $sg1;

    }
    public function update_approval(Request $request,$id)
    {   
        
        $input = $request->all();
        $billing = new Billing();
        $dates = Carbon::today()->toDateString();

        $discount_amount = $input['discount_amount'];
        $approval_status = $input['approval_status'];

        if ($approval_status==1) {
            $usr = Billing::where('fk_bill_appointment_id','=',$id)->update(['approval_status' => '1']);
            $usr = Billing::where('fk_bill_appointment_id','=',$id)->update(['discount_amount' => $discount_amount]);
            $usrs = Appointment::where('pk_int_appoint_id','=',$id)->update(['complete_status' => '2']);
        }else{
            $usr = Billing::where('fk_bill_appointment_id','=',$id)->update(['approval_status' => '0']);
            $usr = Billing::where('fk_bill_appointment_id','=',$id)->update(['discount_amount' => $discount_amount]);
            $usrs = Appointment::where('pk_int_appoint_id','=',$id)->update(['complete_status' => '2']);
        }

            
                Session::flash('message', 'success|Updated successfully ');
                return redirect()->back();

    }

    // public function testbill_pdf(Request $request)
    // {
    //     $pdf = PDF::loadView('admin.billing.testbillingpdf');
    //     return $pdf->download('Consulting_Bill.pdf');
    // }
    

}
