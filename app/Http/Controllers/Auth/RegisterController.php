<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Flash;
use DataTables;
use DB;
use Auth;
use Session;
use Hash;
class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */

     public function register(Request $request)
   {
    // return 1;
       try
      {

            
               if(strcmp($request->get('password'), $request->get('confirm-password')) == 0){
                $input = $request->all();
               $user = new User();
               $user->password = bcrypt($request->get('password'));
               $user->email=$input['email'];
               $user->name=$input['name'];
               $user->status=1;
               $flag=$user->save();
               if($flag)
               {
                // return 1;
                    // return response()->json(['msg'=>"Password changed successfully", 'status' => 'success']);
                    Flash::success('User Added successfully');
                    return redirect()->back()->withInput($input);
               }
               else
               {
                // return 2;
                    // return response()->json(['msg'=>"Something Went Wrong..", 'status' => 'fail']);
                    Flash::error('Something Went Wrong..');
                    return redirect()->back()->withInput($input);
               }


               }
           }
        catch (Exception $e) {

           
           return response()->json(['msg'=>$e->getMessage(), 'status' => 'fail']);
           
       }
   }


    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);
    }
}
