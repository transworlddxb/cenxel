<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use guard;
use Session;
use App\User;
use Auth;
use Lang;
use Hash;
use Redirect;
use Flash;


class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
        protected $redirectTo = 'admin/dashboard';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function login(Request $request)
    {
        // return 1;
        $this->validate($request, [
            'email' => 'required', 'password' => 'required',
        ]);

        $credentials = $request->only('email', 'password');
        $user = User::where('email', $credentials['email'])->first();

        if ($user && Hash::check($credentials['password'], $user->password)) {
            if (Auth::attempt(['email' => $credentials['email'], 'password' => $credentials['password']])) {
                if ($user->int_first_login == 1) {
                    // dd(1);
                    Flash::info("Please change your password");
                    return redirect('admin/changepassword');
                }
                switch ($user->admin_role) {
                    case '1':
                        // return Redirect::intended(route('admin.get_home'));
                    return redirect('/home');
                        break;

                    case '2':
                        return redirect('/admin/administrator_home');
                        break;    

                    case '3':
                        return redirect('/admin/receptionist_home');
                        break;

                    case '4':
                        return redirect('/admin/doctor_home');
                        break;    

                    case '5':
                        return redirect('/admin/account_home');
                        break;

                    case '3':
                        if (Session::get('loginRedirect')) {
                            $url = Session::get('loginRedirect');
                            Session::forget('loginRedirect');
                            return Redirect::intended(url($url));
                        }
                        return Redirect::intended(route('user.get_home'));
                        break;

                    default:
                        return redirect('/');
                        break;
                }
                // return Redirect::intended(route('admin.get_home'));
            }
        }
        return redirect('/login')
            ->withInput($request->only('email'))
            ->withErrors([
                'email' => $this->getFailedLoginMessage(),
            ]);
    }

    protected function getFailedLoginMessage()
    {
        return Lang::has('auth.failed')
            ? Lang::get('auth.failed')
            : 'These credentials do not match our records.';
    }

    /**
     * [logout description]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function logout(Request $request)
    {
        $this->guard()->logout();
        Session::flush();
        return back();
    }


}
