<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Mail;
use Snowfire\Beautymail\Beautymail;
use App\Mail\SendMailable;
use App\Menus;
use App\Role;
use App\Menu_category;
use App\User;

use Validator;
use Flash;
use DataTables;
use DB;
use Auth;
use Session;
use Response;

class RoleController extends Controller
{
	public function index()
	{	
		$role = Role::all()->sortByDesc('pk_int_role_id');
		return view('admin.superadmin.roles',compact("role","category","menu"));
	}
	
	public function store(Request $request)
	{ 
		$input=$request->all();
		
		$validator=validator::make($input, Role::$rule,Role::$message);
        // if validator passes
		if ($validator->passes()) {

			$role = new Role();

			$role->roll_name = $input['roll_name'];
			$role->roll_status = '1';

			$flag = $role->save();            

			if ($flag) {
				Session::flash('message', 'success|Added successfully ');
				return redirect()->back();
			}
			else {
				Session::flash('message', 'danger|Failed, Try again');
				return redirect()->back();
			}
		}
		else {
			Session::flash('error', 'Validation failed');
			Session::flash('alert-class', 'alert-danger');
			return redirect()->back()->withInput($input)->withErrors($validator->messages());
		}
	}

	public function destroy($id)
	{

		$role = Role::find($id);
		if($role!==Null) {

			$flag = $role->delete();

			if ($flag) {
			Session::flash('message', 'success|Deleted successfully ');
			return redirect()->back();
			}
			else {
				Session::flash('message', 'danger|Failed, Try again');
				return redirect()->back();
			}
		}
	} 

	public function edit(Request $request,$id)
	{
		$sms = Role::where('pk_int_role_id', $id)->first();
		// print_r($sms); die();
				// $sms->sgrp_name = $input['sgrp_name'];
		$sg1 = $sms->pk_int_role_id.','.$sms->roll_name;	
		echo $sg1;

	}

	public function update(Request $request,$id)
	{
		$input = $request->all();
		$role =Role::find($input['user_id']);
		// $role =Role::find($id);
		$role->fill($input);
		$role->roll_name = $input['roll_name'];
		$role->roll_status = '1';

		$flag=$role->save();

		if ($flag) {
			Session::flash('message', 'success|Updated successfully ');
			return redirect()->back();
		}
		else {
			Session::flash('message', 'danger|Failed, Try again');
			return redirect()->back();
		}
		
	}

}
