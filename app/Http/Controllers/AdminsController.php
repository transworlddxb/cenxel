<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Mail;
use Snowfire\Beautymail\Beautymail;
use App\Mail\SendMailable;
use App\Users;
use App\Role;
use App\Smsgroup;
use App\Menu_category;
use App\Role;
use App\Menu_category;
use App\Menus;

use Validator;
use Flash;
use DataTables;
use DB;
use Auth;
use Session;
use Response;

class AdminsController extends Controller
{
    public function index()
	{	
		$role = Role::all();
		$category = Menu_category::all();			
		$menu = Menus::all();

		$admins = DB::table('tbl_users')
		->where('users_role','1')
        ->join('tbl_role', 'tbl_role.pk_int_role_id', '=', 'tbl_users.users_role')
        ->join('tbl_sms_group', 'tbl_sms_group.sgrp_pk_id', '=', 'tbl_users.users_sms_group')
        ->select('*')
        ->get(); 
		return view('admin.superadmin.admins-view',compact("admins","role","category","menu"));
	}
	public function destroy($id)
	{

		$users = Users::find($id);
		if($users!==Null) {

			$flag = $users->delete();

			if ($flag) {
			Session::flash('message', 'success|Deleted successfully ');
			return redirect()->back();
			}
			else {
				Session::flash('message', 'danger|Failed, Try again');
				return redirect()->back();
			}
		} 
	}

	public function edit($id)
	{
		$smsgrp = Smsgroup::all();
		$role = Role::all();
		$user = Users::where('pk_int_users_id', $id)->first();
		return view('admin.superadmin.edit_users',compact("user","role","smsgrp"));

	}
}
