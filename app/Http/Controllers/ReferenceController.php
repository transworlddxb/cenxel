<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Mail;
use Snowfire\Beautymail\Beautymail;
use App\Mail\SendMailable;
use App\Users;
use App\User;
use App\Reference;

use DOMDocument;
use Validator;
use Flash;
use DataTables;
use DB;
use Auth;
use Session;
use Response;
use PDF;
use Carbon;
use \File;

class ReferenceController extends Controller
{
    public function index()
	{				
		$reference = Reference::all();	
		$file = DB::table('tbl_refrence_person')
        ->where('ref_status','1')
        ->select('*')
        ->get();
		// $roles_show = User::all();
		return view('admin.reference.reference',compact('reference'));
	}

	public function store(Request $request)
  	{
  		// return 1;
  		$input = $request->all();   
        $reference = new Reference();  
        $reference->reference_person_name = $input['reference_person_name'];
        $reference->ref_description = $input['ref_description'];
        $reference->ref_status = 1;
     	
        $flag = $reference->save();

      	if ($flag) {
				Session::flash('message', 'success|Added successfully ');
				return redirect()->back();
			}
			else {
				Session::flash('message', 'danger|Failed, Try again');
				return redirect()->back();
		}
	}

	public function destroy($id)
	{

		$reference = Reference::find($id);
		if($reference!==Null) {

			$flag = $reference->delete();

			if ($flag) {
				Session::flash('message', 'success|Deleted successfully ');
				return redirect()->back();
			}
			else {
				Session::flash('message', 'danger|Failed, Try again');
				return redirect()->back();
			}
		}
	}

	public function edit(Request $request,$id)
	{
		$sms = Reference::where('refernce_person_id', $id)->first();
		// print_r($sms); die();
				// $sms->sgrp_name = $input['sgrp_name'];
		$sg1 = $sms->refernce_person_id.','.$sms->reference_person_name.','.$sms->ref_description;	
		echo $sg1;

	}

	public function update(Request $request)
	{
		// return 1;
		$input = $request->all();

		$user =Reference::find($input['refernce_person_id']);
		// $role =Role::find($id);
		// dd($user);die();
		$user->fill($input);
		$user->reference_person_name = $input['reference_person_name'];
		$user->ref_description = $input['ref_description'];
		// $role->status = '1';

		$flag=$user->save();

		if ($flag) {
			Session::flash('message', 'success|Updated successfully ');
			return redirect()->back();
		}
		else {
			Session::flash('message', 'danger|Failed, Try again');
			return redirect()->back();
		}
		
	}
}
