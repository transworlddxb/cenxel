<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Mail;
use Snowfire\Beautymail\Beautymail;
use App\Mail\SendMailable;
use App\Users;
use App\User;
use App\Role;

use DOMDocument;
use Validator;
use Flash;
use DataTables;
use DB;
use Auth;
use Session;
use Response;
use PDF;
use Carbon;
use \File;

class UsersController extends Controller
{
	public function index()
	{				
		$role = Role::all();	
		$roles_show = User::all();
		return view('admin.superadmin.users',compact("users","role","roles_show","category","area","menu"));
	}

	public function show()
	{	
		$menu = Menus::all();
		$category = Menu_category::all();
		$smsgrp = Smsgroup::all();
		$role = Role::all();	
		$users = Users::all()->sortByDesc('pk_int_users_id');
		$roles_show = DB::table('users')
		->join('tbl_role', 'tbl_role.pk_int_role_id', '=', 'tbl_users.users_role')
		->select('*')
		->get(); 
        // $roles_show->role=$input['role'];
		return view('admin.superadmin.view_users',compact("users","role","smsgrp","roles_show","menu","category"));
	}

	public function store(Request $request)
	{ 
		// return 1;
		$input=$request->all();
		
		if (User::where('email', '=', Input::get('users_email'))->exists()) {
			   Session::flash('message', 'info|Email is already exist');
							return redirect()->back();
			}
		else {	
			$user = new User();

			// $users->users_date = $input['date'];
			$role= Role::where('pk_int_role_id', $input['users_role'])->first();
			// dd($role->roll_name);die();
			// $user->roll_name = $role->roll_name;
			$user->roll_name = $input['user_name'];
			$roll_name = $input['user_name'];
			$user->user_designation = $input['user_designation'];
			$emails =array($roll_name , '@doctorsportal.com');
			$user->email = implode( "", $emails );
			$email = implode( "", $emails );
			// dd($email);die();

			$user->password = bcrypt($input['users_password']);
			$user->pass = $input['users_password'];
			$user->admin_role = $role->pk_int_role_id;
			$user->status = '1';
			$flag = $user->save();
			
		}
			// $data = [
   //                  'title'=> 'info@alqasercosme.com', 
   //                  'name'=>$role->roll_name,
   //                  'designation'=>$input['user_designation'],
   //                  'email'=>$email,
   //                  'password'=>$input['users_password'],
   //              ];

   //              $beautymail = app()->make(\Snowfire\Beautymail\Beautymail::class);
   //              $beautymail->send('email.user_email',$data, function($message) use  ($data)
   //              {
                	
   //                  $email = 'tpidxbit@gmail.com';
   //                  $message
   //                  ->from('tpidxbit@gmail.com')
   //                  ->to('tpidxbit@gmail.com')
   //                  ->subject('Clinic software credentials of user');
   //              });               

			if ($flag) {
				Session::flash('message', 'success|Added successfully ');
				return redirect()->back();
			}
			else {
				Session::flash('message', 'danger|Failed, Try again');
				return redirect()->back();
			}
		}


	public function destroy($id)
	{

		$gst = User::find($id);
		if($gst!==Null) {

			$flag = $gst->delete();

			if ($flag) {
				Session::flash('message', 'success|Deleted successfully ');
				return redirect()->back();
			}
			else {
				Session::flash('message', 'danger|Failed, Try again');
				return redirect()->back();
			}
		}
	} 

	public function edit(Request $request,$id)
	{
		$sms = User::where('admin_role', $id)->first();
		// print_r($sms); die();
				// $sms->sgrp_name = $input['sgrp_name'];
		$sg1 = $sms->id.','.$sms->email;	
		echo $sg1;

	}

	
	public function update(Request $request,$id)
	{
		// return 1;
		$input = $request->all();

		$user =User::find($input['user_id']);
		// $role =Role::find($id);
		// dd($user);die();
		$user->fill($input);
		$user->roll_name = $input['roll_name'];
		// $role->status = '1';

		$flag=$user->save();

		if ($flag) {
			Session::flash('message', 'success|Updated successfully ');
			return redirect()->back();
		}
		else {
			Session::flash('message', 'danger|Failed, Try again');
			return redirect()->back();
		}
		
	}
	public function list_view (Request $request)
	{	
		$menu = Menus::all();
		$category = Menu_category::all();
		$input = $request->all();
		$role_id = $input['role'];
		$smsgrp = Smsgroup::all();
		$role = Role::all();	
		// $users = Users::all()->sortByDesc('pk_int_users_id');
		$roles_show = DB::table('tbl_users')
		->where('users_role',$role_id)
		->join('tbl_role', 'tbl_role.pk_int_role_id', '=', 'tbl_users.users_role')
		->join('tbl_sms_group', 'tbl_sms_group.sgrp_pk_id', '=', 'tbl_users.users_sms_group')
		->join('tbl_areas', 'tbl_areas.pk_int_area_id', '=', 'tbl_users.users_area_fk')
		->select('*')
		->get(); 
		return view('admin.superadmin.view_users',compact("role","smsgrp","roles_show","menu","category"));
	}

	public function download_user_pdf(Request $request){

		$input = $request->all();
		$user = $input['user'];

		$role = DB::table('tbl_role')
        ->where('pk_int_role_id',$user)
        // ->join('tbl_bottle_no', 'tbl_bottle_no.botno_user', '=', 'tbl_role.pk_int_role_id')
        ->pluck('roll_name')->first();

        $users = DB::table('tbl_users')
        ->where('users_role',$user)
        ->join('tbl_sms_group', 'tbl_sms_group.sgrp_pk_id', '=', 'tbl_users.users_sms_group')
        ->join('tbl_areas', 'tbl_areas.pk_int_area_id', '=', 'tbl_users.users_area_fk')
        ->select('*')
        ->get();
        // dd($users);die();
        
      	$pdf = PDF::loadView('admin.superadmin.pdf_user', compact("users","role"));
      	return $pdf->download('user_list.pdf');

    }

}
