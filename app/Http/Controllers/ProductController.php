<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Mail;
use Snowfire\Beautymail\Beautymail;
use App\Mail\SendMailable;
use App\Users;
use App\User;
use App\Role;
use App\Register;
use App\Branches;
use App\Doctor;
use App\Units_carry;
use App\Appointment;
use App\Billing;
use App\Product;
use App\Purchased;

use Validator;
use Flash;
use DataTables;
use DB;
use Auth;
use Session;
use Response;
use PDF;
use Carbon;
use \File;

class ProductController extends Controller
{
    public function index()
	{				
		// $role = Role::all();	
		// $register = Register::where('pk_int_reg_id', $id)->first();
		$branches = Branches::all();
		// $doctor = Doctor::all();
		$class = Units_carry::all();
        // $doctor = User::where('admin_role',4)->get();
        $product = DB::table('tbl_product')
        // ->join('tbl_units_carrying', 'tbl_units_carrying.class_id', '=', 'tbl_registration.int_reg_category')
        ->select('*')
        ->get();
        // return 1;
		return view('admin.product.add_product',compact("product"));
	}
	public function add_product(Request $request)
	{ 
		// return 1;
		$input=$request->all();
		// dd($input);die();
			$prod = new Product();

			$prod->prod_name = $input['prod_name'];
			$prod->prod_price = $input['prod_price'];
			$prod->prod_count = $input['prod_count'];
			$prod->status = '1';

			$flag = $prod->save();            

			if ($flag) {
				Session::flash('message', 'success|Added successfully ');
				return redirect()->back();
			}
			else {
				Session::flash('message', 'danger|Failed, Try again');
				return redirect()->back();
			}
	}
	public function edit_product(Request $request,$id)
	{
		$sms = Product::where('product_id', $id)->first();
		// print_r($sms); die();
				// $sms->sgrp_name = $input['sgrp_name'];
		$sg1 = $sms->product_id.','.$sms->prod_name.','.$sms->prod_price.','.$sms->prod_count;
		echo $sg1;

	}
	public function update(Request $request,$id)
	{
		$input = $request->all();
		$prod =Product::find($input['product_id']);
		$prod->fill($input);
		$prod->prod_name = $input['prod_name'];
		$prod->prod_price = $input['prod_price'];
		$prod->prod_count = $input['prod_count'];
		$flag=$prod->save();

		if ($flag) {
				Session::flash('message', 'success|Updated successfully ');
				return redirect()->back();
			}
			else {
				Session::flash('message', 'danger|Failed, Try again');
				return redirect()->back();
			}
	}
	public function delete_product($id)
	{

		$prod = Product::find($id);
		if($prod!==Null) {

			$flag = $prod->delete();

			if ($flag) {
			Session::flash('message', 'success|Deleted successfully ');
			return redirect()->back();
			}
			else {
				Session::flash('message', 'danger|Failed, Try again');
				return redirect()->back();
			}
		}
	}
	public function prod_purch($id)
	{				
		// return 1;
		// $role = Role::all();	
		// $register = Register::where('pk_int_reg_id', $id)->first();
		$appoint_id=$id;
		// dd($appoint_id);die();
		$branches = Branches::all();
		// $doctor = Doctor::all();
		$class = Units_carry::all();
        // $doctor = User::where('admin_role',4)->get();
        $product = DB::table('tbl_product')
        ->select('*')
        ->get();
        // return 1;
		return view('admin.product.purchase',compact("product","appoint_id"));
	}
	public function purchased_prod(Request $request,$id)
	{ 
		// return 1;

		$date = Carbon::today()->toDateString();
		$input=$request->all();
		// dd($input);die();
			$prod = new Purchased();

			$prod->purch_appointment_id = $id;
			
			$prod->product_one = $input['product_one'];
			$product_one = $input['product_one'];

			$prod->product_two = $input['product_two'];
			$product_two = $input['product_two'];

			$prod->product_three = $input['product_three'];
			$product_three = $input['product_three'];

			$prod->product_four = $input['product_four'];
			$product_four = $input['product_four'];

			$prod->product_five = $input['product_five'];
			$product_five = $input['product_five'];

			$prod->purchased_date = $date;
			$prod->purchased_status = $input['prod_status'];

			$prod1 = Product::where('product_id', $product_one)
        	->pluck('prod_price')
        	->first();
        	$prod2= Product::where('product_id', $product_two)
        	->pluck('prod_price')
        	->first();
        	$prod3= Product::where('product_id', $product_three)
        	->pluck('prod_price')
        	->first();
        	$prod4= Product::where('product_id', $product_four)
        	->pluck('prod_price')
        	->first();
        	$prod5= Product::where('product_id', $product_five)
        	->pluck('prod_price')
        	->first();

        	$amount=$prod1+$prod2+$prod3+$prod4+$prod5;
        	$prod->purchased_amount=$amount;

			$flag = $prod->save();            

			if($product_one){
			$prod_purch1 = Product::where('product_id', $product_one)
        	->pluck('prod_count')
        	->first();
        	$count1=$prod_purch1-1;
        	// dd($count);die();

        	$usr = Product::where('product_id','=',$product_one)->update(['prod_count' => $count1]);
			}

			if($product_two){
			$prod_purch2 = Product::where('product_id', $product_two)
        	->pluck('prod_count')
        	->first();
        	$count2=$prod_purch2-1;
        	// dd($count);die();

        	$usr = Product::where('product_id','=',$product_two)->update(['prod_count' => $count2]);
			}

			if($product_three){
			$prod_purch3 = Product::where('product_id', $product_three)
        	->pluck('prod_count')
        	->first();
        	$count3=$prod_purch3-1;
        	// dd($count);die();

        	$usr = Product::where('product_id','=',$product_three)->update(['prod_count' => $count3]);
			}

			if($product_four){
			$prod_purch4 = Product::where('product_id', $product_four)
        	->pluck('prod_count')
        	->first();
        	$count4=$prod_purch4-1;
        	// dd($count);die();

        	$usr = Product::where('product_id','=',$product_four)->update(['prod_count' => $count4]);
			}

			if($product_five){
			$prod_purch5 = Product::where('product_id', $product_five)
        	->pluck('prod_count')
        	->first();
        	$count5=$prod_purch5-1;
        	// dd($count);die();

        	$usr = Product::where('product_id','=',$product_five)->update(['prod_count' => $count5]);
			}


			if ($flag) {
				$usr = Appointment::where('pk_int_appoint_id','=',$id)->update(['complete_status' => '3']);
				Session::flash('message', 'success|Added successfully ');
				return redirect('/admin/today_appointment');
			}
			else {
				Session::flash('message', 'danger|Failed, Try again');
				return redirect()->back();
			}
	} 
	
	
}
