<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Mail;
use Snowfire\Beautymail\Beautymail;
use App\Mail\SendMailable;
use App\Units_carry;
use App\Role;
use Validator;
use Flash;
use DataTables;
use DB;
use Auth;
use Session;
use Response;

class ClassController extends Controller
{
   public function view()
	{	
		$role = Role::all();
		$units = Units_carry::all();	
		return view('admin.class.view_class',compact("units","role"));
	}

	public function edit_units(Request $request,$id)
	{
		$carry = Units_carry::where('class_id', $id)->first();
		// print_r($sms); die();
				// $sms->sgrp_name = $input['sgrp_name'];
		$sg1 = $carry->class_id.','.$carry->class_name;	
		echo $sg1;

	}
}
