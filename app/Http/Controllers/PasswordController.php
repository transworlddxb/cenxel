<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Menu_category;
use Validator;
use Flash;
use DataTables;
use DB;
use Auth;
use App\User;
use Session;
use Hash;

class PasswordController extends Controller
{
    public function index()
    {
        // $category = Menu_category::all();
        $users = User::Find(Auth::user()->id);
        $email = $users->email; 
        return view('admin.password-change',compact('email',"category"));
    }

    public function changePassword(Request $request)
   {
    // return 1;
       try
      {

            $input = $request->all();
            // dd($input);die();
           

           $validator =  Validator::make($input, User::$userPasswordRule);
           if ($validator->fails()) {
                 
                   // return redirect()->back()->withInput()->withErrors($validator->messages());
           
  			Flash::error('Whoops! Form validation failed');
            return redirect()->back()->withInput($input)->withErrors($validator->messages());
                   
           }

           else
           {
               $user=User::find(Auth::user()->id);

               if (!(Hash::check($request->get('current-password'), Auth::user()->password))) {
                   // The passwords matches
             // return response()->json(['msg'=>"Your current password does not matches with the password you provided. Please try again.", 'status' => 'fail']);
                   Flash::error('Your current password does not matches with the password you provided. Please try again.');
                   return redirect()->back()->withInput($input)->withErrors($validator->messages());

                 
               }
       
               if(strcmp($request->get('current-password'), $request->get('new-password')) == 0){
                   //Current password and new password are same
                 
               // return response()->json(['msg'=>"New Password cannot be same as your current password. Please choose a different password.", 'status' => 'fail']);
               Flash::error('New Password cannot be same as your current password. Please choose a different password.');
                    return redirect()->back()->withInput($input)->withErrors($validator->messages());

               }

               //Change Password
               $user = Auth::user();
               $user->password = bcrypt($request->get('new-password'));
               $user->email=$input['email'];
               $flag=$user->save();
               if ($flag) {
                Session::flash('message', 'success|Password change successfully ');
                return redirect()->back();
              }
              else {
                Session::flash('message', 'danger|Failed, Try again');
                return redirect()->back();
              }              
           }
        }
        catch (Exception $e) {

           
           return response()->json(['msg'=>$e->getMessage(), 'status' => 'fail']);
           
       }
   }
}
