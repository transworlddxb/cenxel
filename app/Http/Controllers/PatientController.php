<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Mail;
use Snowfire\Beautymail\Beautymail;
use App\Mail\SendMailable;
use App\Users;
use App\User;
use App\Role;
use App\Patients;

use Validator;
use Flash;
use DataTables;
use DB;
use Auth;
use Session;
use Response;
use App\Usercategory;
use DateTime;
use Carbon;

class PatientController extends Controller
{
    public function add_patient()
	{				
		// $role = Role::all();	
		$patient = Patients::all();
		return view('admin.patient.add_patient',compact("patient"));
	}

	public function add_patient_details(Request $request)
	{ 
		$input=$request->all();

			$role = new Patients();

			$role->patient_name = $input['patient_name'];
			$role->patient_age = $input['patient_age'];
			$role->patient_place = $input['patient_place'];
			$role->patient_mobile = $input['patient_mobile'];
			$role->patient_status = '1';

			$flag = $role->save();            

			if ($flag) {
				Session::flash('message', 'success|Added successfully ');
				return redirect()->back();
			}
			else {
				Session::flash('message', 'danger|Failed, Try again');
				return redirect()->back();
			}
	}

	public function delete_patient_details($id)
	{

		$role = Patients::find($id);
		if($role!==Null) {

			$flag = $role->delete();

			if ($flag) {
			Session::flash('message', 'success|Deleted successfully ');
			return redirect()->back();
			}
			else {
				Session::flash('message', 'danger|Failed, Try again');
				return redirect()->back();
			}
		}
	} 
	public function edit_patient($id)
	{
		$sms = Patients::where('patient_id', $id)->first();
		// print_r($sms); die();
				// $sms->sgrp_name = $input['sgrp_name'];
		$sg1 = $sms->patient_id.','.$sms->patient_name.','.$sms->patient_age.','.$sms->patient_place.','.$sms->patient_mobile;	
		echo $sg1;

	}
	public function update_patient_details(Request $request,$id)
	{
		$input = $request->all();
		$role =Patients::find($input['patient_id']);
		// $role =Role::find($id);
		$role->fill($input);
		$role->patient_name = $input['patient_name'];
		$role->patient_age = $input['patient_age'];
		$role->patient_place = $input['patient_place'];
		$role->patient_mobile = $input['patient_mobile'];

		$flag=$role->save();

		if ($flag) {
			Session::flash('message', 'success|Updated successfully ');
			return redirect()->back();
		}
		else {
			Session::flash('message', 'danger|Failed, Try again');
			return redirect()->back();
		}
		
	}
}
