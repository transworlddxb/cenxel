<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Mail;
use Snowfire\Beautymail\Beautymail;
use App\Mail\SendMailable;
use App\Users;
use App\User;
use App\Smsgroup;
use App\Orders;
use App\Area;
use App\Role;
use App\Register;
use App\Appointment;

use Validator;
use Flash;
use DataTables;
use DB;
use Auth;
use Session;
use Response;
use App\Menus;
use App\Usercategory;
use App\Product;
use App\Units_carry;
use DateTime;
use Carbon;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        $logged=Auth::user()->id;

        // dd($rle);die();

        $role = Role::all();
        $product = Product::all()->count();

        $dt = Carbon::today()->toDateString();


        $count=User::where('status',User::ACTIVATE)->count();

        $count1=Role::where('roll_status',User::ACTIVATE)->count();
        $count2=Register::where('status', 1)->count();

        $apoointment=Appointment::where('appointment_date', $dt)->count();

        return view('admin.index',compact('count','count1',"role","count2","apoointment","product"));
        // return view('admin.index');
    }

    public function homepage()
    {
        // $logged=Auth::user()->id;

        return view('frontend.home');
    }



    public function usertable()
    {
        if (!Gate::allows('isSUPERADMIN')) {
            abort(404,"You are not suppose to enter this page");
        }

        $user = User::orderBy("id")->get();
        return view('admin.usertable',compact('user'));
    }

    public function registerview()
    {
        // return 1;
        // $user = User::orderBy("id")->get();
        return view('auth.register');
    }
     public function deleteUser()
    {
        $user = User::find($id);
        if ($user) {
            $flag=$user->delete();
            if ($flag) {
                User::where('id', $id)->delete();
                return response(['msg' => 'Career data is deleted', 'status' => 'success']);
            }
        }
        return response(['msg' => 'Something Went Wrong', 'status' => 'failed']);
    }

    public function view()
    {
        $role = Role::all();
        $category = Menu_category::all();
        $menu = Menus::all();
        $product = Product::all();
        $prod_show = DB::table('tbl_product')
        ->join('tbl_product_category', 'tbl_product_category.prodcat_pk_id', '=', 'tbl_product.prod_category')
        ->join('tbl_units_carrying', 'tbl_units_carrying.carry_unit_id','=', 'tbl_product.prod_unit')
        ->select('*')
        ->get();
        // $role = Role::all();
        // $user = Users::where('pk_int_users_id', $id)->first();
        return view('admin.user_index',compact("prod_show","role","category","menu"));

    }
    public function view_single_product($id)
    {
        // $id = Auth::user()->id;
        $role = Role::all();
        $category = Menu_category::all();
        $menu = Menus::all();
        $product = Product::all();
        $prod_show = DB::table('tbl_product')
        ->join('tbl_product_category', 'tbl_product_category.prodcat_pk_id', '=', 'tbl_product.prod_category')
        ->join('tbl_units_carrying', 'tbl_units_carrying.carry_unit_id','=', 'tbl_product.prod_unit')
        ->select('*')
        ->get();
        $prod = Product::where('prod_pk_id', $id)
        ->join('tbl_units_carrying', 'tbl_units_carrying.carry_unit_id','=', 'tbl_product.prod_unit')
        ->select('*')
        ->first();
        $unit = Units_carry::all();
        // $role = Role::all();
        // $user = Users::where('pk_int_users_id', $id)->first();
        return view('admin.users.single_product',compact("prod_show","product","prod","unit","id","role","category","menu"));

    }
    public function order_view_product()
    {
        $role = Role::all();
        $category = Menu_category::all();
        $menu = Menus::all();
        $product = Product::all();
        $prod_show = DB::table('tbl_product')
        ->join('tbl_product_category', 'tbl_product_category.prodcat_pk_id', '=', 'tbl_product.prod_category')
        ->join('tbl_units_carrying', 'tbl_units_carrying.carry_unit_id','=', 'tbl_product.prod_unit')
        ->select('*')
        ->get();
        // $prod = Product::where('prod_pk_id', $id)
        // ->join('tbl_units_carrying', 'tbl_units_carrying.carry_unit_id','=', 'tbl_product.prod_unit')
        // ->select('*')
        // ->first();
        $unit = Units_carry::all();
        // $user = Users::where('pk_int_users_id', $id)->first();
        return view('admin.users.order_view_product',compact("prod_show","product","prod","unit","role","category","menu"));

    }
    public function order_item(Request $request)
    {
        $authid = Auth::user()->id;
        $usr = DB::table('tbl_users')
        ->where('pk_int_users_id',$authid)
        ->pluck('users_area_fk')
        ->first();
        // dd($usr);die();

        $input = $request->all();
// dd($input);die();

        $Date1 = $input['order_from'];
        $Date2 = $input['order_to'];

        // Declare an empty array
        $array = array();

        // Use strtotime function
        $Variable1 = strtotime($Date1);
        $Variable2 = strtotime($Date2);

        // Use for loop to store dates into array
        // 86400 sec = 24 hrs = 60*60*24 = 1 day
        for ($currentDate = $Variable1; $currentDate <= $Variable2; $currentDate += (86400)) {

        $Store = date('Y-m-d', $currentDate);
        $array[] = $Store;
        // $text = implode(",", $array);
        // echo $text;
        }
          $d1 = implode(",", $array);
          $mnu1=explode(",", $d1);
        // Display the dates in array format
        // print_r($mnu1);die();
        foreach ($mnu1 as $key => $m1) {
            $order = new Orders();
            $order->order_user_id = Auth::user()->id;
            $order->order_area_id = $usr;

            $order->order_product_id = $input['order_product_id'];
            $order->order_quantity = $input['order_quantity'];
            $order->order_prod_unit = $input['order_prod_unit'];
            $order->order_from = $input['order_from'];
            $order->order_to = $input['order_to'];
            $order->ordered_date = $m1;
            $order->order_days_number = $input['order_days_number'];
            $order_days = $input['order_days_number'];
            $order->order_total_price = $input['order_total_price'];
            $order_total = $input['order_total_price'];
            $order->order_single_price = $order_total/$order_days;
            $order->order_status = '1';

            $user_wallet=Users::where('pk_int_users_id','=',$authid)->pluck('wallet_amount')->first();

            // dd($authid);die();
            if ($user_wallet==0) {
                Session::flash('message', 'danger|Please recharge your wallet!');
                return redirect()->back();
            }


            $flag = $order->save();

        }

        if ($flag) {
                Session::flash('message', 'success|Order successfully ');
                return redirect()->back();
            }
            else {
                Session::flash('message', 'danger|Failed, Try again');
                return redirect()->back();
        }
    }

    public function definition()
    {
        $role = Role::all();
        return view('admin.superadmin.definition',compact("role"));
    }


}
