<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Mail;
use Snowfire\Beautymail\Beautymail;
use App\Mail\SendMailable;
use App\Users;
use App\User;
use App\Role;
use App\Register;
use App\Branches;
use App\Doctor;
use App\Units_carry;
use App\Appointment;
use App\Treatment;
use App\Consultdata;
use App\Billing;
use Path\To\DOMDocument;

use Validator;
use Flash;
use DataTables;
use DB;
use Auth;
use Session;
use Response;
use PDF;
use Carbon;
use \File;

class BillingController extends Controller
{
    public function index()
	{				
		// $role = Role::all();	
		// $register = Register::where('pk_int_reg_id', $id)->first();
		$date = Carbon::today()->toDateString();
		$logged=Auth::user()->id;
		$branches = Branches::all();
		$doctor = Doctor::all();
		$class = Units_carry::all();
        $appointment = DB::table('tbl_appointment')
        ->join('tbl_registration', 'tbl_registration.pk_int_reg_id', '=', 'tbl_appointment.fk_customer_reg_id')
        ->join('users', 'users.id', '=', 'tbl_appointment.fk_doctor')
        ->join('tbl_product_purchased', 'tbl_product_purchased.purch_appointment_id', '=', 'tbl_appointment.pk_int_appoint_id')
        ->where('complete_status',4)
        ->orderBy('tbl_appointment.pk_int_appoint_id', 'desc')
        ->select('*')
        ->get();

        // dd($appointment);die();
        // return 1;
		return view('admin.billing.billed_list',compact("appointment","branches","doctor","class","total_amount"));
	}
    public function bill_data_pdf($id)
    {
        // return 1;
        // $usr = Appointment::where('pk_int_appoint_id','=',$id)->update(['complete_status' => '2']);
        // $bill=Billing::count();
        // $billcount = $bill+1;
        $billcount = DB::table('tbl_appointment')
        ->where('pk_int_appoint_id',$id)
        ->pluck('pk_int_appoint_id')
        ->first();
        $today = Carbon::today()->toDateString();
        $date = date('d-m-Y', strtotime($today));
        $branches = Branches::all();
        $doctor = Doctor::all();
        $file = DB::table('tbl_registration')
        ->join('tbl_appointment', 'tbl_appointment.fk_customer_reg_id', '=', 'tbl_registration.pk_int_reg_id')
        ->where('pk_int_appoint_id',$id)
        ->first();
        $cunsultdata = DB::table('tbl__consultdata')
        ->join('tbl_appointment', 'tbl_appointment.fk_customer_reg_id', '=', 'tbl__consultdata.customer_id')
        ->join('users', 'users.id', '=', 'tbl__consultdata.doctor_id')
        ->join('tbl_registration', 'tbl_registration.pk_int_reg_id', '=', 'tbl__consultdata.customer_id')
        ->join('tbl_product_purchased', 'tbl_product_purchased.purch_appointment_id', '=', 'tbl__consultdata.appointment_id')
        ->where('appointment_id',$id)
        ->first();

        $appointment_trt_data = DB::table('tbl_appointment')
        ->where('pk_int_appoint_id',$id)
        ->first();

        $todays = $appointment_trt_data->appointment_date;
        $date_appo = date('d-m-Y', strtotime($todays));

        $prod_one=$cunsultdata->product_one;
        $prod_two=$cunsultdata->product_two;
        $prod_three=$cunsultdata->product_three;
        $prod_four=$cunsultdata->product_four;
        $prod_five=$cunsultdata->product_five;
        // dd($prod_two);die();

        $product_one = DB::table('tbl_product')
        ->join('tbl_product_purchased', 'tbl_product_purchased.product_one', '=', 'tbl_product.product_id')
        ->where('purch_appointment_id',$id)
        ->first();
        $product_two = DB::table('tbl_product')
        ->join('tbl_product_purchased', 'tbl_product_purchased.product_two', '=', 'tbl_product.product_id')
        ->where('purch_appointment_id',$id)
        ->first();
        $product_three = DB::table('tbl_product')
        ->join('tbl_product_purchased', 'tbl_product_purchased.product_three', '=', 'tbl_product.product_id')
        ->where('purch_appointment_id',$id)
        ->first();
        $product_four = DB::table('tbl_product')
        ->join('tbl_product_purchased', 'tbl_product_purchased.product_four', '=', 'tbl_product.product_id')
        ->where('purch_appointment_id',$id)
        ->first();
        $product_five = DB::table('tbl_product')
        ->join('tbl_product_purchased', 'tbl_product_purchased.product_five', '=', 'tbl_product.product_id')
        ->where('purch_appointment_id',$id)
        ->first();

        if($prod_one==Null){
            $prod_amount_one=0;
        }else{
            $prod_amount_one=$product_one->prod_price;
        }
        if($prod_two==Null){
            $prod_amount_two=0;
        }else{
            $prod_amount_two=$product_two->prod_price;
        }
        if($prod_three==Null){
            $prod_amount_three=0;
        }else{
            $prod_amount_three=$product_three->prod_price;
        }
        if($prod_four==Null){
            $prod_amount_four=0;
        }else{
            $prod_amount_four=$product_four->prod_price;
        }
        if($prod_five==Null){
            $prod_amount_five=0;
        }else{
            $prod_amount_five=$product_five->prod_price;
        }

        $total_purch_price=$prod_amount_one+$prod_amount_two+$prod_amount_three+$prod_amount_four+$prod_amount_five;


        $bills = DB::table('tbl__billing')
        ->join('tbl_appointment', 'tbl_appointment.fk_customer_reg_id', '=', 'tbl__billing.costumer_id')
        ->where('fk_bill_appointment_id',$id)
        ->first();

        $app = Appointment::where('pk_int_appoint_id', $id)->first();

        $treatment_one = DB::table('treatment')
        ->join('tbl__consultdata', 'tbl__consultdata.treatment_one', '=', 'treatment.pk_treatment_id')
        ->where('appointment_id',$id)
        ->first();
        $treatment_two = DB::table('treatment')
        ->join('tbl__consultdata', 'tbl__consultdata.treatment_two', '=', 'treatment.pk_treatment_id')
        ->where('appointment_id',$id)
        ->first();
        $treatment_three = DB::table('treatment')
        ->join('tbl__consultdata', 'tbl__consultdata.treatment_three', '=', 'treatment.pk_treatment_id')
        ->where('appointment_id',$id)
        ->first();
        $treatment_four = DB::table('treatment')
        ->join('tbl__consultdata', 'tbl__consultdata.treatment_four', '=', 'treatment.pk_treatment_id')
        ->where('appointment_id',$id)
        ->first();
        $treatment_five = DB::table('treatment')
        ->join('tbl__consultdata', 'tbl__consultdata.treatment_five', '=', 'treatment.pk_treatment_id')
        ->where('appointment_id',$id)
        ->first();

        // $discount = $bills->discount_amount;
        // $total_net_amount = $bills->total_amount-$bills->discount_amount;
        // $due = $cunsultdata->final_due_amount;

        //amount
            $amount_one=$appointment_trt_data->trt_one_amount;
            $amount_one_tax=($amount_one/100)*5;
            $grand_one=$amount_one+$amount_one_tax;

            $amount_two=$appointment_trt_data->trt_two_amount;
            $amount_two_tax=($amount_two/100)*5;
            $grand_two=$amount_two+$amount_two_tax;

            $amount_three=$appointment_trt_data->trt_three_amount;
            $amount_three_tax=($amount_three/100)*5;
            $grand_three=$amount_three+$amount_three_tax;

            $amount_four=$appointment_trt_data->trt_four_amount;
            $amount_four_tax=($amount_four/100)*5;
            $grand_four=$amount_four+$amount_four_tax;

            $amount_five=$appointment_trt_data->trt_five_amount;
            $amount_five_tax=($amount_five/100)*5;
            $grand_five=$amount_five+$amount_five_tax;

         $amount=$amount_one+$amount_two+$amount_three+$amount_four+$amount_five;

        $percent = $file->percent_insurance;
        // dd($file->percent_insurance);die();
        // $grand_total_with_prev = $appointment_trt_data->prev_due_amount + $grand_total;

        if($file->insurance_status==1){
            $total_amount=(($percent*$amount)/100)+$total_purch_price;
            // dd($total_amount);die();
        }
        if($file->insurance_status==0){
            $total=$amount_one+$amount_two+$amount_three+$amount_four+$amount_five;
            $total_amount=$total+$total_purch_price;

            $vat_amount=(5*($total))/100;
            $grand_total=$vat_amount+$total_amount;

            $grand_total_with_prev = $appointment_trt_data->prev_due_amount + $grand_total;
        }
        $pdf = PDF::loadView('admin.billing.billing_data_pdf',compact('cunsultdata','branches','doctor','file','date','billcount','treatment_one','treatment_two','treatment_three','treatment_four','treatment_five','amount','total_amount','discount','due','total_net_amount','bills','app','prod_one','prod_two','prod_three','prod_four','prod_five','product_one','product_two','product_three','product_four','product_five','total_purch_price',"amount_one","amount_two","amount_three","amount_four","amount_five","grand_total","vat_amount","appointment_trt_data","amount_one_tax","amount_two_tax","amount_three_tax","amount_four_tax","amount_five_tax","grand_five","grand_one","grand_two","grand_three","grand_four","date_appo","grand_total_with_prev"));
        return $pdf->download('Consulting_Bill.pdf');

    }
	public function billing_status($id)
    {               

        // dd($id);die();
        // $bill=Billing::count();
        // $billcount = $bill+1;
        $today = Carbon::today()->toDateString();
        $date = date('d-m-Y', strtotime($today));
        // dd($datee);die();
        $branches = Branches::all();
        $doctor = Doctor::all();
        $file = DB::table('tbl_registration')
        ->join('tbl_appointment', 'tbl_appointment.fk_customer_reg_id', '=', 'tbl_registration.pk_int_reg_id')
        ->where('pk_int_appoint_id',$id)
        ->first();

        
        $cunsultdata = DB::table('tbl__consultdata')
        ->join('tbl_appointment', 'tbl_appointment.fk_customer_reg_id', '=', 'tbl__consultdata.customer_id')
        ->join('users', 'users.id', '=', 'tbl__consultdata.doctor_id')
        ->join('tbl_registration', 'tbl_registration.pk_int_reg_id', '=', 'tbl__consultdata.customer_id')
        ->join('tbl_product_purchased', 'tbl_product_purchased.purch_appointment_id', '=', 'tbl__consultdata.appointment_id')
        // ->join('treatment', 'treatment.pk_treatment_id', '=', 'tbl__consultdata.treatment_one')
        ->where('appointment_id',$id)
        ->first();


        $appointment_trt_data = DB::table('tbl_appointment')
        ->where('pk_int_appoint_id',$id)
        ->first();

        $todays = $appointment_trt_data->appointment_date;
        $date_appo = date('d-m-Y', strtotime($todays));

        $getdate = DB::table('tbl_appointment')
        ->where('pk_int_appoint_id',$id)
        ->pluck('appointment_date')
        ->first();
        $d = date_parse_from_format("Y-m-d", $getdate);
        $month = $d["month"];

        $billcount = DB::table('tbl_appointment')
        ->whereMonth('appointment_date',$month)
        ->count();

        // dd($billcount);die();

        
        $prod_one=$cunsultdata->product_one;
        $prod_two=$cunsultdata->product_two;
        $prod_three=$cunsultdata->product_three;
        $prod_four=$cunsultdata->product_four;
        $prod_five=$cunsultdata->product_five;
        // dd($prod_two);die();

        $product_one = DB::table('tbl_product')
        ->join('tbl_product_purchased', 'tbl_product_purchased.product_one', '=', 'tbl_product.product_id')
        ->where('purch_appointment_id',$id)
        ->first();
        $product_two = DB::table('tbl_product')
        ->join('tbl_product_purchased', 'tbl_product_purchased.product_two', '=', 'tbl_product.product_id')
        ->where('purch_appointment_id',$id)
        ->first();
        $product_three = DB::table('tbl_product')
        ->join('tbl_product_purchased', 'tbl_product_purchased.product_three', '=', 'tbl_product.product_id')
        ->where('purch_appointment_id',$id)
        ->first();
        $product_four = DB::table('tbl_product')
        ->join('tbl_product_purchased', 'tbl_product_purchased.product_four', '=', 'tbl_product.product_id')
        ->where('purch_appointment_id',$id)
        ->first();
        $product_five = DB::table('tbl_product')
        ->join('tbl_product_purchased', 'tbl_product_purchased.product_five', '=', 'tbl_product.product_id')
        ->where('purch_appointment_id',$id)
        ->first();

        if($prod_one==Null){
            $prod_amount_one=0;
        }else{
            $prod_amount_one=$product_one->prod_price;
        }
        if($prod_two==Null){
            $prod_amount_two=0;
        }else{
            $prod_amount_two=$product_two->prod_price;
        }
        if($prod_three==Null){
            $prod_amount_three=0;
        }else{
            $prod_amount_three=$product_three->prod_price;
        }
        if($prod_four==Null){
            $prod_amount_four=0;
        }else{
            $prod_amount_four=$product_four->prod_price;
        }
        if($prod_five==Null){
            $prod_amount_five=0;
        }else{
            $prod_amount_five=$product_five->prod_price;
        }

        $total_purch_price=$prod_amount_one+$prod_amount_two+$prod_amount_three+$prod_amount_four+$prod_amount_five;

        // dd($cunsultdata);die();
        $app = Appointment::where('pk_int_appoint_id', $id)->first();

        $treatment_one = DB::table('treatment')
        ->join('tbl__consultdata', 'tbl__consultdata.treatment_one', '=', 'treatment.pk_treatment_id')
        ->where('appointment_id',$id)
        ->first();
        $treatment_two = DB::table('treatment')
        ->join('tbl__consultdata', 'tbl__consultdata.treatment_two', '=', 'treatment.pk_treatment_id')
        ->where('appointment_id',$id)
        ->first();
        $treatment_three = DB::table('treatment')
        ->join('tbl__consultdata', 'tbl__consultdata.treatment_three', '=', 'treatment.pk_treatment_id')
        ->where('appointment_id',$id)
        ->first();
        $treatment_four = DB::table('treatment')
        ->join('tbl__consultdata', 'tbl__consultdata.treatment_four', '=', 'treatment.pk_treatment_id')
        ->where('appointment_id',$id)
        ->first();
        $treatment_five = DB::table('treatment')
        ->join('tbl__consultdata', 'tbl__consultdata.treatment_five', '=', 'treatment.pk_treatment_id')
        ->where('appointment_id',$id)
        ->first();

        // $discount = $bills->discount_amount;
        // $total_net_amount = $bills->total_amount-$bills->discount_amount;
        // $due = $cunsultdata->final_due_amount;

        //amount
            $amount_one=$appointment_trt_data->trt_one_amount;
            $amount_two=$appointment_trt_data->trt_two_amount;
            $amount_three=$appointment_trt_data->trt_three_amount;
            $amount_four=$appointment_trt_data->trt_four_amount;
            $amount_five=$appointment_trt_data->trt_five_amount;

        $amount=$amount_one+$amount_two+$amount_three+$amount_four+$amount_five;

        $percent = $file->percent_insurance;
        // dd($file->percent_insurance);die();

        if($file->insurance_status==1){
            $total_amount=(($percent*$amount)/100)+$total_purch_price;

            $vat_amount=(5*($amount))/100;
            $grand_total=$vat_amount+$total_amount;
            // dd($total_amount);die();
        }
        if($file->insurance_status==0){
            $total=$amount_one+$amount_two+$amount_three+$amount_four+$amount_five;
            $total_amount=$total+$total_purch_price;

            $vat_amount=(5*($total))/100;
            $grand_total=$vat_amount+$total_amount;
            // dd($total_amount);die();
        }

        // return 1;
        return view('admin.billing.billing_status',compact("cunsultdata","branches","doctor","file","date","billcount","treatment_one","treatment_two","treatment_three","treatment_four","treatment_five","amount","total_amount","discount","due","total_net_amount","bills","app","prod_one","prod_two","prod_three","prod_four","prod_five","product_one","product_two","product_three","product_four","product_five","total_purch_price","vat_amount","grand_total","amount_one","amount_two","amount_three","amount_four","amount_five","appointment_trt_data","date_appo"));
    }
    public function billPDF(Request $request,$id)
    {
        // return 1;
        // $usr = Appointment::where('pk_int_appoint_id','=',$id)->update(['complete_status' => '2']);
        dd($id);die();
        // $bill=Billing::count();
        // $billcount = $bill+1;
        $date = Carbon::today()->toDateString();
        $branches = Branches::all();
        $doctor = Doctor::all();
        $file = DB::table('tbl_registration')
        ->join('tbl_appointment', 'tbl_appointment.fk_customer_reg_id', '=', 'tbl_registration.pk_int_reg_id')
        ->where('pk_int_appoint_id',$id)
        ->first();

        $bills = DB::table('tbl__billing')
        ->join('tbl_appointment', 'tbl_appointment.fk_customer_reg_id', '=', 'tbl__billing.costumer_id')
        ->where('fk_bill_appointment_id',$id)
        ->first();
        $billcount = DB::table('tbl_appointment')
        ->where('pk_int_appoint_id',$id)
        ->pluck('pk_int_appoint_id')
        ->first();

        $cunsultdata = DB::table('tbl__consultdata')
        ->join('tbl_appointment', 'tbl_appointment.fk_customer_reg_id', '=', 'tbl__consultdata.customer_id')
        ->join('users', 'users.id', '=', 'tbl__consultdata.doctor_id')
        ->join('tbl__billing', 'tbl__billing.fk_bill_appointment_id', '=', 'tbl__consultdata.appointment_id')
        ->join('tbl_registration', 'tbl_registration.pk_int_reg_id', '=', 'tbl__consultdata.customer_id')
        // ->join('treatment', 'treatment.pk_treatment_id', '=', 'tbl__consultdata.treatment_one')
        ->where('pk_int_appoint_id',$id)
        ->where('fk_bill_appointment_id',$id)
        ->first();

        $treatment_one = DB::table('treatment')
        ->join('tbl__consultdata', 'tbl__consultdata.treatment_one', '=', 'treatment.pk_treatment_id')
        ->first();
        $treatment_two = DB::table('treatment')
        ->join('tbl__consultdata', 'tbl__consultdata.treatment_two', '=', 'treatment.pk_treatment_id')
        ->first();
        $treatment_three = DB::table('treatment')
        ->join('tbl__consultdata', 'tbl__consultdata.treatment_three', '=', 'treatment.pk_treatment_id')
        ->first();
        $treatment_four = DB::table('treatment')
        ->join('tbl__consultdata', 'tbl__consultdata.treatment_four', '=', 'treatment.pk_treatment_id')
        ->first();
        $treatment_five = DB::table('treatment')
        ->join('tbl__consultdata', 'tbl__consultdata.treatment_five', '=', 'treatment.pk_treatment_id')
        ->first();

        $discount = $bills->discount_amount;
        $total_net_amount = $bills->total_amount-$bills->discount_amount;
        $due = $cunsultdata->final_due_amount;

        //amount
        if($cunsultdata->treatment_one==Null){
            $amount_one=0;
        }else{
            $amount_one=$treatment_one->treatment_price;
        }
        if($cunsultdata->treatment_two==Null){
            $amount_two=0;
        }else{
            $amount_two=$treatment_two->treatment_price;
        }
        if($cunsultdata->treatment_three==Null){
            $amount_three=0;
        }else{
            $amount_three=$treatment_three->treatment_price;
        }
        if($cunsultdata->treatment_four==Null){
            $amount_four=0;
        }else{
            $amount_four=$treatment_four->treatment_price;
        }
        if($cunsultdata->treatment_five==Null){
            $amount_five=0;
        }else{
            $amount_five=$treatment_five->treatment_price;
        }

         $amount=$amount_one+$amount_two+$amount_three+$amount_four+$amount_five;

        $percent = $file->percent_insurance;
        // dd($file->percent_insurance);die();

        if($file->insurance_status==1){
            $total_amount=(($percent*$amount)/100)+$cunsultdata->final_due_amount;
            // dd($total_amount);die();
        }
        if($file->insurance_status==0){
            $total=$amount_one+$amount_two+$amount_three+$amount_four+$amount_five;
            $total_amount=($total-$cunsultdata->discount_amount)+$cunsultdata->final_due_amount;
            // dd($total_amount);die();
        }

        $pdf = PDF::loadView('admin.billing.billingpdf',compact("cunsultdata","branches","doctor","file","date","billcount","treatment_one","treatment_two","treatment_three","treatment_four","treatment_five","amount","total_amount","discount","due","total_net_amount"));
        return $pdf->download('Consulting_Bill.pdf');

    }
    
    
    public function paymnt_remrk($id)
    {
        // $sms = Consultdata::where('pk_doctor_id', $id)->first();
        $st = DB::table('tbl_appointment')
        ->where('pk_int_appoint_id',$id)
        ->first();

        $sg1 = $st->pk_int_appoint_id;    
        echo $sg1;

    }
    public function due_remrk($id)
    {
        $due_amt = DB::table('tbl_registration')
        ->where('pk_int_reg_id',$id)
        // ->pluck('final_due_amount')
        ->first();

        $sg1 = $due_amt->final_due_amount;    
        echo $sg1;

    }

    public function add_payment_trt(Request $request)
    {
        $input = $request->all();
        $id = $input['appo_id'];
        $payment =Appointment::find($input['appo_id']);
        $payment->fill($input);
        $total = $input['total_amount'];
        $payed_amount = $input['payed_bill_amount'];

        $payment->total_amount_to_pay = $input['total_amount'];
        $payment->payedbill_amount = $input['payed_bill_amount'];

        $due_amount = $total - $payed_amount;
        $payment->duebill_amount = $due_amount;
        $usr = Appointment::where('pk_int_appoint_id','=',$id)->update(['payment_status' => '1']);
        // $payment->sgrp_pk_id = $input['user_id'];
        $flag=$payment->save();

        if ($flag) {
                Session::flash('message', 'success|Updated successfully ');
                return redirect()->back();
            }
            else {
                Session::flash('message', 'danger|Failed, Try again');
                return redirect()->back();
            }
    }

    public function delete_appo($id)
    {

        $appointment = Appointment::find($id);
        if($appointment!==Null) {

            $flag = $appointment->delete();

            if ($flag) {
            Session::flash('message', 'success|Deleted successfully ');
            return redirect()->back();
            }
            else {
                Session::flash('message', 'danger|Failed, Try again');
                return redirect()->back();
            }
        }
    } 

    

    public function update_appoit_details(Request $request)
    {
        $input = $request->all();
        $id = $input['appoint_id'];
        $payment =Appointment::find($input['appoint_id']);
        $payment->fill($input);

        $product_one = $input['product_one'];
        $product_two = $input['product_two'];
        $product_three = $input['product_three'];
        $product_four = $input['product_four'];
        $product_five = $input['product_five'];
        $product_six = $input['product_six'];

        if($product_one!=null){
            $amount_one = DB::table('tbl_product')
            ->where('product_id',$product_one)
            ->first();
            $product_one_amount = $amount_one->prod_price;
        }
        else{
            $product_one_amount = 0;
        }
        if($product_two!=null){
            $amount_two = DB::table('tbl_product')
            ->where('product_id',$product_two)
            ->first();
            $product_two_amount = $amount_two->prod_price;
        }
        else{
            $product_two_amount = 0;
        }
        if($product_three!=null){
            $amount_three = DB::table('tbl_product')
            ->where('product_id',$product_three)
            ->first();
            $product_three_amount = $amount_three->prod_price;
        }
        else{
            $product_three_amount = 0;
        }
        if($product_four!=null){
            $amount_four = DB::table('tbl_product')
            ->where('product_id',$product_four)
            ->first();
            $product_four_amount = $amount_four->prod_price;
        }
        else{
            $product_four_amount = 0;
        }
        if($product_five!=null){
            $amount_five = DB::table('tbl_product')
            ->where('product_id',$product_five)
            ->first();
            $product_five_amount = $amount_five->prod_price;
        }
        else{
            $product_five_amount = 0;
        }
        if($product_six!=null){
            $amount_six = DB::table('tbl_product')
            ->where('product_id',$product_six)
            ->first();
            $product_six_amount = $amount_six->prod_price;
        }
        else{
            $product_six_amount = 0;
        }
        

        $prod_amount=$product_one_amount+$product_two_amount+$product_three_amount+$product_four_amount+$product_five_amount+$product_six_amount;
        if($prod_amount!=null){
            $payment->prod_total_amount = $prod_amount;
        }

        if($product_one!=null){
            $payment->prod_purch_status = '1';
        }
        else{
            $payment->prod_purch_status = '0';
        }

        $payment->consult_description = $input['consult_description'];
        $payment->product_one = $input['product_one'];
        $payment->product_two = $input['product_two'];
        $payment->product_three = $input['product_three'];
        $payment->product_four = $input['product_four'];
        $payment->product_five = $input['product_five'];
        $payment->product_six = $input['product_six'];

        $payment->product_one_count = $input['product_count_one'];
        $payment->product_two_count = $input['product_count_two'];
        $payment->product_three_count = $input['product_count_three'];
        $payment->product_four_count = $input['product_count_four'];
        $payment->product_five_count = $input['product_count_five'];
        $payment->product_six_count = $input['product_count_six'];

        $flag=$payment->save();

        if ($flag) {
                Session::flash('message', 'success|Updated successfully ');
                return redirect()->back();
            }
            else {
                Session::flash('message', 'danger|Failed, Try again');
                return redirect()->back();
            }
    }
    

    public function edit_details_appo($id)
    {
        $details = DB::table('tbl_appointment')
        ->where('pk_int_appoint_id',$id)
        ->first();

        $sg1 = $details->appoint_patient.','.$details->consult_description.','.$details->product_one.','.$details->product_one_count.','.$details->product_two.','.$details->product_two_count.','.$details->product_three.','.$details->product_three_count.','.$details->product_four.','.$details->product_four_count.','.$details->product_five.','.$details->product_five_count.','.$details->product_six.','.$details->product_six_count.','.$details->pk_int_appoint_id;    
        echo $sg1;

    }
    
    public function update_payment(Request $request)
    {   
        // dd($id);die();
        $input = $request->all();
        $billing = new Billing();
        $appoit_id = $input['appo_id'];
        // dd($appoit_id);die();
        $dates = Carbon::today()->toDateString();
        $cunsultdata = DB::table('tbl__consultdata')
        ->join('tbl_appointment', 'tbl_appointment.fk_customer_reg_id', '=', 'tbl__consultdata.customer_id')
        ->join('users', 'users.id', '=', 'tbl__consultdata.doctor_id')
        ->join('tbl_registration', 'tbl_registration.pk_int_reg_id', '=', 'tbl__consultdata.customer_id')
        ->where('appointment_id',$appoit_id)
        ->first();
        // dd($cunsultdata->customer_id);die();
         $due_amt = DB::table('tbl_registration')
        ->where('pk_int_reg_id',$appoit_id)
        ->pluck('final_due_amount')
        ->first();

        // $due = $input['due_amount'];

        $total_due=$due_amt;

        $usr = Register::where('pk_int_reg_id','=',$appoit_id)->update(['final_due_amount' => $total_due]);

        $billing->total_amount = $input['total_amount'];
        $billing->prev_due_amount = $input['prev_due_amount'];
        $billing->Discount_amount = $input['Discount_amount'];
        $Discount_amount = $input['Discount_amount'];
        if($Discount_amount==0){
            $usr = Appointment::where('pk_int_appoint_id','=',$appoit_id)->update(['complete_status' => '2']);
        }else{
            $usr = Appointment::where('pk_int_appoint_id','=',$appoit_id)->update(['complete_status' => '11']);
        }
        // $billing->bill_due_amount = $input['due_amount'];
        $billing->fk_bill_appointment_id = $appoit_id;
        $billing->date = $dates;
        $billing->doctor_id = $cunsultdata->doctor_id;
        $billing->costumer_id = $cunsultdata->customer_id;
        $billing->status = '1';
        $billing->approval_status = '1';

        $flag = $billing->save();

        if ($flag) {
            
                Session::flash('message', 'success|Updated successfully ');
                return redirect()->back();
            }
            else {
                Session::flash('message', 'danger|Failed, Try again');
                return redirect()->back();
            }
    }
    public function update_due(Request $request,$id)
    {
        $input = $request->all();
        $billing = new Billing();

        $due_amt = DB::table('tbl_registration')
        ->where('pk_int_reg_id',$id)
        ->pluck('final_due_amount')
        ->first();

        $new_due= $input['new_due_amounts'];

        $total_due=$due_amt+$new_due;
        $usr = Register::where('pk_int_reg_id','=',$id)->update(['final_due_amount' => $total_due]);

        if ($usr) {
                Session::flash('message', 'success|Updated successfully ');
                return redirect()->back();
            }
            else {
                Session::flash('message', 'danger|Failed, Try again');
                return redirect()->back();
            }
    }
    public function bill_pdf(Request $request,$id)
    {   
        // dd($id);die();
        // $usr = Appointment::where('pk_int_appoint_id','=',$id)->update(['complete_status' => '2']);
        $usr = Appointment::where('pk_int_appoint_id','=',$id)->update(['complete_status' => '4']);

        $input = $request->all();
        $payed= $input['payed_amount'];

        $usr = Appointment::where('pk_int_appoint_id','=',$id)->update(['payedbill_amount' => $payed]);

        // $bill=Billing::count();

        // $billcount = $bill+1;
        $billcount = DB::table('tbl_appointment')
        ->where('pk_int_appoint_id',$id)
        ->pluck('pk_int_appoint_id')
        ->first();

        $date = Carbon::today()->toDateString();
        $branches = Branches::all();
        $doctor = Doctor::all();
        $file = DB::table('tbl_registration')
        ->join('tbl_appointment', 'tbl_appointment.fk_customer_reg_id', '=', 'tbl_registration.pk_int_reg_id')
        ->where('pk_int_appoint_id',$id)
        ->first();

        $cunsultdata = DB::table('tbl__consultdata')
        ->join('tbl_appointment', 'tbl_appointment.fk_customer_reg_id', '=', 'tbl__consultdata.customer_id')
        ->join('users', 'users.id', '=', 'tbl__consultdata.doctor_id')
        ->join('tbl_registration', 'tbl_registration.pk_int_reg_id', '=', 'tbl__consultdata.customer_id')
        ->join('tbl_product_purchased', 'tbl_product_purchased.purch_appointment_id', '=', 'tbl__consultdata.appointment_id')
        ->where('pk_int_appoint_id',$id)
        ->first();

        $prev_due_of = $cunsultdata->final_due_amount;
        // dd($prev_due_of);die();

        $due_of_usr = Appointment::where('pk_int_appoint_id','=',$id)->update(['prev_due_amount' => $prev_due_of]);

         $purch_prod = DB::table('tbl_product_purchased')
        ->where('purch_appointment_id',$id)
        ->first();
// dd($cunsultdata);die();

        $appointment_trt_data = DB::table('tbl_appointment')
        ->where('pk_int_appoint_id',$id)
        ->first();

        $prod_one=$purch_prod->product_one;
        $prod_two=$purch_prod->product_two;
        $prod_three=$purch_prod->product_three;
        $prod_four=$purch_prod->product_four;
        $prod_five=$purch_prod->product_five;

        $product_one = DB::table('tbl_product')
        ->join('tbl_product_purchased', 'tbl_product_purchased.product_one', '=', 'tbl_product.product_id')
        ->where('purch_appointment_id',$id)
        ->first();
        $product_two = DB::table('tbl_product')
        ->join('tbl_product_purchased', 'tbl_product_purchased.product_two', '=', 'tbl_product.product_id')
        ->where('purch_appointment_id',$id)
        ->first();
        $product_three = DB::table('tbl_product')
        ->join('tbl_product_purchased', 'tbl_product_purchased.product_three', '=', 'tbl_product.product_id')
        ->where('purch_appointment_id',$id)
        ->first();
        $product_four = DB::table('tbl_product')
        ->join('tbl_product_purchased', 'tbl_product_purchased.product_four', '=', 'tbl_product.product_id')
        ->where('purch_appointment_id',$id)
        ->first();
        $product_five = DB::table('tbl_product')
        ->join('tbl_product_purchased', 'tbl_product_purchased.product_five', '=', 'tbl_product.product_id')
        ->where('purch_appointment_id',$id)
        ->first();

        if($prod_one==Null){
            $prod_amount_one=0;
        }else{
            $prod_amount_one=$product_one->prod_price;
        }
        if($prod_two==Null){
            $prod_amount_two=0;
        }else{
            $prod_amount_two=$product_two->prod_price;
        }
        if($prod_three==Null){
            $prod_amount_three=0;
        }else{
            $prod_amount_three=$product_three->prod_price;
        }
        if($prod_four==Null){
            $prod_amount_four=0;
        }else{
            $prod_amount_four=$product_four->prod_price;
        }
        if($prod_five==Null){
            $prod_amount_five=0;
        }else{
            $prod_amount_five=$product_five->prod_price;
        }

        $total_purch_price=$prod_amount_one+$prod_amount_two+$prod_amount_three+$prod_amount_four+$prod_amount_five;


        $treatment_one = DB::table('treatment')
        ->join('tbl__consultdata', 'tbl__consultdata.treatment_one', '=', 'treatment.pk_treatment_id')
        ->where('appointment_id',$id)
        ->first();
        $treatment_two = DB::table('treatment')
        ->join('tbl__consultdata', 'tbl__consultdata.treatment_two', '=', 'treatment.pk_treatment_id')
        ->where('appointment_id',$id)
        ->first();
        $treatment_three = DB::table('treatment')
        ->join('tbl__consultdata', 'tbl__consultdata.treatment_three', '=', 'treatment.pk_treatment_id')
        ->where('appointment_id',$id)
        ->first();
        $treatment_four = DB::table('treatment')
        ->join('tbl__consultdata', 'tbl__consultdata.treatment_four', '=', 'treatment.pk_treatment_id')
        ->where('appointment_id',$id)
        ->first();
        $treatment_five = DB::table('treatment')
        ->join('tbl__consultdata', 'tbl__consultdata.treatment_five', '=', 'treatment.pk_treatment_id')
        ->where('appointment_id',$id)
        ->first();

        // $discount = $cunsultdata->discount_amount;
        $due = $cunsultdata->final_due_amount;

        
        //amount
            $amount_one=$appointment_trt_data->trt_one_amount;
            $amount_one_tax=($amount_one/100)*5;
            $grand_one=$amount_one+$amount_one_tax;

            $amount_two=$appointment_trt_data->trt_two_amount;
            $amount_two_tax=($amount_two/100)*5;
            $grand_two=$amount_two+$amount_two_tax;

            $amount_three=$appointment_trt_data->trt_three_amount;
            $amount_three_tax=($amount_three/100)*5;
            $grand_three=$amount_three+$amount_three_tax;

            $amount_four=$appointment_trt_data->trt_four_amount;
            $amount_four_tax=($amount_four/100)*5;
            $grand_four=$amount_four+$amount_four_tax;

            $amount_five=$appointment_trt_data->trt_five_amount;
            $amount_five_tax=($amount_five/100)*5;
            $grand_five=$amount_five+$amount_five_tax;

         $amount=$amount_one+$amount_two+$amount_three+$amount_four+$amount_five;

         $usrs = Appointment::where('pk_int_appoint_id','=',$id)->update(['trt_total_amount' => $amount]);

        $percent = $file->percent_insurance;
        // dd($file->percent_insurance);die();

        if($file->insurance_status==1){
            $total_amount=(($percent*$amount)/100)+$total_purch_price;
            $vat_amount=(5*($amount))/100;
            $grand_total=$vat_amount+$total_amount;
            $usrs = Appointment::where('pk_int_appoint_id','=',$id)->update(['vat_amount' => $vat_amount]);
            // dd($total_amount);die();
        }
        if($file->insurance_status==0){
            $total=$amount_one+$amount_two+$amount_three+$amount_four+$amount_five;
            $total_amount=($total)+$cunsultdata->final_due_amount+$total_purch_price;
            $vat_amount=(5*($total))/100;
            $grand_total=$vat_amount+$total_amount;

            $grand_total_with_prev = $appointment_trt_data->prev_due_amount + $grand_total;
            $usrs = Appointment::where('pk_int_appoint_id','=',$id)->update(['vat_amount' => $vat_amount]);
            // dd($total_amount);die();
        }
        if($payed!=$grand_total){
            $final_bill_due=$grand_total-$payed;
            $usr = Register::where('pk_int_reg_id','=',$cunsultdata->customer_id)->update(['final_due_amount' => $final_bill_due]);
            $usrs = Appointment::where('pk_int_appoint_id','=',$id)->update(['duebill_amount' => $final_bill_due]);
        }
        else{
        $final_bill_due=$grand_total-$payed;
        $usr = Register::where('pk_int_reg_id','=',$cunsultdata->customer_id)->update(['final_due_amount' => $final_bill_due]);
        $usrs = Appointment::where('pk_int_appoint_id','=',$id)->update(['duebill_amount' => $final_bill_due]);
        }

        $totl_amnt = Appointment::where('pk_int_appoint_id','=',$id)->update(['total_amount_to_pay' => $grand_total]);

        $doctor_name = DB::table('tbl_appointment')
        ->where('pk_int_appoint_id',$id)
        ->join('users', 'users.id', '=', 'tbl_appointment.fk_doctor')
        ->pluck('roll_name')
        ->first();

        $appointment_date = DB::table('tbl_appointment')
        ->where('pk_int_appoint_id',$id)
        ->pluck('appointment_date')
        ->first();

        $appointment_time = DB::table('tbl_appointment')
        ->where('pk_int_appoint_id',$id)
        ->pluck('appointment_time')
        ->first();
        //email

        // $data = [
        //             'firstname'=> $file->first_name, 
        //             'middlename'=> $file->middle_name, 
        //             'lastname'=> $file->last_name, 
        //             'file'=> $file, 
        //             'amount'=> $amount, 
        //             'discount'=> $discount, 
        //             'due'=> $due, 
        //             'vat_amount'=> $vat_amount, 
        //             'grand_total'=> $grand_total, 
        //             'payed'=> $payed, 
        //             'doctor_name'=> $doctor_name, 
        //             'appointment_date'=> $appointment_date, 
        //             'appointment_time'=> $appointment_time, 
        //             'total_purch_price'=> $total_purch_price, 
        //         ];

        //         $beautymail = app()->make(\Snowfire\Beautymail\Beautymail::class);
        //         $beautymail->send('email.bill_email',$data, function($message) use  ($data)
        //         {
                    
        //             $email = 'tpidxbit@gmail.com';
        //             $message
        //             ->from('tpidxbit@gmail.com')
        //             ->to($email)
        //             ->to('info@alqasercosme.com')
        //             ->subject('Billing Statement');
        //         }); 


        $pdf = PDF::loadView('admin.billing.final_bill',compact("cunsultdata","branches","doctor","file","date","billcount","treatment_one","treatment_two","treatment_three","treatment_four","treatment_five","amount","total_amount","due","payed","prod_one","prod_two","prod_three","prod_four","prod_five","product_one","product_two","product_three","product_four","product_five","total_purch_price","grand_total","vat_amount","amount_one","amount_two","amount_three","amount_four","amount_five","appointment_trt_data","amount_one_tax","amount_two_tax","amount_three_tax","amount_four_tax","amount_five_tax","grand_five","grand_one","grand_two","grand_three","grand_four","date_appo","grand_total_with_prev"));
        return $pdf->download('Final_Bill.pdf');

    }
    
}
