<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Mail;
use Snowfire\Beautymail\Beautymail;
use App\Mail\SendMailable;
use App\Users;
use App\User;
use App\Advance;
use App\Register;

use DOMDocument;
use Validator;
use Flash;
use DataTables;
use DB;
use Auth;
use Session;
use Response;
use PDF;
use Carbon;
use \File;

class AdvanceController extends Controller
{
    public function index($id)
	{				
		$register_id = $id;	
		// dd($register_id);die();
		$data = DB::table('tbl_registration')
        ->where('pk_int_reg_id',$register_id)
        ->first();

        $adv_data = DB::table('tbl_advance')
        ->join('tbl_registration', 'tbl_registration.pk_int_reg_id', '=', 'tbl_advance.advance_cust_id')
        ->where('adv_status','1')
        ->select('*')
        ->get();
        // dd($adv_data);die();
		// $roles_show = User::all();
		return view('admin.advance.advance',compact('register_id','data','adv_data'));
	}

	public function add_advance(Request $request,$id)
	{ 
		// return 1;
		$input=$request->all();
		// dd($id);die();
		$date = Carbon::today()->toDateString();
		// dd($date);die();
			$advance = new Advance();
			$advance_amt = $input['adv_amount'];

			$advance->advance_cust_id = $id;
			$advance->advance_amount = $input['adv_amount'];
			$advance->advance_date = $date;
			$advance->adv_status = 1;

			$usr = Register::where('pk_int_reg_id','=',$id)->update(['adv_amount' => $advance_amt]);

			$flag = $advance->save();            

			if ($flag) {
				Session::flash('message', 'success|Added successfully ');
				return redirect()->back();
			}
			else {
				Session::flash('message', 'danger|Failed, Try again');
				return redirect()->back();
			}
	}
	public function advance_report()
	{				
		// return 1;

        $adv_data = DB::table('tbl_advance')
        ->join('tbl_registration', 'tbl_registration.pk_int_reg_id', '=', 'tbl_advance.advance_cust_id')
        ->where('adv_status','1')
        ->select('*')
        ->get();
        // dd($adv_data);die();
		// $roles_show = User::all();
		return view('admin.advance.advance_report',compact("adv_data"));
	}
	public function clear_advance($id)
	{				
		$adv_data = DB::table('tbl_advance')
        ->join('tbl_registration', 'tbl_registration.pk_int_reg_id', '=', 'tbl_advance.advance_cust_id')
        ->where('advance_id',$id)
        ->first();

		$usr = Register::where('pk_int_reg_id','=',$adv_data->advance_cust_id)->update(['adv_amount' => 0]);

		$usr = Advance::where('advance_id','=',$id)->update(['adv_status' => 0]);



			if ($usr) {
			Session::flash('message', 'success|Deleted successfully ');
			return redirect()->back();
			}
			else {
				Session::flash('message', 'danger|Failed, Try again');
				return redirect()->back();
			}
	}
	public function today_advance_report(Request $request)
    {
    	// return 1;
        $date = Carbon::today()->toDateString();

        $advance = DB::table('tbl_advance')
        ->join('tbl_registration', 'tbl_registration.pk_int_reg_id', '=', 'tbl_advance.advance_cust_id')
        // ->where('complete_status','1')
        ->where('advance_date',$date)
        ->select('*')
        ->get();

        $pdf = PDF::loadView('admin.advance.advance_reportpdf',compact("advance","date"));
        return $pdf->download('advance_report.pdf');

    }
    public function datewise_advance__pdf(Request $request)
    {
    	// return 1;
    	$input=$request->all();
		$report_date = $input['report_date'];

        // $date = Carbon::today()->toDateString();

        $advance = DB::table('tbl_advance')
        ->join('tbl_registration', 'tbl_registration.pk_int_reg_id', '=', 'tbl_advance.advance_cust_id')
        // ->where('complete_status','1')
        ->where('advance_date',$report_date)
        ->select('*')
        ->get();

        $pdf = PDF::loadView('admin.advance.datewise_adv_report',compact("advance","report_date"));
        return $pdf->download('advance_report.pdf');

    }
    public function monthly_advance_pdf(Request $request)
    {
    	// return 1;
    	$input=$request->all();
		$report_month = $input['report_month'];
		$report_year = $input['report_year'];
		
        // $date = Carbon::today()->toDateString();

        $advance = DB::table('tbl_advance')
        ->join('tbl_registration', 'tbl_registration.pk_int_reg_id', '=', 'tbl_advance.advance_cust_id')
        // ->where('complete_status','1')
        ->whereMonth('tbl_advance.created_at',$report_month)
        ->whereYear('tbl_advance.created_at',$report_year)
        ->select('*')
        ->get();

        $pdf = PDF::loadView('admin.advance.monthly_adv_report',compact("advance","report_month","report_year"));
        return $pdf->download('advance_report.pdf');

    }
    public function search__advance_date(Request $request)
    {               
        $input=$request->all();
		$search_date = $input['search_date'];
		
        // $date = Carbon::today()->toDateString();
		$adv_data = DB::table('tbl_advance')
        ->join('tbl_registration', 'tbl_registration.pk_int_reg_id', '=', 'tbl_advance.advance_cust_id')
        ->where('adv_status','1')
        ->where('advance_date',$search_date)
        ->select('*')
        ->get();


        return view('admin.advance.advance_report',compact("adv_data"));
    }

    public function date_daily_advance_search(Request $request)
    {               
        $input=$request->all();
		$from_date = $input['from_date'];
		$to_date = $input['to_date'];
		
        // $date = Carbon::today()->toDateString();
		$adv_data = DB::table('tbl_advance')
        ->join('tbl_registration', 'tbl_registration.pk_int_reg_id', '=', 'tbl_advance.advance_cust_id')
        ->where('adv_status','1')
        ->where('advance_date','>=', $from_date)
        ->where('advance_date','<=', $to_date)
        ->select('*')
        ->get();


        return view('admin.advance.advance_report',compact("adv_data"));
    }
	
}
