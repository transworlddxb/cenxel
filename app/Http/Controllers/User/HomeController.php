<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Career;
use App\UserCareer;
use App\ContactUs;
use Validator;
use Flash;
use DataTables;
use DB;
use Auth;
use App\User;
use Session;

class HomeController extends Controller
{
    public function index()
    {
    	return view('user.homepage');
    }
    public function about()
    {
    	return view('user.pages.about');
    }
    public function contactUs()
    {
    	return view('user.pages.contactus');
    }

    public function blog()
    {
    	return view('user.pages.blog');
    }

    public function career()
    {
        $career=Career::orderby('career_id','DESC')->where('status',Career::ACTIVATE)->get();
    	return view('user.pages.career',compact('career'));
    }

    public function services()
    {
        return view('user.pages.services');
    }
    public function drafting_and_compliance()
    {
        return view('user.pages.services.drafting_and_compliance_support');
    }
    public function intellectual_property()
    {
        return view('user.pages.services.intellectual_property_rights_management');
    }

    public function intellectual_property_rights_trademark()
    {
        return view('user.pages.services.intellectual_property_rights_trademark');
    }
    public function intellectual_property_rights_copyright()
    {
        return view('user.pages.services.intellectual_property_rights_copyright');
    }
    public function intellectual_property_rights_patent()
    {
        return view('user.pages.services.intellectual_property_rights_patent');
    }
    public function intellectual_property_rights_industrial()
    {
        return view('user.pages.services.intellectual_property_rights_industrial');
    }

    ///accounting
    public function accounting_and_book_keeping()
    {
        return view('user.pages.accounting.accounting_and_book_keeping');
    }

    public function iso_certification_standard()
    {
        return view('user.pages.accounting.iso-certification-standard');
    }
    
    public function pro_services()
    {
        return view('user.pages.accounting.pro-services');
    }

    public function company_formation()
    {
        return view('user.pages.accounting.company_formation');
    }

    public function company_formation_MainLand()
    {
        return view('user.pages.accounting.company_formation_MainLand');
    }

    public function company_formation_FreeZone()
    {
        return view('user.pages.accounting.company_formation_FreeZone');
    }
    public function company_formation_OffshoreSetup()
    {
        return view('user.pages.accounting.company_formation_OffshoreSetup');
    }
    public function other_corporate_helps()
    {
        return view('user.pages.accounting.other_corporate_helps');
    }

    public function business_set_up()
    {
        return view('user.pages.accounting.business_set_up');
    }
    public function registration_and_renewal_of_license()
    {
        return view('user.pages.accounting.registration_and_renewal_of_license');
    }
    
    public function services_under_legal_supervision()
    {
        return view('user.pages.accounting.services_under_legal_supervision');
    }

    public function postCareer(Request $request)
    {
          // get the POST data
        $input = $request->all();
        
        //DO validation here
        $validator=validator::make($input, UserCareer::$rule, UserCareer::$message);
        //dd($input);

        // if validator passes
        if ($validator->passes()) {
            
            try
            {
                if($request->hasFile('resume'))
                {
                    $user = new UserCareer();
                    $user->fill($input);
                     //image upload

                    $testimonialFolder = public_path().'/uploads/resume';
                    if (!file_exists($testimonialFolder)) {
                            @mkdir($testimonialFolder,666,true);
                      }
                 
                    $getimageName = rand(10, 100).trim($input['user_name']).'.'.$request->resume->getClientOriginalExtension();
                    $request->file('resume')->move('public/uploads/resume', $getimageName);
                    $path='public/uploads/resume/'.$getimageName;
                    $user->user_file_path=$path;
                    $user->status = UserCareer::ACTIVATE;
                    $flag=$user->save();
                    if($flag)
                    {
                         return response()->json(['msg'=>'Resume submitted', 'status'=>'success']);
                    }
                    else
                    {
                         return response()->json(['msg'=>'Something went wrong, please try again later.', 'status'=>'fail']);
                    }
                }
                else
                {

                         return response()->json(['msg'=>'NO FILE RECEIVED', 'status'=>'fail']);
                }
                
            }
            catch(\Exception $e)
            {
                return response()->json(['msg'=>$e->getMessage(), 'status' => 'fail']);

                //return $e->getMessage();
            }

        } else {
             return response()->json(['msg'=>$validator->messages(), 'status' => 'fail']);
        }
    }

    public function postContactus(Request $request)
    {
          // get the POST data
        $input = $request->all();
        
        //DO validation here
        $validator=validator::make($input, ContactUs::$rule, ContactUs::$message);
        //dd($input);

        // if validator passes
        if ($validator->passes()) {
            
            try
            {
                    $user = new ContactUs();
                    $user->fill($input);
                    $user->status = ContactUs::ACTIVATE;
                    $flag=$user->save();
                    if($flag)
                    {
                        $data = [
                        'title'=> 'info@bisandjuris.com', 
                        'name'=>$input['name'],
                        'email'=>$input['email'],
                        'mobile'=>$input['mobile'],
                        'subject'=> $input['subject'],
                        'content'=>$input['content'],
                        'admin_email'=>'manager@bisandjuris.com',
                        'admin_name'=>'Bis and Juris'
                ];

                    //for admin
                    $beautymail = app()->make(\Snowfire\Beautymail\Beautymail::class);
                   // $beautymail->send('emails.business-submission',$data, function($message) use ($data)
                    
                   $beautymail->send('email.contactus',$data, function($message) use ($data)
                     {
                        $message->from($data['email'],$data['name'])
                        ->to($data['admin_email'],$data['admin_name'])
                        ->subject('Contact us');
                     });
           
                         return response()->json(['msg'=>'Request submitted', 'status'=>'success']);
                    }
                    else
                    {
                         return response()->json(['msg'=>'Something went wrong, please try again later.', 'status'=>'fail']);
                    }
                
                
            }
            catch(\Exception $e)
            {
                return response()->json(['msg'=>$e->getMessage(), 'status' => 'fail']);

                //return $e->getMessage();
            }

        } else {
             return response()->json(['msg'=>$validator->messages(), 'status' => 'fail']);
        }
    }

}
