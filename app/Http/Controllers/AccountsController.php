<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Mail;
use Snowfire\Beautymail\Beautymail;
use App\Mail\SendMailable;
use App\Users;
use App\User;
use App\Role;
use App\Register;
use App\Branches;
use App\Doctor;
use App\Units_carry;
use App\Appointment;
use App\Billing;
use App\Purchased;
use Barryvdh\DomPDF\Facade as PDF;

use Validator;
use Flash;
use DataTables;
use DB;
use Auth;
use Session;
use Response;
// use PDF;
use Carbon;
use \File;

class AccountsController extends Controller
{
    public function index()
	{				
		// $role = Role::all();	
		// $register = Register::where('pk_int_reg_id', $id)->first();
		$branches = Branches::all();
		// $doctor = Doctor::all();
		$class = Units_carry::all();
        // $doctor = User::where('admin_role',4)->get();
        // $register = DB::table('tbl_registration')
        // ->join('tbl_units_carrying', 'tbl_units_carrying.class_id', '=', 'tbl_registration.int_reg_category')
        // ->select('*')
        // ->get();
        // return 1;
		return view('admin.accounts.dashboard',compact("register","branches","doctor","class"));
	}
	public function daily_accounts()
	{				
		// $role = Role::all();	
		// $register = Register::where('pk_int_reg_id', $id)->first();
		$date = Carbon::today()->toDateString();
		$branches = Branches::all();
		$doctor = Doctor::all();
		$class = Units_carry::all();

        $appointment = DB::table('tbl_appointment')
        ->join('tbl_registration', 'tbl_registration.pk_int_reg_id', '=', 'tbl_appointment.fk_customer_reg_id')
        ->join('users', 'users.id', '=', 'tbl_appointment.fk_doctor')
        // ->join('tbl__billing', 'tbl__billing.fk_bill_appointment_id', '=', 'tbl_appointment.pk_int_appoint_id')
        ->join('tbl_product_purchased', 'tbl_product_purchased.purch_appointment_id', '=', 'tbl_appointment.pk_int_appoint_id')
        ->where('tbl_appointment.complete_status','4')
        ->where('appointment_date',$date)
        ->select('*')
        ->get();

        $vat_amount = Appointment::where('appointment_date', $date)->sum('vat_amount');
        $prod_purch_amount = Purchased::where('purchased_date', $date)->sum('purchased_amount');
        $total_amount_to_pay = Appointment::where('appointment_date', $date)->sum('trt_total_amount');
        $payed_bill_amount = Appointment::where('appointment_date', $date)->sum('payedbill_amount');
        $discount_bill_amount = Billing::where('date', $date)->sum('discount_amount');

        $balance=($total_amount_to_pay+$vat_amount+$prod_purch_amount)-$payed_bill_amount;
        // $total_treatment=sum($appointment->total_amount);
        // $balance=$appointment->total_amount-
        // dd($total_amount_to_pay);die();
        // return 1;
		return view('admin.accounts.daily_accounts',compact("appointment","branches","doctor","class","date","total_amount_to_pay","payed_bill_amount","discount_bill_amount","balance","prod_purch_amount","vat_amount"));
	}
	public function reportaccountPDF()
	{				
		// return 1;
		// $role = Role::all();	
		// $register = Register::where('pk_int_reg_id', $id)->first();
		$date = Carbon::today()->toDateString();
		$branches = Branches::all();
		$doctor = Doctor::all();
		$class = Units_carry::all();

        $appointment = DB::table('tbl_appointment')
        ->join('tbl_registration', 'tbl_registration.pk_int_reg_id', '=', 'tbl_appointment.fk_customer_reg_id')
        ->join('users', 'users.id', '=', 'tbl_appointment.fk_doctor')
        // ->join('tbl__billing', 'tbl__billing.fk_bill_appointment_id', '=', 'tbl_appointment.pk_int_appoint_id')
        ->join('tbl_product_purchased', 'tbl_product_purchased.purch_appointment_id', '=', 'tbl_appointment.pk_int_appoint_id')
        ->where('tbl_appointment.complete_status','4')
        ->where('appointment_date',$date)
        ->select('*')
        ->get();

        $vat_amount = Appointment::where('appointment_date', $date)->sum('vat_amount');
        $prod_purch_amount = Purchased::where('purchased_date', $date)->sum('purchased_amount');
        $total_amount_to_pay = Appointment::where('appointment_date', $date)->sum('trt_total_amount');
        $payed_bill_amount = Appointment::where('appointment_date', $date)->sum('payedbill_amount');

        $balance=($total_amount_to_pay+$vat_amount+$prod_purch_amount)-$payed_bill_amount;
        // $total_treatment=sum($appointment->total_amount);
        // $balance=$appointment->total_amount-
        // dd($total_amount_to_pay);die();
        // return 1;
        $pdf = PDF::loadView('admin.accounts.daily_accounts_pdf',compact("appointment","branches","doctor","class","date","total_amount_to_pay","payed_bill_amount","discount_bill_amount","balance","prod_purch_amount","vat_amount"));
        return $pdf->download('report.pdf');
	}
	public function datewise_account_pdf(Request $request)
	{				
		$input = $request->all();
    	$report_date = $input['report_date'];
		$date = Carbon::today()->toDateString();
		$branches = Branches::all();
		$doctor = Doctor::all();
		$class = Units_carry::all();

        $appointment = DB::table('tbl_appointment')
        ->join('tbl_registration', 'tbl_registration.pk_int_reg_id', '=', 'tbl_appointment.fk_customer_reg_id')
        ->join('users', 'users.id', '=', 'tbl_appointment.fk_doctor')
        // ->join('tbl__billing', 'tbl__billing.fk_bill_appointment_id', '=', 'tbl_appointment.pk_int_appoint_id')
        ->join('tbl_product_purchased', 'tbl_product_purchased.purch_appointment_id', '=', 'tbl_appointment.pk_int_appoint_id')
        ->where('tbl_appointment.complete_status','4')
        ->where('appointment_date',$report_date)
        ->select('*')
        ->get();

        $vat_amount = Appointment::where('appointment_date', $report_date)->sum('vat_amount');
        $prod_purch_amount = Purchased::where('purchased_date', $report_date)->sum('purchased_amount');
        $total_amount_to_pay = Appointment::where('appointment_date', $report_date)->sum('trt_total_amount');
        $payed_bill_amount = Appointment::where('appointment_date', $report_date)->sum('payedbill_amount');

        $balance=($total_amount_to_pay+$vat_amount+$prod_purch_amount)-$payed_bill_amount;
        // $total_treatment=sum($appointment->total_amount);
        // $balance=$appointment->total_amount-
        // dd($total_amount_to_pay);die();
        // return 1;
        $pdf = PDF::loadView('admin.accounts.daily_accounts_pdf',compact("appointment","branches","doctor","class","date","total_amount_to_pay","payed_bill_amount","discount_bill_amount","balance","prod_purch_amount","vat_amount"));
        return $pdf->download('report.pdf');
	}
	public function monthly_account_pdf(Request $request)
	{				
		$input = $request->all();
    	$report_month = $input['report_month'];
    	$report_year = $input['report_year'];
		$date = Carbon::today()->toDateString();
		$branches = Branches::all();
		$doctor = Doctor::all();
		$class = Units_carry::all();

        $appointment = DB::table('tbl_appointment')
        ->join('tbl_registration', 'tbl_registration.pk_int_reg_id', '=', 'tbl_appointment.fk_customer_reg_id')
        ->join('users', 'users.id', '=', 'tbl_appointment.fk_doctor')
        // ->join('tbl__billing', 'tbl__billing.fk_bill_appointment_id', '=', 'tbl_appointment.pk_int_appoint_id')
        ->join('tbl_product_purchased', 'tbl_product_purchased.purch_appointment_id', '=', 'tbl_appointment.pk_int_appoint_id')
        ->where('tbl_appointment.complete_status','4')
        ->whereMonth('tbl_appointment.created_at', $report_month)
        ->whereYear('tbl_appointment.created_at', $report_year)
        ->select('*')
        ->get();

        $vat_amount = Appointment::whereMonth('appointment_date', $report_month)->whereYear('appointment_date', $report_year)->sum('vat_amount');
        $prod_purch_amount = Purchased::whereMonth('purchased_date', $report_month)->whereYear('purchased_date', $report_year)->sum('purchased_amount');

        $total_amount_to_pay = Appointment::whereMonth('created_at', $report_month)->whereYear('created_at', $report_year)->sum('trt_total_amount');
        $payed_bill_amount = Appointment::whereMonth('created_at', $report_month)->whereYear('created_at', $report_year)->sum('payedbill_amount');

        $balance=($total_amount_to_pay+$vat_amount+$prod_purch_amount)-$payed_bill_amount;
        // $total_treatment=sum($appointment->total_amount);
        // $balance=$appointment->total_amount-
        // dd($total_amount_to_pay);die();
        // return 1;
        $pdf = PDF::loadView('admin.accounts.monthly_acc_report_pdf',compact("appointment","branches","doctor","class","date","total_amount_to_pay","payed_bill_amount","discount_bill_amount","balance","prod_purch_amount","vat_amount","report_month","report_year"));
        return $pdf->download('report.pdf');
	}
	public function datewise_account()
	{				
		// $role = Role::all();	
		// $register = Register::where('pk_int_reg_id', $id)->first();
		$date = Carbon::today()->toDateString();
		$branches = Branches::all();
		$doctor = Doctor::all();
		$class = Units_carry::all();

        $appointment = DB::table('tbl_appointment')
        ->join('tbl_registration', 'tbl_registration.pk_int_reg_id', '=', 'tbl_appointment.fk_customer_reg_id')
        ->join('users', 'users.id', '=', 'tbl_appointment.fk_doctor')
        // ->join('tbl__billing', 'tbl__billing.fk_bill_appointment_id', '=', 'tbl_appointment.pk_int_appoint_id')
        ->join('tbl_product_purchased', 'tbl_product_purchased.purch_appointment_id', '=', 'tbl_appointment.pk_int_appoint_id')
        ->where('tbl_appointment.complete_status','4')
        ->where('appointment_date',$date)
        ->select('*')
        ->get();

        $vat_amount = Appointment::where('appointment_date', $date)->sum('vat_amount');
        $prod_purch_amount = Purchased::where('purchased_date', $date)->sum('purchased_amount');
        $total_amount_to_pay = Appointment::where('appointment_date', $date)->sum('trt_total_amount');
        $payed_bill_amount = Appointment::where('appointment_date', $date)->sum('payedbill_amount');

        $balance=($total_amount_to_pay+$vat_amount+$prod_purch_amount)-$payed_bill_amount;

		return view('admin.accounts.datewise_accounts',compact("appointment","branches","doctor","class","date","total_amount_to_pay","payed_bill_amount","discount_bill_amount","balance","prod_purch_amount","vat_amount"));
	}
	public function datebtw_account_pdf(Request $request)
	{	
		// return 1;			
		$input = $request->all();
    	$from_date = $input['from_date'];
    	$to_date = $input['to_date'];
		$date = Carbon::today()->toDateString();
		$branches = Branches::all();
		$doctor = Doctor::all();
		$class = Units_carry::all();

        $appointment = DB::table('tbl_appointment')
        ->join('tbl_registration', 'tbl_registration.pk_int_reg_id', '=', 'tbl_appointment.fk_customer_reg_id')
        ->join('users', 'users.id', '=', 'tbl_appointment.fk_doctor')
        // ->join('tbl__billing', 'tbl__billing.fk_bill_appointment_id', '=', 'tbl_appointment.pk_int_appoint_id')
        ->join('tbl_product_purchased', 'tbl_product_purchased.purch_appointment_id', '=', 'tbl_appointment.pk_int_appoint_id')
        ->where('tbl_appointment.complete_status','4')
        ->where('appointment_date','>=', $from_date)
        ->where('appointment_date','<=', $to_date)
        ->select('*')
        ->get();


        $vat_amount = Appointment::where('appointment_date','>=', $from_date)->where('appointment_date','<=', $to_date)->sum('vat_amount');

        $prod_purch_amount = Purchased::where('purchased_date','>=', $from_date)->where('purchased_date','<=', $to_date)->sum('purchased_amount');

        $total_amount_to_pay = Appointment::where('appointment_date','>=', $from_date)->where('appointment_date','<=', $to_date)->sum('trt_total_amount');
        $payed_bill_amount = Appointment::where('appointment_date','>=', $from_date)->where('appointment_date','<=', $to_date)->sum('payedbill_amount');

        $balance=($total_amount_to_pay+$vat_amount+$prod_purch_amount)-$payed_bill_amount;
        // $total_treatment=sum($appointment->total_amount);
        // $balance=$appointment->total_amount-
        // dd($total_amount_to_pay);die();
        // return 1;
        $pdf = PDF::loadView('admin.accounts.date-to-date_report',compact("appointment","branches","doctor","class","date","total_amount_to_pay","payed_bill_amount","discount_bill_amount","balance","prod_purch_amount","vat_amount","from_date","to_date"));
        return $pdf->download('report.pdf');
	}
		
}
