<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Mail;
use Snowfire\Beautymail\Beautymail;
use App\Mail\SendMailable;
use App\Users;
use App\User;
use App\Role;
use App\Register;
use App\Branches;
use App\Doctor;
use App\Units_carry;
use App\Appointment;
use App\Treatment;
use App\Consultdata;

use Validator;
use Flash;
use DataTables;
use DB;
use Auth;
use Session;
use Response;
use PDF;
use Carbon;
use \File;

class ConsultationController extends Controller
{
    public function index()
	{
		// $role = Role::all();
		// $register = Register::where('pk_int_reg_id', $id)->first();
		$date = Carbon::today()->toDateString();
		$logged=Auth::user()->id;
		$branches = Branches::all();
		$doctor = Doctor::all();
		$class = Units_carry::all();
        $appointment = DB::table('tbl_appointment')
        ->join('tbl_registration', 'tbl_registration.pk_int_reg_id', '=', 'tbl_appointment.fk_customer_reg_id')
        ->join('users', 'users.id', '=', 'tbl_appointment.fk_doctor')
        ->where('fk_doctor',$logged)
        // ->where('appointment_date',$date)
        ->where('complete_status','0')
        // ->orWhere('complete_status','1')
        ->select('*')
        ->get();
        // return 1;
        // dd($appointment);die();
		return view('admin.doctor.appointments',compact("appointment","branches","doctor","class"));
	}
	public function open_data($id)
	{
		// return 1;
        // dd($id);die();

		$appointment = Appointment::where('pk_int_appoint_id', $id)->where('complete_status','0')->first();
        // dd($appointment->pk_int_appoint_id);die();

		// $appointment = $id;
		$treatment = Treatment::all();
        // dd($treatment);die();
		return view('admin.doctor.cunslting_data',compact("treatment","appointment"));
	}
	public function store_other_deatails(Request $request,$id)
  	{
  		// return 1;
  		$logged=Auth::user()->id;
  		$input = $request->all();
        $consult = new Consultdata();

        $reg_id = DB::table('tbl_appointment')
        ->where('pk_int_appoint_id',$id)
        ->pluck('fk_customer_reg_id')
        ->first();
        // dd($id);die();

        $consult->appointment_id = $id;
        $consult->customer_id = $reg_id;
        $consult->doctor_id = $logged;

        $consult->patient_name = $input['patient_name'];
        $consult->height_weight_check = $input['height_weight_check'];
        $consult->height_weight_check_descrip = $input['height_weight_check_descrip'];
        $consult->blood_pressure_check = $input['blood_pressure_check'];
        $consult->blood_pressure_check_descrip = $input['blood_pressure_check_descrip'];
        $consult->cholestrol_check = $input['cholestrol_check'];
        $consult->cholestrol_check_descrip = $input['cholestrol_check_descrip'];
        $consult->blood_sugar_test = $input['blood_sugar_test'];
        $consult->blood_sugar_test_descrip = $input['blood_sugar_test_descrip'];
        $consult->throat_check = $input['throat_check'];
        $consult->throat_check_descrip = $input['throat_check_descrip'];
        $consult->ear_check = $input['ear_check'];
        $consult->ear_check_descrip = $input['ear_check_descrip'];
        $consult->eye_check = $input['eye_check'];
        $consult->eye_check_descrip = $input['eye_check_descrip'];
        $consult->electrocardiogram_check = $input['electrocardiogram_check'];
        $consult->electrocardiogram_check_descrip = $input['electrocardiogram_check_descrip'];

        $flag = $consult->save();

        $other_deatails_status = Appointment::where('pk_int_appoint_id','=',$id)->update(['other_details_status' => '1']);

        // return 1;
      	if ($flag) {
				Session::flash('message', 'success|Added successfully ');
				return redirect('/admin/today_appointment');
			}
			else {
				Session::flash('message', 'danger|Failed, Try again');
				return redirect()->back();
		}
	}

    public function contact_us(Request $request)
    {
        // return 1;
        $input = $request->all();

        $name = $input['name'];
        $phone = $input['phone'];
        $email = $input['email'];
        $messages = $input['messages'];

        $data = [
                    'title'=> 'CENXEL.COM',
                    'name'=>$name,
                    'email'=>$email,
                    'phone'=>$phone,
                    'messages'=>$messages,
                ];

                $beautymail = app()->make(\Snowfire\Beautymail\Beautymail::class);
                $beautymail->send('email.user_email',$data, function($message) use  ($data)
                {
                    $email = Input::get('users_forgot_pass_email');
                    $message
                    ->from('cenxel.social@gmail.com')
                    ->to('info@cenxel.com')
                    ->subject('CENXEL.COM - WEBSITE ENQUIRY');
                });
                Session::flash('message', 'success| Done.');
                return redirect()->back();

        if ($message) {
            Session::flash('message', 'success| Done.');
                return redirect()->back();
            }
            else {
                Session::flash('message', 'danger|Failed, Try again');
                return redirect()->back();
        }
    }

     public function trt_details($id)
    {
        // $sms = Consultdata::where('pk_doctor_id', $id)->first();
        $cunsultdata = DB::table('tbl_appointment')
        ->where('pk_int_appoint_id',$id)
        ->first();

        $sg1 = $cunsultdata->pk_int_appoint_id;
        echo $sg1;

    }

    public function add_trt_amount(Request $request)
    {
        // return 1;
        $logged=Auth::user()->id;
        $input = $request->all();
        // $consult = new Consultdata();
        $id = $input['appointment_id'];
        $trt_amount = $input['trt_amount'];

        $usr = Appointment::where('pk_int_appoint_id','=',$id)->update(['trt_total_amount' => $trt_amount]);
        $usr = Appointment::where('pk_int_appoint_id','=',$id)->update(['complete_status' => '2']);

        if ($usr) {
                Session::flash('message', 'success|Added successfully ');
                return redirect('/admin/today_appointment');
            }
            else {
                Session::flash('message', 'danger|Failed, Try again');
                return redirect()->back();
        }
    }


}
