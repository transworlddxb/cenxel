<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Consultdata extends Model
{
    protected $dates = ['deleted_at'];

	protected $primaryKey ='pk_consult_id';

	protected $table = 'tbl__consultdata';

	protected $fillable = ['customer_id','appointment_id','doctor_id','height_weight_check','blood_pressure_check','cholestrol_check','blood_sugar_test','throat_check','ear_check','eye_check','electrocardiogram_check','height_weight_check_descrip','blood_pressure_check_descrip','cholestrol_check_descrip','blood_sugar_test_descrip','throat_check_descrip','ear_check_descrip','eye_check_descrip','electrocardiogram_check_descrip'];
}
