<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Branches extends Model
{
    protected $dates = ['deleted_at'];

	protected $primaryKey ='pk_branch_id';

	protected $table = 'tbl_branch';

	protected $fillable = ['branch_name','status'];
}
