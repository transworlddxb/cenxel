<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Treatment extends Model
{
    protected $dates = ['deleted_at'];

    protected $primaryKey ='pk_treatment_id';

    protected $table = 'treatment';

    protected $fillable = ['treatment_name','treatment_price','roll_status'];

    public static $rule=[
    	'roll_name'=>'required',
	];
	public static $message=[
		'roll_name.required'=>'Role is required',
    ];
}
