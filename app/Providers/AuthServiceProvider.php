<?php

namespace App\Providers;

use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Response;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

         Gate::define('isSUPERADMIN', function($user){
            return $user->admin_role == '1';
        });

        Gate::define('isADMINISTRATOR', function($user){
            return $user->admin_role == '2';
        });

         Gate::define('isRECEPTIONIST', function($user){
            return $user->admin_role == '3';
        });

          Gate::define('isDOCTOR', function($user){
            return $user->admin_role == '4';
        });

          Gate::define('isACCOUNTS', function($user){
            return $user->admin_role == '5';
        });  
    }
}
