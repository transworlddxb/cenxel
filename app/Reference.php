<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reference extends Model
{
    protected $dates = ['deleted_at'];

	protected $primaryKey ='refernce_person_id';

	protected $table = 'tbl_refrence_person';

	protected $fillable = ['reference_person_name','ref_description','ref_status'];
}
