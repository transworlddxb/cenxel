<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Carrying_item extends Model
{
    protected $dates = ['deleted_at'];

    protected $primaryKey ='carry_items_id';

    protected $table = 'tbl_carrying_items';

    protected $fillable = ['carry_items',];

    public static $rule=[
    	'carry_items'=>'required',
	];
	
	public static $message=[
		'carry_items.required'=>'Item is required',
    ];
}
