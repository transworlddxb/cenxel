<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Doctor extends Model
{
    protected $dates = ['deleted_at'];

	protected $primaryKey ='pk_doctor_id';

	protected $table = 'tbl_doctor';

	protected $fillable = ['doctor_name','status'];
}
