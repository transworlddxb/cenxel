<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Appointment extends Model
{
    protected $dates = ['deleted_at'];

	protected $primaryKey ='pk_int_appoint_id';

	protected $table = 'tbl_appointment';

	protected $fillable = ['fk_customer_reg_id','fk_doctor','appointment_date','appointment_time','appoint_patient','payedbill_amount','duebill_amount','consult_status','payment_status','complete_status','vat_amount','total_amount_to_pay','prev_due_amount','trt_total_amount','inv_nmbr','product_one','product_two','product_three','product_four','product_five','product_six','product_one_count','product_two_count','product_three_count','product_four_count','product_five_count','product_six_count','consult_description','prod_purch_status','other_details_status'];
}
