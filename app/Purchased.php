<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Purchased extends Model
{
    protected $dates = ['deleted_at'];

	protected $primaryKey ='purchased_id';

	protected $table = 'tbl_product_purchased';

	protected $fillable = ['purch_appointment_id','product_one','product_two','product_three','product_four','product_five','purchased_amount','purchased_date','purchased_status'];
}
