<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Units_carry extends Model
{
    protected $dates = ['deleted_at'];

    protected $primaryKey ='class_id';

    protected $table = 'tbl_units_carrying';

    protected $fillable = ['class_name',];

    public static $rule=[
    	'class_name'=>'required',
	];
	public static $message=[
		'class_name.required'=>'Unit is required',
    ];
}
