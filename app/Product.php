<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $dates = ['deleted_at'];

	protected $primaryKey ='product_id';

	protected $table = 'tbl_product';

	protected $fillable = ['prod_name','prod_price','prod_count','status'];
}
