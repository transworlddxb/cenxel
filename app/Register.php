<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Register extends Model
{
    protected $dates = ['deleted_at'];

	protected $primaryKey ='pk_int_reg_id';

	protected $table = 'tbl_registration';

	protected $fillable = ['doctor_id','file_no','first_name','middle_name','last_name','reg_dob','mob_number','gender','marital_status','final_due_amount','status','adv_amount','patient_place','patient_age'];

}
