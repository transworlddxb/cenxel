<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Users extends Model
{
	protected $dates = ['deleted_at'];

	protected $primaryKey ='pk_int_users_id';

	protected $table = 'tbl_users';

	protected $fillable = ['users_date','users_first_name','users_second_name','users_email','users_mobile','users_address','users_street','users_city','users_role','users_sms_group','users_username','users_password','user_pass','user_status','users_latitude','users_longitude','users_area_fk','users_area_code','user_botl_status','wallet_amount','salary'];

	public static $rule=[
		
		'users_first_name' =>'required',
		'users_second_name' =>'required',
		'users_mobile' =>'required',
		'users_address' =>'required',		
		'users_role' =>'required',
		'users_sms_group' =>'required',
		'users_password' =>'required',
		'users_area_fk' => 'required',
	];
	public static $message=[

		'users_name.required' =>'Name is required',
		'users_email.required' =>'Email is required',
		'users_mobile.required' =>'Mobile is required',
		// 'users_mobile.regex' =>'Mobile number not valid',
		'users_address.required' =>'Address is required',
		'users_role.required' =>'Select role',
		'users_sms_group.required' =>'Select sms type',
		'users_password.required' =>'Password is required',
		'users_area_fk.required' =>'Area is required',
	];
}
