<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Advance extends Model
{
    protected $dates = ['deleted_at'];

	protected $primaryKey ='advance_id';

	protected $table = 'tbl_advance';

	protected $fillable = ['advance_cust_id','advance_amount','advance_date','adv_status'];
}
