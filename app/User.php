<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Authenticatable
{
    use Notifiable;
    use SoftDeletes;

    const ACTIVATE = 1;
    const DEACTIVATE = 0;

    const USER = 0;
    const SUPERADMIN = 1;
    const ADMIN = 2;
    const STAFF = 3;
    const DELIVERYBOY = 4;
    const CUSTOMER = 5;
    /**
     * The attributes that are mass assignable.
     *
     * @var array


     */

    protected $dates = ['deleted_at'];
   // protected $primaryKey = 'id';
    //protected $table = 'tbl_banner_images';

    protected $fillable = [
        'roll_name', 'email', 'password','admin_role','user_designation','status'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden =[
        'password', 'remember_token',
    ];
    protected $table='users';
    public static $userPasswordRule=[
     'current-password' => 'required',
     'new-password'=> 'required|min:6|confirmed'];
}
