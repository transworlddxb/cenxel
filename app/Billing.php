<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Billing extends Model
{
    protected $dates = ['deleted_at'];

	protected $primaryKey ='billing_id';

	protected $table = 'tbl__billing';

	protected $fillable = ['costumer_id','doctor_id','fk_bill_appointment_id','date','total_amount','prev_due_amount','discount_amount','status','approval_status'];
}
