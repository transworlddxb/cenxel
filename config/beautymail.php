<?php
return [
    // These CSS rules will be applied after the regular template CSS
    /*
        'css' => [
            '.button-content .button { background: red }',
        ],
    */
    'colors' => [
        'highlight' => '#eff2f2',
        'button'    => '#00a29c',
    ],
    'view' => [
        'senderName'  => null,
        'reminder'    => null,
        'unsubscribe' => null,
        'address'     => null,
        'logo'        => [
            'path'   => 'https://cenxel.com/assets/frontend/images/cenexl logo color.svg',
            'width'  => '150',
            'height' => '70',
        ],
        'twitter'  => '',
        'facebook' => '',
    ],
];
