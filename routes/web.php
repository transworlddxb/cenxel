<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/admin', function () {
    return redirect('/login');
});

Route::get('/', function () {
    return view('frontend/indexMain');
});

Route::get('/residency_permit', function () {
    return view('frontend/realEstate');
});

Route::get('/contact', function () {
    return view('frontend/contactUs');
});
Route::get('/manpowerSupply', function () {
    return view('frontend/manpowerSupply');
});
Route::get('/hospitality', function () {
    return view('frontend/hospitality');
});
Route::get('/foodNbeverage', function () {
    return view('frontend/foodNbeverage');
});

Route::get('/agriculture', function () {
    return view('frontend/agriculture');
});

Route::get('/blog', function () {
    return view('frontend/blogList');
});

Route::get('/single_prop', function () {
    return view('frontend/ppty_selected');
});
Route::get('/single_prop2', function () {
    return view('frontend/ppty_selected2');
});

Route::get('/single_prop3', function () {
    return view('frontend/ppty_selected3');
});

Route::get('/single_prop4', function () {
    return view('frontend/ppty_selected4');
});

Route::post('/submit_contact_form','ContactusController@submit_contact_form');
Route::post('/submit_realestate_form','ContactusController@submit_realestate_form');
Route::post('/submit_agricul_form','ContactusController@submit_agricul_form');
Route::post('/submit_food_form','ContactusController@submit_food_form');
Route::post('/submit_manpower_form','ContactusController@submit_manpower_form');
Route::post('/submit_hospitality_form','ContactusController@submit_hospitality_form');



Auth::routes();

Route::post('/login', 'Auth\LoginController@login')->name('login');
Route::get('/home', 'HomeController@index');
Route::post('/reg-user','Auth\RegisterController@register');

// Route::get('/','HomeController@register');


Route::group(['prefix' => 'admin', 'middleware' => ['admin_auth']], function () {
Route::get('/logout', 'Auth\LoginController@logout')->name('admin.auth.logout');
Route::get('/dashboard', 'HomeController@index')->name('admin.get_home');


Route::get('/index','HomeController@index');
Route::get('/usertable','HomeController@usertable');
Route::get('/register','HomeController@registerview');
Route::get('/definition','HomeController@definition');

// Route::get('/definition', array('as' => 'definition', 'uses' => 'HomeController@definition'));


Route::get('/reset','PasswordController@index');
Route::post('/evaluate','PasswordController@changePassword');

Route::get('/delete-user/{id}','HomeController@deleteUser');

//header
Route::get('/password-change','PasswordController@index');

Route::get('/head','Menu_categoryController@index');

//role
	Route::get('/roles','RoleController@index');
	Route::post('/add_role','RoleController@store');
	Route::get('/delete-role/{id}','RoleController@destroy');
	// Route::get('/edit-role/{id}','RoleController@edit');
	Route::post('/update-role/{id}','RoleController@update');
	Route::get('/read-role-data/{id}','RoleController@edit');

//branches
	Route::get('/branches','BranchController@index');
	Route::post('/add_branches','BranchController@add_branches');
	Route::get('/edit-branch/{id}','BranchController@edit');
	Route::post('/update-branch/{id}','BranchController@update_branch');
	Route::get('/delete-branch/{id}','BranchController@destroy');

//doctor list
	Route::get('/doctor_list','DoctorController@index');
	Route::post('/add_doctor','DoctorController@store');
	Route::get('/edit-doctor/{id}','DoctorController@edit');
	Route::post('/update-doctor/{id}','DoctorController@update');
	Route::get('/delete-doctor/{id}','DoctorController@destroy');

//reg-admin
	Route::get('/view_users','UsersController@show');
	Route::get('/users','UsersController@index');
	Route::post('/add_users','UsersController@store');
	Route::get('/delete_users/{id}','UsersController@destroy');
	Route::get('/edit-user/{id}','UsersController@edit');
	Route::post('/update-user/{id}','UsersController@update');
	Route::post('/download_user_pdf','UsersController@download_user_pdf');

//References
	Route::get('/reference','ReferenceController@index');
	Route::post('/add_ref_person','ReferenceController@store');
	Route::get('/delete_reference/{id}','ReferenceController@destroy');
	Route::get('/edit_reference/{id}','ReferenceController@edit');
	Route::post('/update_reference','ReferenceController@update');

//advance
	Route::get('/advance/{id}','AdvanceController@index');
	Route::post('/add_advance/{id}','AdvanceController@add_advance');
	Route::get('/advance_report','AdvanceController@advance_report');
	Route::get('/clear_advance/{id}','AdvanceController@clear_advance');
	Route::get('/today_advance_report','AdvanceController@today_advance_report');
	Route::post('/datewise_advance__pdf','AdvanceController@datewise_advance__pdf');
	Route::post('/monthly_advance_pdf','AdvanceController@monthly_advance_pdf');
	Route::post('/search__advance_date','AdvanceController@search__advance_date');
	Route::post('/date_daily_advance_search','AdvanceController@date_daily_advance_search');


//registration
	Route::get('/reg_user','RegisterController@add');
	Route::get('/reg_usr','RegisterController@reg_usr');
	Route::post('/store_registration','RegisterController@store');
	Route::get('/edit_details/{id}','RegisterController@edit_details');
	Route::post('/store_edited_registration/{id}','RegisterController@update');

//Appointment
	Route::get('/appointment/{id}','AppointmentController@index');
	Route::post('/store_appointment/{id}','AppointmentController@store');
	Route::get('/today_appointment','AppointmentController@today_list');
	Route::post('/search_time','AppointmentController@search_time');
	Route::get('/approval_list','AppointmentController@approval_list');
	Route::get('/approval_remrks/{id}','AppointmentController@approval_remrks');
	Route::post('/update_approval/{id}','AppointmentController@update_approval');
	Route::post('/search_date_appo','AppointmentController@search_date_appo');

//billing
	Route::get('/billing/{id}','AppointmentController@billing');
	Route::get('/download_bill_pdf/{id}','AppointmentController@billPDF');
	Route::get('/testbill_pdf','AppointmentController@testbill_pdf');

	Route::get('/billing_list','BillingController@index');
	Route::get('/open_billed_status/{id}','BillingController@billing_status');
	Route::get('/download_billed_data_pdf/{id}','BillingController@bill_data_pdf');
	Route::get('/paymnt_remrks/{id}','BillingController@paymnt_remrk');
	Route::post('/update_payment','BillingController@update_payment');
	Route::get('/due_remrks/{id}','BillingController@due_remrk');
	Route::post('/update_due/{id}','BillingController@update_due');
	Route::post('/download_statement_pdf','AppointmentController@statementPDF');
	Route::post('/update_bill_paymnt/{id}','BillingController@bill_pdf');
	Route::post('/add_payment_trt','BillingController@add_payment_trt');
	Route::get('/edit-details/{id}','BillingController@edit_details_appo');
	Route::post('/update_appoit_details','BillingController@update_appoit_details');
	Route::get('/delete-appo/{id}','BillingController@delete_appo');



//doctor cunsultation
	Route::get('/pending_cunsult','ConsultationController@index');
	Route::get('/open_appointment/{id}','ConsultationController@open_data');
	Route::post('/store_cunsult_details/{id}','ConsultationController@store_data');
	Route::get('/trt_details/{id}','ConsultationController@trt_details');
	Route::post('/add_trt_amount','ConsultationController@add_trt_amount');


//treatment
	Route::get('/treatment','TreatmentController@add');
	Route::get('/normal','TreatmentController@normal_price');
	Route::get('/gov_list','TreatmentController@gov_list');
	Route::get('/student_list','TreatmentController@stud_list');
	Route::post('/add_treatment','TreatmentController@store');
	Route::get('/delete-traetment/{id}','TreatmentController@destroy');
	Route::get('/read-treatment/{id}','TreatmentController@edit');
	Route::post('/update-treatment/{id}','TreatmentController@update');

//unit
	Route::get('/classadd','ClassController@view');
	Route::post('/add_carry_units','Product_carryingController@store_units');
	Route::get('/delete-unit/{id}','Product_carryingController@destroy_units');
	Route::get('/class-edit/{id}','ClassController@edit_units');
	Route::post('/update-unit/{id}','Product_carryingController@update_units');

//administrator
	Route::get('/administrator_home','AdministratorController@index');

//reception
	Route::get('/receptionist_home','ReceptionController@index');

//doctor
	Route::get('/doctor_home','DoctorController@home');

//repots
	Route::get('/daily','ReportController@daily_report');
	Route::get('/today_report','ReportController@reportPDF');
	Route::post('/datewise_pdf','ReportController@datewisePDF');
	Route::post('/monthly_pdf','ReportController@monthlyPDF');
	Route::get('/monthly','ReportController@monthly_report');
	Route::get('/weekly','ReportController@weekly_report');
	Route::post('/date_search','ReportController@date_search');
	Route::post('/date_daily_search','ReportController@date_daily_search');
	Route::post('/search_date','ReportController@search_date');
	Route::post('/month_year_search','ReportController@month_year_search');
	Route::post('/bet_date_pdf','ReportController@bet_date_pdf');
	Route::get('/doctor_report','ReportController@doctor_report');
	Route::post('/datewise_doctor_pdf','ReportController@datewise_doctor_pdf');
	Route::post('/doctor_search_pdf','ReportController@doctor_search_pdf');
	Route::post('/monthwise_doctor_pdf','ReportController@monthwise_doctor_pdf');
	Route::post('/doctor_monthsearch_pdf','ReportController@doctor_monthsearch_pdf');
//Accounts
	Route::get('/account_home','AccountsController@index');
	Route::get('/daily_accounts','AccountsController@daily_accounts');
	Route::get('/today_account_report','AccountsController@reportaccountPDF');
	Route::post('/datewise_account_pdf','AccountsController@datewise_account_pdf');
	Route::post('/monthly_account_pdf','AccountsController@monthly_account_pdf');
	Route::get('/datewise_account','AccountsController@datewise_account');
	Route::post('/datebtw_account_pdf','AccountsController@datebtw_account_pdf');

//product
	Route::get('/add_product','ProductController@index');
	Route::post('/add_product_list','ProductController@add_product');
	Route::get('/delete_product/{id}','ProductController@delete_product');
	Route::get('/edit_product/{id}','ProductController@edit_product');
	Route::post('/update_product/{id}','ProductController@update');
	Route::get('/prod_purch/{id}','ProductController@prod_purch');
	Route::post('/purchased_prod/{id}','ProductController@purchased_prod');
	Route::get('/add_prod_cunsult/{id}','ProductController@add_prod_cunsult');
	Route::get('/prod_purch_details/{id}','ProductController@prod_purch_details');

//details
	Route::get('/add_other_deatils/{id}','AppointmentController@add_other_deatils');
	Route::post('/store_other_deatails/{id}','ConsultationController@store_other_deatails');
	Route::post('/update_other_deatails/{id}','ConsultationController@update_other_deatails');
	Route::get('/add_other_deatils_edit/{id}','AppointmentController@add_other_deatils_edit');
	Route::get('/view_full_report/{id}','AppointmentController@view_full_report');
	Route::get('/customer_consultation_list/{id}','AppointmentController@customer_consultation_list');



// other routes redirected to login
Route::get('/{any}', function () {
return redirect('/login');
})->where('any', '.*s');
});

